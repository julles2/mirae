@inject('repo','App\Repository\Home')
@extends('layouts.layout')
@section('content')

<section class="banner">
    <img src="{{ assetContent(@$setting->banner_image) }}" alt="banner" />
    <div class="caption">
        <h6>{{ @$setting->title_caption }}</h6>
        <h1>{!! @$setting->title_main !!}</h1>
        <p>{{ @$setting->sub_title }}</p>
    </div>
</section>
<section class="std-content">
    <div class="wrapper product-compare">
        <div class="nav-produk no-border">
            <div class="left">
                <p>{{ trans('product.mutualfundtype') }}</p>
            </div>
            <div class="right">
            <?php /*
                <a onclick = "filter('')" href="javascript::void(0)"><span style="background:url({{ assetfrontend(null)  }}images/material/cat-prod-0.png)"></span>{{ trans('product.all') }}</a>
                <a onclick = "filter('EQ')" href="javascript::void(0)"><span style="background:url({{ assetfrontend(null)  }}images/material/cat-prod-1.png)"></span>{{ trans('type_fund.eq') }}</a>
                <a onclick = "filter('MX')" href="javascript::void(0)"><span style="background:url({{ assetfrontend(null)  }}images/material/cat-prod-2.png)"></span>{{ trans('type_fund.mx') }}</a>
                <a onclick = "filter('FI')" href="javascript::void(0)"><span style="background:url({{ assetfrontend(null)  }}images/material/cat-prod-3.png)"></span>{{ trans('type_fund.fi') }}</a>
                <a onclick = "filter('MM')" href="javascript::void(0)"><span style="background:url({{ assetfrontend(null)  }}images/material/cat-prod-5.png)"></span>{{ trans('type_fund.mm') }}</a>
                <a onclick = "filter('SR')" href="javascript::void(0)"><span style="background:url({{ assetfrontend(null)  }}images/material/cat-prod-4.png)"></span>{{ trans('type_fund.sr') }}</a>
            */

            ?>
            @foreach($filterTypes as $key => $val)
                <a class="{{ $val['class'] }}" onclick = "filter('{{$key != 'ALL' ? $key : ""}}')" href="javascript::void(0)"><span style="background:url({{ assetfrontend(null)  }}images/material/{{ $val['image']  }})"></span>{{ trans($val['trans']) }}</a>
            @endforeach
            </div>
        </div>
        <div class="compare-form">
            {!! Form::open(['method'=>'post','url'=>urlLang("compare/store-fund"),'id'=>'form_compare']) !!}
                <div class="col-3">
                    <input type = "hidden" name="fund_field" value = "{{ $fieldShare }}" id = "fund_field" />
                    <input type = "hidden" name="fund_month" value = "{{ $monthShare }}" id = "fund_month" />
                    @for($i=0;$i<3;$i++)
                        <div>
                            {!! Form::select('im_compare[]',$repo->selectIm(),@$selectedIm[$i],['class'=>'select2','onchange'=>"changeReksaDana(this.value,'$i')"]) !!}
                        </div>
                    @endfor
                </div>
                <div class="col-3">
                    @php($no=-1)
                    @for($i=0;$i<3;$i++)
                    @php($no++)
                        <div>
                            {!! Form::select('fund_compare[]',$repo->selectFund(@$selectedIm[$i]),@$compareFunds['funds'][$no],['class'=>'select2','id'=>"fund_compare$i"]) !!}
                        </div>
                    @endfor
                </div>
                <div class="btn">
                    <input type="submit" value="{{ trans('product.compare') }}" class="btn-std">
                </div>
            {!! Form::close() !!}
        </div>

        <div class="shadow-spar"></div>

        <div class="wrap-tab false-tab" id="chart-wrap">
            <div class="nav-tab">
                <a onclick='return reloadPost({ "fund_field":"nav"})' class="{{ $fieldShare=='nav' ? 'active':'' }}" href="#compare-tab-1" >{{ trans('product.returnnavcompare') }}</a>
                <a onclick='reloadPost({ "fund_field":"aum"})' class="{{ $fieldShare=='aum' ? 'active':'' }}" href="#compare-tab-1">{{ trans('product.aumcompare') }}</a>
                <a onclick='reloadPost({ "fund_field":"unit"})' href="#compare-tab-1" class="{{ $fieldShare=='unit' ? 'active':'' }}" >{{ trans('product.unitcompare') }}</a>
            </div>
            <div class="content-tab" id="compare-tab-1">
                <h2>
                    @if($fieldShare == 'nav')
                        {{ trans('product.returnnavcompare') }}
                    @elseif($fieldShare == 'aum')
                        {{ trans('product.aumcompare') }}
                    @else
                        {{ trans('product.unitcompare') }}
                    @endif
                </h2>
                <div class="filter_top">
                    <form id = "form_filter">
                        <div class="left">
                            <h6>{{ trans('product.period') }}</h6>
                        </div>
                        <div class="right">
                        @foreach($periode as $month => $label)
                        	<label onclick = "return reloadPost({fund_month: {{$month}} })" class="check opt">
                                <input type="checkbox" />
                                <span>{{ $label }}</span>
                            </label>
                        @endforeach


                            <div class="list_filt">
                                <label>{{ trans('product.from') }}</label>
                                <input type="text" onchange = "return monthRange()" id = "from" class="datepicker2" value="{{ $dateTextBox->from }}"/>
                            </div>
                            <div class="list_filt">
                                <label>{{ trans('product.to') }}</label>
                                <input type="text" onchange = "return monthRange()" id = "to" class="datepicker2" value="{{ $dateTextBox->to }}"/>
                            </div>
                            <div class="submit">
                                <input type="submit"  onclick = "return reloadForm()" value=" ">

                            </div>
                        </div>
                    </form>
                </div>
                <div class="wrap-chart">
                    <div class="chart" id="chart-compare"></div>

                </div>

            </div>

        </div>
        <div class="shadow-spar"></div>
        <div class="ringkasan-perbandingan">
            <h2>{{ trans('product.comparesummary') }}</h2>
            <div class="after_clear">
                @php($i=0)
                @foreach($funds as $row)
                @php($i++)
                    <div class="box">
                        <div class="desc">
                            <div class="ico"><img src="{{ $row->result_image }}" alt="prod"></div>
                            <div class="text">
                                <h6>{{ $row->fund_name }}</h6>
                                <p>{{ $row->im->im_nm }}</p>
                            </div>
                        </div>
                        <div class="col-2">
                            <div class="col">
                                <p>
                                    <span>{{ trans('product.mutualfundtype') }}</span><br/>
                                    {{ $row->result_type }}
                                </p>
                            </div>
                            <div class="col">
                                <p>
                                    <span>{{ trans('product.establishmentdate') }}</span><br/>
                                    @if(!empty($row->fund_orcl->fund_cd))
                                        {{ carbon_lang($row->fund_orcl->estl_date) }}
                                    @endif
                                </p>
                            </div>
                        </div>
                        <div class="a-center sec">
                            <p>
                                <span>Bank {{ trans('product.custodian') }}</span><br/>
                                {{ @$row->fund_orcl->custodian_bank->cb_nm }}
                            </p>
                        </div>
                        <div class="col-2 sec">
                            <div class="after_clear">
                                <div class="left">
                                    <p><b>{{ trans('product.latest_nav') }}</b></p>
                                </div>
                                <div class="right">
                                    <p>{{ $row->last_nab }}</p>
                                </div>
                            </div>
                            <div class="after_clear">
                                <div class="left">
                                    <p><b>{{ trans('product.latest_aum') }}</b></p>
                                </div>
                                <div class="right">
                                    <p>{{ formatUang($row->last_aum) }}</p>
                                </div>
                            </div>
                            <div class="after_clear">
                                <div class="left">
                                    <p><b>{{ trans('product.latest_unit') }}</b></p>
                                </div>
                                <div class="right">
                                    <p>{{ formatUangPop($row->last_unit) }}</p>
                                </div>
                            </div>
                            <div class="after_clear">
                                <div class="left">
                                    <p><b>{{ trans('product.riskprofile') }}</b></p>
                                </div>
                                <div class="right">
                                    <p> {{ $row->fund_orcl->result_fund_risk }}</p>
                                </div>
                            </div>

                        </div>
                        <div class="return a-center col-2">
                            <p>{{ trans('global.growth') }} (%)</p>
                        </div>
                        <div class="table sec">
                            <table>
                                <tr>
                                    <td></td>
                                    <th>3{{ trans("global.mt") }}</th>
                                    <th>6{{ trans("global.mt") }}</th>
                                    <th>1{{ trans("global.yr") }}</th>
                                    <th>3{{ trans("global.yr") }}</th>
                                </tr>
                                <tr>
                                    <th>NAB</th>
                                    <td>{{ valueDecimal(@$row->daily_nav->three_months) }} </td>
                                    <td>{{ valueDecimal(@$row->daily_nav->six_months) }} </td>
                                    <td>{{ valueDecimal(@$row->daily_nav->one_years) }} </td>
                                    <td>{{ valueDecimal(@$row->daily_nav->three_years) }} </td>
                                </tr>
                                <tr>
                                    <th>AUM</th>
                                    <td>{{ valueDecimal(@$row->fund_performance->aum_three_months) }} </td>
                                    <td>{{ valueDecimal(@$row->fund_performance->aum_six_months) }} </td>
                                    <td>{{ valueDecimal(@$row->fund_performance->aum_one_year) }} </td>
                                    <td>{{ valueDecimal(@$row->fund_performance->aum_three_year) }} </td>
                                </tr>
                                <tr>
                                    <th>UNIT</th>
                                   	<td>{{ valueDecimal(@$row->fund_performance->ou_three_months) }} </td>
                                    <td>{{ valueDecimal(@$row->fund_performance->ou_six_months) }} </td>
                                    <td>{{ valueDecimal(@$row->fund_performance->ou_one_year) }} </td>
                                    <td>{{ valueDecimal(@$row->fund_performance->ou_three_year) }} </td>
                                </tr>
                            </table>
                        </div>
                        <div class="col-2 sec">
                            <h6>{{ trans('compare.fee_information') }}</h6>
                            <!--div class="after_clear">
                                <div class="left">
                                    <p>Fee Information</p>
                                </div>
                                <div class="right">
                                    <p>2%</p>
                                </div>
                            </div-->
                            <div class="after_clear">
                                <div class="left">
                                    <p>{{ trans('product.subscription_fee') }}</p>
                                </div>
                                <div class="right">
                                    <p>{{ $row->fee_subs }}</p>
                                </div>
                            </div>
                            <div class="after_clear">
                                <div class="left">
                                    <p>{{ trans('product.redemption_fee') }}</p>
                                </div>
                                <div class="right">
                                    <p>{{ $row->result_fee_redm }}</p>
                                </div>
                            </div>
                            <div class="after_clear">
                                <div class="left">
                                    <p>{{ trans('product.switching_fee') }}</p>
                                </div>
                                <div class="right">
                                    <p>{{ $row->result_fee_swc }}</p>
                                </div>
                            </div>

                        </div>
                        <div class="col-2 sec">
                            <h6>{{ trans('compare.transaction_info') }}</h6>
                            <div class="after_clear">
                                <div class="left">
                                    <p>{{ trans('product.minimum_subscription') }}</p>
                                </div>
                                <div class="right">
                                    <p>IDR {{ formatUang($row->min_investment) }}</p>
                                </div>
                            </div>
                            <div class="after_clear">
                                <div class="left">
                                    <p>{{ trans('product.minimum_increment') }}</p>
                                </div>
                                <div class="right">
                                    <p>IDR {{ formatUang($row->min_increment) }}</p>
                                </div>
                            </div>
                            <div class="after_clear">
                                <div class="left">
                                    <p>Minimum Balance</p>
                                </div>
                                <div class="right">
                                    <p>IDR {{ formatUang($row->min_balance) }}</p>
                                </div>
                            </div>
                            <!--div class="after_clear">
                                <div class="left">
                                    <p>Minimum Portofolio</p>
                                </div>
                                <div class="right">
                                    <p>IDR 500.000</p>
                                </div>
                            </div-->
                        </div>
                        <a href="javascript:void(0)" onclick = "addWatchLists('{{ $row->fund_cd }}')" class="watch-list">Add To Watchlist</a>

                        <div class="col-2">
                            <a href="{{ urlLang("product/download/".$row->prospectus) }}" class="btn-donwload">Prospectus</a>
                            <a href="{{ urlLang("product/download/".$row->fund_fact_sheet) }}" class="btn-donwload">Fund FactSheet</a>
                        </div>
                        <div class="btn-wrap">
                            <a href="{{ urlLang('product?pop='.$row->fund_cd) }}" class="" >
                                {{ trans('product.buy') }}
                            </a>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>

        <p>&nbsp;</p>
        <div class="disclaimer-box">
            <h4>{{ trans('disclaimer.title_performa') }}</h4>
            <p>
                {{ trans('disclaimer.performa') }}
            </p>
        </div>
    </div>

</section>
<!-- end of middle -->

@include("compare.pop")
@endsection
@push('scripts')
<script type="text/javascript" src="//code.highcharts.com/stock/highstock.js"></script>
<script src="//code.highcharts.com/modules/exporting.js"></script>

<script>
    $(document).ready(function(){
        $.ajax({
            url:'{{ urlLang('compare/ajax-chart') }}',
            beforeSend: function(){
                $("#chart-compare").html("<img src='{{ asset('loading.gif') }}' />");
            },
            success: function(response){
                Highcharts.chart('chart-compare', {
                    chart: {
                        type: 'line',
                    },
                    title: {
                        text: ''
                    },
                    yAxis: {
                        title: {
                            text: '{{ ucwords($fieldShare) }}',
                        },
                        labels: {
                            formatter: function () {
                              return this.value / 1000000000 + ' {{ trans("global.billion") }}';
                            }
                          }
                    },
                     plotOptions: {
                        series: {
                            connectNulls: true
                        }
                    },
                    scrollbar: {
                            enabled: true
                    },
                    credits: false,
                    xAxis: {
                        categories: JSON.parse(response.dates),
                        //tickLength: 0,
                        tickInterval: 15,
                    },

                    series:  JSON.parse(response.data),
                });
            },
        });
    });
</script>

<script type="text/javascript">

    $(document).ready(function(){
        $("#form_filter").on("submit", function (e) {
            e.preventDefault();
        });

        $(".datepicker2").datepicker({
            dateFormat: "yy-mm-dd"
        });

    });

    function reload(newUrl)
    {
        $newUrl = '{{ urlLang("compare") }}/compare-fund?' + newUrl + "#chart-wrap";
        document.location.href=$newUrl;
    }

    function changeReksaDana($im_id,$counter)
    {
        $.ajax({
            url: '{{ urlLang("compare/ajax-change-reksa-dana") }}',
            data: {
                im_id: $im_id,
                type: '{{ get("type") }}',
            },
            success: function(respon){
                $("#fund_compare" + $counter).html("");
                $("#fund_compare" + $counter).html(respon.result);
            },

        });
    }

    function monthRange()
    {
        from = $("#from").val();
        from = moment(from).format('YYYYMMDD');
        to = $("#to").val();
        to = moment(to).format('YYYYMMDD');
        from_to = from + '-' + to;
        $("#fund_month").val(from_to);
    }

    function reloadRange()
    {
       $.ajax({
        url: '{{ urlLang("compare/ajax-range-fund") }}',
        data: {
            from: $("#from").val(),
            to: $("#to").val(),
            queryString: "{{ changeQueryString(['m'=>'']) }}",
        },
        success: function(respon){
            reload(respon.result);
        },
       });
    }

    function filter(param)
    {
        url = '{{ changeQueryString([]) }}' + "&filter="+param;
        reload(parser_decode(url));
    }

    function parser_decode($html)
    {
        var parser = new DOMParser;
        var dom = parser.parseFromString($html,'text/html');
        var decodedString = dom.body.textContent;
        return decodedString;
    }

    function reloadPost(data)
    {
        $.each(data,function(key,val){
            $("#" + key).val(val);
        });

        $("#form_compare").submit();
    }

    function reloadForm()
    {
        $from = $("#from").val();
        $to = $("#to").val();
        $resultFrom = new Date($from);
        $resultTo = new Date($to);
        if($resultFrom > $resultTo)
        {
            swal('{{ trans("global.notif_date_from_more_than") }}');
        }else{
            $("#form_compare").submit();
        }
    }

</script>
@endpush
@include("flash")
