 <div class="left">
    <div class="nav-tab">
        @foreach($modelInfoProduct as $index => $data)
            <a href='#infoprod-tab-{{ $index }}' <?= $index == 0 ? 'class="active"' : "" ?>>
                <small>{{ $data->title1 }}</small>
                <p>{{ $data->title2 }}</p>
            </a>
        @endforeach
    </div>                    
</div>
<div class="right">
    @foreach($modelInfoProduct as $index => $data)
        <div class="content-tab" id="infoprod-tab-{{ $index }}">
            {!! $data->description !!}    
            {{ $data->first }}
        </div>
    @endforeach
</div>