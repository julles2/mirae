
@if(!empty($modelInfoCompany))	
	<div class="left a-center">
	    <img src="{{ assetContent(null) }}/{{ $modelInfoCompany->image }}" alt="prod" />
	</div>
	<div class="right">  
	    {!! $modelInfoCompany->description !!}
	</div>
@endif