@if(count($listing) > 0)
        <?php $no = $offset; ?>
        @foreach($listing as $row)
        <?php
        $no++;
        $detail = $row;
        ?>
                @if(!empty($detail->id))

                    <tr class="tr_listing">
                        <td>{{ $no }}</td>
                        <td>
                            <label class="opt">
                                <input value = "{{ $row->fund_cd }}" class = "fund_compare" type="checkbox" />
                            </label>
                        </td>
                        <td onclick = "javascript:loadPop('{{ $row->fund_cd }}')">{{ $row->fund_name }}</td>
                        @if(empty(get('type')))
                            <td onclick = "javascript:loadPop('{{ $row->fund_cd }}')">{{ $row->fund_type }}</td>
                        @endif
                        <td style="text-align: right;" onclick = "javascript:loadPop('{{ $row->fund_cd }}')">{{ formatUangKoma2($detail->nav_per_unit) }}</td>
                        <td style="text-align: right;" onclick = "javascript:loadPop('{{ $row->fund_cd }}')">{{ $detail->round_one_day }}</td>
                        <td style="text-align: right;" onclick = "javascript:loadPop('{{ $row->fund_cd }}')">{{ $detail->round_three_weeks }}</td>
                        <td style="text-align: right;" onclick = "javascript:loadPop('{{ $row->fund_cd }}')">{{ $detail->round_one_month }}</td>
                        <td style="text-align: right;" onclick = "javascript:loadPop('{{ $row->fund_cd }}')">{{ $detail->round_three_months }}</td>
                        <td style="text-align: right;" onclick = "javascript:loadPop('{{ $row->fund_cd }}')">{{ $detail->round_six_months }}</td>
                        <td style="text-align: right;" onclick = "javascript:loadPop('{{ $row->fund_cd }}')">{{ $detail->round_one_years }}</td>
                        <td style="text-align: right;" onclick = "javascript:loadPop('{{ $row->fund_cd }}')">{{ $detail->round_ytd }}</td>
                        <td style="text-align: right;" onclick = "javascript:loadPop('{{ $row->fund_cd }}')">{{ $detail->round_three_years }}</td>
                        <td style="text-align: right;" onclick = "javascript:loadPop('{{ $row->fund_cd }}')">{{ $detail->round_five_years }}</td>
                        <td style="text-align: right;" onclick = "javascript:loadPop('{{ $row->fund_cd }}')">{{ $detail->aum_listing }}</td>
                        <td style="text-align: right;" onclick = "javascript:loadPop('{{ $row->fund_cd }}')">{{ $detail->unit_listing }}</td>
                        {{-- <td><a href="javascript:loadPop('{{ $row->fund_cd }}')" class="cart_btn"><img src="{{ assetFrontend(null) }}images/material/cart_ico.png"</a></td> --}}
                    </tr>
                @endif
        @endforeach
@else
        <tr class = "tr_empty">
            <td colspan="17" style="text-align:center;">{{ trans('product.dataisempty') }}</td>
        </tr>
@endif
