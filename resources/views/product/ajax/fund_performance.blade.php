<tr>
    <td></td>
    <td style="text-align:right;">1M</td>
    <td style="text-align:right;">3M</td>
    <td style="text-align:right;">6M</td>
    <td style="text-align:right;">YTD</td>
    <td style="text-align:right;">1YR</td>
    <td style="text-align:right;">3YR</td>
</tr>
@foreach($params as $var => $label)
<?php

$value = function($field)use($var,$row){
    return $row[$var.$field];
};

$classMin = function($result){
    return $result < 0 ? 'min' : '';
};

?>
<tr>
    <td class = "">{{ $label }}</td>
    <td class = "{{ $classMin($value('one_month')) }}" style="text-align:right;">
        {{ formatUangKoma2($value('one_month')) }}
    </td>
    <td class = "{{ $classMin($value('three_months')) }}" style="text-align:right;">
        {{ formatUangKoma2($value('three_months')) }}
    </td>
    <td class = "{{ $classMin($value('six_months')) }}" style="text-align:right;">
        {{ formatUangKoma2($value('six_months')) }}
    </td>
    <td class = "{{ $classMin($value('ytd')) }}" style="text-align:right;">
        {{ formatUangKoma2($value('ytd')) }}
    </td>
    <td class = "{{ $classMin($value('one_year')) }}" style="text-align:right;">
        {{ formatUangKoma2($value('one_year')) }}
    </td>
    <td class = "{{ $classMin($value('three_year')) }}" style="text-align:right;">
        {{ formatUangKoma2($value('three_year')) }}
    </td>
    
</tr>
@endforeach
