@extends('layouts.layout')
@section('content')
<!-- middle -->
<section class="banner">
    <img src="{{ assetContent(@$setting->banner_image) }}" alt="banner" />
    <div class="caption">
        <h6>{{ @$setting->title_caption }}</h6>
        <h1>{!! @$setting->title_main !!}</h1>
        <p>{{ @$setting->sub_title }}</p>
    </div>
</section>
<section class="std-content">
    <div class="wrapper">
        <div class="nav-produk">
            <a href="{{ urlLang('product') }}" class="left" class="{{ $active('') }}">
                <span class="btn_nav">
                    {{ trans('product.all') }}
                </span
            </a>
            <div class="right">
                <a href="{{ urlLang('product?type=EQ') }}" class="{{ $active('EQ') }}"><span style="background:url({{ assetFrontend(null) }}images/material/cat-prod-1.png)"></span>{{ trans('type_fund.eq') }}</a>
                <a href="{{ urlLang('product?type=MX') }}" class="{{ $active('MX') }}"><span style="background:url({{ assetFrontend(null) }}images/material/cat-prod-2.png)"></span>{{ trans('type_fund.mx') }}</a>
                <a href="{{ urlLang('product?type=FI') }}" class="{{ $active('FI') }}"><span style="background:url({{ assetFrontend(null) }}images/material/cat-prod-3.png)"></span>{{ trans('type_fund.fi') }}</a>
                <a href="{{ urlLang('product?type=MM') }}" class="{{ $active('MM') }}"><span style="background:url({{ assetFrontend(null) }}images/material/cat-prod-5.png)"></span>{{ trans('type_fund.mm') }}</a>
                <a href="{{ urlLang('product?type=SR') }}" class="{{ $active('SR') }}"><span style="background:url({{ assetFrontend(null) }}images/material/cat-prod-4.png)"></span>{{ trans('type_fund.sr') }}</a>
            </div>
        </div>
        {!! Form::open(['method'=>'post','url'=>urlLang('compare/store-fund'),'id'=>'compare_form']) !!}
            <div id = "compare_div">
            
            </div>
        {!! Form::close() !!}
        <div class="category">
            <div class="date-view">
                <p>{{ trans('product.as_of') }} {{ Mirae::dateAsOf() }}</p>
            </div>
            <div class="right" style="width:auto;">
                <div class="left">
                    <input type="text" placeholder="{{ trans('product.find') }} {{ trans('product.mutualfund') }}" value = "" id = "cari">
                    <input type="button" id = "btn_cari" />
                </div>
                <div class="left">
                    <a href="javascript::void(0)" onclick="compare_check()" class="btn-std left">{{ trans('home.compare') }}</a>
                </div>
            </div>
        </div>
        <input type="hidden" id = "offset" value="0" />
        <input type="hidden" id = "sort_by" value = "">
        <input type="hidden" id = "sort_by_order" value = "asc">
        <div style="text-align: right;"><b style="color:orange;">{{ ucwords(trans("global.explanation")) }} : AUM (IDR {{ trans('global.billion') }}) &nbsp; UNIT ({{ trans('global.million') }})</b></div><br/>
        <div class="overflow">
            <table class="product product-list-table" id = "table_fund">
                <thead>
                    <tr style="cursor:pointer;">
                        <th>#</th>
                        <th>
                            <label class="opt">
                                <input type="checkbox" id = "check_all" />
                            </label>
                        </th>
                        <th onclick = "reloadTableSortBy('fund_name')">{{ trans('home.mutualfundname') }}</th>
                        @if(empty(get('type')))
                            <th onclick = "reloadTableSortBy('fund_type')">{{ language('Type','Jenis') }}</th>
                        @endif
                        <th onclick = "reloadTableSortBy('nav_per_unit')">{{ trans("global.nav") }}/Unit</a></th>
                        <th onclick = "reloadTableSortBy('one_day')" >{{ language('1Day','1Hr') }}</th>
                        <th onclick = "reloadTableSortBy('three_weeks')">{{ language('1Week','1Mg') }}</th>
                        <th onclick = "reloadTableSortBy('one_month')">{{ language('1Mth','1Bl') }}</th>
                        <th onclick = "reloadTableSortBy('three_months')">{{ language('3Mth','3Bl') }}</th>
                        <th onclick = "reloadTableSortBy('six_months')">{{ language('6Mth','6Bl') }}</th>
                        <th onclick = "reloadTableSortBy('one_years')">{{ language('1Yr','1Th') }}</th>
                        <th onclick = "reloadTableSortBy('ytd')">YTD</th>
                        <th onclick = "reloadTableSortBy('three_years')">{{ language('3Yr','3Th') }}</th>
                        <th onclick = "reloadTableSortBy('five_years')">{{ language('5Yr','5Th') }}</th>
                        <th onclick = "reloadTableSortBy('aum')">AUM</th>
                        <th onclick = "reloadTableSortBy('outstanding_unit')">UNIT</th>
                        {{-- <th></th> --}}
                    </tr>
                </thead>
                <tbody id = "list_reksadana">

                </tbody>
            </table>

        </div>
        @if($countButton > 0)
        <div class="a-center">
            <button type="button" class="btn-std" id="load_more" style="margin-top:70px;">Load More</button>
            <img src="{{ asset('loading.gif') }}" id="loading_image" style="display:none;" />
        </div>
        @endif
        <?php
        /*
          <div class="table-nav">
          <div class="entries left">
          {{ trans('product.show') }}
          <select id = "pagination" onchange = "return reloadTable(this.value)">
          <option value = "15">15</option>
          </select>
          {{ trans('product.allentries') }}
          </div>
          <div class="info right">
          {{ trans('product.showing') }} 1 {{ trans('product.to') }} <label id = "to"></label> {{ trans('product.of') }} <label id = "max_entries"></label> {{ trans('product.entries') }}
          </div>
          </div>
         */
        ?>
    </div>
</section>
@include('product.pop.product_detail')
@include('product.pop.beli')
<!-- end of middle -->
@endsection

@include("product.scripts")
@push('scripts')

@endpush
