<div class="wrap-popup product-detail penjualan col-1 " id="pembelian">
    <div class="head">
        <h5>{{ trans('product.purchaseofmutualfund') }}</h5>
        <a href="javascript:closePop('pembelian')" class="close">{{ trans('product.close') }}</a>
    </div>
    <div class="content">
        <div class="title-content after_clear">
            <h3 class="left">{{ trans('product.productinfo') }}</h3>
        </div>
        <div class="myaccount box-content-popup">
            <div class="box-list-data pembelian-list-data">
                <div class="list-data list_2">
                    <div class="inner-list-data">
                        <div class="box-title-list">
                            <div class="left">
                                <figure><img class="imgCompany"></figure>
                            </div>
                            <div class="right">
                                <h5 class = "imName">Avrist Balanced Amar Syariah</h5>
                                <p class = "fund_name">Avrist Asset Management, PT</p>
                            </div>
                        </div>
                        <div class="desc-list">
                            <div class="row">
                                <div class="list ">
                                    <div class="inner-list-data">
                                        <label>{{ trans('product.minimumpurchase') }}</label>
                                        <strong><label class="min_investment inline"></label> IDR</strong>
                                    </div>
                                </div>
                                <div class="list ">
                                    <div class="inner-list-data">
                                        <label>{{ trans('product.minimumfurtherinvestment') }}</label>
                                        <strong><label class="min_increment inline"></label> IDR</strong>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="list ">
                                    <div class="inner-list-data">
                                        <label>{{ trans('product.transactioncost') }}</label>
                                        <strong><label class = "fee_subs inline"></label> %</strong>
                                    </div>
                                </div>
                                <div class="list ">
                                    <div class="inner-list-data">
                                        <label>{{ trans('product.minimum_portfolio') }}</label>
                                        <strong><label class = "min_balance inline"></label> IDR</strong>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="list">
                                    <div class="inner-list-data a-center">
                                        <label>{{ trans('product.latest_nav') }}
                                            (per: <label class = "latestNavDate">20/03/2017)</label></label>
                                        <strong class="ex-blue"><label class = "latestNavValue inline">4,916 IDR</label></strong>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="list-data list_2 std-content">
                    {{ Form::open(['method'=>'post','url'=>lang().'/product/pembelian','id'=>'form_pembelian']) }}

                        <div class="desc-list row_btm">
                            <div class="row no-border">
                                <div class="left">{{ trans('product.inputvalue') }}</div>
                            </div>
                            <div class="row no-border">
                                <input type="text" id = "nilai_pembelian" onkeyup = "uangField(this)" name="nilai" class="a-right price">
                            </div>
                            <div class="row no-border ">
                                <label class="radio-btn"><input type="radio" name="order_type" checked="checked" value = "subscription" class="showhide-type2" id = "" />{{ trans('product.purchaseonce') }}</label>
                                <label class="radio-btn"><input type="radio" name="order_type" value = "subscription_auto_debet" class="showhide-type2 trigger-show" id = "" />{{ trans('product.periodicalBuy') }}</label>
                            </div>
                            <div class="row no-border expand-priode">
                                <div class="col">
                                    <label>{{ trans('product.everydate') }}</label>
                                    {{-- <input id = "tanggal"  type="text" name="tanggal" onkeypress="return onlyNumber(event)" maxlength="2"> --}}
                                    {!! Form::selectRange('tanggal',1,27,['id'=>'tanggal']) !!}
                                </div>
                                <div class="col">
                                    <label>{{ trans('product.period') }}</label>
                                    <select name = "tahun">
                                        <option value = "1">1 {{ trans('product.year') }}</option>
                                        <option value = "2">2 {{ trans('product.year') }}</option>
                                        <option value = "3">3 {{ trans('product.year') }}</option>
                                    </select>
                                </div>
                            </div>
                            <div class="row no-border">
                                <div class="disclaimer-box rounded">
                                    <h4>{{ trans('disclaimer.title_performa') }}</h4>
                                    <p>
                                        {{ trans('disclaimer.performa') }}
                                    </p>
                                </div>
                            </div>
                            <div class="row no-border a-right">
                                <button type = "button" id = "confirm_pembelian" class="btn-oval btn-orange">{{ trans('product.confirm') }}</button>
                            </div>

                        </div>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>

</div>
