<div style="visibility: hidden;" class = "id"></div>
<div  style="visibility: hidden;" class = "ajax_fund_cd"></div>
<div class="wrap-popup product-detail" id="detail-prod">

    <div class="head">
        <h5>{{ trans('product.mutual_fund_information') }}</h5>
        <a href="javascript:closePop('detail-prod')" class="close">close</a>
    </div>

    <div class="content wrap-tab">

        <div class="nav-tab">
            <a href="#pd-ringkasan" class="active">{{ trans('product.summary') }}</a>
            <a href="#pd-infoprod">{{ trans('product.productinfo') }}</a>
            <a href="#pd-infoperusahaan">{{ trans('product.companyinfo') }}</a>
            <div class="right">
                <a class="watchlist-btn left" id="watchlist">{{ trans('product.addwatchlist') }}</a>
                <div class="btn-donwload left">
                    <a onclick="#" id="prospectus">{{ trans('product.prospectus') }}</a>
                    <a id="fundfactsheet">{{ trans('product.fundfuctsheet') }}</a>
                </div>
            </div>
        </div>
        <div>

        </div>
        <div class="content-tab inner ringkasan" id="pd-ringkasan" id = "divDefault">
            <div class="title-content after_clear">
                <h3 class="left">{{ trans('product.productsummary') }}</h3>
                <div class="logo-prod">
                    <div class="ico">
                        <img class="imgCompany" alt="produk logo" />
                    </div>
                    <div class="text">
                        <p class = "imName"></p>
                        <h6 class = "fund_name"></h6>
                    </div>
                </div>
            </div>
            <div class="latest-info col-3">
                <div class="box">
                    <p>
                        {{ trans('product.latest_nav') }}
                        <br/><span id = "">(per: <label class = "latestNavDate">{{ trans('product.date') }}</label>)</span>
                    </p>
                    <h6 id = ""><label class = "latestNavValue">4,916</label> IDR</h6>
                </div>
                <div class="box">
                    <p>
                        {{ trans('product.latest_aum') }}
                        <br/><span>(per: <label class = "latestNavDate">20/03/2017</label>)</span>
                    </p>
                    <h6><label class = "aum">105,062,541,743</label> IDR</h6>
                </div>
                <div class="box">
                    <p>
                        {{ trans('product.latest_unit') }}
                        <br/><span>(per: <label class = "latestNavDate">20/03/2017</label>)</span>
                    </p>
                    <h6><label class = "unit">21,969,239</label></h6>
                </div>
                <div class="box">
                    <p>
                        {{ trans('product.riskprofile2') }}
                    </p>
                    <h6><label class = "risk_profile">21,969,239</label></h6>
                </div>
            </div>
            <div>
                <div class="box-blue">
                    <h5>{{ trans('product.fund_performance') }}</h5>
                    <table >
                        <tbody  class = "fundPerformanceTable">

                        </tbody>
                    </table>
                </div>
                <div class="box-blue">
                    <h5>{{ trans('product.chart_nav_performance') }}</h5>
                    <?php /*
                    <a href='' class="detil">{{ trans('product.clickfordetail') }}</a>
                    */ ?>
                    <div id='chart-nav'>
                    
                    </div>
                </div>
                <div class="box-blue">
                    <h5>{{ trans('product.chart_aum_performance') }}</h5>
                    {{-- <a href='' class="detil">{{ trans('product.clickfordetail') }}</a> --}}
                    <div id='chart-aum'>
                    
                    </div>
                </div>
                <div class="box-blue">
                    <h5>{{ trans('product.chart_unit_performance') }}</h5>
                    {{-- <a href='' class="detil">{{ trans('product.clickfordetail') }}</a> --}}
                    <div id='chart-unit'>
                    
                    </div>
                </div>
                <div class="clear"> </div>
            </div>
        </div>
        <div class="content-tab inner info-prod" id="pd-infoprod">
            <div class="title-content after_clear">
                <h3 class="left">{{ trans('product.productinfo') }}</h3>
                <div class="logo-prod">
                    <div class="ico">
                        <img class="imgCompany" alt="produk logo" />
                    </div>
                    <div class="text">
                        <p class = "imName"></p>
                        <h6 class = "fund_name"></h6>
                    </div>
                </div>
            </div>
            <div class="col-2 wrap-tab div_info_produk" >

            </div>
        </div>
        <div class="content-tab inner info-prod" id="pd-infoperusahaan">
            <div class="title-content after_clear">
                <h3 class="left">{{ trans('product.companyinfo') }}</h3>
                <div class="logo-prod">
                    <div class="ico">
                        <img class="imgCompany" alt="produk logo" />
                    </div>
                    <div class="text">
                        <p class = "imName"></p>
                        <h6 class = "fund_name"></h6>
                    </div>
                </div>
            </div>

            <div class="col-2 wrap-tab div_info_company">

            </div>
        </div>

        <hr/>

        <div class="inner">
            <div class="latest-info col-4">
                <div class="box">
                    <p>
                        {{ trans('product.minimumpurchase') }}
                    </p>
                    <h6 id = ""><label class="min_investment"></label> IDR</h6>
                </div>
                <div class="box">
                    <p>
                        {{ trans('product.minimumfurtherinvestment') }}
                    </p>
                    <h6><label class="min_increment"></label> IDR</h6>
                </div>
                <div class="box">
                    <p>
                        {{ trans('product.transactioncost') }}
                    </p>
                    <h6><label class = "fee_subs">100,000</label> %</h6>
                </div>
                <div class="box">
                    <p>
                        {{ trans('product.minimum_portfolio') }}
                    </p>
                    <h6><label id = "min_balance">100,000</label> IDR</h6>
                </div>
            </div>
            <div class="disclaimer-box">
                <h4>{{ trans('disclaimer.title_performa') }}</h4>
                <p>
                    {{ trans('disclaimer.performa') }}
                </p>
            </div>
            <div class="bottom">
                <div class="left">
                    <h6 style="cursor:pointer;" onclick = "">{{ trans('product.termconditionprospectus') }}</h6><br/>
                    <label>
                        <input type="checkbox" id="syarat"/>
                        {{ trans('product.ihaveread') }}
                    </label>
                </div>
                {!! Form::open(['url'=>urlLang("compare/store-fund") ,'method'=>'post','id'=>'form_store_compare']) !!}
                    <input type = "hidden" name = "fund_cd[]" id="hidden_fund_cd" />
                {!! Form::close() !!}
                <div class="right" style="margin-top:10px;">
                    <a href='javascript::void(0)' onclick="compare()" class="btn-std">{{ trans('product.compare') }}</a>
                    <a href='javascript::void(0)' onclick = "pop_beli()" class="btn-std yellow">{{ trans('product.buy') }}</a>
                </div>
            </div>
        </div>
    </div>

</div>
@push('scripts')
<script type="text/javascript">
    Highcharts.setOptions({
        lang: {
            numericSymbols: ['K', 'M','B','T']
        }
    });
    
    function chartNav(fund_cd,title)
    {
        $.ajax({
            url:'{{ urlLang("product/chart-nav") }}',
            data:{
                fund_cd:fund_cd,
            },
            success:function(response){
                $("#chart-nav-loader").hide();
                navs = response.value;
                dates = response.date;
                interval = response.interval;
                var myChart = Highcharts.chart('chart-nav', {
                    chart: {
                        type: 'area'
                    },
                    title: {
                        text: title
                    },
                    xAxis: {
                        categories: JSON.parse(dates),
                        tickInterval: interval
                    },
                    yAxis: {
                        title: {
                            text: 'Nav'
                        }
                    },
                    credits : false,
                    series: [{
                        name: 'Nav',
                        data: JSON.parse(navs),
                    }]
                });
            },
        });
    }

    function chartAum(fund_cd,title)
    {
        $.ajax({
            url:'{{ urlLang("product/chart-aum") }}',
            data:{
                fund_cd:fund_cd,
            },
            success:function(response){
                $("#chart-aum-loader").hide();
                aums = response.value;
                dates = response.date;
                interval = response.interval;
                var myChart = Highcharts.chart('chart-aum', {
                    chart: {
                        type: 'area'
                    },
                    title: {
                        text: title
                    },
                    xAxis: {
                        categories: JSON.parse(dates),
                        tickInterval: interval
                    },
                    yAxis: {
                        title: {
                            text: 'AUM'
                        }
                    },
                    credits : false,
                    series: [{
                        name: 'AUM',
                        data: JSON.parse(aums),
                    }]
                });
            },
        });
    }

    function chartUnit(fund_cd,title)
    {
        $.ajax({
            url:'{{ urlLang("product/chart-unit") }}',
            data:{
                fund_cd:fund_cd,
            },
            success:function(response){
                $("#chart-unit-loader").hide();
                aums = response.value;
                dates = response.date;
                interval = response.interval;
                var myChart = Highcharts.chart('chart-unit', {
                    chart: {
                        type: 'area'
                    },
                    title: {
                        text: title
                    },
                    xAxis: {
                        categories: JSON.parse(dates),
                        tickInterval: interval
                    },
                    yAxis: {
                        title: {
                            text: 'Unit'
                        }
                    },
                    credits : false,
                    series: [{
                        name: 'Unit',
                        data: JSON.parse(aums),
                    }]
                });
            },
        });
    }

    function compare()
    {
        $fundCode = $(".ajax_fund_cd").html();
        $("#hidden_fund_cd").val($fundCode);
        $("#form_store_compare").submit();
        //$url = '{{ urlLang("compare/compare-fund?fund1=") }}' + $fundCode + "&m=1&f=nav";
        //document.location.href=$url;
    }

    function viewPdf(field)
    {
        $id = $(".id").html();
        url = '{{ urlLang("product/view-pdf") }}' + '/' +$id + '/' + field;
        window.open(url, 'Prospectus',"width=1000,height=1000");
    }

    $(document).ready(function(){
        $syarat = $("#syarat");

        $syarat.on('click',function(){
            if($syarat.is(':checked'))
            {
                viewPdf('prospectus');
                $url = '{{ urlLang("terms-conditions") }}';
                window.open($url, 'term and condition',"width=1000,height=1000");

            }
        });
});
</script>
@endpush
<script src="//code.highcharts.com/highcharts.js"></script>
<script src="//code.highcharts.com/modules/exporting.js"></script>
