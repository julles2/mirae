@push('scripts')
@if(!empty(get('pop')))
<script type="text/javascript">
    $(window).load(function(){
        loadPop('{{get("pop")}}');
    });
</script>
@endif

<script type="text/javascript">
    $(document).ready(function(){
        $("#check_all").click(function(){
            $this = $(this);
            if($this.is(':checked'))
            {
                $('input:checkbox').not(this).prop('checked', this.checked);
            }else{
                $('input:checkbox').removeAttr('checked');
            }
        });

        reloadTable();
        //pagination();
        $("#confirm_pembelian").on('click',function(){
            $orderType = $("input[name='order_type']:checked").val();
            $.ajax({
                    url : '{{ urlLang("product/validate-pembelian") }}',
                    type : 'get',
                    data : {
                        fund_id : $(".id").html(),
                        nilai : $("#nilai_pembelian").val(),
                        _token : '{{ csrf_token() }}',
                        orderType : $orderType,
                        tanggal : $("#tanggal").val(),
                    },
                    success : function(response){
                        if(response.result == 'false')
                        {
                            swal_error("{{ trans('product.Value-Less-than-minimum-investment-value') }}");
                        }else if(response.result == 'not_login'){
                            swal_info('{{ trans("global.you_are_not_logged_in") }}');
                        }else if(response.result == 'minus_cash'){
                            swal_error('{{ trans('global.notif_cash_balance_less') }}');
                        }else if(response.result == 'true'){
                            $("#form_pembelian").attr("action", "{{ urlLang('product/pembelian/') }}/" + response.model.id);
                            $("#form_pembelian").submit();
                        }else if(response.result == 'tanggal_30'){
                            swal_error('{{ trans('notif.tanggal_30') }}');
                        }else if(response.result == 'tanggal_00'){
                             swal_error('{{ trans('notif.tanggal_30') }}');
                        }else{
                            swal({
                              title: "{{ trans('global.are_you_sure') }}",
                              text: "<p>{{ trans('global.notif_double_subs1') }} <b>"+ response.view.ref_no +"</b> {{ trans('global.notif_double_subs2') }} <b>"+ response.view.tr_date + " "+ response.view.tr_time +"</b>.</p><p>{{ trans('global.notif_double_subs3') }}</p>",
                              type: "warning",
                              showCancelButton: true,
                              confirmButtonColor: "#DD6B55",
                              confirmButtonText: "Lanjutkan",
                              cancelButtonText: "Batal",
                              closeOnConfirm: false,
                              html: true,
                            },
                            function(){
                                $("#form_pembelian").attr("action", "{{ urlLang('product/pembelian/') }}/" + response.model.id);
                                $("#form_pembelian").submit();
                            });
                        }
                    },
            });

        });

        $("#load_more").on('click',function(){
            $offset = parseInt($("#offset").val());
            $("#offset").val($offset + 15);
            reloadTable();
        });

    });

    $("#btn_cari").click(function(){
        reloadTableDefault();
    });

    $("#cari").keypress(function(e){
        if (!e) e = window.event;
            var keyCode = e.keyCode || e.which;
            if (keyCode == '13'){
                $("#offset").val("0");
                $("#list_reksadana").html("");
                reloadTable();
                pagination();
            }
    });

    function pagination(limit="")
    {
        $.ajax({
            url : '{{ Mirae::urlAction("ajax-pagination") }}',
            data : {
                type : '{{ request()->get("type") }}',
            },
            success : function(respon){
                $("#pagination").html("");
                $("#pagination").html(respon.options);
            },
        });
    }

    function reloadTableSortBy(field)
    {
        $("#sort_by").val(field);
        sort_by = $("#sort_by").val();
        sort_by_order = $("#sort_by_order");
        if(sort_by_order.val() == 'asc')
        {
           sort_by_order.val('desc');
        }else{
            sort_by_order.val('asc');
        }
        reloadTableDefault();
    }

    function reloadTableDefault()
    {
        $("#offset").val("0");
        $("#list_reksadana").html("");
        reloadTable();
        pagination();
    }

    function reloadTable(type=null,limit=null)
    {
        list_reksadana = $("#list_reksadana");
        
        $.ajax({
            url : '{{ Mirae::urlAction('ajax-listing-reksadana') }}',
            data : {
                type : '{{ request()->get("type") }}',
                limit : limit,
                return : '{{ get("return") }}',
                time : '{{ get("time") }}',
                category : '{{ get("category") }}',
                fund_name : $("#cari").val(),
                offset : $("#offset").val(),
                sort_by: $("#sort_by").val(),
                sort_by_order: $("#sort_by_order").val(),
            },
            beforeSend : function(){
                // list_reksadana.html("");
                $("#load_more").hide();
                $("#loading_image").show();
            },
            success : function(respon){
                // list_reksadana.html("");
                 $("#loading_image").hide();
                 $("#load_more").show();
                if(respon.count > 0)
                {
                    $(".tr_empty").hide();
                    //list_reksadana.html("");
                    list_reksadana.append(respon.listing);
                    //  $('html, body').animate({
                    //         scrollTop: $("#table_fund tr:last").offset().top
                    // }, 1000);
                }else{
                    var td = $("#table_fund").children('tbody').children('tr').children('td').length;
                    if(td > 0)
                    {
                        alert('reska dana sudah ditampilkan semua');
                    }else{
                        list_reksadana.html("");
                        list_reksadana.append(respon.listing);
                    }
                }
            },
        });
    }

    function chartLoader()
    {
        $loading = "<img src='{{ asset('loading.gif') }}' />";
        $("#chart-unit").html($loading);
        $("#chart-aum").html($loading);
        $("#chart-nav").html($loading);
        $("#chart-unit-loader").show();
        $("#chart-aum-loader").show();
        $("#chart-nav-loader").show();
    }

    function loadPop(fund_cd)
    {
        //alert(numeral(1000).format('0,0'));
        $.ajax({
            url : '{{ Mirae::urlAction('ajax-pop-reksa-dana') }}',
            data:{
                fund_cd : fund_cd,
            },
            beforeSend : function(){
                $("#divDefault").hide();
                showPop('loaderpopup');
                chartLoader();
            },
            success : function(respon){
                closePop('loaderpopup');
                showPop('detail-prod');

                $(".latestNavDate").html(respon.vars.latestNavDate);

                $.each(respon.vars , function(key,val){

                    $("."+key).html(val);
                });

                $.each(respon.model , function(key,val){
                    if(isNaN(val) == true)
                    {
                        result = val;
                    }else{
                        result = numeral(val).format('0,0');
                    }

                    $("."+key).html(result);
                });
                
                chartAum(respon.model.fund_cd , respon.model.fund_name);
                chartNav(respon.model.fund_cd , respon.model.fund_name);
                chartUnit(respon.model.fund_cd , respon.model.fund_name);

                $(".imgCompany").attr('src','{{ asset(null) }}contents/' + respon.vars.image_logo_company);

                $("#prospectus").hide()
                $("#fundfactsheet").hide()

                // console.log('INI FILE : '+ respon.model.prospectus);

                if (respon.model.prospectus != '' && respon.model.prospectus != null) {
                    $("#prospectus").show()
                    $("#prospectus").attr('onclick',"downloadFilePdf('"+ respon.model.prospectus +"')");
                }

                if (respon.model.fund_fact_sheet != '' && respon.model.fund_fact_sheet != null) {
                    $("#fundfactsheet").show()
                    $("#fundfactsheet").attr('onclick',"downloadFilePdf('"+ respon.model.fund_fact_sheet +"')");
                }

                $("#watchlist").attr('onclick',"addWatchLists('"+ respon.model.fund_cd +"')");

                tab();
//                closePop('loaderpopup');
//                setTimeout(function(){
//                    showPop('detail-prod');
//                },500);

            },
        });
    }

    function downloadFilePdf(filenya)
    {
        window.open("{{ asset(null) }}contents/pdf/" + filenya, '_blank');
    }

    function pop_beli()
    {
        if($("#syarat").is(':checked'))
        {
            fund_id = $(".id").html();

            $.ajax({
                url : "{{ Mirae::urlAction('ajax-pop-beli') }}",
                success : function(){
                    closePop("detail-prod");
                    showPop("pembelian");
                },
                data : {
                    fund_id : fund_id,
                },
            });
        }else{
            swal('{{ trans("global.notif_please_tick") }}');
        }

    }


    function compare_check()
    {
        $html = "";
        $count = 0;
        $.each($(".fund_compare:checkbox:checked"),function(key,val){
            $count = $count + 1;
            this_val = $(this).val();

            $html += "<input type = 'hidden' value = '"+this_val+"' name = 'fund_compare[]' />";
        });
        if($count > 0)
        {
            if($count > 3)
            {
                swal_error('{{ trans("notif.max_compare") }}');
            }else{
                $("#compare_div").html($html);
                $("#compare_form").submit(); 
            }
                
        }else{
            swal('{{ trans("global.you_have_not_selected_fund") }}');
        }
            
    }


</script>

@endpush
