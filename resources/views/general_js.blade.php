@push('scripts')
<script type="text/javascript">
$(document).ready(function(){

    $("#form_newsletter").submit(function(){
        return false;
    });

    $(".select2").select2({
      width: '100%'
    });
    $(document).ready(function(){
        $(".datepicker").datepicker({
            dateFormat : 'dd-mm-yy',
            changeMonth: true,
            changeYear: true,
            showButtonPanel: true,
        });
    });
});
function addWatchLists(fund_cd)
{
  $.ajax({
      url: '{{ urlLang("watchlists/addwatchlists") }}',
      type:'GET',
      data: {
          fund_cd: fund_cd,
      },
      beforeSend: function(){
      },
      success: function(respon){
          if (respon.status == 'success')
          {
              alert(respon.success);
          }else{
                alert('Silahkan login terlebih dahulu');
                //console.log(respon);
               //window.location.href=respon.url;
          }
      },
  });

}

function newsletter()
{
  var email = $('#email_newsletter').val();
  if(email != "")
  {
      $.ajax({
        url: '{{ urlLang("newsletter/newsletter") }}',
        type:'GET',
        data: {
            email: email,
        },
        beforeSend: function(){

        },
        success: function(respon){
            if (respon.status == 'success')
            {
                alert(respon.success);
                $('#email_newsletter').val('')
            }else{
                alert(respon.success);
                $('#email_newsletter').val('')
            }

        },
      });
  }else{
      alert("{{ language('Email is Required','Email Harus di isi') }}");
  }


}

function search(e)
{
  var keyword = $('#txtSearch').val();

  if (e.keyCode == 13) {
        $.ajax({
          url: '{{ urlLang("search/search") }}',
          data: {
              keyword: keyword,
          },
          beforeSend: function(){
          },
          success: function(respon){
              window.location.href = respon.url;
          },
        });
    }

}
</script>
