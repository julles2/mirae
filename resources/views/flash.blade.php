@push('scripts')

<script type="text/javascript">
    function swal_error(pesan)
    {
        swal({
            type : 'error',
            title : 'Error',
            html : true,
            text : pesan,
        });
    }
    function swal_info(pesan)
    {
        swal({
            type : 'info',
            title : "{{ trans('notif.information') }}",
            html : true,
            text : pesan,
        });
    }
    function swal_success(pesan)
    {
        swal({
            type : 'success',
            title : "{{ trans('notif.success!') }}",
            html : true,
            text : pesan,
        });
    }
</script>
@endpush

@if(session()->has('success'))
        @push('scripts')
            <script type="text/javascript">
                swal_success('{{ session()->get("success") }}');
            </script>
        @endpush
@endif

@if(session()->has('info_global'))
        @push('scripts')
            <script type="text/javascript">
                swal_info('{{ session()->get("info_global") }}');
            </script>
        @endpush
@endif

@if(@$errors->any())
        <?php
            $str = "<ul>";
            	foreach(@$errors->all() as $error)
                {
            		$str.="<li>$error</li>";
            	}
            $str .= "</ul>";
        ?>
        @push('scripts')
            <script type="text/javascript">
                swal_error('{!! $str !!}');
            </script>
        @endpush
@endif
