@extends('layouts.layout')
@section('content')
<!-- middle -->
<section class="banner">
    <img src="{{  assetFrontend(null) }}images/content/banner-dashboard.jpg" alt="banner" />
    <div class="caption">
        <h6>{{ ucwords(trans('global.transaction')) }}</h6>
        <h1>Checkout</h1>
        <p>&nbsp;</p>
    </div>
</section>
<section class="std-content">
    <div class="wrapper myaccount">
        <div class="title-content after_clear">
            <h3><i class="icwp ic_checklist"></i> {{ trans('global.transactions_successfully_processed') }}</h3>
        </div>
        <p class="thanks-p">{!! trans('product.thanks-registrasi') !!} :</p>
        <table class="table-radius">
            <thead>
                <tr>
                    <th style="width: 100px;">{{ trans('product.time') }}</th>
                    <th>{{ trans('product.mutualfundname') }}</th>
                    <th>{{ trans('product.instruction') }}</th>
                    <th>{{ trans('product.total_unit') }}</th>
                    <th>{{ trans('product.purchase_value') }}</th>
                    <th>{{ trans('product.transaction_cost') }}</th>
                    <!--th>Total Investasi</th-->
                    <th>Status</th>
                </tr>
            </thead>
            <tbody>
                @foreach($model as $row)

                    @if($row->type == 'sa')
                        @php
                          $modelTransaction = $transactionAutoDebet->getFind($row->tr_date,$userId,$row->fund_cd,$row->recur_date);
                        @endphp
                        @include("checkout.thank.pembelian_auto_debet")
                    @elseif($row->type == 'sw')
                        @php
                          $modelTransaction = $transactionSwt->where('ref_no',$row->ref_no)->first();
                        @endphp
                        @include("checkout.thank.pengalihan")

                    @else
                        @php
                            $modelTransaction = $transaction->where('ref_no',$row->ref_no)->first();
                        @endphp

                        @if($row->type == 's')
                            @include("checkout.thank.pembelian")
                        @else
                            @include("checkout.thank.penjualan")
                        @endif
                    @endif

                @endforeach
            </tbody>
        </table>
        <div class="box-link-terms a-center">
            <a href="{{ urlLang('user/dashboard') }}" class="btn-oval btn-orange">Go to Dashboard</a>
            <!--a href="" class="btn-oval btn-trans-blue">CANCEL</a>
            <a href="" class="btn-oval btn-orange">CONFIRM</a-->
        </div>
    </div>
</section>
<!-- end of middle -->
@endsection
