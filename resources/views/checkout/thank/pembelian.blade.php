@php
$cekPersent = $fund->percentageOrAmount($row->fund,'fee_subs');
$persent = Mirae::percentageOrAmount($row->fee) == 1 ? "(".$row->fee."%)" : "";
@endphp
<tr>
    <td>
    	{{ carbon()->parse($row->tr_date)->format("d-F-Y") }}
    	<br>
    	{{ carbon()->parse($row->time)->format("H:i:s") }} WIB
    </td>
    <td>{{ $row->fund->fund_name }}</td>
    <td>Pembelian Sekali</td>
    <?php /*
        <td>{{ $transaction->source($row->source) }}</td>
    */ ?>
    <td>-</td>
    <td>IDR {{ formatUang($row->bill) }}</td>
    <td>IDR {{ formatUang($row->total_fee) }} {{$persent}}</td>
    <?php /*<td>IDR {{ formatUang($row->bill - $row->total_fee) }}</td> */ ?>
    <td><a style="cursor:text;">{{ $transaction->status(@$modelTransaction->proc_cd) }}</a></td>
</tr>
