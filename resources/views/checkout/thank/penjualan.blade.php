@php
$billOrUnit = function($bill,$unit){

    $result = "";

    if(!empty($bill))
    {
        $result = 'bill';
    }

    if(!empty($unit))
    {
        $result = 'unit';
    }

    return $result;
};

$nav = $dailyNav->nabLastByDate($row->fund_cd,$row->tr_date);
$persent = "";
$totalInvestasi = 0;

if($billOrUnit($row->bill,$row->unit) == 'bill')
{
    $bill = $row->bill;
}else{
    $cekFee = Mirae::percentageOrAmount($row->fee);
    if($cekFee == 1)
    {
        $persent = "(".$row->fee."%)";
        $bill = $row->unit * $nav;
    }else{
        $bill = $row->unit * $row->fee;
    }

}

$totalInvestasi = $bill - $row->total_fee;

@endphp

<tr>
    <td>
    	{{ carbon()->parse($row->tr_date)->format("d-F-Y") }}
    	<br>
    	{{ carbon()->parse($row->time)->format("H:i:s") }} WIB
    </td>
    <td>{{ $row->fund->fund_name }}</td>
    <td>Redemption</td>
    <?php /*
        <td>{{ $transaction->source($row->source) }}</td>
    */
    ?>
    <td>{{ formatUang($row->unit) }}</td>
    <td>IDR {{ formatUang($bill) }}</td>
    <td>IDR {{ formatUang($row->total_fee) }} {{$persent}}</td>
    <?php /*<td>IDR {{ formatUang($totalInvestasi) }}</td> */ ?>
    <td><a style="cursor:text;">{{ $transaction->status(@$modelTransaction->proc_cd) }}</a></td>
</tr>
