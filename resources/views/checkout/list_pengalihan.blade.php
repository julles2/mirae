<div class="title-content after_clear">
                <h3><i class="icwp ic_bag"></i> Pengalihan</h3>
            </div>
            <table class="table-radius table-pembelian">
                <thead>
                    <tr>
                        <th class="a-left">{{ ucwords(trans('global.switching')) }} {{ ucwords(trans('global.from')) }}</th>
                        <th class="a-left">{{ ucwords(trans('global.switching')) }} {{ ucwords(trans('global.to')) }}</th>
                        <th>Biaya</th>
                        <th class="a-right net-investment">{{ ucwords(trans('global.total_investment')) }}</th>
                        <th> &nbsp; </th>
                        <th> &nbsp; </th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $no = 0;
                    $temp = injectModel('TransactionTemporary');
                    foreach($carts as $cart)
                    {
                        $cartId = substr($cart->id,2,10);
                        $modelFund = $fund($cartId);
                        if($cart->options->type == 'switch')
                        {
                            $no++;
                            $toFund = $fund($cart->options->to,'fund_cd');
                            $fromP = @$modelFund->portofolio;
                            $toP = @$toFund->portofolio;
                            @$transactionToday = $temp->transactionTodaySwc($modelFund->fund_cd,$toFund->fund_cd,['sw']);
                    ?>
                        <input type = "hidden" name = "from_fund[]" value = "{{ $modelFund->fund_cd }}" />
                        <input type = "hidden" name = "to_fund[]" value = "{{ $toFund->fund_cd }}" />
                        <tr class="{{ $transactionToday->exist == 'true' ? 'highlight' : '' }}">
                            <td class="a-left">
                                <label>{{ $modelFund->fund_name }}</label>
                                <div class="text-display">
                                    <span><small>{{ $modelFund->im->im_nm }}</small></span>
                                </div>
                                <?php /*
                                <p>
                                    Nilai Investasi Semula : IDR {{ formatUang(@$fromP->nilai_investasi_semula) }}<br>
                                    Perkiraan Nilai Investasi Terakhir : IDR {{ formatUang(@$fromP->nilai_investasi_terakhir) }}<br/>
                                    NAB/UNIT Terakhir : IDR {{ formatUang(@$fromP->last_nav) }}<br/>
                                    Jenis : {{ $modelFund->result_type }}<br>
                                </p>
                                */ ?>
                                <a href="{{ urlLang('checkout/delete-cart/'.$cart->rowId) }}" onclick = "return confirm('are you sure want to delete this data?')" class="link link_line_orange">Delete</a>
                            </td>
                             <td class="a-left">
                                <label>{{ $toFund->fund_name }}</label>
                                <div class="text-display">
                                    <span><small>{{ $toFund->im->im_nm }}</small></span>
                                </div>
                                <?php /*
                                <p>
                                    Perkiraan Jumlah yang di alihkan : IDR ASK<br>
                                    NAB/UNIT Terakhir : IDR {{ formatUang($toP->last_nav) }}<br/>
                                    Jenis : {{ $toFund->result_type }}<br>
                                </p>
                                */ ?>
                            </td>
                            <td>{{ $modelFund->result_fee_swc }} %</td>
                            <td class="a-right net-investment" colspan = "3">
                                <div class="desc-list row_btm">
                                    <?php
                                    /*
                                        <div class="row no-border">
                                            <div class="left">Masukan Nilai</div>
                                            <div class="right">
                                                <label class="radio-btn"><input type="radio" {{ $checked($cart->options->order_type,'nominal') }} value = "nominal"  name="order_type{{ $no }}">Nominal</label>
                                                <label class="radio-btn"><input type="radio" {{ $checked($cart->options->order_type,'unit') }} value = "unit"  name="order_type{{ $no }}">Unit</label>
                                            </div>
                                        </div>
                                    */
                                    ?>
                                    <input type = "hidden" name = "order_type{{$no}}" value="unit" />
                                    <div class="row no-border">
                                        <div class="left">Unit</div>
                                        {!! Form::text("pengalihan_nilai[]",formatUang($cart->price),['fundid'=>$modelFund->fund_cd,'id'=>"pengalihan_unit$no",'class'=>'a-righ','readonly'=>true,'style'=>'text-align:right;']) !!}
                                        <span style="color:red;font-size:10px;" class="pengalihan_error" id = "pengalihan_error{{$no}}">asdf</span>
                                    </div>
                                    <div class="row no-border">
                                        <div class="left">Amount</div>
                                        {!! Form::text("pengalihan_amount[]",$cart->options->amount,['class'=>'a-righ','id'=>"pengalihan_amount$no",'readonly'=>true,'style'=>'text-align:right;']) !!}
                                    </div>
                                    @if($cart->options->all_unit == true)
                                        @push('scripts_bottom')
                                            <script type="text/javascript">
                                               pengalihanChangeToall('{{$no}}','{{$modelFund->fund_cd}}');
                                            </script>
                                        @endpush
                                    @endif
                                    <?php /*
                                    <div class="row no-border">
                                        <label class="radio-btn"><input onclick = "return pengalihanChangeToall('{{$no}}','{{$modelFund->fund_cd}}')" type="checkbox" {{ $checked($cart->options->all_unit,'all') }} value = "all" id = "pengalihan_box_all{{$no}}" name="pengalihan_box_all[]">Semua Unit</label>
                                    </div>
                                    */
                                    ?>
                                </div>
                            </td>
                        </tr>
                        @if($transactionToday->exist == 'true')
                            <tr class="note-display">
                                <td colspan="6">
                                {{ trans('global.notif_double_swc1') }} <b>{{ $transactionToday->ref_no }}</b>
                                {{ trans('global.notif_double_swc2') }} <b>{{ $transactionToday->tr_date }} {{ $transactionToday->tr_time }}</b>.
                                {{ trans('global.notif_double_swc3') }}
                                </td>
                            </tr>
                        @endif
                    <?php
                        }
                    }
                    ?>
                </tbody>
                <?php /*
                <tfoot>
                    <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td colspan="3"><b>TOTAL PEMBAYARAN</b></td>
                        <td class="a-right"><b>{{ formatUang(Mirae::cartSubtotalByType('switch')) }}</b></td>
                    </tr>
                </tfoot>
                */ ?>
            </table>
            <br>
            <br>
            <br>
