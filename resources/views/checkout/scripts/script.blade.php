@push('scripts')

<script type="text/javascript">

    $(document).ready(function(){
        $(".penjualan_error").hide();
        $(".pengalihan_error").hide();
        $("#confirm").on('click',function(){
            showPop('loaderpopup');
            pembelian = checkPembelian();
            penjualan = checkPenjualan();
            pengalihan = checkPenjualan();
            if(pembelian == 'true' && penjualan == 'true' && pengalihan == 'true')
            {
                //alert(pembelian + " " + "penjualan" + " " + pengalihan);
                $("#form_checkout").submit();
            }else{
                swal_error("{{ trans('notif.checkout_validation') }}");
                closePop('loaderpopup');
            }
        });

    });

    function checkPengalihan()
    {
        $no=0;
        $error = 0;
        $.each($("input[id^=pengalihan_unit]"),function(key,val){
            $no++;
            id = val.id;
            fundcd = $("#"+id).attr('fundid');
            unit = $("#"+id).val();

            $.ajax({
                url : '{{ urlLang("user/portfolio/ajax-validate-pengalihan") }}',
                data : {
                    'fund_cd': fundcd,
                    'unit': unit,
                },
                success: function(respon){
                    result = respon.result;

                    if(result == 'saldo_kurang')
                    {
                        $("#pengalihan_error" + $no).html("{{ trans('global.sorry_the_units') }}").show();
                        $error = $error + 1;
                    }else if(result == 'false'){
                        $("#pengalihan_error" + $no).html("{{ trans('global.your_unit_exceeds') }}").show();

                        $error = $error + 1;
                    }else{
                        $("#pengalihan_error" + $no).hide();
                    }
                },
                async: false,
            });
        });

        if($error > 0)
        {
            return 'false';
        }else{
            return 'true';
        }
    }

    function checkPenjualan()
    {
        $no=0;
        $error = 0;
        $.each($("input[id^=penjualan_unit]"),function(key,val){
            $no++;
            id = val.id;
            fundcd = $("#"+id).attr('fundid');
            unit = $("#"+id).val();

            $.ajax({
                url : '{{ urlLang("user/portfolio/ajax-validate-penjualan") }}',
                data : {
                    'fund_cd': fundcd,
                    'unit': unit,
                },
                success: function(respon){
                    result = respon.result;

                    if(result == 'saldo_kurang')
                    {
                        $("#penjualan_error" + $no).html("{{ trans('global.sorry_the_units') }}").show();
                        $error = $error + 1;
                    }else if(result == 'false'){
                        $("#penjualan_error" + $no).html("{{ trans('global.your_unit_exceeds') }}").show();
                        $error = $error + 1;
                    // }else if($(this).val() == ""){
                        // $("#penjualan_error" + $no).html("unit tidak boleh kosong").show();
                        // $error = $error + 1;
                    }else{
                        $("#penjualan_error" + $no).hide();
                    }
                },
                async: false,
            });
        });

        if($error > 0)
        {
            return 'false';
        }else{
            return 'true';
        }
    }

    function checkPembelian()
    {
        $arr = [];
        $error = 0;
        totalNilai = 0;
        result = 'true';
        counter = 0;
        $.each($("input[id^=nilai]") , function(key,val){
                counter =  counter + 1;
                id = val.id;
                fundId = $("#"+id).attr('fundid');
                nilai = $("#"+id).val();
                totalNilai = totalNilai + numeral(nilai).value();
                $.ajax({
                    url : '{{ urlLang("checkout/validate-pembelian") }}',
                    type : 'get',
                    data : {
                        counter_nilai : id,
                        fund_id : fundId,
                        nilai : nilai,
                        _token : '{{ csrf_token() }}',
                    },
                    success : function(respon){
                        if(respon.result == 'false')
                        {
                            $("#error"+respon.counter_nilai).show();
                            $error = $error + 1;
                        }else{
                            $risk = $("input[name='risk_profile"+fundId+"']").val();
                            $risk_checked = $("input[name='risk_profile"+fundId+"']:checked").val();
                            
                            if($risk)
                            {
                                if(!$risk_checked)
                                {
                                    $("#errorpilihan"+counter).show();
                                    $error = $error + 1;
                                }
                            }else{
                                $("#error"+respon.counter_nilai).hide();
                            }
                        }
                    },
                    async: false,
                });

        });
        if($error <= 0)
        {
            $.ajax({
                url: '{{ urlLang("checkout/cek-cash-balance") }}',
                data : {
                    total_nilai : totalNilai,
                    user_id : '{{ $userId }}',
                },
                success : function(respon){
                    if(respon.result == 'true')
                    {
                        result = 'true';
                    }else{
                        closePop('loaderpopup');
                        result = 'false';
                        swal('{{ trans("global.the_balance_is_not_sufficient") }}');
                    }
                },
                async: false,
            });

            //$("#form_checkout").submit();
        }else{
            result = 'false';
        }

        return result;
    }

    function countMoneyPembelian(myself)
    {
        uangField(myself);

        $total = 0;
        $.each($("input[id^=nilai]"),function(key,val){
            $id = val.id;
            $result = numeral($("#"+$id).val());
            $total = $total + $result.value();
        });
        $total = numeral($total).format('0,0');
        $("#total_pembayaran_beli").html($total);

    }

    function changeToAmount(unit,no,fundCd)
    {
        $.ajax({
            url : '{{ urlLang("user/portfolio/ajax-change-to-amount") }}',
            data : {
                unit : unit,
                fundCd : fundCd,
            },
            success : function(respon){
                $("#penjualan_amount"+no).val(respon.result);
            },
        });
    }


    function changeToUnit(amount,no,fundCd)
    {
        $amount = numeral(amount);
        $amountValue = ($amount.value());
        $.ajax({
            url : '{{ urlLang("user/portfolio/ajax-change-to-unit") }}',
            data : {
                amount : $amountValue,
                fundCd : fundCd,
            },
            success : function(respon){
                $("#penjualan_unit"+no).val(respon.result);
            },
        });
    }


    function testingna()
    {
        alert('s');
    }

    function changeToall(no,fundCd)
    {
        $box_all = $("#penjualan_box_all" + no);
        $penjualan_unit = $("#penjualan_unit" + no);
        $penjualan_amount = $("#penjualan_amount" + no);
        if($box_all.is(":checked"))
        {
            $.ajax({
                url : '{{ urlLang("user/portfolio/ajax-change-to-all") }}',
                data : {
                    fundCd : fundCd,
                },
                success : function(respon){
                    $penjualan_unit.val(respon.unit);
                    $penjualan_amount.val(respon.amount);
                    $penjualan_unit.attr('readonly','readonly');
                    $penjualan_amount.attr('readonly','readonly');

                    $penjualan_unit.css('background-color','#ccc');
                    $penjualan_amount.css('background-color','#ccc');
                },
                sync : false,
            });
        }else{
            $penjualan_unit.removeAttr('readonly');
            $penjualan_amount.removeAttr('readonly');
            $penjualan_unit.css('background-color','white');
            $penjualan_amount.css('background-color','white');
        }


    }


    function pengalihanChangeToAmount(unit,no,fundCd)
    {
        $.ajax({
            url : '{{ urlLang("user/portfolio/ajax-change-to-amount") }}',
            data : {
                unit : unit,
                fundCd : fundCd,
            },
            success : function(respon){
                $("#pengalihan_amount"+no).val(respon.result);
            },
        });
    }


    function pengalihanChangeToUnit(amount,no,fundCd)
    {
        $amount = numeral(amount);
        $amountValue = ($amount.value());
        $.ajax({
            url : '{{ urlLang("user/portfolio/ajax-change-to-unit") }}',
            data : {
                amount : $amountValue,
                fundCd : fundCd,
            },
            success : function(respon){
                $("#pengalihan_unit"+no).val(respon.result);
            },
        });
    }


    function pengalihanChangeToall(no,fundCd)
    {
        $box_all = $("#pengalihan_box_all" + no);
        $pengalihan_unit = $("#pengalihan_unit" + no);
        $pengalihan_amount = $("#pengalihan_amount" + no);
        if($box_all.is(":checked"))
        {
            $.ajax({
                url : '{{ urlLang("user/portfolio/ajax-change-to-all") }}',
                data : {
                    fundCd : fundCd,
                },
                success : function(respon){
                    $pengalihan_unit.val(respon.unit);
                    $pengalihan_amount.val(respon.amount);
                    $pengalihan_unit.attr('readonly','readonly');
                    $pengalihan_amount.attr('readonly','readonly');

                    $pengalihan_unit.css('background-color','#ccc');
                    $pengalihan_amount.css('background-color','#ccc');
                },
                sync : false,
            });
        }else{
            $pengalihan_unit.removeAttr('disabled');
            $pengalihan_amount.removeAttr('disabled');
            $pengalihan_unit.css('background-color','white');
            $pengalihan_amount.css('background-color','white');
        }


    }


</script>

@endpush
