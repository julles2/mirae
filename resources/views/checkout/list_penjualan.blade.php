<div class="title-content after_clear">
                <h3><i class="icwp ic_bag"></i>{{ trans('global.redemption') }}</h3>
            </div>
            <table class="table-radius table-pembelian">
                <thead>
                    <tr>
                        <th class="a-left">{{ ucwords(trans('product.mutualfundname')) }}</th>
                        <th>{{ ucwords(trans('product.mutualfundtype')) }}</th>
                        <th class="a-right net-investment">{{ ucwords(trans('global.total_investment')) }}</th>
                        <th>&nbsp;</th>
                        <th>&nbsp;</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $no = 0;
                    $temp = injectModel('TransactionTemporary');
                    foreach($carts as $cart)
                    {
                    $cartId = substr($cart->id,1,10);
                    $modelFund = $fund($cartId);
                    $portfolio = @$modelFund->portofolio;
                        if($cart->options->type == 'redeem')
                        {
                            $no++;
                            @$transactionToday = $temp->transactionToday($modelFund->fund_cd,['r']);
                    ?>
                        <input type = "hidden" name = "penjualan_fund_cd[]" value = "{{ $modelFund->fund_cd }}" />
                        <tr class="{{ $transactionToday->exist == 'true' ? 'highlight' : '' }}">
                            <td class="a-left">
                                <label>{{ $modelFund->fund_name }}</label>
                                <div class="text-display">
                                    <span><small>{{ $modelFund->im->im_nm }}</small></span>
                                </div>
                                <?php /*
                                <p>
                                    Nilai Investasi Semula : IDR {{ formatUang(@$portfolio->nilai_investasi_semula) }}<br>
                                    Return : <i class="icwp ic_{{ @$portfolio->minus_or_plus }}"></i> {{ formatUang(@$portfolio->return) }}<br>
                                    NAB/UNIT Terakhir : IDR {{ formatUang(@$portfolio->last_nav) }}<br/>
                                    Jumlah Unit : {{ formatUang($portfolio->unit) }}<br/>
                                    Perkiraan Nilai Investasi Terakhir : IDR {{ formatUang(@$portfolio->nilai_investasi_terakhir) }}<br/>
                                    Gain/Loss : {{ formatUang(@$portfolio->gain_loss) }}<br/>
                                </p>
                                */ ?>
                                <a href="{{ urlLang('checkout/delete-cart/'.$cart->rowId) }}" onclick = "return confirm('are you sure want to delete this data?')" class="link link_line_orange">Delete</a>
                            </td>
                            <td>{{ $model->labelType($modelFund->fund_cd,$modelFund->fund_type) }}</td>
                            <td class="a-right net-investment" colspan="3">
                                <div class="desc-list row_btm">
                                    <?php
                                    /*
                                        <div class="row no-border">
                                            <div class="left">Masukan Nilai</div>
                                            <div class="right">
                                                <label class="radio-btn"><input type="radio" {{ $checked($cart->options->order_type,'nominal') }} value = "nominal"  name="order_type{{ $no }}">Nominal</label>
                                                <label class="radio-btn"><input type="radio" {{ $checked($cart->options->order_type,'unit') }} value = "unit"  name="order_type{{ $no }}">Unit</label>
                                            </div>
                                        </div>
                                    */
                                    ?>
                                   <input type = "hidden" name = "order_type{{$no}}" value="unit" />
                                    <div class="row no-border">
                                        <div class="left">{{ trans('global.total_redemption_unit') }}</div>
                                        {!! Form::text("penjualan_nilai[]",$cart->price,['fundid'=>$modelFund->fund_cd,'id'=>"penjualan_unit$no",'class'=>'a-righ','readonly'=>true,'style'=>'text-align:right;']) !!}
                                        <span style="color:red;font-size:10px;" class="penjualan_error" id = "penjualan_error{{$no}}">asdf</span>
                                    </div>
                                    <div class="row no-border">
                                        <div class="left">Nominal Redemption</div>
                                        {!! Form::text("penjualan_amount[]",$cart->options->amount,['readonly'=>true,'class'=>'a-righ','id'=>"penjualan_amount$no",'style'=>'text-align:right;']) !!}
                                         @if(lang() == 'id')   
                                            <small style="color:red;">Perkiraan Nominal hanya sebagai gambaran berdasarkan NAB/Unit {{ @$portfolio->date_last_nav }}.
                                                Nominal reksa dana yang Anda dapat akan dihitung kemudian berdasarkan NAB/Unit yang ditentukan oleh Bank Kustodian.
                                            </small>
                                        @else
                                            <small style="color:red;">The estimate nominal is only an illustration as a result of NAV / Units per {{ @$portfolio->date_last_nav }}.
                                                The nominal value of your mutual fund will be calculated later, based on the NAV / Units which determined by the Custodian Bank
                                            </small>
                                        @endif

                                    </div>

                                    {{-- @if($cart->options->all_unit == true)
                                        @push('scripts_bottom')
                                            <script type="text/javascript">
                                               changeToall('{{$no}}','{{$modelFund->fund_cd}}');
                                            </script>
                                        @endpush
                                    @endif --}}
                                    <input type="hidden" name = "penjualan_box_all[]" value = "{{ $cart->options->all_unit }}">
                                    <?php
                                    /*
                                    <div class="row no-border">
                                        <label class="radio-btn"><input onclick = "return changeToall('{{$no}}','{{$modelFund->fund_cd}}')" type="checkbox" {{ $checked($cart->options->all_unit,true) }} value = "all" id = "penjualan_box_all{{$no}}" name="penjualan_box_all[]">Semua Unit</label>
                                    </div>
                                    */?>
                                </div>
                            </td>
                        </tr>
                        @if($transactionToday->exist == 'true')
                            <tr class="note-display">
                                <td colspan="5">
                                {{ trans('global.notif_double_redeem1') }} <b>{{ $transactionToday->ref_no }}</b>
                                {{ trans('global.notif_double_redeem2') }} <b>{{ $transactionToday->tr_date }} {{ $transactionToday->tr_time }}</b>.
                                {{ trans('global.notif_double_redeem3') }}
                                </td>
                            </tr>
                        @endif
                    <?php
                        }
                    }
                    ?>
                </tbody>
                <tfoot>
                    <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td colspan="2"><b>{{ trans('global.total_redemption') }}</b></td>
                        <?php
                        /*
                            <td class="a-right"><b>{{ formatUang(Mirae::cartSubtotalByType('redeem')) }}</b></td>
                        */ ?>

                        <td class="a-right"><b>{{ formatUang(Mirae::cartSubtotalByType('redeem')) }}</b></td>
                    </tr>
                </tfoot>
            </table>
            <br>
            <br>
            <br>
