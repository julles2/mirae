@extends('layouts.layout')
@section('content')
<section class="banner">
    <img src="{{ assetFrontend(null) }}images/content/banner-dashboard.jpg" alt="banner" />
    <div class="caption">
        <h6>{{ trans('checkout.transaction') }}</h6>
        <h1>Checkout</h1>
        <p>&nbsp;</p>
    </div>
</section>
<section class="std-content">
    <div class="wrapper myaccount">
        <div class="title-bg bg-darkblue">
            <div class="left">
                <h3><i class="icwp ic_money_white"></i> My Cash</h3>
            </div>
            <div class="right">
                <h3>IDR {{ formatUang($balance) }}</h3>
            </div>
        </div>

        {!! Form::open(['id'=>'form_checkout']) !!}
            @if(Mirae::cartCountByType('subscription') > 0 || Mirae::cartCountByType('subscription_auto_debet') > 0)
                @include("checkout.list_pembelian")
            @endif
            @if(Mirae::cartCountByType('redeem') > 0)
                @include("checkout.list_penjualan")
            @endif

            @if(Mirae::cartCountByType('switch') > 0)
                @include("checkout.list_pengalihan")
            @endif

            <div class="box-link-terms">
                <div class="left">
                    {{ trans('checkout.dalam_rupiah') }}<br>
                    <a href = "{{ urlLang('terms-conditions') }}" target="_blank">{{ trans('general.term') }}</a>
                </div>
                @if(Cart::count() > 0)
                <div class="right">
                    <a href="{{ urlLang('checkout/cancel') }}" onclick = "return confirm('anda yakin untuk membatalkan semua transaksi ?')" class="btn-oval btn-trans-blue">CANCEL</a>
                    <button id = "confirm"  type = "button" class="btn-oval btn-orange">CONFIRM</button>
                </div>
                @else

                @endif
            </div>
        {!! Form::close() !!}

        <div class="row no-border">
            <div class="disclaimer-box rounded">
                <h4>{{ trans('disclaimer.title_transaksi') }}</h4>
                <p>
                    {!! trans('disclaimer.transaksi') !!}
                </p>
            </div>
        </div>
    </div>
</section>
@endsection
@include("checkout.scripts.script")
