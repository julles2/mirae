<div class="title-content after_clear">
                <h3><i class="icwp ic_bag"></i> {{ trans('product.purchase') }}</h3>
            </div>
            <table class="table-radius table-pembelian">
                <thead>
                    <tr>
                        <th class="a-left">{{ ucwords(trans('product.mutualfundname')) }}</th>
                        <th>{{ ucwords(trans('product.mutualfundtype')) }}</th>
                        <th>{{ ucwords(trans('product.latest_nav')) }}</th>
                        <th>Sales</th>
                        <th class="a-right net-investment">{{ trans('product.net_investment') }}</th>
                    </tr>
                </thead>
                <tbody>
                	<?php
                	$tr = 0;
                    $temp = injectModel('TransactionTemporary');
                    foreach($carts as $cart)
                    {
                    $tr++;
                    $cartId = substr($cart->id,1,10);
                    $modelFund = $fund($cartId);
                    @$transactionToday = $temp->transactionToday($modelFund->fund_cd,['s','sa']);
                    ?>
                    	@if($cart->options->type == 'subscription' || $cart->options->type == 'subscription_auto_debet')
                    			<input type = "hidden" name = "fund_cd[]" value = "{{ $modelFund->fund_cd }}" />
                    			<input type = "hidden" name = "type[]" value = "{{ $cart->options->type }}" />
                                <tr class="{{ $transactionToday->exist == 'true' ? 'highlight' : '' }}">
		                            <td class="a-left">
		                                <label>{{ $modelFund->fund_name }}</label>
                                        <div class="text-display">
                                            <span><small>{{ $modelFund->im->im_nm }}</small></span>
                                        </div>
                                        @if($modelFund->fund_orcl->fund_risk > $user->risk_profile)
                                            <div class="text-display" style="font-size:12px;">
                                                {{ trans('checkout.risk_notes') }}
                                                <label class="radio-btn" >
                                                    <input type="radio" value="{{ $modelFund->id }}" name="risk_profile{{$modelFund->id}}">{{ trans('checkout.yes') }}
                                                </label>
                                                <label class="radio-btn">
                                                    <input type="radio" name="risk_profile{{$modelFund->id}}" id = "risk_profile{{$modelFund->id}}" onclick = "risk_profile_no('{{$modelFund->id}}','{{ $cart->rowId }}')">{{ trans('checkout.no') }}
                                                </label>
                                                <span style="color:red;display:none;" id = "errorpilihan{{ $tr }}">Pilihan tidak boleh kosong</span>
                                        
                                                
                                            </div>
                                        @endif
                                            <?php /*
                                                <div class="text-display">
                                                    <span>Min. Pembelian :</span>
                                                    <span>IDR {{ formatUang($modelFund->min_investment) }}</span>
                                                </div>
                                                <div class="text-display">
                                                    <span>Selanjutnya :</span>
                                                    <span>IDR {{ formatUang($modelFund->min_increment) }}</span>
                                                </div>
                                                <div class="text-display">
                                                    <span>Biaya Transaksi :</span>
                                                    <span>{{ $modelFund->fee_subs }} %</span>
                                                </div>
                                                <div class="text-display">
                                                    <span>Manager Investasi :</span>
                                                    <span>{{ $modelFund->im->im_nm }}</span>
                                                </div>
                                            */ ?>
		                                <a href="{{ urlLang('checkout/delete-cart/'.$cart->rowId) }}" onclick = "return confirm('are you sure want to delete this data?')" class="link link_line_orange">Delete</a>
		                            </td>
		                            <td>{{ $model->labelType($modelFund->fund_cd,$modelFund->fund_type) }}</td>
		                            <td>{{ Mirae::roundSubstr($model->nabLast($modelFund->id),4) }}</td>
		                            <td>{{ $model->labelType($modelFund->fund_cd,$modelFund->sales) }}</td>
		                            <td class="a-right net-investment">
		                                <div class="row">
		                                    {!! Form::text("nilai[]",formatUang($cart->price),['fundid'=>$modelFund->id,'id'=>"nilai$tr",'class'=>'a-righ','readonly'=>true,'style'=>'text-align:right;']) !!}

                                            <span style="color:red;display:none;" id = "errornilai{{ $tr }}">Uang Kurang</span>
		                                </div>
		                                <div class="row a-right">
		                                    <div class="inlineopt btn-periode">
		                                        <label class="radio-btn"><input type="radio" name="rpem{{ $tr }}" {{ $checked($cart->options->type,'subscription') }} data-toggle="sekali" />{{ trans('product.purchaseonce') }}</label>
		                                        <label class="radio-btn"><input type="radio" name="rpem{{ $tr }}" {{ $checked($cart->options->type,'subscription_auto_debet') }}  data-toggle="berkala" />{{ trans('product.periodicalBuy') }}</label>
		                                    </div>
		                                </div>
		                                <div class="box-periode-berkala" style="display: {{ $cart->options->type == 'subscription_auto_debet' ? 'inherit' : 'none' }};">
		                                    <div class="row">
		                                        <div class="left">
		                                            <label>Setiap Tanggal </label>
		                                            <input type="text" readonly="true" value = "{{ $cart->options->tanggal }}" name="tanggal[]">
		                                        </div>
		                                        <div class="right">
		                                        	<label>Periode</label>
		                                        	{!! Form::text("tahun[]",$cart->options->tahun,['readonly']) !!} (Tahun)
		                                        </div>
		                                    </div>
		                                </div>
		                            </td>
		                        </tr>
                                @if($transactionToday->exist == 'true')
                                    <tr class="note-display">
                                        <td colspan="5">
                                        {{ trans('product.today-you-have-done-purchase-instruction-with-the-same-mutual-fund-product,-with-transaction-number') }} <b>{{ $transactionToday->ref_no }}</b>
                                        {{ trans('product.at-the-date-of') }} <b>{{ $transactionToday->tr_date }} {{ $transactionToday->tr_time }}</b>.
                                        {{ trans('product.are-you-still-going-to-make-a-purchase?') }}
                                        </td>
                                    </tr>
                                @endif
		                @endif
                    <?php
                    }
                    ?>
                </tbody>
                <tfoot>
                    <tr>
                        <!--td>&nbsp;</td-->
                        <td>&nbsp;</td>
                        <td colspan="3"><b>{{ trans('product.total-payment') }}</b></td>
                        <td class="a-right"><b id = "total_pembayaran_beli">{{ formatUang(Mirae::cartSubtotalByType('subscription') + Mirae::cartSubtotalByType('subscription_auto_debet')) }}</b></td>
                    </tr>
                </tfoot>
            </table>
            <br>
            <br>
            <br>


@push('scripts')
<script type="text/javascript">
    function risk_profile_no($fundid,$rowId)
    {
        tanya = confirm("Anda yakin untuk tidak membeli produk ini ? , jika ya produk akan di hapus dari cart anda");
        if(tanya)
        {
            document.location.href="{{ urlLang('checkout/delete-cart') }}/" + $rowId;
        }else{
            document.getElementById('risk_profile'+$fundid).checked = false;
           
        }
    }
</script>
@endpush