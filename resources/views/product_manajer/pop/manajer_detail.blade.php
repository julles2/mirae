
<div class="wrap-popup product-detail" id="detail-Minvest" >
    <div class="head"> 
        <h5>Informasi Manajer Investasi</h5>
        <a href="javascript:closePop('detail-Minvest')" class="close">close</a>
    </div>
    <div class="content wrap-tab">

        <div class="nav-tab">
            <a href="#pd-info" >General Info </a>
            <a href="#pd-profile">Company Profile</a>
            <a href="#pd-perform" >Performance</a>
            <a href="#pd-fund" class="active">Fund List</a>
        </div>
        <div class="content-tab inner manajer-invest" id="pd-info">
            <div class="title-content after_clear">
                <h3 class="left">General Info</h3>
                <div class="logo-prod"> 
                    <div class="ico">
                        <img src="{{ assetFrontend(null) }}images/content/hprod-2.jpg" alt="produk logo" />
                    </div>
                    <div class="text">
                        <h6>Asanusa Asset Management, PT</h6>
                    </div>
                </div>
            </div>
            <div class="general-info">
                <div class="col-2">
                    <div class="box">
                        <div class="column">
                            <div class="box">
                                <h3>Asanusa Asset Management, PT</h3>
                                <p>Sebelumnya - AAA Asset Management PT Pemisahan Kegiatan Usaha sebagai Manajer Investasi dari PT AAA Sekuritas</p>
                                <span  class="code_mi">Kode MI	<b>BJ002</b></span>
                            </div>
                        </div>
                        <div class="column col-2">
                            <div class="box">
                                <label>Latest AUM  <span>(per : 28-Feb-2017)</span></label>
                                <h6>641.710.505.372 IDR</h6>
                            </div>
                            <div class="box">
                                <label>Pengelolaan</label>
                                <h6>Kelola Reksa Dana</h6>
                            </div>
                        </div>
                        <div class="column col-2">
                            <div class="box">
                                <label>Modal Dasar</label>
                                <h6>100,000,000,000 IDR</h6>
                            </div>
                            <div class="box child col-2">
                                <div class="box">
                                    <label>Kepemilikan</label>
                                    <h6>Nasional</h6>
                                </div>
                                <div class="box">
                                    <label>Status</label>
                                    <h6>Aktif</h6>
                                </div>
                            </div>
                        </div>
                        <div class="column col-2">
                            <div class="box">
                                <label>Modal Disetor</label>
                                <h6>100,000,000,000 IDR</h6>
                            </div>
                            <div class="box">
                                <label>Izin Usaha</label>
                                <h6>MI : KEP-08/BL/MI/2012 (2012-10-29)</h6>
                            </div>
                        </div>
                    </div>
                    <div class="box">
                        <div class="column">
                            <div class="box">
                                <label>STAKEHOLDER</label>
                                <div class="stkholder">
                                    <table>
                                        <thead>
                                            <tr>
                                                <th>Nama</th>
                                                <th>Kepemilikan(IDR)</th>
                                                <th>%</th>                                                
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>PT Asanusa Tasco Investama</td>
                                                <td>29,700,000,000</td>
                                                <td>99%</td>                                                
                                            </tr>
                                            <tr>
                                                <td>RD. Zamzam Reza</td>
                                                <td> 300,000,000</td>
                                                <td>1%</td>                                                
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="column col-2">
                            <div class="box">
                                <label>BOARD OF DIRECTORS</label>
                                <h6 class="board">Siswa Rizali <i>- Direktur Utama</i></h6>
                                <h6 class="board">Arke Nurdjatni Markis Tinasari <i>- Direktur</i></h6>
                            </div>
                            <div class="box">
                                <label>COMMISSIONERs</label>
                                <h6 class="board">Zainal Abidinsyah Siregar <i>- Komisaris Utama</i></h6>
                                <h6 class="board">Gahet L. Ascobat <i>- Komisaris</i></h6>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-tab inner info-prod" id="pd-profile">
            <div class="title-content after_clear">
                <h3 class="left">Company Profile</h3>
                <div class="logo-prod"> 
                    <div class="ico">
                        <img src="{{ assetFrontend(null) }}images/content/hprod-2.jpg" alt="produk logo" />
                    </div>
                    <div class="text">
                        <h6>Asanusa Asset Management, PT</h6>
                    </div>
                </div>
            </div>
            <div class="col-2 wrap-tab">
                <div class="left a-center">
                    <img src="{{ assetFrontend(null) }}images/content/logo-infoprod.jpg" alt="prod" />
                </div>
                <div class="right">  
                    <h4>Tentang Kami</h4>
                    <p>
                        PT Sucorinvest Asset Management merupakan anak perusahaan dari Sucorinvest Group yang merupakan perusahaan penyedia produk dan layanan sekuritas, manajemen aset serta brokerage yang telah beroperasi selama lebih dari 20 tahun. Jenis reksa dana yang dikelola mencakup reksa dana campuran, saham, pendapatan tetap serta penyertaan terbatas baik untuk nasabah individu maupun perusahaan. Kinerja Sucorinvest tercermin dari pencapaian Sucorinvest Flexi Fund yang ditetapkan majalah investor sebagai salah satu dari tiga besar untuk kategori Balanced Fund tahun 2010 dan 2011. Perusahaan memiliki ijin sebagai Manajer Investasi dari Bapepam-LK dengan No KEP-01/PM/MI/1999   tanggal 1 Juni 1999.
                    </p>

                    <h4>Visi</h4>
                    <p>
                        Menjadi perusahaan Manajer Investasi yang handal dan terpercaya sehingga dapat memberikan nilai tambah dalam jangka panjang bagi stakeholders
                    </p>
                    <h4>Misi</h4>
                    <p>
                        Menyediakan produk dan layanan investasi yang unik dan inovatif sesuai dengan kebutuhan investor
                        Memberikan tingkat pengembalian yang optimal dan berkesinambungan kepada para pemangku kepentingan
                        Filosofi Investasi
                        Berinvestasi pada bisnis yang kami pahami serta memposisikan diri sebagai business owner
                        Berinvestasi pada perusahaan yang memiliki pengalaman sukses jangka panjang, manajemen yang dapat dipercaya dan memiliki prospek pertumbuhan berkelanjutan
                        Risiko bisnis investee adalah faktor penting dalam mempertimbangkan keputusan investasi
                    </p>
                </div>
            </div>
        </div>
        <div class="content-tab inner info-prod" id="pd-perform">
            <div class="title-content after_clear">
                <h3 class="left">Performance</h3>
                <div class="logo-prod"> 
                    <div class="ico">
                        <img src="{{ assetFrontend(null) }}images/content/hprod-2.jpg" alt="produk logo" />
                    </div>
                    <div class="text">
                        <h6>Asanusa Asset Management, PT</h6>
                    </div>
                </div>
            </div>
            <div class="sec_perform">
                <div class="filter_top">
                    <form action="">
                        <div class="left">
                            <h6>Periode</h6>
                        </div>
                        <div class="right">
                            <div class="list_filt">
                                <label>From</label>
                                <select class="month">
                                    <option>Januari</option>
                                </select>
                                <select class="year">
                                    <option>2016</option>
                                </select>
                            </div>
                            <div class="list_filt">
                                <label>To</label>
                                <select class="month">
                                    <option>Desember</option>
                                </select>
                                <select class="year">
                                    <option>2016</option>
                                </select>
                            </div>
                            <div class="submit">
                                <input type="submit" value=" ">
                            </div>
                        </div>
                    </form>
                </div>
                <div class="chart">

                    <div  class="ket" style="display: none">
                        <span class="orange">Total AUM</span>
                        <span class="blue">Total UNIT</span>
                    </div>
                </div>
                <div class="chart_item" id="chart-1" style="min-width: 970px; height: 400px; margin: 0 auto">

                </div>
            </div>

        </div>
        <div class="content-tab inner info-prod" id="pd-fund">
            <div class="title-content after_clear">
                <h3 class="left">Info Produk</h3>
                <div class="logo-prod"> 
                    <div class="ico">
                        <img src="{{ assetFrontend(null) }}images/content/hprod-1.jpg" alt="produk logo" />
                    </div>
                    <div class="text">
                        <p>Sucorinvest Asset Management, PT</p>
                        <h6>Sucorinvest Maxi Fund</h6>
                    </div>
                </div>
            </div>
            <div class="list-product">  
                <?php
                for ($c = 0; $c < 2; $c++) {
                    for ($i = 1; $i <= 4; $i++) {
                        ?>
                        <div class="box">
                            <div class="desc">
                                <div class="ico">
                                    <img src="{{ assetFrontend(null) }}images/content/hprod-<?= $i; ?>.jpg" alt="prod" />
                                </div>
                                <div class="text">
                                    <h5>Simas Danamas Saham</h5>
                                    <p>NAB   <span>3.085,56</span><br/>
                                        As Of 11 April 2017
                                    </p>
                                </div>
                            </div>
                            <div class="return">
                                <p>Return (%)</p>
                            </div>
                            <div class="float-3">
                                <div>
                                    <p>1 Hr  <span>0,15</span></p>
                                </div>
                                <div>
                                    <p>1 Hr  <span>0,15</span></p>
                                </div>
                                <div>
                                    <p>1 Hr  <span>0,15</span></p>
                                </div>
                            </div>
                            <div class="btn">
                                <a href="">BELI</a>
                                <a href="">BANDINGKAN</a>
                            </div>
                        </div>
                        <?php
                    }
                }
                ?>
            </div>
        </div>
    </div>

</div>


<script src="//code.highcharts.com/highcharts.js"></script>
<script src="//code.highcharts.com/modules/exporting.js"></script>
<script>

    Highcharts.chart('chart-1', {
        colors: ['#f58220', '#2e76a2', '#8d4654', '#7798BF', '#aaeeee', '#ff0066', '#eeaaee',
            '#55BF3B', '#DF5353', '#7798BF', '#aaeeee'],
        chart: {
            type: 'column'
        },
        title: {
            text: ''
        },
        subtitle: {
            text: ''
        },
        xAxis: {
            categories: [
                'Jan 2016',
                'Feb 2016',
                'Mar 2016',
                'Apr 2016',
                'May 2016',
                'Jun 2016',
                'Jul 2016',
                'Aug 2016',
                'Sep 2016',
                'Oct 2016',
                'Nov 2016',
                'Dec 2016'
            ],
            crosshair: true
        },
        yAxis: {
            min: 0,
            title: {
                text: 'AUN (IDR)',
                style: {
                    color: '#f58220', fontSize: '12px', textTransform: 'uppercase', fontWeight: 'bold', letterSpacing: '0.5px'
                }
            },
            labels: {
                style: {
                    color: '#f58220', fontSize: '12px'
                }
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                    '<td style="padding:0"><b>{point.y:.1f} mm</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {
                pointPadding: 0.5,
                borderWidth: 0,
                pointWidth: 20
            }
        },
        series: [{
                name: 'Total AUM',
                data: [49.9, 71.5, 106.4, 129.2, 144.0, 176.0, 135.6, 148.5, 256.4, 194.1, 95.6, 54.4]

            }, {
                name: 'Total UNIT',
                data: [83.6, 78.8, 98.5, 93.4, 106.0, 84.5, 105.0, 104.3, 91.2, 83.5, 106.6, 92.3]

            }],

    });
</script>