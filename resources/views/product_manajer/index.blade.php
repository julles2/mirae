@extends('layouts.layout')
@section('content')
<!-- middle -->
<section class="banner">
    <img src="{{ assetFrontend(null) }}images/content/banner-produk.jpg" alt="banner" />
    <div class="caption">
        <h6>Produk</h6>
        <h1>Daftar Manajer<br/>
            Investasi</h1>
        <p>&nbsp;</p>
    </div>
</section>
<section class="std-content">
    <div class="wrapper">
        <div class="category">
            <div class="left">
                <a href="" class="btn-std"/>Bandingkan</a>
            </div>
            <div class="right">
                <form action="">
                    <input type="text" placeholder="Cari Manajer Investassi"/>
                    <input type="submit" value=""/>
                </form>
            </div>
        </div>
        <table class="product m_invest">
            <thead>
                <tr>
                    <th rowspan="2">#</th>
                    <th rowspan="2">
                        <label class="opt">
                            <input type="checkbox" />
                        </label>
                    </th>
                    <th rowspan="2">MANAJER INVESTASI</th>
                    <th rowspan="2">Periode</th>
                    <th colspan="2">latest aum</th>
                    <th colspan="2">latest unit</th>
                    <th rowspan="2"></th>
                </tr>
                <tr>
                    <th>IDR</th>
                    <th>USD</th>
                    <th>IDR</th>
                    <th>USD</th>
                </tr>
            </thead>
            <tbody id = "list-manager-body">
            </tbody>
        </table>

        <div class="table-nav">
            <div class="entries left">
                Show
                <select>
                    <option>15</option>
                </select>
                All entries
            </div>
            <div class="info right">
                Showing 1 to 15 of 157 entries
            </div>
        </div>
	</div>
</section>

@include('product_manajer.pop.manajer_detail')

@endsection

@push('scripts')
<script type="text/javascript">
	$(document).ready(function(){
		var listManager = $("#list-manager-body");

		$.ajax({
			url : '{{ Mirae::urlAction('ajax-listing-manajer') }}',
			beforeSend : function(){
                listManager.html("");
                listManager.html(loadingTrTd());
			},
			success : function(respon){
                listManager.html("");
                listManager.append(respon.listing);
			}
		});

	});
</script>
@endpush
