@extends('layouts.layout')

@section('content')

<?php $ind = 0; ?>
<section class="banner">
    <img src="{{ assetContent(@$setting->banner_image) }}" alt="banner" />
    <div class="caption"> 
        <h6>{{ @$setting->title_caption }}</h6>
        <h1>{!! @$setting->title_main !!}</h1>
        <p>{{ @$setting->sub_title }}</p>
    </div>
</section>
<section class="std-content gn-content">
    <div class="wrapper myaccount">
        <div class="wrapper-content">
            @if(!empty($setting->title_content))
            <h2 class="title-shadow">
                {{ @$setting->title_content }}
            </h2>
            @endif
<!-- 
            <div class="sel_glossary">
                <select>
                    <option>Afiliasy</option>
                    <option>Agen</option>
                    <option>Agen Pembayaran</option>
                </select>
            </div> -->
            <div class="box-slide-sorting">
                <div class="slide-sorting">
                    
                    @foreach(range('A', 'Z') as $index => $char)
                        @if($sort == $char)
                            <?php $ind = $index; ?>
                            <div class="slick-slide slick-current slick-active current-active" onclick="sort('{{ $char }})"><a href="{{ urlLang('glossary').'?sort='.$char }}">{{ $char }}</a></div>
                        @else
                            <div class="slick-slide slick-active" onclick="sort('{{ $char }}')"><a href="{{ urlLang('glossary').'?sort='.$char }}">{{ $char }}</a></div>
                        @endif

                    @endforeach
                </div>
            </div>

            <script type="text/javascript">
                
            </script>

            <div class="box-accordion">
                @foreach($help as $helps)
                <div class="list-accordion">
                    <div class="btn-acc">
                        {{ $helps->title }}
                        <span class="arrow-acc"></span>
                    </div>
                    <div class="box-acc">
                        {!! $helps->description !!}
                    </div>
                </div>
                @endforeach
            </div>

            <div class="paging a-center">
                {!! with(new App\Pagination\WebarqPagination($help))->render(); !!}
            </div>
        </div>
    </div>
</section>

@endsection
@push('scripts')
<script type="text/javascript">
    function sort(alpha)
    {
        url = '{{ urlLang("glossary?sort=") }}' + alpha;
        document.location.href=url;
    }
    $(function(){
        $(".slide-sorting").slick('slickGoTo','<?=$ind?>');
    })

</script>

@endpush