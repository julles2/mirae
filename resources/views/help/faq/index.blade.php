@extends('layouts.layout')

@section('content')

<section class="banner">
    <img src="{{ assetContent(@$setting->banner_image) }}" alt="banner" />
    <div class="caption"> 
        <h6>{{ @$setting->title_caption }}</h6>
        <h1>{!! @$setting->title_main !!}</h1>
        <p>{{ @$setting->sub_title }}</p>
    </div>
</section>
<section class="std-content gn-content">
    <div class="wrapper myaccount">
        <div class="wrapper-content">
            <h2 class="title-shadow">
                {{ @$setting->title_content }}
            </h2>


            <div class="box-accordion">
                @foreach($help as $helps)
                <div class="list-accordion">
                    <div class="btn-acc">
                        {{ $helps->title }}
                        <span class="arrow-acc"></span>
                    </div>
                    <div class="box-acc">
                        {!! $helps->description !!}
                    </div>
                </div>
                @endforeach
            </div>

            <div class="paging a-center">
                {!! with(new App\Pagination\WebarqPagination($help))->render(); !!}
            </div>
        </div>
    </div>
</section>

@endsection