<html lang="en-US"><head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta charset="utf-8">
        <link href='//fonts.googleapis.com/css?family=Roboto:400,700' rel='stylesheet' type='text/css'>
    </head>
    <body>
        <table style="font-family:'Calibri';font-size:14px;width: 100%;margin:auto;display: block;line-height: 24px;">
            <tr>
                <td align="right" style="padding: 0 0 30px">
                    <img src="{{ asset('') }}/frontend/images/material/logo-transparent.png" alt="logo" />
                </td>
            </tr>
            <tr>
                <td style="padding-bottom: 50px;">
                    Berikut merupakan pesan dari customer:
                    <table style="width: 100%;border:1px solid #CCC;border-collapse: collapse;margin:10px 0 0 0">
                        <tr>
                            <td style="border:1px solid #CCC;padding:10px">Nama </td>
                            <td style="border:1px solid #CCC;padding:10px">{{ $name }}</td>
                        </tr>
                        <tr>
                            <td style="border:1px solid #CCC;padding:10px">Email </td>
                            <td style="border:1px solid #CCC;padding:10px">{{ $email }}</td>
                        </tr>
                        <tr>
                            <td style="border:1px solid #CCC;padding:10px">Pesan </td>
                            <td style="border:1px solid #CCC;padding:10px">{{ $body_message }}</td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td><b>Jika anda memerlukan informasi atau bantuan silahkan hubungi Customer Service kami di:</b></td>
            </tr>
            <tr>
                <td style="padding:0 0 70px 0;">
                    <table>
                        <tr>
                            <td>Telp</td>
                            <td>:</td>
                            <td>021 – 2553 1000</td>
                        </tr>
                        <tr>
                            <td>Email</td>
                            <td>:</td>
                            <td><a href="" style="color:#0563c1">customerservice@miraeasset.co.id</a></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="padding:0 0 90px 0;">
                    Terima Kasih,<br/>
                    <b>Mirae Asset Sekuritas Indonesia</b>
                </td>
            </tr>
            <tr>
                <td style="border:1px solid #333;font-size:14px;padding: 10px">
                    <h6 style="font-size: 14px;color:#ff0000;margin:0 0 5px;">Syarat dan Ketentuan:</h6>
                    <ul style="list-style: decimal;list-style-position: outside;margin:0 0 0 -20px">
                        <li>Instruksi pembelian yang tercatat masuk sistem PT Mirae Asset Sekuritas Indonesia sebelum 12.00 WIB pada hari yang sama akan mengikuti perhitungan NAB pada hari bursa berikutnya.</li>
                        <li>Instruksi pembelian yang tercatat masuk di sistem PT Mirae Asset Sekuritas Indonesia  setelah pukul 12.00 WIB pada hari yang sama akan mengikuti perhitungan NAB pada 2 (dua) hari bursa berikutnya.</li>
                        <li>Instruksi pembelian akan otomatis dibatalkan jika tidak ada konfirmasi pembayaran hingga 3 (tiga) hari bursa berikutnya.</li>
                    </ul>
                </td>
            </tr>
        </table>

    </body>
</html>
