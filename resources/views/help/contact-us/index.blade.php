@extends('layouts.layout')

@section('content')

<section class="banner">
    <img src="{{ assetContent(@$setting->banner_image) }}" alt="banner" />
    <div class="caption"> 
        <h6>{{ @$setting->title_caption }}</h6>
        <h1>{!! @$setting->title_main !!}</h1>
        <p>{{ @$setting->sub_title }}</p>
    </div>
</section>
<section class="std-content gn-content">
    <div class="wrapper myaccount">
        <div class="wrapper-content">
            <h2 class="title-shadow">
                {{ @$setting->title_content }}
            </h2>

            <div class="box-help-contact">
                <div class="left">
                    <h4>{{ @$setting->title_contact }}</h4>
                    <h5>{{ @$setting->sub_title_contact }}</h5>
                    <br>
                    {!! @$setting->address !!}
                </div>
                <div class="right">
                    @include('backend.common.errors')
                    {!! Form::open(['method'=>'post','url'=>urlLang('contact-us'),'class'=>'box-form']) !!}
                    <div class="row">
                        {!! Form::text('name',null,['placeholder'=> trans('home.yourname') ]) !!}
                    </div>
                    <div class="row">
                        {!! Form::text('email',null,['placeholder'=>'E-Mail']) !!}
                    </div>
                    <div class="row">
                        {!! Form::textarea('message' , null ,['placeholder' => trans('home.message')]) !!}
                    </div>
                    <div class="row">
                        <!-- {!! app('captcha')->display(); !!} -->
                    </div>

                    <div class="row bt">
                        <input type="submit" value="{{ trans('home.send') }}" class="btn-oval btn-blue"/>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
        <div class="maps">
            <iframe src="{{ @$setting->maps }}" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
        </div>

        <div class="list-contact">
            <div class="box">
                <h4>BALI</h4>
                <p>
                    Jl.Raya Puputan No.108B Renon Denpasar Bali 80239<span class="spar"></span>
                    T: 0361-221951<br/>
                    T: 0361-221975<br/>
                    T: 0361-221994
                </p>
            </div>
            <div class="box">
                <h4>BANDUNG</h4>
                <p>
                    Jl. Rajiman No 1, Cidendo. Pasir Kaliki. Bandung 40171<span class="spar"></span>
                    T: 022-4264922<br/>
                    T: 022-4264921
                </p>
            </div>
            <div class="box">
                <h4>DEPOK</h4>
                <p>
                    Ruko Apartement Margonda Residence Jl. Margonda Raya Blok 1 Depok - 16424<span class="spar"></span>
                    T: 021-78880710<br/>
                    T: 021-78880702
                </p>
            </div>
            <div class="box">
                <h4>BINTARO</h4>
                <p>
                    Ruko Kebayoran Arcade 1 Jl. Boulevard Bintaro Jaya Blok C3 No.53, Pondok Jaya, Pondok Aren, Tangerang, Banten 1522<span class="spar"></span>
                    T: 021-22215158<br/>
                    T: 021-22215333<br/>
                    T: 021-22215274
                </p>
            </div>
            <div class="box">
                <h4>BSD</h4>
                <p>
                    Ruko ITC BSD No.28 Jl.pahlawan Seribu. Serpong. TangSel 15322<span class="spar"></span>
                    T: 021-5381244<br/>
                    T: 021-5381284<br/>
                    T: 021-5381134	
                </p>
            </div>
            <div class="box">
                <h4>CIBUBUR</h4>
                <p>
                    Jl.Alternatif Ruko Cibubur Square Blok B1/9 Cibubur Jatisempurna, Bekasi Jawa Barat 17433<span class="spar"></span>
                    T: 021-84302497<br/>
                    T: 021-84302498
                </p>
            </div>
            <div class="box">
                <h4>PLUIT</h4>
                <p>
                    Jl. Pluit Sakti Raya No.31 A Jakarta Utara 14450<span class="spar"></span>
                    T: 021-66678482<br/>
                    T: 021-66678483<br/>
                    T: 021-66678454	
                </p>
            </div>
            <div class="box">
                <h4>MEGA KUNINGAN (Kantor Taman)</h4>
                <p>
                    Kawasan Mega Kuningan, Kantor Taman A9 Unit A3-A Lt.3 , Jakarta Selatan 12950<span class="spar"></span>
                    T: 021-5762867<br/>
                    T: 021-5762873
                </p>
            </div>
            <div class="box">
                <h4>PURI</h4>
                <p>
                    Rukan Grand Puri Niaga Blok K-6 No.1P Puri Kencana Jakarta Barat<span class="spar"></span>
                    T: 021-58304242<br/>
                    T: 021-58304244
                </p>
            </div>
            <div class="box">
                <h4>FATMAWATI</h4>
                <p>
                    Jl. RS Fatmawati No 33H, Gandaria Selatan, Cilandak, Jakarta Selatan 12420<span class="spar"></span>
                    T: 021-75817575<br/>
                    T: 021-75817577
                </p>
            </div>
            <div class="box">
                <h4>ROXY</h4>
                <p>
                    Ruko ITC Roxy Mas Jl. KH. Hasyim Ashari Blok D-3/4, Cideng, Gambir, Jakarta Pusat, 10150<span class="spar"></span>
                    T: 021-63858582<br/>
                    T: 021-22633257<br/>
                    T: 021-22631526
                </p>
            </div>
            <div class="box">
                <h4>MANGGA DUA</h4>
                <p>
                    Harco Mangga dua. Jln Mangga dua raya blok E no 27, Mangga dua selatan, Sawah Besar, Jakarta Pusat 10730<span class="spar"></span>
                    T: 021-62201265<br/>
                    T: 021-62201266
                </p>
            </div>
            <div class="box">
                <h4>KELAPA GADING</h4>
                <p>
                    Jl.Raya Boulevard Barat LC 6/5, Kelapa Gading Barat, Kelapa Gading, Jakarta Utara 14240<span class="spar"></span>
                    T: 021-24520354<br/>
                    T: 021-24520353<br/>
                    T: 021-24520355<br/>
                    T: 021-24520352
                </p>
            </div>
            <div class="box">
                <h4>MALANG</h4>
                <p>
                    Ruko S. Parman A3, Jl. Letjend. S. Parman No. 56 Malang<span class="spar"></span>
                    T: 0341-496481<br/>
                    T: 0341-418420	
                </p>
            </div>
            <div class="box">
                <h4>SOLO</h4>
                <p>
                    Ruko Super Makmur 2 no. 9 B Jln Ir. Soekarno Solo Baru 57552<span class="spar"></span>
                    T: 0271-622862
                </p>
            </div>
            <div class="box">
                <h4>MEDAN</h4>
                <p>
                    Jl.Kapten Maulana Lubis No.08 Lantai UG Gd. Grand Palladium Mall Medan-20112<span class="spar"></span>
                    T: 061-4556544<br/>
                    T: 061-4552274<br/>
                    T: 061-4529731	
                </p>
            </div>
            <div class="box">
                <h4>PALEMBANG</h4>
                <p>
                    Jl.Jend. Sudirman No.80 Palembang 30126<span class="spar"></span>
                    T: 0711-362163<br/>
                    T: 0711-362153
                </p>
            </div>
            <div class="box">
                <h4>SEMARANG</h4>
                <p>
                    Jl. Pemuda No.171 Graha Sucofindo Lobby Lt.1, Semarang-50132<span class="spar"></span>
                    T: 024-3582449
                </p>
            </div>
            <div class="box">
                <h4>SURABAYA KOMBES DURYAT</h4>
                <p>
                    Ruko Surya Inti Permata III Blok B5 Jl. Kombes Pol M Duryat No.14-16 Surabaya 60262<span class="spar"></span>
                    T: 031-5349500<br/>
                    T: 031-5349501<br/>
                    T: 031-5349505
                </p>
            </div>
            <div class="box">
                <h4>SURABAYA HR MUHAMMAD</h4>
                <p>
                    Ruko HR Muhammad Square Blok A-11, Jl HR Muhammad No. 140, Surabaya 60226<span class="spar"></span>
                    T: 031-7313782<br/>
                    T: 031-7313787<br/>
                    T: 031-7313790
                </p>
            </div>
            <div class="box">
                <h4>BALIKPAPAN</h4>
                <p>
                    Kantor Perwakilan Bursa Efek Indonesia Balikpapan. Jl. Jend. Sudirman No. 33 B Kota Balikpapan, Kalimantan Timur 76114<span class="spar"></span>
                    T: 0542-8501873
                </p>
            </div>
            <div class="box">
                <h4>YOGYAKARTA</h4>
                <p>
                    Jl. Magelang No.79 Kel. Kricak, Kec. Tegalrejo, Yogyakarta 55242<span class="spar"></span>
                    T: 0274-548804	
                </p>
            </div>
            <div class="box">
                <h4>PADANG</h4>
                <p>
                    KP IDX Padang, Gedung bursa efek Padang PIPM PT Mirae Asset Sekuritas Indonesia. jl pondok no.90A padang Sumatera Barat<span class="spar"></span>
                    T: 0751-8955249
                </p>
            </div>
            <div class="box">
                <h4>MAKASSAR</h4>
                <p>
                    Jl Dr. Sam Ratulangi no 124 Mariso, Makassar Sulawesi Selatan<span class="spar"></span>
                    T: 0411-8913128
                </p>
            </div>

        </div>

    </div>
</section>

@endsection

@section('script')
{!! JsValidator::formRequest('App\Http\Requests\ContactusRequest') !!}

@endsection
