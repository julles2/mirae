@if(!empty($modelInfoGeneral))  
<div class="col-2">
    <div class="box">
        <div class="column">
            <div class="box">
                <h3>{{ $imName($modelInfoGeneral->im_cd) }}</h3>
                <p>{!! $modelInfoGeneral->description !!}</p>
                <span  class="code_mi">Kode MI  <b>{{ $modelInfoGeneral->im_cd }}</b></span>
            </div>
        </div>
        <div class="column col-2">
            <div class="box">
                <label>Latest AUM  <span>(per : 28-Feb-2017)</span></label>
                <h6>641.710.505.372 IDR</h6>
            </div>
            <div class="box">
                <label>{{ trans('product.management') }}</label>
                <h6>{{ $modelInfoGeneral->pengelolaan }}</h6>
            </div>
        </div>
        <div class="column col-2">
            <div class="box">
                <label>{{ trans('product.authorizedcapital') }}</label>
                <h6>{{ $modelInfoGeneral->modal_dasar }}</h6>
            </div>
            <div class="box child col-2">
                <div class="box">
                    <label>{{ trans('product.ownership') }}</label>
                    <h6>{{ $modelInfoGeneral->kepemilikan }}</h6>
                </div>
                <div class="box">
                    <label>Status</label>
                    <h6>{{ $modelInfoGeneral->status = 'y' ? "Aktif" : "Tidak Aktif" }}</h6>
                </div>
            </div>
        </div>
        <div class="column col-2">
            <div class="box">
                <label>{{ trans('product.paidupcapital') }}</label>
                <h6>{{ $modelInfoGeneral->modal_disetor }}</h6>
            </div>
            <div class="box">
                <label>{{ trans('product.businesspermit') }}</label>
                <h6>{{ $modelInfoGeneral->izin_usaha }}</h6>
            </div>
        </div>
    </div>
    <div class="box">
        <div class="column">
            <div class="box">
                <label>STAKEHOLDER</label>
                <div class="stkholder">
                    <table>
                        <thead>
                            <tr>
                                <th>{{ trans('product.name') }}</th>
                                <th>{{ trans('product.ownership') }} (IDR)</th>
                                <th>%</th>                                                
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($stakeHolder as $stake)
                            <tr>
                                <td>{{ $stake->nama }}</td>
                                <td>{{ $stake->kepemilikan }}</td>
                                <td>{{ $stake->jumlah }} %</td>                          
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="column col-2">
            <div class="box">
                <label>{{ trans('product.boarddirectur') }}</label>
                @foreach($department as $depart)
                    <?php similar_text(strtolower($depart->jabatan),strtolower('Direktur'), $percent); ?>
                    @if($percent > 50)
                        <h6 class="board">{{ $depart->nama }} <i>- {{ $depart->jabatan }}</i></h6>
                    @endif
                @endforeach
            </div>
            <div class="box">
                <label>{{ trans('product.commissioners') }}</label>
                @foreach($department as $depart)
                    <?php similar_text(strtolower($depart->jabatan),strtolower('Komisaris'), $percent); ?>
                    @if($percent > 50)
                        <h6 class="board">{{ $depart->nama }} <i>- {{ $depart->jabatan }}</i></h6>
                    @endif
                @endforeach
            </div>
        </div>
    </div>
</div>
@endif