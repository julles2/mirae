@if($modelFundList->count() > 0)
        @foreach($modelFundList as $fund)
        @php($imageNa = $fundImage($fund->fund_cd))
        <div class="box">
            <div class="desc">
                <div class="ico">
                    @if(!empty($imageNa->id))
                        <img src="{{ asset(null) }}contents/{{ @$fundImage($fund->fund_cd)->image }}" alt="prod">
                    @else
                        <img src="{{ asset('noimage.png') }}" alt="prod">

                    @endif
                </div>
                <div class="text">
                    <h5>{{ $fund->fund_name }}</h5>
                    <p>NAB   <span>{{ valueDecimal($fund->nav_per_unit) }}</span>
                    </p>
                </div>
            </div>
            <div class="return">
                <p>Return (%)</p>
            </div>
            <div class="float-3">
                <div>
                    <p>1 {{ trans('global.dy') }} <span>{{ valueDecimal($fund->one_day) }}</span></p>
                </div>
                <div>
                    <p>1 {{ trans('global.mt') }} <span>{{ valueDecimal($fund->one_month) }}</span></p>
                </div>
                <div>
                    <p>1 {{ trans('global.yr') }}  <span>{{ valueDecimal($fund->one_years) }}</span></p>
                </div>
            </div>
            <a onclick="addWatchLists('{{ $fund->fund_cd }}')" {!! checkWishList($fund->fund_cd, acnt_no()) !!} class="watch-list">Add To Watchlist</a>
            <div class="btn">
                <a href="{{ urlLang('product?pop='.$fund->fund_cd) }}">{{ trans('product.buy') }}</a>
                <a href="">{{ trans('product.compare') }}</a>
            </div>
        </div>
        @endforeach
@else
    <h3 style="text-align:center;">DATA TIDAK ADA</h3>
@endif
