@push('scripts')
@if(!empty(get('pop')))
<script type="text/javascript">
    $(document).ready(function(){
        loadPop('{{get("pop")}}');
    });
</script>
@endif
<script type="text/javascript">

    offset = 0;

    $(document).ready(function(){
        reloadTable(offset);

        $("#load_more").on('click',function(){
            reloadTable(offset);
        });
    });

    $("#btn_cari").click(function(){
        reloadTable();
        pagination();
    });

    $("#cari").keypress(function(e){
        if (!e) e = window.event;
            var keyCode = e.keyCode || e.which;
            if (keyCode == '13'){
                reloadTable();
                pagination();
            }
    });

    function search()
    {
        $cari = $("#cari").val();

        if($cari != "")
        {
            $("#limit").val("{{ $limit }}");
            reloadTable();
        }
    }

    function reloadTable(offset=null)
    {
        list_reksadana = $("#table_mi");
        limit = $("#limit").val();
        $.ajax({
            url : '{{ urlLang('manager-investment/reload-table') }}',
            data : {
                limit : limit,
                offset: offset,
                im_name : $("#cari").val(),
            },
            beforeSend : function(){
                list_reksadana.html("");
                showPop('loaderpopup');
            },
            success : function(respon){
                $resultLimit = parseInt(limit) + parseInt('{{ $limit }}');
                $("#limit").val($resultLimit);
                list_reksadana.html("");
                list_reksadana.append(respon.listing);
                closePop('loaderpopup');
            },
        });
    }

    function loadPop(im_cd)
    {
        console.log('1');
        showPop('detail-Minvest');

        $.ajax({
            url : '{{ Mirae::urlAction('ajax-pop') }}',
            data:{
                im_cd : im_cd,
            },
            success : function(respon){
                $.each(respon.vars , function(key,val){
                    if(key == 'im_cd')
                    {
                        $("#"+key).val(val);
                    }else{
                        $("."+key).html(val);
                    }

                });
                $(".imgCompany").attr('src',respon.vars.imImage);
                chart1(respon.chartDates,respon.chartIdr);
                chart2(respon.chartDates,respon.chartUsd);
            },
        });
    }
</script>
<script src="//code.highcharts.com/highcharts.js"></script>
<script src="//code.highcharts.com/modules/exporting.js"></script>
<script>
function reloadChart()
{
    from= $("#start_year").val() + "-" +$("#start_month").val();
    to= $("#end_year").val() + "-" +$("#end_month").val();
    im_cd= $("#im_cd").val();

    $.ajax({
        url: '{{urlLang('manager-investment/ajax-reload-chart')}}',
        data:{
            from: from,
            to: to,
            im_cd: im_cd
        },
        success: function(respon){
            chart1(respon.chartDates,respon.chartIdr);
            chart2(respon.chartDates,respon.chartUsd);
        },
    });
}

function chart1(categories,series)
{
    Highcharts.chart('chart-1', {
        colors: ['#f58220', '#2e76a2', '#8d4654', '#7798BF', '#aaeeee', '#ff0066', '#eeaaee',
            '#55BF3B', '#DF5353', '#7798BF', '#aaeeee'],
        chart: {
            type: 'column'
        },
        title: {
            text: ''
        },
        credits: false,
        subtitle: {
            text: ''
        },
        xAxis: {
            categories: categories,
            crosshair: true
        },
        yAxis: {
            min: 0,
            title: {
                text: 'IDR',
                style: {
                    color: '#f58220', fontSize: '12px', textTransform: 'uppercase', fontWeight: 'bold', letterSpacing: '0.5px'
                }
            },
            labels: {
                style: {
                    color: '#f58220', fontSize: '12px'
                },
                formatter: function () {
                  return this.value / 1000000000 + ' {{ trans("global.billion") }}';
                }
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                    '<td style="padding:0"><b>Rp{point.y:,.0f} </b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {
                pointPadding: 0.5,
                borderWidth: 0,
                pointWidth: 20
            }
        },
        series: series,

    });
}

function chart2(categories,series)
{
    Highcharts.chart('chart-2', {
        colors: ['#f58220', '#2e76a2', '#8d4654', '#7798BF', '#aaeeee', '#ff0066', '#eeaaee',
            '#55BF3B', '#DF5353', '#7798BF', '#aaeeee'],
        chart: {
            type: 'column'
        },
        title: {
            text: ''
        },
        credits: false,
        subtitle: {
            text: ''
        },
        xAxis: {
            categories:categories,
            crosshair: true
        },
        yAxis: {
            min: 0,
            title: {
                text: 'USD',
                style: {
                    color: '#f58220', fontSize: '12px', textTransform: 'uppercase', fontWeight: 'bold', letterSpacing: '0.5px'
                }
            },
            labels: {
                style: {
                    color: '#f58220', fontSize: '12px'
                },
                formatter: function () {
                  return this.value / 1000000 + ' {{ trans("global.million") }}';
                }
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                    '<td style="padding:0"><b>$ {point.y:,.0f}</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {
                pointPadding: 0.5,
                borderWidth: 0,
                pointWidth: 20
            }
        },
        series:series,

    });
}

$(document).ready(function(){
  $("#check_all").click(function(){
        $this = $(this);
        if($this.is(':checked'))
        {
            $('input:checkbox').not(this).prop('checked', this.checked);
        }else{
            $('input:checkbox').removeAttr('checked');
        }
    });  
});

function compare_check()
{
    $html = "";
    $count = 0;
    $.each($("#table_mi input:checkbox:checked"),function(key,val){
        if($(this).val() != "")
        {
            
            this_val = $(this).val();
            $html += "<input type = 'hidden' value = '"+this_val+"' name = 'im"+$count+"' />";
            $count = $count + 1;
        }
            
    });
    
    if($count > 0)
    {
        if($count > 3)
        {
            swal_error('{{ trans("notif.max_compare") }}');
        }else{
            $("#compare_div").html($html);
            $("#compare_form").submit(); 
        }
            
    }else{
        alert('anda belum memilih fund');
    }
        
}

function hiddenParent()
{
    $("#check_all").removeAttr('checked');
}

</script>
@endpush
