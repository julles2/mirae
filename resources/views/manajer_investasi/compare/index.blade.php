@extends('layouts.layout')
@section('content')
<section class="banner">
    <img src="{{ assetContent(@$setting->banner_image) }}" alt="banner" />
    <div class="caption">
        <h6>{{ @$setting->title_caption }}</h6>
        <h1>{!! @$setting->title_main !!}</h1>
        <p>{{ @$setting->sub_title }}</p>
    </div>
</section>
<section class="std-content">
<div class="wrapper product-compare">

    {!! Form::open(['method'=>'get']) !!}
        <div class="compare-form">
            <input type = "hidden" name="month" value = "12" />
            <div class="col-3">
                @for($a=0;$a<3;$a++)
                    <div>
                        {!! Form::select("im$a",$selectMi,null,['class'=>'select2']) !!}
                    </div>
                @endfor

            </div>
			<div class="a-center">
				<input type="submit"  value="{{ trans('product.compare') }}" class="btn-std">
			</div>
        </div>
    {!! Form::close() !!}

    <div class="shadow-spar"></div>

    <div class="wrap-tab">
        <div class="nav-tab">
            <a href="#compare-tab-1" class="active">{{ trans('product.aumcompare') }}</a>
            <a href="#compare-tab-2">{{ trans('product.unitcompare') }}</a>
        </div>

        

        <div class="filter_top">
            @if(!empty($repo->handleCode()))
                {!! Form::open(['method'=>'get','url'=>urlLang('manager-investment-comare/filter-range')]) !!}
                    <div class="left">
                            <h6>{{ trans('product.period') }}</h6>
                        </div>
                        <div class="right">
                        @foreach($repo->periode as $key=>$val)
                            <label class="check opt" onclick="return changeUrl('{{$key}}')">
                                <input type="checkbox" {{ get('month') == $key ? 'checked' : '' }}/>
                                <span>{{ $val }}</span>
                            </label>
                        @endforeach
                        @foreach($repo->handleCode() as $index => $c)
                            <input type="hidden" name="{{$index}}" value="{{ $c }}">
                        @endforeach
                            <div class="list_filt">
                                <label>From</label>
                                <?php /* <input type="text" class="datepicker" name="start_date" value="{{ get('start_date') }}"/> */ ?>
                                {!! Form::selectMonth('start_month',@$repo->monthComboStart()) !!}
                                {!! Form::selectRange('start_year',date("Y")-5,date("Y"),@$repo->yearComboStart()) !!}
                            </div>
                            <div class="list_filt">
                                <label>To</label>
                                <?php /* <input type="text" class="datepicker" name="end_date" value="{{ get('end_date') }}"/>*/?>
                                {!! Form::selectMonth('end_month',@$repo->monthComboEnd()) !!}
                                {!! Form::selectRange('end_year',date("Y")-5,date("Y"),@$repo->yearComboEnd()) !!}
                            </div>
                            <div class="submit">
                                <input type="submit" value=" ">
                            </div>

                        </div>
                    {!! Form::close() !!}
                </div>

                <div class="content-tab" id="compare-tab-1">
                    <h2>{{ trans('product.aumcompare') }}</h2>
                    <div class="wrap-chart">
                        <div class="chart" id="chart_aum" style="min-width:970px;"></div>
                        <div class="chart" id="chart_aum_usd" style="min-width:970px;margin-top:10px;"></div>
                    </div>
                </div>
                <div class="content-tab" id="compare-tab-2">
                    <h2>{{ trans('product.unitcompare') }}</h2>
                    <div class="wrap-chart">
                        <div class="chart" id="chart_unit" style="min-width:970px;"></div>
                        <div class="chart" id="chart_unit_usd" style="min-width:970px;margin-top:10px;"></div>
                    </div>
                </div>
            @endif
    </div>
    <div class="shadow-spar"></div>
    <div class="ringkasan-perbandingan">
        <h2>{{ trans('product.comparesummary') }}</h2>
        <div class="after_clear">

        @if(count($repo->listRingkasan()) > 0)
            @foreach($repo->listRingkasan() as $i => $im)
            @php
            $ringkasan = $repo->ringkasan($im->im_cd);
            $ringkasan_feed = $repo->ringkasan_feed($im->im_cd);
            @endphp
                <div class="box">
                    <div class="desc">
                        <div class="ico"><img src="{{ $im->result_image }}" alt="prod"></div>
                        <div class="text">
                            <h6>{{ $im->im_nm  }}</h6>

                        </div>
                    </div>

                    <div class="col-2 sec">
                        <div class="after_clear">
                            <div class="left">
                                <p><b>{{ trans('global.unit') }} (IDR)</b></p>
                            </div>
                            <div class="right">

                                <p>{!! formatUang($im->latest_unit_feed->unit_idr) !!}</p>
                            </div>
                        </div>
                        <div class="after_clear">
                            <div class="left">
                                <p><b>{{ trans('global.unit') }} (USD)</b></p>
                            </div>
                            <div class="right">
                                <p>{!! formatUang($im->latest_unit_feed->unit_usd) !!}</p>
                            </div>
                        </div>
                        <div class="after_clear">
                            <div class="left">
                                <p><b>{{ trans('global.aum') }} (IDR)</b></p>
                            </div>
                            <div class="right">

                                <p>{!! formatUang($im->latest_unit_feed->aum_idr) !!}</p>
                            </div>
                        </div>
                        <div class="after_clear">
                            <div class="left">
                                <p><b>{{ trans('global.aum') }} (USD)</b></p>
                            </div>
                            <div class="right">
                                <p>{!! formatUang($im->latest_unit_feed->aum_usd) !!}</p>
                            </div>
                        </div>
                    </div>
                    <div class="table sec">
                        <table>
                            <tr>
                                <td></td>
                                <th>6{{ trans('global.mt') }}</th>
                                <th>9{{ trans('global.mt') }}</th>
                                <th>12{{ trans('global.mt') }}</th>
                                <th>YTD</th>
                            </tr>
                            @foreach($repo->fieldRingkasan() as $key=>$val)
                                <tr>
                                    <th>{{$val}}</th>
                                    @foreach(array_keys((array)($ringkasan_feed)) as $time)
                                        <td><?php echo $ringkasan_feed->{$time}->{$key}; ?></td>
                                        {{-- <td>0</td> --}}
                                    @endforeach
                                </tr>
                            @endforeach
                        </table>
                    </div>

                    <div class="btn-wrap">
                        <a href="javascript::void(0)" onclick = "loadPop('{{$im->im_cd}}')" class="btn" >
                            {{ trans('product.buy') }}
                        </a>
                    </div>
                </div>
            @endforeach
        @endif
        </div>
        <p>&nbsp;</p>
        <div class="disclaimer-box">
            <div><b>{{ trans("global.explanation") }}</b></div>
            AUM (IDR {{ trans('global.billion') }})  &nbsp;
            AUM (USD {{ trans('global.million') }})  &nbsp;
            UNIT (IDR {{ trans('global.million') }})  &nbsp;
            UNIT (USD {{ trans('global.thousand') }})  &nbsp;
        </div>
        <p>&nbsp;</p>
        <div class="disclaimer-box">
            <h4>{{ trans('disclaimer.title_performa') }}</h4>
            <p>
                {{ trans('disclaimer.performa') }}
            </p>
        </div>
    </div>
</div>
</section>
@include("manajer_investasi.compare.pop.list_fund")
@endsection
@push('scripts')
    <script type="text/javascript">
        function loadPop(im_cd)
        {
            showPop('detail-Minvest');

            $.ajax({
                url : '{{ urlLang("manager-investment/ajax-pop") }}',
                data:{
                    im_cd : im_cd,
                },
                success : function(respon){
                    $.each(respon.vars , function(key,val){
                        if(key == 'im_cd')
                        {
                            $("#"+key).val(val);
                        }else{
                            $("."+key).html(val);
                        }

                    });
                    $(".imgCompany").attr('src',respon.vars.imImage);
                },
            });
        }

        function changeUrl(month)
        {
            urlNonMonth = '{{ $repo->urlNonMonth() }}';
            var parser = new DOMParser;
            var dom = parser.parseFromString(
                '<!doctype html><body>' + urlNonMonth,
                'text/html');
            var resultUrl = dom.body.textContent;
            $url = resultUrl + '&month=' + month;
            //alert($url);
            document.location.href=$url;
        }
    </script>
    <script src="//code.highcharts.com/highcharts.js"></script>
    <script src="//code.highcharts.com/modules/exporting.js"></script>
    <script>
        Highcharts.chart('chart_aum', {
            chart: {
                type: 'line'
            },
            title: {
                text: '{{ trans('product.aumcompare') }} (IDR)'
            },
            credits:false,
            yAxis: {
                title: {
                    text: 'AUM (IDR)',
                },
                labels: {
                    formatter: function () {
                      return this.value / 1000000000 + ' {{ trans("global.billion") }}';
                    }
                }
            },
            scrollbar: {
                    enabled: true
            },
            xAxis: {
                categories: {!! $range !!},
                //tickInterval: 15,
            },
            series: {!! $dataAum !!}
        });

        Highcharts.chart('chart_aum_usd', {
            chart: {
                type: 'line'
            },
            title: {
                text: '{{ trans('product.aumcompare') }} (USD)'
            },
            credits:false,
            yAxis: {
                title: {
                    text: 'AUM (USD)',
                },
                labels: {
                    formatter: function () {
                      return this.value / 1000000 + ' {{ trans("global.million") }}';
                    }
                }
            },
            scrollbar: {
                    enabled: true
            },
            xAxis: {
                categories: {!! $range !!},
                //tickInterval: 15,
            },
            series: {!! $dataAumUsd !!}
        });

        Highcharts.chart('chart_unit', {
            chart: {
                type: 'line'
            },
            title: {
                text: '{{ trans('product.unitcompare') }}'
            },
            credits:false,
            yAxis: {
                title: {
                    text: 'UNIT (IDR)',
                }
            },
            xAxis: {
                categories: {!! $range !!},
                //tickInterval: 15,
            },
            series: {!! $dataUnit !!}
        });

        Highcharts.chart('chart_unit', {
            chart: {
                type: 'line'
            },
            title: {
                text: '{{ trans('product.unitcompare') }} (USD)'
            },
            credits:false,
            yAxis: {
                title: {
                    text: 'UNIT (IDR)',
                }
            },
            xAxis: {
                categories: {!! $range !!},
                //tickInterval: 15,
            },
            series: {!! $dataUnit !!}
        });

        Highcharts.chart('chart_unit_usd', {
            chart: {
                type: 'line'
            },
            title: {
                text: '{{ trans('product.unitcompare') }} (USD)'
            },
            credits:false,
            yAxis: {
                title: {
                    text: 'UNIT (USD)',
                }
            },
            xAxis: {
                categories: {!! $range !!},
                //tickInterval: 15,
            },
            series: {!! $dataUnitUsd !!}
        });

    </script>
@endpush
