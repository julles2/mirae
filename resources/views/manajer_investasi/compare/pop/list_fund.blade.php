<div class="wrap-popup product-detail" id="detail-Minvest" >
    <input type = "hidden" id = "im_cd" />
    <div class="head">
        <h5>Informasi Manajer Investasi</h5>
        <a href="javascript:closePop('detail-Minvest')" class="close">close</a>
    </div>
    <div class="content wrap-tab">

        <div class="nav-tab">
            <a href="#pd-info" >{{ trans('product.generalinfo') }}</a>
            <a href="#pd-profile">{{ trans('product.companyinfo') }}</a>
            <?php /* <a href="#pd-perform">{{ trans('product.performance') }}</a> */ ?>
            <a href="#pd-fund" class="active" >{{ trans('product.fundlist') }}</a>
        </div>
        <div class="content-tab inner manajer-invest" id="pd-info">
            <div class="title-content after_clear">
                <h3 class="left">{{ trans('product.companyinfo') }}</h3>
                <div class="logo-prod">
                    <div class="ico">
                        <img class="imgCompany" alt="Im logo" />
                    </div>
                    <div class="text">
                        <h6 class="im_Name"></h6>
                    </div>
                </div>
            </div>
            <div class="general-info div_info_general">

            </div>
        </div>
        <div class="content-tab inner info-prod" id="pd-profile">
            <div class="title-content after_clear">
                <h3 class="left">{{ trans('product.companyinfo') }}</h3>
                <div class="logo-prod">
                    <div class="ico">
                        <img class="imgCompany" alt="Im logo" />
                    </div>
                    <div class="text">
                        <h6 class="im_Name"></h6>
                    </div>
                </div>
            </div>
            <div class="col-2 wrap-tab div_info_companies">

                <!-- INFO COMPANIES IM -->

            </div>
        </div>
        <div class="content-tab inner info-prod" id="pd-perform">
            <div class="title-content after_clear">
                <h3 class="left">{{ trans('product.performance') }}</h3>
                <div class="logo-prod">
                    <div class="ico">
                        <img class="imgCompany" alt="Im logo" />
                    </div>
                    <div class="text">
                        <h6 class="im_Name"></h6>
                    </div>
                </div>
            </div>
            <div class="sec_perform">
                <div class="filter_top">
                    <form action="">
                        <div class="left">
                            <h6>{{ trans('product.period') }}</h6>
                        </div>
                        <div class="right">
                            <div class="list_filt">
                                <label>{{ trans('product.from') }}</label>
                                {!! Form::select('start_month',Mirae::select_months(),null,['id'=>'start_month','class'=>'month']) !!}
                                {!! Form::select('start_year',Mirae::select_years(),date("Y"),['id'=>'start_year','class'=>'year']) !!}

                            </div>
                            <div class="list_filt">
                                <label>{{ trans('product.to') }}</label>
                                {!! Form::select('end_month',Mirae::select_months(),12,['id'=>'end_month','class'=>'month']) !!}
                                {!! Form::select('end_year',Mirae::select_years(),date("Y"),['id'=>'end_year','class'=>'year']) !!}

                            </div>
                            <div class="submit">
                                <input type="button" onclick="reloadChart()" value="">

                            </div>
                        </div>
                    </form>
                </div>
                <div class="chart">

                    <div  class="ket" style="display: none">
                        <span class="orange">Total AUM</span>
                        <span class="blue">Total UNIT</span>
                    </div>
                </div>
                <div class="chart_item" id="chart-1" style="">

                </div>
                <div class="chart_item" id="chart-2" style="">

                </div>
            </div>

        </div>
        <div class="content-tab inner info-prod" id="pd-fund">
            <div class="title-content after_clear">
                <h3 class="left">{{ trans('product.productinfo') }}</h3>
                <div class="logo-prod">
                    <div class="ico">
                        <img class="imgCompany" alt="Im logo" />
                    </div>
                    <div class="text">
                        <h6 class="im_Name"></h6>
                    </div>
                </div>
            </div>
            <div class="list-product div_info_fund">

            </div>
        </div>
    </div>

</div>
