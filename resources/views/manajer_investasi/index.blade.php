@extends('layouts.layout')
@section('content')

<section class="banner">
    <img src="{{ assetContent(@$setting->banner_image) }}" alt="banner" />
    <div class="caption">
        <h6>{{ @$setting->title_caption }}</h6>
        <h1>{!! @$setting->title_main !!}</h1>
        <p>{{ @$setting->sub_title }}</p>
    </div>
</section>
<section class="std-content">
    <div class="wrapper">
        <div class="category type-2">
            {!! Form::open(['method'=>'get','url'=>urlLang('manager-investment-comare'),'id'=>'compare_form']) !!}
                <div id = "compare_div">
                
                </div>
            {!! Form::close() !!}
            <div class="left">
                <a href="javascript::void(0);" onclick = "compare_check()" class="btn-std"/>{{ trans('product.compare') }}</a>
                &nbsp;
            </div>
            <div class="right">
                <!--form action=""-->
                    <input type="text" id = "cari" placeholder="{{ trans('product.search') }} {{ trans('product.investmentmanager') }}"/>
                    <input type="button" onclick = "search()" value=""/>
                <!--/form-->
            </div>
        </div>
        <div class="overflow">
            <input type="hidden" id = "limit" name="" value="{{ $limit }}">
            <table class="product m_invest">
                <thead>
                    <tr>
                        <th rowspan="2">#</th>
                        <th rowspan="2">
                            <label class="opt">
                                <input id = "check_all" type="checkbox" />
                            </label>
                        </th>
                        <th rowspan="2">{{ trans('product.investmentmanager') }}</th>
                        <th rowspan="2">{{ trans('product.period') }}</th>
                        <th colspan="2">{{ trans('global.latest_aum') }}</th>
                        <th colspan="2">{{ trans('global.latest_unit') }}</th>
                        {{-- <th rowspan="2"></th> --}}
                    </tr>
                    <tr>
                        <th>IDR</th>
                        <th>USD</th>
                        <th style="text-align:center;">IDR</th>
                        <th>USD</th>
                    </tr>
                </thead>
                <div class="mi_compare">
                    <tbody id = "table_mi">

                    </tbody>
                </div>
            </table>
        </div>
        <div class="a-center">
            @if($count > $limit)
                <button type="button" class="btn-std" id="load_more" style="margin-top:70px;">Load More</button>
            @endif
        </div>
        <?php
        /*
        <div class="table-nav">
            <div class="entries left">
                {{ trans('product.show') }}
                <select id = "pagination" onchange = "return reloadTable(this.value)">
                    <option value = "15">15</option>
                </select>
                {{ trans('product.allentries') }}
            </div>
            <div class="info right">
                {{ trans('product.showing') }} 1 {{ trans('product.to') }} <label id = "to"></label> {{ trans('product.of') }} <label id = "max_entries"></label> {{ trans('product.entries') }}
            </div>
        </div>
        */
        ?>
    </div>
</section>

@include('manajer_investasi.pop.manajer_investasi')
@endsection

@include("manajer_investasi.scripts")

@push('scripts')
@include("backend.common.sweet_flashes")
@endpush
