<div class="form-child-menu child-menu">
    <span class="close-btn"></span>
    <h3>{{ trans('menu.mybasket') }}</h3>
    <table>
        <thead>
            <tr>
                <th>{{ trans('menu.namemutualfund') }}</th>
                <th>{{ trans('menu.instruction') }}</th>
                <th>&nbsp;</th>
            </tr>
        </thead>
        <tbody>
            @if(Cart::count() > 0)
                @foreach(Cart::content() as $cart)
                    <tr>
                        <td>{{ $cart->name }}</td>
                        <td>{!! ucwords(str_replace("_", " ", labelCart($cart->options->type))) !!}</td>
                        <td><a href="{{ urlLang('checkout/delete-cart/'.$cart->rowId) }}" onclick = "return confirm('Are you sure want to delete this item ?')"><i class="icwp ic_trash"></i></a></td>
                    </tr>
                @endforeach
            @else
                    <tr>
                        <td colspan="3" style="text-align:center">{{ trans('menu.cartempty') }}</td>
                    </tr>
            @endif
        </tbody>
    </table>
    <hr class="line-bluetop">
    <div class="a-center">
        <a href="{{ urlLang('checkout') }}" class="btn-oval btn-orange">CHECKOUT</a>
    </div>
</div>
