<ul>
    <li><a href="{{ url('/') }}" class="home"></a></li>
    <li><a href="{{ urlLang('mileage') }}" class="">Mileage</a></li>
    <li class="have-child">
        <a href="#" class="<?= $page === 'tentangkami' ? 'active' : '' ?>">{{ trans('menu.aboutus') }}</a>
        <div class="child-menu">
            <a href="{{ urlLang('about-us') }}">{{ trans('menu.aboutmirae') }}</a>
            <a href="{{ urlLang('about-milestone') }}">{{ trans('menu.milestone') }}</a>
        </div>
    </li>
    <li class="have-child"><a href="{{ urlLang('product') }}" class="<?= $page === 'produk' ? 'active' : '' ?>">{{ trans('menu.product') }}</a>

        <div class="child-menu">
            <div class="list-child-menu have-ltl-child">
                <a href="{{ urlLang('product') }}">{{ trans('menu.mutualfund') }}</a>
                <div class="ltl-child-menu">
                    <div class="list-ltl-child"><a href="{{ urlLang('product') }}">{{ trans('menu.listmutualfund') }}</a></div>
                    <div class="list-ltl-child"><a href="{{ urlLang('compare/compare-fund') }}">{{ trans('menu.comparisonmutualfund') }}</a></div>
                </div>
            </div>
            <div class="list-child-menu">
                <a href="{{ urlLang('manager-investment') }}">{{ trans('menu.investmentmanager') }}</a>

                <div class="ltl-child-menu">
                    <div class="list-ltl-child"><a href="{{ urlLang('manager-investment') }}">{{ trans('menu.listinvestmentmanager') }}</a></div>
                    <div class="list-ltl-child"><a href="{{ urlLang('manager-investment-comare') }}">{{ trans('menu.comparisoninvestmentmanager') }}</a>
                    </div>
                </div>
            </div>
        </div>
    </li>
    <li><a href="{{ urlLang('news') }}" class="<?= $page === 'berita' ? 'active' : '' ?>">{{ trans('menu.news') }}</a></li>
    <li class="have-child">
        <a href="#" class="<?= $page === 'faq' ? 'active' : '' ?>">{{ trans('menu.help') }}</a>
        <div class="child-menu">
            <a href="{{ urlLang('faq') }}" class="<?= $page === 'faq' ? 'active' : '' ?>">{{ trans('menu.faq') }}</a>
            <a href="{{ urlLang('glossary') }}" class="<?= $page === 'glossary' ? 'active' : '' ?>">{{ trans('menu.glossary') }}</a>
            <a href="{{ urlLang('mutual-fund') }}" class="<?= $page === 'mutual-fund' ? 'active' : '' ?>">{{ trans('menu.about_mutual_fund') }}</a>
            <a href="{{ urlLang('disclaimer') }}" class="<?= $page === 'disclaimer' ? 'active' : '' ?>">{{ trans('menu.disclaimer') }}</a>
            <a href="{{ urlLang('contact-us') }}" class="<?= $page === 'contact-us' ? 'active' : '' ?>">{{ trans('menu.contactus') }}</a>
        </div>

    </li>
    <?php /*<li><a href="http://miraeasset.co.id" class="">Mirae Asset Sekuritas</a></li> */ ?>
    @if (!isset($broker))
        <li class="have-child">
            <a href="{{ urlLang('user/login') }}">{{ trans('menu.login') }}</a>
            @include('layouts.form_login_header')
        </li>
    @else
        <li class="have-child">
            <a href="{{ urlLang('user/profile') }}">{{ trans('menu.profile') }}</a>
            <div class="child-menu">
                <a href="{{ urlLang('user/profile') }}">{{ trans('menu.profile') }}</a>
                <a href="{{ urlLang('user/logout') }}">{{ trans('menu.logout') }}</a>
            </div>
        </li>
    @endif
    <?php
        $url = request()->fullUrl();
        $segments = getSegments($url);
        $changeLang = changeLang($segments);
    ?>
    
    

    <li class="drop-menu">
        <a class="arr">{{ strtoupper(lang()) }}</a>

        <div class="box-drop-menu">
            @foreach(langs() as $lang => $val)
                <a href="{{ $changeLang }}&to={{ $lang }}">{{ strtoupper($lang) }}</a>
            @endforeach
        </div>
    </li>

    <li class="search-header">
        <a> <i class="icwp ic_search"></i></a>

        <div class="box-search-header">
            <input type="text" onkeypress="return search(event)" id="txtSearch" name="" placeholder="{{ trans('menu.search') }}">
        </div>
    </li>
    <li class="have-child">
        <a href="" class="cart" data-cart="{{ Cart::count() }}"></a>
        @include('layouts.form_chart_header')
    </li>
</ul>
