<?php $page = "."; ?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>MaxFund</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no"/>

        <link rel="icon" href="{{ asset('contents/favicon.ico') }}">

        <!--Style-->
        <!--build:css css/styles.min.css-->
        <link rel="stylesheet" href="{{ assetFrontend(null) }}css/jquery.mCustomScrollbar.min.css">
        <link rel="stylesheet" href="{{ assetFrontend(null) }}css/style.css">
        <link rel="stylesheet" href="{{ assetFrontend(null) }}css/style2.css">
        <link rel="stylesheet" href="{{ assetFrontend(null) }}css/desktop.css">
        <link rel="stylesheet" href="{{ assetFrontend(null) }}css/tablet.css">
        <link rel="stylesheet" href="{{ assetFrontend(null) }}css/mobile.css">
        <link rel="stylesheet" href="{{ assetFrontend(null) }}css/app.css">
        <!--endbuild-->

        <link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">
        <link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css">
        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        <!--js-->
        <!--build:js js/main.min.js -->

        <script type="text/javascript" src="{{ assetFrontend(null) }}js/jquery-1.11.2.min.js"></script>
        <script type="text/javascript" src="{{ assetFrontend(null) }}js/TweenMax.min.js"></script>
        <script type="text/javascript" src="{{ assetFrontend(null) }}js/jquery.mCustomScrollbar.concat.min.js"></script>
        <script type="text/javascript" src="//code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
        <script type="text/javascript" src="{{ assetFrontend(null) }}js/slick.min.js"></script>
        <script type="text/javascript" src="{{ assetFrontend(null) }}js/js_lib.js"></script>
        <script type="text/javascript" src="{{ assetFrontend(null) }}js/js_run.js"></script>
        <script type="text/javascript" src="{{ asset(null) }}app.js"></script>
        <!--endbuild-->

        <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/TypeWatch/3.0.0/jquery.typewatch.js"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/numeral.js/2.0.6/numeral.min.js"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.full.min.js"></script>
        <script src="//momentjs.com/downloads/moment.min.js"></script>
        <script src="{{ asset(null) }}finance/finance.js"></script>

    </head>
    <body>


        <!-- header -->
        <div class="overlay-popup">

        </div>
        <header>
            <div class="wrapper">
                <div class="logo">
                    <a href="{{ url('/') }}"><img src="{{ assetFrontend(null) }}images/material/logo.png" alt="logo " /></a>
                </div>
                @include('layouts.menu')
            </div>
        </header>
        <!-- end of header -->
