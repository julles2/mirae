<!--Footer -->
<div class="wrap-popup loaderpopup" id="loaderpopup">
    <img src="{{ assetFrontend(null) }}images/material/loader.gif" alt="{{ trans('menu.pleasewait') }}" />
</div>
<footer>
    <div class="wrapper">
        <div class="left">
            <div class="logo-wrap after_clear">
                <div class="logo"><img src="{{ assetFrontend(null) }}images/material/logo.png" alt="logo" /></div>
                <div class="logo-ojk"><img src="{{ assetFrontend(null) }}images/material/logo-ojk.png" alt="logo ojk"/></div>
            </div>
            <nav>
                <div class="col">
                    <a href="{{ urlLang('product') }}">{{ trans('menu.mutualfund_right') }}</a>
<!--                    <a href="transaction-checkout.php">TRANSACTION</a>-->
                    <a href="{{ urlLang('news') }}">{{ trans('menu.news') }}</a>
                    <a href="{{ urlLang('about-us') }}">{{ trans('menu.aboutus') }}</a>
<!--                    <a href="">SIMULASI</a>
                    <a href="">KALKULATOR</a>-->
                </div>
                <div class="col">
                    <a href="{{ urlLang("user/portfolio") }}">{{ trans('menu.myaccount') }}</a>
                    <a href="{{ urlLang("user/login") }}">{{ trans('menu.login') }}</a>
                    <?php /* <a href="javascript::void(0)" onclick = "alert('on progress')">{{ trans('menu.registration') }}</a> */ ?>
                </div>
                <div class="col">
                    <a href="{{ urlLang('faq') }}">{{ trans('menu.faq') }}</a>
                    <a href="{{ urlLang('glossary') }}">{{ trans('menu.glossary') }}</a>
                    <a href="{{ urlLang('mutual-fund') }}">{{ trans('menu.mutualfund') }}</a>
                    <a href="{{ urlLang('disclaimer') }}">{{ trans('menu.disclaimer') }}</a>
                    <a href="{{ urlLang('contact-us') }}">{{ trans('menu.contactus') }}</a>
                </div>
                {{-- <div class="col">
                    <a href="http://miraeasset.co.id/global-network" class="arr">{{ trans('menu.global_network') }}</a>
                    <!-- <a href="" class="arr">LANGUAGE</a> -->
                </div> --}}
            </nav>
        </div>
        <div class="right">
            <h6>{{ trans('home.newsletter') }}</h6>
            <p>{{ trans('home.subscribeinfo') }}</p>
            <div class="subscribe after_clear">
                <!--
                <input type="text" id="email_newsletter" placeholder="Your E-mail Address" />
                <input onclick="newsletter()" type="submit" value="subscribe" />
 -->
                {!! Form::open(['method'=>'post','url'=>urlLang('newsletter').'/newsletter','id'=>'form_newsletter']) !!}
                    {!! Form::text('email',null,['placeholder' => trans('global.email'),'id' => 'email_newsletter','class'=>'form-control']) !!}
                    <input onclick="newsletter()" type="submit" value="{{ trans('home.subscribe') }}" />
                {!! Form::close() !!}




            </div>
            <p>
               {{--  &PT Mirae Asset Sekuritas Indonesia.
                site by <a href="http://webarq.com">webarq</a> --}}
                1997-2017 PT Mirae Asset Sekuritas Indonesia. All rights reserved. site <a href="http://webarq.com">webarq</a>
            </p>    
        </div>
    </div>
    <script type="text/javascript">

        function loadingTrTd()
        {
            return '<tr><td coslpan="9"><img src = "{{ asset('loading.gif') }}" style="" /></td></tr>';
        }

    </script>


    @include("flash")
    @include("general_js")
    @stack('scripts')
    @stack('scripts_bottom')
</footer>
<!--end of Footer -->
</body>
</html>
