<div class="child-menu">
    <div class="box-login-form std-content">
        <div class="inner-login">
            <h3>Member Login</h3>

            <p>{{ trans('menu.pleaselogin') }}</p>
            {!! Form::open(['route' => ['user.login', 'en']]) !!}
            <div class="row"><input type="text" name="uid" placeholder="User ID"></div>
            <div class="row"><input type="password" name="pwd" placeholder="Password"></div>
            <div class="row box-checklogin">
                <div class="left inlineopt">
                    <label class="opt">
                        <input type="checkbox" name="" id="login">
                    </label>
                    <label for="login">{{ trans('menu.rememberme') }}</label>
                </div>
                {{-- <div class="right">
                    <a href="https://www.miraeasset.co.id/tr/loginTR-forgotPassword.do" class="link link_line_white">{{ trans('menu.forgotpassword') }}</a>
                </div> --}}
            </div>
            <div class="row submit-login">
                <input type="submit" name="" value="{{ trans('menu.signin') }}" class="btn-oval btn-blue-light">
                <?php /* <span>{{ trans('menu.or') }}</span> */ ?>
                <!-- <a href="oa-individu-1.php" class="btn-oval btn-orange">{{ trans('menu.signup') }}</a> -->
                <?php /* <input type="submit" name="" value="{{ trans('menu.signup') }}" class="btn-oval btn-orange"> */ ?>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
