@extends('layouts.layout')

@section('content')

<section class="banner">
    <img src="{{ assetContent(@$setting->banner_image) }}" alt="banner" />
    <div class="caption"> 
        <h6>{{ @$setting->title_caption }}</h6>
        <h1>{!! @$setting->title_main !!}</h1>
        <p>{{ @$setting->sub_title }}</p>
    </div>
</section>
<section class="std-content">
    <div class="wrapper myaccount">
        <div class="box-sorting">
            <div class="left">
                <label>{{ trans('news.category') }}</label>
                {!! Form::select('category' , $categoryTranslations , get('category'),['onchange' => 'filter()','id'=>'category']) !!}
            </div>
            <div class="right">
                <label>{{ trans('news.sortby') }}</label>
                {!! Form::select('sort' , ['desc' => trans('news.newest'), 'asc' => trans('news.oldest')] , get('sort'), ['onchange' => 'filter()','id'=>'sort']) !!}
            </div>
        </div>
        <div class="box-list-news">
             @foreach($pinned as $row)
                <a href="{{ urlLang('news/read/'.$row->slug) }}" class="list-news">
                    <figure>
                        <img src="{{ asset('contents/'.$row->image_intro) }}">
                        <span style="background-color:{{ $row->color  }}" class="caps">{{ $row->category }}</span>
                    </figure>
                    <div class="desc a-center">
                        <span>{{ carbonLocalize($row->date) }}</span>
                        <h5>{{ $char($row->title) }}</h5>
                        <span class="link-read-more">{{ trans('news.readmore') }}</span>
                    </div>
                </a>
            @endforeach
            @foreach($new as $news)
                <a href="{{ urlLang('news/read/'.$news->slug) }}" class="list-news">
                    <figure>
                        <img src="{{ asset('contents/'.$news->image_intro) }}">
                        <span style="background-color:{{ $news->color  }}" class="caps">{{ $news->category }}</span>
                    </figure>
                    <div class="desc a-center">
                        <span>{{ carbonLocalize($news->date) }}</span>
                        <h5>{{ $char($news->title) }}</h5>
                        <span class="link-read-more">{{ trans('news.readmore') }}</span>
                    </div>
                </a>
            @endforeach
        </div>

        <div class="paging a-center">
            {!! with(new App\Pagination\WebarqPagination($new))->render(); !!}
        </div>
    </div>
</section>

@endsection

@push('scripts')
<script type="text/javascript">

    function filter()
    {
        category = $("#category").val();
        sort = $("#sort").val();

        url = '{{ urlLang("news?sort=") }}' + sort + "&category=" + category;
        document.location.href=url;
    }

</script>

@endpush