<?php


<div class="paging a-center">
	{{ $results->nextPageUrl() }}
	<a href="" class="prev disabled"><i class="icwp ic_prev_paging"></i></a>
	<a href="" class="active">1</a>
	<a href="">2</a>
	<a href="">3</a>
	<a href="">4</a>
	<span class="dots_paging">...</span>
	<a href="">10</a>
	<a href="" class="next"><i class="icwp ic_next_paging"></i></a>
</div>