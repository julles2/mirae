@extends('layouts.layout')

@section('content')

<section class="banner">
    @if(!empty($details->image_detail))
        <img src="{{ asset('contents/'.$details->image_detail) }}" alt="banner" />
    @else
        <img src="{{ assetFrontend(null) }}images/content/banner-produk.jpg" alt="banner" />
    @endif
</section>
<section class="std-content gn-content">
    <div class="wrapper myaccount">
        <div class="wrapper-content">
            <div class="box-title-top-page">
                <div class="left">
                    <label>{{ $details->category }}</label>
                </div>
                <div class="right">
                    <i>{{ date('l d F Y', strtotime($details->date)) }}</i>
                </div>
            </div>

            <h2 class="title-shadow">{{ $details->title }}</h2>
            <p>{!! $details->description !!}</p>

            <hr class="line-bgrey">
            <div class="news-bottom">
                <div class="left">
                    <div class="box-share-content">
                        <label>{{ trans('news.shareon') }}</label>
                        <a href="//www.facebook.com/sharer/sharer.php?p={{ urlLang('news/read/'.$details->slug) }}" target="_blank">
                            <img src="{{ asset('frontend') }}/images/material/ic_fb_cl.png">
                        </a>
                        <a href="//twitter.com/share?text={{ substrText($details->title,15) }}&url={{ urlLang('news/read/'.$details->slug) }}" target="_blank">
                            <img src="{{ asset('frontend') }}/images/material/ic_tw_cl.png">
                        </a>
                    </div>
                </div>
                <div class="right">
                    <a href="{{ urlLang('news')}}" class="btn-oval btn-trans-orange"><i class="icwp ic_back"></i> {{ trans('news.back') }}</a>
                </div>
            </div>
        </div>

        <div class="box-list-more-news">
            <h3 class="a-center title-blue">{{ trans('news.moreonnews') }}</h3>
            <div class="box-list-news">

            @foreach($newsMore as $newsMores)
                <a href="{{ urlLang('news/read/'.$newsMores->slug)}}" class="list-news">
                    <figure>
                        <img src="{{ asset('contents/'.$newsMores->image_intro) }}">
                        <span class="caps orange-caps">{{ $newsMores->category }}</span>
                    </figure>
                    <div class="desc a-center">
                        <span>{{ carbonLocalize($newsMores->date) }}</span>
                        <h5>{{ $newsMores->title }}</h5>
                        <span class="link-read-more">{{ trans('news.readmore') }}</span>
                    </div>
                </a>
            @endforeach
            </div>
        </div>

    </div>
</section>
@endsection
