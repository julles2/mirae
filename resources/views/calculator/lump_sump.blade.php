<form id = "pembayaran_berkala">
    <div class="box-form login">
        <div class="row">
            <div class="left">
                <label>
                    Cicilan per Bulan
                </label>
            </div>
            <div class="right">
                <input type="text" name = "cicilan_perbulan" id = "cicilan_perbulan" onkeyup="uangField(this)"/>
            </div>
        </div>
        <div class="row">
            <div class="left">
                <label>
                    Jangka Waktu
                </label>
            </div>
            <div class="right">
                <input type="text" id = "pertahun" onkeyup="year_to_month(this.value,'#perbulan')" onkeypress="return onlyNumber(event)" maxlength="2" /> / tahun
            </div>
        </div>
        <div class="row">
            <div class="left">
                <label>
                    Perbulan
                </label>
            </div>
            <div class="right">
                <input type="text" id = "perbulan" readonly="true" style="background-color:#f2f4f7;" /> 
            </div>
        </div>
        <div class="row">
            <div class="left">
                <label> 
                    Tingkat Pengembalian / tahun
                </label>
            </div>
            <div class="right">
                <input id = "tingkat_pengembalian_pertahun" onkeyup="persent_per_month(this.value,'#tingkat_pengembalian_perbulan')" onkeypress="return onlyNumber(event)" maxlength="2"  type="text"/> (%)
            </div>
        </div>
        <div class="row">
            <div class="left">
                <label> 
                    Tingkat Pengembalian / bulan
                </label>
            </div>
            <div class="right">
                <input id = "tingkat_pengembalian_perbulan" readonly="true" style="background-color:#f2f4f7;" /> (%)
            </div>
        </div>
        <div class="row btn"> 
            <div class="right">
                <div class="col">
                    <button type="button" id = "btn_calculate" class="btn-std sea" onclick="lump_sump()">Calculate</button>
                </div>
            </div>
        </div>
        <p>&nbsp;</p>
        <div class="row">
            <div class="left">
                <label>
                    Dana di akhir periode
                </label>
            </div>
            <div class="right">
                <input type="text" id = "dana" readonly="true" style="background-color:#f2f4f7;"/>
            </div>
        </div>
        
    </div>
</form>