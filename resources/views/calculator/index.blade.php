@extends('layouts.layout')
@section('content')
<!-- middle -->
<section class="banner">
    <img alt="banner" src="{{ asset(null) }}/frontend/images/content/banner-about.jpg"/>
    <div class="caption">
        <h1>
            Mirae Asset
        </h1>
    </div>
</section>
<section class="std-content">
    <div class="wrapper login-page">
        <h2 class="a-center">
            Calculator
        </h2>
        <div class="shadow-spar">
        </div>
        <div id="tabs">
            <ul>
                <li>
                    <a href="#tabs-1">
                        Pembayaran Lump Sum di Awal
                    </a>
                </li>
                <li>
                    <a href="#tabs-2">
                        Pembayaran berkala yang harus disetor
                    </a>
                </li>
                <li>
                    <a href="#tabs-3">
                        Pembyaran berkala
                    </a>
                </li>
                <li>
                    <a href="#tabs-4">
                        Skenario 4
                    </a>
                </li>
            </ul>
            <div id="tabs-1">
                @include('calculator.lump_sump')
            </div>
            <div id="tabs-2">
                <p>
                    @include('calculator.setor')
                </p>
            </div>
            <div id="tabs-3">
                <p>
                    @include('calculator.pembayaran_berkala')
                </p>
            </div>
            <div id="tabs-4">
                <p>
                    @include('calculator.calculator4')
                </p>
            </div>
        </div>
    </div>
</section>
@endsection
@include('calculator.js')
