@push('scripts')
<script>
    
    $( function() {
        $( "#tabs" ).tabs();
    });

    function toInt(variable)
    {
        return numeral(variable).value();
    }

    function toMoney(variable)
    {
        return numeral(variable).format(0,0);
    }

    function setor(target_dana,persent_bulan,bulan,selector)
    {
        persent_bulan = persent_bulan / 100;

        hasil = target_dana * (persent_bulan / ( 1 - Math.pow((1 + persent_bulan) , bulan) ) ) / (1 +  persent_bulan);
    
        round = Math.round(hasil * 100) / 100;

        result = Math.abs(round);

        $(selector).val(result);
    }

    function actionSetor()
    {
        target_dana = toInt($("#setor_target_dana").val());
        bulan = toInt($("#setor_perbulan").val());
        persent_bulan = parseFloat($("#setor_tingkat_pengembalian_perbulan").val());
        setor(target_dana,persent_bulan,bulan,'#setor_pembayaran');
    }

    function fv(cicilan,persen_tahun,tahun,selector)
    {
        future_value = cicilan * Math.pow((1 + persen_tahun / 100), tahun);

        result =  Math.round(future_value);
        
        result = toMoney(result);

        $(selector).val(result);
    }

    function lump_sump()
    {
        cicilan = toInt($('#cicilan_perbulan').val());
        persent_tahun = toInt($("#tingkat_pengembalian_pertahun").val());
        tahun = toInt($("#pertahun").val());
        fv(cicilan,persent_tahun,tahun,'#dana');
    }

    function fv_berkala()
    {
        cicilan = toInt($("#berkala_cicilan_perbulan").val());
        tahun = toInt($("#berkala_pertahun").val());
        bulan = toInt($("#berkala_perbulan").val());
        persent_tahun = parseFloat($("#berkala_tingkat_pengembalian_pertahun").val()) / 100;
        persent_bulan = parseFloat($("#berkala_tingkat_pengembalian_perbulan").val()) / 100;
        result =cicilan*((Math.pow(1+persent_bulan , bulan) - 1)/persent_bulan)*(1+persent_bulan);
        result = Math.round(result);
        $("#berkala_dana").val(toMoney(result));
    }

    function year_to_month(year,selector)
    {
    	tahun = parseInt(year);

    	month = tahun * 12;

        if(isNaN(month))
        {
        month = 0;
        }

    	$(selector).val(month);
    }

    function persent_per_month(persent,selector)
    {
    	intPersent = parseInt(persent);

    	result = intPersent / 12;

        if(isNaN(result))
        {
            result = 0;
        }

    	$(selector).val(result);
    }

    function actionCalculator4()
    {
        target_dana = toInt($("#calculator4_target_dana").val());
        tahun = toInt($("#calculator4_pertahun").val());
        bulan = toInt($("#calculator4_perbulan").val());
        persent_tahun = parseFloat($("#calculator4_tingkat_pengembalian_pertahun").val()) / 100;
        persent_bulan = parseFloat($("#calculator4_tingkat_pengembalian_perbulan").val()) / 100;
        
        result = target_dana / Math.pow((1 + persent_tahun) , tahun);
        result = Math.round(result);
        $("#calculator4_penempatan_awal").val(toMoney(result));
    }

</script>
@endpush
