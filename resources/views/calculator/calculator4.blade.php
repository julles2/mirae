<form id = "pembayaran_berkala">
    <div class="box-form login">
        <div class="row">
            <div class="left">
                <label>
                    Target Dana
                </label>
            </div>
            <div class="right">
                <input type="text" id = "calculator4_target_dana" name = "calculator4_target_dana" onkeyup="uangField(this)"/>
            </div>
        </div>
        <div class="row">
            <div class="left">
                <label>
                    Jangka Waktu
                </label>
            </div>
            <div class="right">
                <input type="text" id = "calculator4_pertahun" onkeyup="year_to_month(this.value,'#calculator4_perbulan')" onkeypress="return onlyNumber(event)" maxlength="2" /> / tahun
            </div>
        </div>
        <div class="row">
            <div class="left">
                <label>
                    Perbulan
                </label>
            </div>
            <div class="right">
                <input type="text" id = "calculator4_perbulan" readonly="true" style="background-color:#f2f4f7;" /> 
            </div>
        </div>

         <div class="row">
            <div class="left">
                <label> 
                    Tingkat Pengembalian / tahun
                </label>
            </div>
            <div class="right">
                <input id = "calculator4_tingkat_pengembalian_pertahun" onkeyup="persent_per_month(this.value,'#calculator4_tingkat_pengembalian_perbulan')" onkeypress="return onlyNumber(event)" maxlength="2"  type="text"/> (%)
            </div>
        </div>
        <div class="row">
            <div class="left">
                <label> 
                    Tingkat Pengembalian / bulan
                </label>
            </div>
            <div class="right">
                <input id = "calculator4_tingkat_pengembalian_perbulan" readonly="true" style="background-color:#f2f4f7;" /> (%)
            </div>
        </div>
        
        <div class="row btn"> 
            <div class="right">
                <div class="col">
                    <button type="button" onclick = "actionCalculator4()" id = "btn_calculate" class="btn-std sea">Calculate</button>
                </div>
            </div>
        </div>
        <p>&nbsp;</p>
        <div class="row">
            <div class="left">
                <label>
                    Penempatan Awal
                </label>
            </div>
            <div class="right">
                <input type="text" id = "calculator4_penempatan_awal" readonly="true" style="background-color:#f2f4f7;"/>
            </div>
        </div>

        
    </div>
</form>