@extends('layouts.layout')

@section('content')
<section class="banner">
    <img src="{{ assetContent(@$setting->banner_image) }}" alt="banner" />
    <div class="caption">
        <h6>{{ @$setting->title_caption }}</h6>
        <h1>{!! @$setting->title_main !!}</h1>
        <p>{{ @$setting->sub_title }}</p>
    </div>
</section>
<section class="std-content gn-content">
    <div class="wrapper myaccount">
        <div class="wrapper-content">
            <h2 class="title-shadow">{{ @$asset->title }}</h2>
            <p>{!! @$asset->description !!}</p>
        </div>
    </div>
</section>

@endsection
