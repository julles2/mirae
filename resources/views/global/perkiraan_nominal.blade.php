@if(lang() == 'id')
<small style="color:red;text-align:justify;line-height: 18px;">
    Perkiraan Nominal hanya sebagai gambaran berdasarkan NAB/Unit
    <div class="ajax_date_last">
        25-Aug-2017
    </div>
    . Nominal reksa dana yang Anda dapat akan dihitung kemudian berdasarkan NAB/Unit yang ditentukan oleh Bank Kustodian.
</small>
@else
<small style="color:red;text-align:justify;">
    <div>
            The estimate nominal is only an illustration as a result of NAV / Units per
            <div class="ajax_date_last">
                25-Aug-2017
            </div>
   	</div>
    <div style="margin-top: 5px;">
        	The nominal value of your mutual fund will be calculated later, based on the NAV / Units which determined by the Custodian Bank.
    </div>
</small>
@endif
