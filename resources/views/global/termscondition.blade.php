@extends('layouts.layout')

@section('content')

<section class="banner">
    <img src="{{ asset(null) }}contents/{{ @$setting->banner_image }}" alt="banner" />
    <div class="caption"> 
        <h6>{{ @$setting->title_caption }}</h6>
        <h1>{!! @$setting->title_main !!}</h1>
        <p>{{ @$setting->sub_title }}</p>
    </div>
</section>
<section class="std-content gn-content">
    <div class="wrapper myaccount">
        <div class="wrapper-content">
            <h2 class="title-shadow">
                {{ @$about->title }}
            </h2>

            {!! @$about->description !!}
            
        </div>
    </div>
</section>

@endsection