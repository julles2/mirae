<?php $title = !empty($title) ? $title : 'Pembelian'; ?>
<table style="font-size:12px;">
    <tr>
        <td>1.</td>
        <td>Instruksi {!! $title !!} kembali yang tercatat di sistem PT Mirae Asset Sekuritas Indonesia
        sebelum pukul 12.00 WIB akan menggunakan perhitungan NAB pada hari yang bersangkutan.</td>
    </tr>
    <tr>
        <td>2.</td>
        <td>Instruksi {!! $title !!} kembali yang tercatat di sistem PT Mirae Asset Sekuritas Indonesia setelah pukul 12.00 WIB akan menggunakan perhitungan NAB pada hari berikutnya.</td>
    </tr>
    <tr>
        <td>3.</td>
        <td>Instruksi {!! $title !!} kembali akan otomatis dibatalkan jika ketentuan saldo minimum reksa dana tidak terpenuhi.</td>
    </tr>
    <tr>
        <td>4.</td>
        <td>Bank Kustodian akan mengirimkan dana hasil {!! $title !!} kembali reksa dana selambat-lambatnya 7 (tujuh) hari bursa. Jika hingga dalam batas waktu tersebut dana belum diterima di rekening, Anda dapat menghubungi kami.</td>
    </tr>
    <tr>
        <td>5.</td>
        <td>Dana yang diterima di rekening Nasabah merupakan hasil {!! $title !!} Reksa Dana setelah dikurangi biaya {!! $title !!} dan biaya transfer (jika ada).</td>
    </tr>
</table>
