@extends('layouts.layout')

@section('content')

    <section class="banner">
    <img src="{{ assetContent(@$setting->banner_image) }}" alt="banner" />
    <div class="caption"> 
        <h6>{{ @$setting->title_caption }}</h6>
        <h1>{!! @$setting->title_main !!}</h1>
        <p>{{ @$setting->sub_title }}</p>
    </div>
</section>
<section class="std-content gn-content">
    <div class="wrapper myaccount">
        <div class="wrapper-content">
            <h2 class="title-shadow">{{ @$setting->title_content }}</h2>
            <div class="box-milestone">
                <div class="btn-milestone">
                
                @foreach($milestone as $key => $value)

                    @if ($key == 0)
                        <div class="list-milestone active">
                    @else
                        <div class="list-milestone">
                    @endif
                    
                        <a>{{ $value->year }}</a>
                        <div class="box-desc-milestone">
                            <h3>{{ $value->title }}</h3>
                            @if(!empty($value->image))
                                <figure><img src="{{ asset('contents/'.$value->image) }}"></figure>
                            @endif
                            <p>{!! $value->description !!}</p>
                        </div>
                    </div>

                @endforeach

                </div>
                <div class="desc-milestone-content"></div>
                <div class="clear"></div>
            </div>
        </div>
    </div>
</section>

@endsection