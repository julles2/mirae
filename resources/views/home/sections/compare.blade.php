<section class="h-compare">
    <div class="wrapper">
        <div class="top">
            <h2 class="left">{{ trans('home.mutualfundcompare') }}</h2>
            <!--div class="right">
                <label>Kategori</label>
                <select>
                    <option>Semua</option>
                </select>
            </div-->
        </div>
      
        {!! Form::open(['method'=>'post','url'=>urlLang("compare/store-fund")]) !!}
            <div class="relative">
                <div class="col-3">
                @for($i=0;$i<3;$i++)
                    <div>
                        {!! Form::select('im_compare[]',$repo->selectIm(),null,['class'=>'select2','onchange'=>"changeReksaDana(this.value,'$i')"]) !!}
                        {!! Form::select('fund_compare[]',[trans('home.selectmutualfund')],null,['class'=>'select2','id'=>"fund_compare$i"]) !!}
                    </div>
                @endfor
                </div>
                <div class="btn">
                    <input type="submit" value="{{ trans('home.compare') }}" class="btn-std"/>
                </div>
            </div>
        {!! Form::close() !!}
    </div>
</section>

@push('scripts')
<script type="text/javascript">
    function changeReksaDana($im_id,$counter)
    {
        $.ajax({
            url: '{{ urlLang("compare/ajax-change-reksa-dana") }}',
            data: {
                im_id: $im_id,
            },
            success: function(respon){
                $("#fund_compare" + $counter).html("");
                $("#fund_compare" + $counter).html(respon.result);
            },

        });
    }
</script>
@endpush
