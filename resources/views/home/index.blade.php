@extends('layouts.layout')

@section('content')
<?php $page = "home" ?>
<!-- middle -->
<section class="banner-float">
    <div class="left">
        <div class="slider">

            @foreach($mainSlider as $mainSliders)
            <?php
            $sliders = $slider(lang(), $mainSliders->id);
            ?>
            <div>
                <img src="{{ asset(null) }}contents/{{ @$mainSliders->image }}" alt="banner" />
                <div class="caption">
                    <h2>{{ @$sliders->title }}</h2>
                    <p>
                        {!! @$sliders->wording !!}
                    </p>
                    @if(!empty($sliders->status) && $sliders->status == "y")
                    @if(strpos($sliders->url, 'http') !== false)
                    <a href="{{ @$sliders->url }}" class="btn-border white">{{ $sliders->button_caption }}</a>
                    @else
                    <a href="http://{{ @$sliders->url }}" class="btn-border white">{{ $sliders->button_caption }}</a>
                    @endif
                    @endif
                </div>
            </div>
            @endforeach
        </div>
    </div>
    <div class="right return-harian wrap-tab">
        <div class="nav-tab">
            <a href="#return-tab1" class="active">{{ trans('home.returndaily') }}</a>
            <a href="#return-tab2">{{ trans('home.findproduct') }}</a>
        </div>
        <div class="content-tab" id="return-tab1">
            <h4>{{ trans('home.returndailyhigh') }}</h4>
            <div class="wrap-scroll">
                <table>
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>{{ trans('home.mutualfundname') }}</th>
                            <th>{{ trans('home.return') }}</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @php($no=0)
                        @foreach($returnHarian as $row)

                        @php($no++)
                        <tr>
                            <td class="{{ $row['icon'] }}">{{ $no }}</td>
                            <td>
                                {{ $row['reksa_dana'] }}
                                <span>NAV {{ valueDecimal($row['nav']) }}</span>
                            </td>
                            <td>{{ valueDecimal($row['return']) }}%</td>
                            <td>
                                <a href="{{ urlLang('product?pop='.$row['fund_cd']) }}">{{ trans('home.buy') }}</a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <div class="content-tab" id="return-tab2">
            <div class="wrap-scroll">
                <div class="box-form find-product">
                    <h2>{{ trans('home.findproduct') }}</h2>
                    {!! Form::open(['method'=>'get','url'=>lang().'/product']) !!}
                    <div class="row">
                        {!! Form::select('type',$selectTypeReksaDana,null,['class'=>'']) !!}
                    </div>
                    <div class="row">
                        {!! Form::select('category',$repo->categories(),null) !!}
                    </div>
                    <div class="row">
                        <div class="col"><input type="text" name = "return" placeholder="Return(%)" /></div>
                        <div class="col">{!! Form::select('time',$repo->listTime(),null) !!}</div>
                    </div>
                    <div class="row bt">
                        <input type="submit" value="{{ trans('home.find') }}" class="btn-std sea"/>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</section>
<section class="h-step">
    <div class="wrapper">
        @foreach($mainStep as $mainSteps)
        <?php
        $step = $steps(lang(), $mainSteps->id);
        ?>
        <div class="box">
            <div class="ico"><img src="{{ asset(null) }}contents/{{ @$mainSteps->icon }}" alt="{{ @$step->title }}"/></div>
            <div class="text">
                <h6>{{ @$step->title }}</h6>
                {!! @$step->wording !!}
            </div>
        </div>
        @endforeach
    </div>
</section>
<section class="h-product">
    <div class="wrapper">
        <div class="filter">
            <div class="col">
                {!! Form::select('tipe',$selectTypeReksaDana,null,['id'=>'beli_type','onchange'=>'change_fund()','class'=>'select2']) !!}
            </div>
            <div class="col">
                {!! Form::select('im',$repo->selectIm(),null,['id'=>'beli_im_id','onchange'=>'change_fund()','class'=>'select2']) !!}
            </div>
            <div class="col">
                {!! Form::select('fund',$repo->selectFund(),null,['id'=>'beli_fund','class'=>'select2']) !!}
            </div>
            <div class="col">
                <input type="button" onclick="beli()" value="{{ trans('home.buy') }}" class="btn-std">
            </div>
        </div>
        <h2>{{ trans('home.dailymutualfundperformance') }}</h2>
        <p>
            {{ trans('home.wording_daily_mutual') }}
        </p>

        <div class="category-prod">
            <a id = "category_prod_EQ" onclick="changeType('EQ')" href="javascript:" class="active"><span style="background:url({{ assetFrontend(null) }}images/material/cat-prod-1.png)"></span>{{ trans('type_fund.eq') }}</a>
            <a id = "category_prod_MX" onclick="changeType('MX')" href="javascript:"><span style="background:url({{ assetFrontend(null) }}images/material/cat-prod-2.png)"></span>{{ trans('type_fund.mx') }}</a>
            <a id = "category_prod_FI" onclick="changeType('FI')" href="javascript:"><span style="background:url({{ assetFrontend(null) }}images/material/cat-prod-3.png)"></span>{{ trans('type_fund.fi') }}</a>
            <a id = "category_prod_SR" onclick="changeType('SR')" href="javascript:"><span style="background:url({{ assetFrontend(null) }}images/material/cat-prod-4.png)"></span>{{ trans('type_fund.sr') }}</a>
            <a id = "category_prod_MM" onclick="changeType('MM')" href="javascript:"><span style="background:url({{ assetFrontend(null) }}images/material/cat-prod-5.png)"></span>{{ trans('type_fund.mm') }}</a>
        </div>
        <div class="shadow-spar"></div>
        <div class="list-product" id="box_performa_reksa_dana_harian">
            @foreach($repo->performaReksaDanaHarian() as $row)
            <?php
            $fundCompany = $fundImage($row->fund_cd);
            $checkwish = $checkWishlist($row->fund_cd, @acnt_no());
            $reksaDanaImage = !empty($fundCompany->image) ? asset('contents/' . $fundCompany->image) : asset('noimage.png');
            ?>
            <div class="box">
                <div class="name">
                    <p>{{ $row->im_nm }}</p>
                </div>
                <div class="desc">
                    <div class="ico">
                        <img src="{{ $reksaDanaImage }}" alt="prod" />
                    </div>
                    <div class="text">
                        <h5 style="font-size:12px;">{{ $row->fund_name }}</h5>
                        <p>NAB   <span>{{ valueDecimal($row->nav_per_unit) }}</span></p>
                    </div>
                </div>
                <div class="return">
                    <p>Return (%)</p>
                </div>
                <div class="float-3">
                    <div>
                        <p>1 {{ trans('global.dy') }}  <span>{{ valueDecimal($row->one_day) }}</span></p>
                    </div>
                    <div>
                        <p>1 {{ trans('global.mt') }}  <span>{{ valueDecimal($row->one_month) }}</span></p>
                    </div>
                    <div>
                        <p>1 {{ trans('global.yr') }}  <span>{{ valueDecimal($row->one_years) }}</span></p>
                    </div>
                </div>
                <a onclick="addWatchLists('{{ $row->fund_cd }}')" {!! $checkwish !!} class="watch-list">{{ trans('home.addwatchlist') }}</a>
                <div class="btn">
                    <a href="{{ urlLang('product?pop='.$row->fund_cd) }}">{{ trans('home.buy') }}</a>
                    <a href="javascript::void(0)" onclick="compareHome('{{ $row->fund_cd }}')">{{ trans('home.compare') }}</a>
                </div>
            </div>
            @endforeach
            {!! Form::open(['id'=>'form_comare','method'=>'post','url'=>urlLang("compare/store-fund")]) !!}
                {!! Form::hidden('fund_compare[]',null,['id'=>'fund_compare']) !!}
            {!! Form::close() !!}
            @push('scripts')
                <script type="text/javascript">
                    function compareHome(fund)
                    {
                        $("#fund_compare").val(fund);
                        $("#form_comare").submit();
                    }
                </script>
            @endpush
        </div>
        <div class="a-center">
            <a href="{{ urlLang('product') }}" class="btn-border "> {{ trans('home.viewall') }}</a>
        </div>
        <div class="shadow-spar">

        </div>
    </div>
</section>
@include("home.sections.compare")
<section class="h-how">
    <div class="box-video left video">
        <?php
        $thumbnail = !empty(@$video->url) ? youtube_thumbnail(@$video->url) : assetFrontend('images/content/placeholder-vid.jpg');
        ?>
        <img style = "width:1275px;height: 520px;" src="{{ $thumbnail }}" alt="placeholder video" class="thumb"/>
        <div class="caption">
            {!! @$video->description !!}
            <a class="play">play</a>
        </div>
        <div class="vids">
            @php
            if (strpos(@$video->url, '?rel=0') !== false) {
                $video = @$video->url;
            }else{
                $video = @$video->url.'?rel=0';
            }
            @endphp
            <iframe id="thisvids" src="{{ $video }}" frameborder="0" allowfullscreen=""></iframe>
        </div>
    </div>

<!--    <div class="left video" youtube-id="asdf23as">
        <div class="caption">
            <h3>{{ trans('home.how_it_works') }}</h3>
            <p>{{ trans('home.learn_how') }}</p>
            <a href="" class="play">play</a>
        </div>
    </div>-->

    <div class="right">
        <h4>{{ trans('home.customervoice') }}</h4>
        {!! Form::open(['method'=>'post','url'=>urlLang('home'),'class'=>'box-form','id' => 'contact_us']) !!}
        <div class="row">
            {!! Form::text('name',null,['placeholder' => trans('home.yourname'),'id' => 'contact_name','class'=>'form-control']) !!}
        </div>
        <div class="row">
            {!! Form::text('email',null,['placeholder' => 'E-Mail','id' => 'contact_email','class'=>'form-control']) !!}
        </div>
        <div class="row">
            {!! Form::textarea('message' , null ,['placeholder' => trans('home.message'),'id' => 'contact_message','class'=>'form-control']) !!}
        </div>

        <div class="row bt">
            <!-- {!! app('captcha')->display(); !!} -->
            <input type="submit" value="{{ trans('home.send') }}" class="hide btn-oval btn-blue"/>
        </div>
        {!! Form::close() !!}
        <?php /* {!! JsValidator::formRequest('App\Http\Requests\Backend\CompanyRequest') !!} */ ?>
    </div>
</section>
<section class="h-news">
    <div class="wrapper">
        <h3 class="left">{{ trans('home.newsresearch') }}</h3>
        <a href="{{ urlLang('news') }}" class="btn-border right" >{{ trans('home.viewall') }}</a>

        <div class="clear"></div>

        <div class="list-news">
            @foreach($mainNews as $row)
                <div class="box" onclick="document.location.href = '{{ urlLang('news/read/'.@$row->slug)}}'">
                    <div class="image">
                        <img src="{{ asset(null) }}contents/{{ @$row->image_intro }}" alt="image" />
                        <span style="background-color:{{ $row->color }}">{{ $row->category }}</span>
                    </div>
                    <div class="text">
                        <small>{{ date('l d F Y', strtotime(@$row->date)) }}</small>
                        <h6>{{ @$row->title }}</h6>
                        <a href="{{ urlLang('news/read/'.@$row->slug)}}">{{ trans('home.readmore') }}</a>
                    </div>
                </div>
            @endforeach
        </div>

    </div>
</section>
<!-- end of middle -->
@endsection
@include("home.scripts")
