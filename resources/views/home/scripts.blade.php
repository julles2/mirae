@push('scripts')

<script type="text/javascript">
    function change_fund()
    {
        im_id = $("#beli_im_id").val();
        type = $("#beli_type").val();

        $.ajax({
            url: '{{ urlLang("home/ajax-fund") }}',
            data: {
                im_id: im_id,
                type: type,
            },
            success: function(respon){
                $("#beli_fund").html(respon.result);
            },
            async: false,
        });
    }

    function changeType(type)
    {
        $.ajax({
            url: '{{ urlLang("home/ajax-change-type") }}',
            data: {
                type: type,
            },
            beforeSend: function(){
                // showPop('loaderpopup');
            },
            success: function(respon){
                $("#box_performa_reksa_dana_harian").html(respon.result);
                $('[id^=category_prod_]').removeClass('active');
                $("#category_prod_" + type).attr('class','active');
                
                // closePop('loaderpopup');
            },
        });

    }

    function contact_us()
    {
        var name = $("#contact_name").val();
        var email = $("#contact_email").val();
        var message = $("#contact_message").val();

        $.ajax({
            url: '{{ urlLang("home/contact_us") }}',
            data: {
                name: name,
                email: email,
                message: message,
            },
            beforeSend: function(){
                // showPop('loaderpopup');
            },
            success: function(respon){
                alert('{{ trans("home.data-has-been-sent") }}');
                // closePop('loaderpopup');
            },
        });

    }

    function beli()
    {
        $fund = $("#beli_fund").val();

        if($fund == "")
        {
            alert("Anda belum memilih reksa dana");
        }else{
            document.location.href="{{ urlLang('product?pop=') }}" + $fund;
        }
    }
    
</script>
@endpush
