@if($model->count() > 0)
        @foreach($model as $row)
        <?php
            $fundCompany = $fundImage($row->fund_cd);
        ?>
                <div class="box">
                    <div class="name">
                        <p>{{ $row->im_nm }}</p>
                    </div>
                    <div class="desc">
                        <div class="ico">
                            <img src="{{ asset(null) }}contents/{{ @$fundCompany->image }}" alt="prod" />
                        </div>
                        <div class="text">
                            <h5 style="font-size:12px;">{{ $row->fund_name }}</h5>
                            <p>NAB   <span>{{ valueDecimal($row->nav_per_unit) }}</span></p>
                        </div>
                    </div>
                    <div class="return">
                        <p>Return (%)</p>
                    </div>
                    <div class="float-3">
                        <div>
                            <p>1 Hr  <span>{{ valueDecimal($row->one_day) }}</span></p>
                        </div>
                        <div>
                            <p>1 Bln  <span>{{ valueDecimal($row->one_month) }}</span></p>
                        </div>
                        <div>
                            <p>1 Yr  <span>{{ valueDecimal($row->one_years) }}</span></p>
                        </div>
                    </div>
                    <a href="" class="watch-list">Add To Watchlist</a>
                    <div class="btn">
                        <a href="{{ urlLang('product?pop='.$row->fund_cd) }}">BELI</a>
                        <a href="">BANDINGKAN</a>
                    </div>
                </div>
        @endforeach
@else
    <h3 style="text-align:center;">DATA TIDAK ADA</h3>
@endif
