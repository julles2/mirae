<p>Mirae Asset Sekuritas Indonesia mengucapkan terima kasih kepada Anda semua atas kepercayaan Anda untuk memilih kami sebagai partner kepercayaan Anda dalam berinvestasi saham.</p>
<p>Sebagai bentuk terima kasih atas kepercayaan Anda, kami ingin menginformasikan bahwa kami memiliki program reward yang kami sebut dengan Mirae Asset Sekuritas Indonesia Mileage Program.</p>
<p>
	Anda akan mendapatkan mileage secara otomatis setiap kali anda bertransaksi melalui Mirae Asset Sekuritas Indonesia sebesar: <br/>
	2% X biaya transaksi bersih <br>
	(1% bagi nasabah remisier). <br>
</p>

<p>
	30% dari nilai Mileage yang Anda miliki akan kami donasikan untuk digunakan sebagai program amal(berupa beasiswa, bantuan sekolah, dan lain-lain). Mirae Asset Sekuritas Indonesia juga akan mendonasikan sejumlah nominal yang sama sesuai dengan yang diberikan nasabah.
</p>

<p>
	Per 31 Maret 2018 kami akan menghentikan program mileage dan memberikan Anda kesempatan untuk menukarkan poin mileage Anda mulai sejak 9 mei 2018 hingga 2 Juli 2018.Jumlah Mileage yang ditampilkan adalah jumlah Mileage yang tersedia setelah dikurangi 30% untuk program amal
</p>

<p>
	Akan ada 2 opsi penukaran mileage poin yang Anda miliki yaitu Cashback dan Invest. Adapun penjelasannya sebagai berikut: <br/>
	1. CASHBACK <br> 
	Dana akan langsung masuk ke RDN Anda setelah dipotong pajak <br> 
	Anda akan dikenakan pajak final sebesar 25% <br> 
	Dapat ditukarkan melalui sistem HOTS<br/> <br>
	2. INVEST <br> 
	Poin akan dikonversikan menjadi kepemilikan reksadana <br>
	Tidak ada pajak yang dibebankan kepada anda <br>
	Melalui max.miraeasset.co.id
</p>

<p>
	<a style="font-size: 18px;text-decoration: underline;" href="{{ urlLang('user/login') }}"><b>{{ strtoupper("Saya ingin menukarkan mileage point saya") }}</b></a>
</p>