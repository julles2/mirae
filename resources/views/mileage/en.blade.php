<p>Mirae Asset Sekuritas Indonesia would like to thank you for your trust to choose us as your investment partner. As a form of gratitude for your trust, we would like to inform you that we have a reward program called “Mirae Asset Sekuritas Indonesia Mileage Program”. </p>
<p>You will automatically get a mileage point every time you do buy and/or sell transaction through Mirae Asset Sekuritas Indonesia which is 
	2% from your net transaction fee 
	(1% for remiser customers). 
</p>
<p>30% of the Mileage point you have will be donated  to a charity program (i.e scholarships, school assistance, etc.). </p>
<p>As of March 31st, 2018 we will terminate the mileage program and you can redeem your mileage points starting from April 2nd, 2018 to July 2nd, 2018. </p>
<p>This mileage point redemption process can be made through our website at www.miraeasset.co.id. The number of Mileage shown is the number of available Mileage after minus 30% for the charity program.</p>

<p>
	There will be 2 mileage point redemption options avaialble <br>
	1. CASHBACK <br> 
	The money will be directly transfered to customer's RDN <br>
	the tax will be paid by customer <br>
	Via HOTS <br> <br>
	2. INVEST <br> 
	The point will be converted to mutual fund products 
	<br> There will not be tax paid by customer 
	<br> via max.miraeasset.co.id 
</p>

<p>
	<a style="font-size: 18px;text-decoration: underline;" href="{{ urlLang('user/login') }}"><b>{{ strtoupper("I want to redeem my mileage point") }}</b></a>
</p>