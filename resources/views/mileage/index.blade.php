@extends('layouts.layout')

@section('content')
<!-- middle -->
<section class="banner">
    <img src="{{ asset('frontend') }}/images/content/banner-about.jpg" alt="banner" />
    <div class="caption"> 
        <h6>Mileage</h6>
        <h1>{{ lang() == 'en' ? 'About' : 'Tentang' }} <br> Mileage</h1>
        {{-- <p>Lorem Ipsum is simply dummy text of the printing.</p> --}}
    </div>
</section>
<section class="std-content gn-content">
    <div class="wrapper myaccount">
        <div class="wrapper-content">
            {{-- <h2 class="title-shadow">Mirae Asset Sekuritas merupakan salah satu pemain terbaik dan terdepan di Pasar Modal Indonesia.</h2> --}}
            @include("mileage.".lang())
        </div>
    </div>
</section>
<!-- end of middle -->

@endsection