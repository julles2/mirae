@extends('layouts.layout')

@section('content')

<section class="banner">
    <img src="{{ assetFrontend(null) }}images/content/banner-individusyar.jpg" alt="banner" />
    <div class="caption">
        <h6>Search</h6>
        <h1>Search Result</h1>
        <p><!-- News --></p>
    </div>
</section>
<section class="std-content gn-content">
    <div class="wrapper myaccount">
        <div class="wrapper-content">

        <h2 style="text-align:center;">We Found {{ $jumlah }} Search Result.</h2>

            <div class="box-accordion">
                @foreach($search as $data)
                <div class="list-accordion">
                    <div class="btn-acc">
                        {!! str_replace(strtolower($parameter),"<b style='color:red;'>".strtolower($parameter)."</b>",strtolower($data->title)) !!}
                        <span class="arrow-acc"></span>
                    </div>
                    <div class="box-acc">
                        {!! str_replace(strtolower($parameter),"<b style='color:red;'>".strtolower($parameter)."</b>",strtolower($data->description)) !!}
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>
</section>
@endsection
