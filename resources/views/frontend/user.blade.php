<?php
/**
 * Created by PhpStorm.
 * User: DanielSimangunsong
 * Date: 5/17/2017
 * Time: 3:14 PM
 */ ?>

@include('layouts.header')

<section class="banner">
    <img src="{{ asset('frontend/images/content/banner-dashboard.jpg')  }}" alt="banner"/>

    <div class="caption center">
        <h6>{{ trans('global.my_account') }}</h6>
        <figure><img src="{{ $imageUser }}"></figure>
        <h2>Hello, {{ $broker->custNm }}</h2>

        <p>
                <i>{{ trans('global.account_number') }}: {{ acnt_no() }}</i> <br/>
                <i>{{ trans('global.your_risk_profile') }}: {{ $globalRiskProfile }}</i> <br/>
        </p>
    </div>
</section>
<section class="std-content">
    <div class="wrapper myaccount">
        <div class="wrap-tab-general">
            @include('frontend.users.menu', ['vMenu' => $vMenu])
            @if($statusUser == 'completed')
                @yield('content')
            @elseif($statusUser == 'pending')
                <h2>Your Status User is Pending</h2>
            @elseif($statusUser == 'idexpired')
				<h3>{{ trans('global.idexpired') }}</h3>
                <p style="margin-top:30px;"><a href="https://www.miraeasset.co.id/hmpg/custSvc/faq/updateData.do?lnbIdx=4" target="_blank" rel="nofollow" class="btn-std">Update the data ID</a></p>
			@else
                <h3>Your status is not complete, please complete your data first</h3>
                <p style="margin-top:30px;"><a href="{{ urlLang('user/question') }}" class="btn-std">Complete the data</a></p>
            @endif


        </div>

    </div>

</section>
@stack('content2')
@yield('content2')

@include('layouts.footer')
