<?php
/**
 * Created by PhpStorm.
 * User: DanielSimangunsong
 * Date: 5/17/2017
 * Time: 1:08 PM
 */ ?>
@extends('layouts.layout')

@section('content')
    <section class="banner">
        <img src="{{ asset('frontend/images/content/banner-produk.jpg') }}" alt="banner"/>

        <div class="caption">
            <h6>LOGIN</h6>

            <h1>{!! trans('login.welcome') !!}</h1>

            <p>&nbsp;</p>
        </div>
    </section>
    <section class="std-content">
        <div class="wrapper login-page">
            <h2 class="a-center">Login PIN</h2>

            <div class="shadow-spar"></div>
            {!! Form::open(['url' => $url]) !!}
                <div class='box-form login'>
                    <div class="row">
                        <div class="left" style="margin-left:10px;">
                            <label>Your Account Number:</label>
                        </div>
                        <div class="right" >
                            <label>{{ acnt_no() }}</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="left">
                            <label>PIN</label>
                        </div>
                        <div class="right">
                            <input name="pwd" value = "" type='password'/>
                        </div>
                    </div>
                    <div class="row btn haslink">
                        <div class="right">
                            <div class="col">
                                <input type="submit" value="{{ trans('login.confirm') }}" class="btn-std sea"/>
                                {{-- <a href="" class="reset-pin">reset pin</a> --}}
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </section>
@endsection
@push('scripts')
<script type="text/javascript">
    $(document).ready(function(){
        $.ajax({
            url: '{{ urlLang("user/cek-login") }}',
            success: function(response){
                if(response.result == 'true')
                {
                    document.location.href='{{ urlLang("user/portfolio") }}';
                }
            },
        });
    });
</script>
@if(!empty($errorMsg))
    <script type="text/javascript">
        swal_error('{{$errorMsg}}');
    </script>
@endif

@endpush
