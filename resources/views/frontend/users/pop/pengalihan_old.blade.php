<div class="wrap-popup product-detail pengalihan" id="pengalihan">

    <div class="head"> 
        <h5>Pengalihan Reksa Dana</h5>
        <a href="javascript:closePop('pengalihan')" class="close">close</a>
    </div>
    <div class="content">
        <div class="title-content after_clear">
            <h3 class="left">Pengalihan</h3>
        </div>
        {!! Form::open(['url'=>urlLang("user/portfolio/pengalihan") , "type"=>'post',"id"=>"form_pengalihan"]) !!}
        <div class="myaccount box-content-popup">
            <input type="hidden" class="ajax_fund_cd" name = "from_fund">
            
            <div class="box-list-data">
                <div class="list-data list_2">
                    <div class="inner-list-data">
                        <div class="box-title-list">
                            <div class="left">
                                <figure><img src="{{ asset('noimage.png') }}"></figure>
                            </div>
                            <div class="right">
                                <h5 class="ajax_fund_name">Avrist Balanced Amar Syariah</h5>
                                <p class="ajax_im_name">Avrist Asset Management, PT</p>
                            </div>
                        </div>
                        <div class="desc-list">
                            <div class="row">
                                <div class="list list_6">
                                    <div class="inner-list-data">
                                        <label>Nilai Investasi Semula</label>
                                        <strong class="ajax_nilai_investasi_semula">250.000,00</strong>
                                    </div>
                                </div>
                                <div class="list list_4">
                                    <div class="inner-list-data">
                                        <label>Return</label>
                                        <strong><i class="icwp ic_up"></i> <span class="ajax_return">16,82</span></strong>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="list list_6">
                                    <div class="inner-list-data">
                                        <label>NAB/UNIT Terakhir</label>
                                        <span class="ajax_date_last">Per 30-Nov-2016</span>
                                        <strong class="ajax_last_nab">-</strong>
                                    </div>
                                </div>
                                <div class="list list_4">
                                    <div class="inner-list-data">
                                        <label>Jumlah Unit</label>
                                        <strong class="ajax_unit">168,0211</strong>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="list list_6">
                                    <div class="inner-list-data">
                                        <label>Perkiraan Nilai Investasi Terakhir</label>
                                        <strong class="ajax_nilai_investasi_terakhir">IDR 1.738,1740</strong>
                                    </div>
                                </div>
                                <div class="list list_4">
                                    <div class="inner-list-data">
                                        <label>Gain/Loss</label>
                                        <strong class="ajax_gain_los">168,0211</strong>
                                    </div>
                                </div>
                            </div>
                            <!--                            <div class="row">
                                                            <div class="list">
                                                                <div class="inner-list-data a-center">
                                                                    <label>Perkiraan Nilai Investasi Terakhir</label>
                                                                    <strong class="ex-blue">IDR 292.049,98</strong>
                                                                </div>
                                                            </div>
                                                        </div>-->
                        </div>
                    </div>
                </div>
                <div class="list-data list_2 std-content">
                    <div class="desc-list row_btm">
                        <div class="row no-border">
                            Reksa Dana Tujuan
                        </div>
                        <div class="row no-border in_full">
                            {!! Form::select('to_fund',[''=>'Tidak ada fund yang bisa di pilih'],['class'=>'select2','id'=>'list_fund']) !!}
                        </div>
                        <div class="row no-border bg-transorange">
                            <div class="left">
                                <label>NAB Terakhir</label><br>
                                <span class="tsmall" id = "ajax_to_date_last">Per -</span>
                            </div>
                            <div class="right a-right">
                                <h5 id="ajax_to_nab_last">-</h5>
                            </div>
                        </div>
                        <div class="row no-border">
                            <div class="left">Masukan Nilai</div>
                            <div class="right">
                                <label class="radio-btn"><input type="radio" value = "nominal" name="order_type"/>Nominal</label>
                                <label class="radio-btn"><input type="radio" value = "unit" name="order_type"/>Unit</label>
                            </div>
                        </div>
                        <div class="row no-border">
                            <input type="text"  name="nilai" class="a-right">
                        </div>
                        <div class="row no-border">    
                            <label class="radio-btn"><input type="radio" value = "all" name="order_type"/>Semua Unit</label>
                        </div>

<!--                        <div class="row no-border a-right">
                            <input type="submit" name="" value="CONFIRM" class="btn-oval btn-orange">
                        </div>-->
                    </div>
                </div>
                <div class="clear"></div>
                <div class="row no-border">
                    <div class="disclaimer-box rounded">
                        <h4>Penyangkalan Transaksi </h4>
                        <p>
                            Reksa Dana adalah produk pasar modal dan
                            bukan merupakan produk bank. Produk ini
                            tidak dijamin oleh bank dan tidak termasuk
                            simpanan yang merupakan objek program
                            penjaminan pemerintah atau penjaminan
                            simpanan.
                            Reksa dana bukan merupakan produk PT
                            Mirae Asset Sekuritas Indonesia akan tetapi
                            merupakan produk yang dikelola oleh
                            Manajer Investasi.
                            PT Mirae Asset Sekuritas Indonesia hanya
                            berkedudukan sebagai agen penjual efek
                            reksa dana.

                            PT Mirae Asset Sekuritas Indonesia tidak
                            bertanggung jawab atas keterlambatan
                            penyampaian instruksi nasabah ke Manajer
                            Investasi yang diakibatkan oleh gangguan
                            jaringan atau hal-hal yang berkaitan
                            dengan keadaan di luar kemampuan (force
                            majeure). 

                            Berinvestasi di reksa dana mengandung
                            resiko. Calon pemodal wajib untuk
                            membaca, memahami dan menyetujui
                            prospektus sebelum memutuskan untuk
                            berinvestasi melalui reksa dana. Kinerja
                            masa lalu tidak bisa dijadikan acuan
                            sebagai kinerja masa datang.

                            PT Mirae Asset Sekuritas tidak bertanggung
                            jawab atas segala kerugian, kehilangan,
                            tuntutan maupun gugatan yang dialamiNasabah yang mungkin timbul sebagai
                            akibat dari penyampaian informasi
                            Nasabah yang tidak lengkap termasuk
                            tetapi tidak terbatas pada pembatalan atau
                            perubahan instruksi yang disampaikan
                            kepada PT Mirae Asset Sekuritas Indonesia. 

                            Dengan menggunakan layanan jual dan beli
                            reksa dana melalui PT Mirae Asset Sekuritas
                            Indonesia sebagai agen penjual efek Reksa
                            Dana, maka Nasabah bertanggung jawab
                            penuh atas segala tindakan yang dilakukan
                            serta membebaskan PT Mirae Asset
                            Sekuritas Indonesia, termasuk anak
                            Perusahaan, afiliasi, pemegang saham,
                            direksi, pejabat, karyawan sepenuhnya dari
                            segala tuntutan dan kerugian, pembayaran,
                            maupun ongkos apapun yang timbul di
                            kemudian hari yang diajukan kepada PT
                            Mirae Asset Sekuritas Indonesia baik secara
                            langsung maupun tidak langsung,
                            termasuk tetapi tidak terbatas pada biaya
                            penasehat hukum dan biaya perkara, yang
                            berhubungan dengan dilaksanakannya
                            atau tidak dilaksanakannya instruksi dari
                            Nasabah oleh Manager Investasi.
                        </p>
                    </div>
                </div>                
                <br/><br/>
                <div class="row no-border a-right">
                    <button type="button" onclick = "validateFormPengalihan();"  class="btn-oval btn-orange">
                        CONFIRM
                    </button>
                </div>
                <br/><br/>
            </div>
        </div>
        {!! Form::close() !!}
    </div>

</div>