{!! Form::open(['id'=>'formBeli','url'=>lang().'/user/portfolio/penjualan','method'=>'post']) !!}
<input type="hidden" name = "fund_cd" style="" id = "penjualan_fund_cd" class="ajax_fund_cd" />
<div class="wrap-popup product-detail penjualan col-1" id="penjualan">
    <div class="head">
        <h5>{{ trans('login.mutualfundsales') }}</h5>
        <a href="javascript:closePop('penjualan')" class="close">{{ trans('login.close') }}</a>
    </div>
    <div class="content">
        <div class="title-content after_clear">
            <h3 class="left">{{ trans('login.productinfo') }}</h3>
        </div>
        <div class="myaccount box-content-popup">
            <div class="box-list-data">
                <div class="list-data list_2">
                    <div class="inner-list-data">
                        <div class="box-title-list">
                            <div class="left">
                                <figure><img class="ajax_image"></figure>
                            </div>
                            <div class="right">
                                <h5 class="ajax_fund_name">Avrist Balanced Amar Syariah</h5>
                                <p class="ajax_im_name">Avrist Asset Management, PT</p>
                            </div>
                        </div>
                        <div class="desc-list">
                            <div class="row">
                                <div class="list list_6">
                                    <div class="inner-list-data">
                                        <label>{{ trans('login.originalinvestmentvalue') }}</label>
                                        <strong class="ajax_nilai_investasi_semula">250.000,00</strong>
                                    </div>
                                </div>
                                <div class="list list_4">
                                    <div class="inner-list-data">
                                        <label>Return</label>
                                        <strong><i class="icwp ic_up"></i> <span class="ajax_return">16,82</span></strong>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="list list_6">
                                    <div class="inner-list-data">
                                        <label>NAB/{{ trans('login.lastunit') }}</label>
                                        <span class="ajax_date_last">Per 30-Nov-2016</span>
                                        <strong class="ajax_last_nab">IDR 1.738,1740</span></strong>
                                    </div>
                                </div>
                                <div class="list list_4">
                                    <div class="inner-list-data">
                                        <label>{{ trans('login.totalunit') }}</label>

                                        <strong class="ajax_unit">168,0211</strong>
                                        <span style="font-size:10px;" class = "ajax_unit_temporary"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="list list_6">
                                    <div class="inner-list-data">
                                        <label>Biaya Penjualan</label>
                                        <strong class="ajax_biaya">IDR 1.738,1740</span></strong>
                                    </div>
                                </div>
                                <div class="list list_4">
                                    <div class="inner-list-data">
                                        <label>Minimum Saldo</label>
                                        <strong class="ajax_balance"></strong>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="list list_6">
                                    <div class="inner-list-data">
                                        <label>{{ trans('login.estimatedvalueofthelastinvestment') }}</label>
                                        <strong class="ajax_nilai_investasi_terakhir">IDR 1.738,1740</strong>
                                    </div>
                                </div>
                                <div class="list list_4">
                                    <div class="inner-list-data">
                                        <label>Gain/Loss</label>
                                        <strong class="ajax_gain_los">168,0211</strong>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="list list_12">
                                    <div class="inner-list-data">
                                        <label>Minimum Redeem</label>
                                        <strong class="ajax_min_redm">IDR 1.738,1740</strong>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="list-data list_2 std-content">
                    <form>
                        <div class="desc-list row_btm">

                            <div class="row no-border">
                                <div class="left">Unit Redemption</div>
                            </div>
                            <div class="row no-border">
                                <input type="text" class = "unit" onkeyup = "return changeToAmount(this.value)" name="penjualan_unit" class="a-right">
                            </div>

                            <div class="row no-border">
                                <div class="left">{{ trans('login.amount') }}</div>
                            </div>

                            <div class="row no-border">
                                <input type="text" class = "amount" onkeyup = "return changeToUnit(this.value)" name="penjualan_amount" class="a-right">
                            </div>
                            <div class="row no-border">
                                <label class="radio-btn"><input onclick = "return changeToall()" type="checkbox" name="order_type" class = "box_all" value = "all"  />{{ trans('login.allunit') }}</label>
                            </div>
                            <div class="row no-border">
                            @include("global.perkiraan_nominal")
                            </div>
                            <div class="row no-border">
                                <div class="disclaimer-box rounded">
                                    <h4>{{ trans('disclaimer.title_transaksi') }}</h4>
                                    <p>
                                        {!! trans('disclaimer.transaksi') !!}
                                    </p>
                                </div>
                            </div>
                            <div class="row no-border a-right">
                                <button type="button" onclick = "validateForm()" class="btn-oval btn-orange">{{ trans('login.confirm') }}</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

</div>
{!! Form::close() !!}
