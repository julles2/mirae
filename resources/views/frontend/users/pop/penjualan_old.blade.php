{!! Form::open(['id'=>'formBeli','url'=>lang().'/user/portfolio/penjualan','method'=>'post']) !!}
<input type="text" name = "fund_cd" class="ajax_fund_cd" />
<div class="wrap-popup product-detail penjualan col-1" id="penjualan">
    <div class="head">
        <h5>Penjualan Reksa Dana</h5>
        <a href="javascript:closePop('penjualan')" class="close">close</a>
    </div>
    <div class="content">
        <div class="title-content after_clear">
            <h3 class="left">Info Produk</h3>
        </div>
        <div class="myaccount box-content-popup">
            <div class="box-list-data">
                <div class="list-data list_2">
                    <div class="inner-list-data">
                        <div class="box-title-list">
                            <div class="left">
                                <figure><img class="ajax_image"></figure>
                            </div>
                            <div class="right">
                                <h5 class="ajax_fund_name">Avrist Balanced Amar Syariah</h5>
                                <p class="ajax_im_name">Avrist Asset Management, PT</p>
                            </div>
                        </div>
                        <div class="desc-list">
                            <div class="row">
                                <div class="list list_6">
                                    <div class="inner-list-data">
                                        <label>Nilai Investasi Semula</label>
                                        <strong class="ajax_nilai_investasi_semula">250.000,00</strong>
                                    </div>
                                </div>
                                <div class="list list_4">
                                    <div class="inner-list-data">
                                        <label>Return</label>
                                        <strong><i class="icwp ic_up"></i> <span class="ajax_return">16,82</span></strong>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="list list_6">
                                    <div class="inner-list-data">
                                        <label>NAB/UNIT Terakhir</label>
                                        <span class="ajax_date_last">Per 30-Nov-2016</span>
                                        <strong class="ajax_last_nab">IDR 1.738,1740</span></strong>
                                    </div>
                                </div>
                                <div class="list list_4">
                                    <div class="inner-list-data">
                                        <label>Jumlah Unit</label>
                                        <strong class="ajax_unit">168,0211</strong>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="list list_6">
                                    <div class="inner-list-data">
                                        <label>Perkiraan Nilai Investasi Terakhir</label>
                                        <strong class="ajax_nilai_investasi_terakhir">IDR 1.738,1740</strong>
                                    </div>
                                </div>
                                <div class="list list_4">
                                    <div class="inner-list-data">
                                        <label>Gain/Loss</label>
                                        <strong class="ajax_gain_los">168,0211</strong>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="list-data list_2 std-content">
                    <form>
                        <div class="desc-list row_btm">
                            <div class="row no-border">
                                <div class="left">Masukan Nilai</div>
                                <div class="right">
                                    <label class="radio-btn"><input type="radio" name="order_type" value = "nominal"  />Nominal</label>
                                    <label class="radio-btn"><input type="radio" name="order_type" value = "unit"  />Unit</label>
                                </div>
                            </div>
                            <div class="row no-border">
                                <input type="text" id = "nilai" onkeyup = "uangField2(this.value)" name="nilai" class="a-right">
                            </div>
                            <div class="row no-border">
                                <label class="radio-btn"><input type="radio" name="order_type" value = "all"  />Semua Unit</label>
                            </div>
                            <div class="row no-border">
                                <div class="disclaimer-box rounded">
                                    <h4>Penyangkalan Transaksi </h4>
                                    <p>
                                        Reksa Dana adalah produk pasar modal dan
                                        bukan merupakan produk bank. Produk ini
                                        tidak dijamin oleh bank dan tidak termasuk
                                        simpanan yang merupakan objek program
                                        penjaminan pemerintah atau penjaminan
                                        simpanan.
                                        Reksa dana bukan merupakan produk PT
                                        Mirae Asset Sekuritas Indonesia akan tetapi
                                        merupakan produk yang dikelola oleh
                                        Manajer Investasi.
                                        PT Mirae Asset Sekuritas Indonesia hanya
                                        berkedudukan sebagai agen penjual efek
                                        reksa dana.

                                        PT Mirae Asset Sekuritas Indonesia tidak
                                        bertanggung jawab atas keterlambatan
                                        penyampaian instruksi nasabah ke Manajer
                                        Investasi yang diakibatkan oleh gangguan
                                        jaringan atau hal-hal yang berkaitan
                                        dengan keadaan di luar kemampuan (force
                                        majeure).

                                        Berinvestasi di reksa dana mengandung
                                        resiko. Calon pemodal wajib untuk
                                        membaca, memahami dan menyetujui
                                        prospektus sebelum memutuskan untuk
                                        berinvestasi melalui reksa dana. Kinerja
                                        masa lalu tidak bisa dijadikan acuan
                                        sebagai kinerja masa datang.

                                        PT Mirae Asset Sekuritas tidak bertanggung
                                        jawab atas segala kerugian, kehilangan,
                                        tuntutan maupun gugatan yang dialamiNasabah yang mungkin timbul sebagai
                                        akibat dari penyampaian informasi
                                        Nasabah yang tidak lengkap termasuk
                                        tetapi tidak terbatas pada pembatalan atau
                                        perubahan instruksi yang disampaikan
                                        kepada PT Mirae Asset Sekuritas Indonesia.

                                        Dengan menggunakan layanan jual dan beli
                                        reksa dana melalui PT Mirae Asset Sekuritas
                                        Indonesia sebagai agen penjual efek Reksa
                                        Dana, maka Nasabah bertanggung jawab
                                        penuh atas segala tindakan yang dilakukan
                                        serta membebaskan PT Mirae Asset
                                        Sekuritas Indonesia, termasuk anak
                                        Perusahaan, afiliasi, pemegang saham,
                                        direksi, pejabat, karyawan sepenuhnya dari
                                        segala tuntutan dan kerugian, pembayaran,
                                        maupun ongkos apapun yang timbul di
                                        kemudian hari yang diajukan kepada PT
                                        Mirae Asset Sekuritas Indonesia baik secara
                                        langsung maupun tidak langsung,
                                        termasuk tetapi tidak terbatas pada biaya
                                        penasehat hukum dan biaya perkara, yang
                                        berhubungan dengan dilaksanakannya
                                        atau tidak dilaksanakannya instruksi dari
                                        Nasabah oleh Manager Investasi.
                                    </p>
                                </div>
                            </div>
                            <div class="row no-border a-right">
                                <button type="button" onclick = "validateForm()" class="btn-oval btn-orange">CONFIRM</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

</div>
{!! Form::close() !!}
