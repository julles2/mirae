<div class="wrap-popup product-detail pengalihan" id="pengalihan">

    <div class="head">
        <h5>{{ trans('login.transferofmutualfund') }}</h5>
        <a href="javascript:closePop('pengalihan')" class="close">{{ trans('login.close') }}</a>
    </div>
    <div class="content">
        <div class="title-content after_clear">
            <h3 class="left">{{ trans('login.redirect') }}</h3>
        </div>
        {!! Form::open(['url'=>urlLang("user/portfolio/pengalihan") , "type"=>'post',"id"=>"form_pengalihan"]) !!}
        <div class="myaccount box-content-popup">
            <input type="hidden" class="ajax_fund_cd" name = "from_fund">

            <div class="box-list-data">
                <div class="list-data list_2">
                    <div class="inner-list-data">
                        <div class="box-title-list">
                            <div class="left">
                                <figure><img class = "ajax_image" src="{{ asset('noimage.png') }}"></figure>
                            </div>
                            <div class="right">
                                <h5 class="ajax_fund_name">Avrist Balanced Amar Syariah</h5>
                                <p class="ajax_im_name">Avrist Asset Management, PT</p>
                            </div>
                        </div>
                        <div class="desc-list">
                            <div class="row">
                                <div class="list list_6">
                                    <div class="inner-list-data">
                                        <label>{{ trans('login.originalinvestmentvalue') }}</label>
                                        <strong class="ajax_nilai_investasi_semula">250.000,00</strong>
                                    </div>
                                </div>
                                <div class="list list_4">
                                    <div class="inner-list-data">
                                        <label>Return</label>
                                        <strong><i class="icwp ic_up"></i> <span class="ajax_return">16,82</span></strong>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="list list_6">
                                    <div class="inner-list-data">
                                        <label>NAB/{{ trans('login.lastunit') }}</label>
                                        <span class="ajax_date_last">Per 30-Nov-2016</span>
                                        <strong class="ajax_last_nab">-</strong>
                                    </div>
                                </div>
                                <div class="list list_4">
                                    <div class="inner-list-data">
                                        <label>{{ trans('login.totalunit') }}</label>
                                        <strong class="ajax_unit">168,0211</strong>
                                        <span style="font-size:10px;" class = "ajax_unit_temporary"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="list list_6">
                                    <div class="inner-list-data">
                                        <label>{{ trans('login.estimatedvalueofthelastinvestment') }}</label>
                                        <strong class="ajax_nilai_investasi_terakhir">IDR 1.738,1740</strong>
                                    </div>
                                </div>
                                <div class="list list_4">
                                    <div class="inner-list-data">
                                        <label>Gain/Loss</label>
                                        <strong class="ajax_gain_los">168,0211</strong>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="list list_6">
                                    <div class="inner-list-data">
                                        <label>{{ trans("product.minimum_swc") }}</label>
                                        <strong class="ajax_min_unit_out">-</strong>
                                    </div>
                                </div>
                                <div class="list list_4">
                                    <div class="inner-list-data">
                                        <label>{{ trans("product.balance_minimum") }}</label>
                                        <strong class="ajax_min_balance">-</strong>
                                    </div>
                                </div>
                            </div>
                            <!--                            <div class="row">
                                                            <div class="list">
                                                                <div class="inner-list-data a-center">
                                                                    <label>Perkiraan Nilai Investasi Terakhir</label>
                                                                    <strong class="ex-blue">IDR 292.049,98</strong>
                                                                </div>
                                                            </div>
                                                        </div>-->
                        </div>
                    </div>
                </div>
                <div class="list-data list_2 std-content">
                    <div class="desc-list row_btm">
                        <div class="row no-border">
                            {{ trans('login.mutualfunddestination') }}
                        </div>
                        <div class="row no-border in_full">
                            {!! Form::select('to_fund',[''=>'Tidak ada fund yang bisa di pilih'],['class'=>'select2','id'=>'list_fund']) !!}
                        </div>
                        <div class="row no-border bg-transorange">
                            <div class="left">
                                <label><small>{{ trans('login.lastnab') }}</small></label><br>
                                <span class="tsmall" id = "ajax_to_date_last">Per -</span>
                            </div>
                            <div class="right a-right">
                                <h5 id="ajax_to_nab_last">-</h5>
                            </div>
                        </div>
                        <div class="row no-border bg-transorange">
                            <div class="left">
                                <label><small>{{ trans('product.minimum_subscription') }}</small></label><br>
                            </div>
                            <div class="right a-right">
                                <h5 id="ajax_to_min_investment">-</h5>
                            </div>
                        </div>
                        <div class="row no-border bg-transorange">
                            <div class="left">
                                <label><small>{{ trans('product.minimum_increment') }}</small></label><br>
                            </div>
                            <div class="right a-right">
                                <h5 id="ajax_to_min_increment">-</h5>
                            </div>
                        </div>
                        <div class="row no-border bg-transorange">
                            <div class="left">
                                <label><a target="_blank" id = "ajax_url_prospektus" href = "javascript::void(0);">{{ trans("product.prospectus") }}</a></label><br>
                            </div>
                            <div class="right a-right">
                                <h5 id="">&nbsp;</h5>
                            </div>
                        </div>
                        <div class="row no-border">
                                <div class="left">Unit</div>
                            </div>
                            <div class="row no-border">
                                <input type="text" class = "unit" onkeyup = "return changeToAmount(this.value)" name="unit" class="a-right">
                            </div>

                            <div class="row no-border">
                                <div class="left">{{ trans('login.amount') }}</div>
                            </div>

                            <div class="row no-border">
                                <input type="text" class = "amount" onkeyup = "return changeToUnit(this.value)" name="amount" class="a-right">
                            </div>
                        </div>

                        <div class="row no-border">
                            <label class="radio-btn"><input onclick = "return changeToall()" type="checkbox" name="order_type" class = "box_all" value = "all"  />Semua Unit</label>
                        </div>
                        <div class="row no-border">
                            <div style="margin-top:20px;">@include("global.perkiraan_nominal")</div>
                        </div>
<!--                        <div class="row no-border a-right">
                            <input type="submit" name="" value="CONFIRM" class="btn-oval btn-orange">
                        </div>-->
                    </div>
                </div>
                <div class="clear"></div>
                <div class="row no-border">
                    <div class="disclaimer-box rounded">
                        <h4>{{ trans('disclaimer.title_transaksi') }}</h4>
                        <p>
                            {!! trans('disclaimer.transaksi') !!}
                        </p>
                    </div>
                </div>
                <br/><br/>
                <div class="row no-border a-right">
                    <button type="button" onclick = "validateFormPengalihan();"  class="btn-oval btn-orange">
                        {{ trans('login.confirm') }}
                    </button>
                </div>
                <br/><br/>
            </div>
        </div>
        {!! Form::close() !!}
    </div>

</div>
