<div class="wrap-popup product-detail" id="popup-chart" style="display: none">
    <div class="head">
        <h5>
        </h5>
        <a class="close" href="javascript:closePop('popup-chart')">
            close
        </a>
    </div>
    <div class="content">
        <div class="myaccount box-content-popup sec_perform">
            <?php
            /*
            <div class="filter_top">
            <form action="">
                <div class="left">
                    <h6>
                        Periode
                    </h6>
                </div>
                <div class="right">
                    <div class="list_filt">
                        <label>
                            From
                        </label>
                        <select class="month">
                            <option>
                                Januari
                            </option>
                        </select>
                        <select class="year">
                            <option>
                                2016
                            </option>
                        </select>
                    </div>
                    <div class="list_filt">
                        <label>
                            To
                        </label>
                        <select class="month">
                            <option>
                                Desember
                            </option>
                        </select>
                        <select class="year">
                            <option>
                                2016
                            </option>
                        </select>
                    </div>
                    <div class="submit">
                        <input type="submit" value=" ">
                        </input>
                    </div>
                </div>
            </form>
        </div>
        */
            ?>
        <div class="chart_item" id="chart-1" style="width:100%;">
        </div>
        <div class="disclaimer-box rounded">
            <h4>{{ trans('disclaimer.title_portfolio') }}</h4>
            <p>
                {{ trans('disclaimer.portfolio') }}
            </p>
        </div>
    </div>
</div>
@push('scripts')
<script src="//code.highcharts.com/highcharts.js">
</script>
<script src="//code.highcharts.com/modules/exporting.js">
</script>
@endpush
