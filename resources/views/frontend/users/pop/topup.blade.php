<div class="wrap-popup product-detail penjualan col-1 " id="topup">
    <div class="head">
        <h5>{{ trans('login.purchaseofmutualfunds') }}</h5>
        <a href="javascript:closePop('topup')" class="close">{{ trans('login.close') }}</a>
    </div>
    <div class="id" style="visibility: hidden;"></div>
    <div class="content">
        <div class="title-content after_clear">
            <h3 class="left">{{ trans('login.infoproduct') }}</h3>
        </div>
        <div class="myaccount box-content-popup">
            <div class="box-list-data pembelian-list-data">
                <div class="list-data list_2">
                    <div class="inner-list-data">
                        <div class="box-title-list">
                            <div class="left">
                                <figure><img class="ajax_image" src="{{ asset(null) }}noimage.png"></figure>
                            </div>
                            <div class="right">
                                <h5 class = "im_name">Avrist Balanced Amar Syariah</h5>
                                <p class = "fund_name">Avrist Asset Management, PT</p>
                            </div>
                        </div>
                        <div class="desc-list">
                            <div class="row">
                                <div class="list ">
                                    <div class="inner-list-data">
                                        <label>{{ trans('login.minimumpurchase') }}</label>
                                        <strong><label class="min_investment inline">100,000</label> IDR</strong>
                                    </div>
                                </div>
                                <div class="list ">
                                    <div class="inner-list-data">
                                        <label>{{ trans('login.nextminimuminvestment') }}</label>
                                        <strong><label class="min_increment inline"></label> IDR</strong>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="list ">
                                    <div class="inner-list-data">
                                        <label>{{ trans('login.transactioncost') }}</label>
                                        <strong><label class = "fee_subs inline">100,000</label> %</strong>
                                    </div>
                                </div>
                                <div class="list ">
                                    <div class="inner-list-data">
                                        <label>Minimum Portofolio</label>
                                        <strong><label class = "min_balance inline">100,000</label> IDR</strong>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="list">
                                    <div class="inner-list-data a-center">
                                        <label>Latest NAV
                                            (per: <label class = "latestNavDate">20/03/2017)</label></label>
                                        <strong class="ex-blue"><label class = "latestNavValue inline">4,916 IDR</label></strong>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="list-data list_2 std-content">
                    {{ Form::open(['method'=>'post','url'=>lang().'/user/portfolio','id'=>'form_pembelian']) }}

                        <div class="desc-list row_btm">
                            <div class="row no-border">
                                <div class="left">{{ trans('login.inputvalue') }}</div>
                            </div>
                            <div class="row no-border">
                                <input type="text" id = "nilai_pembelian" onkeyup = "uangField(this)" name="nilai" class="a-right price">
                            </div>
                            <div class="row no-border ">
                                <label class="radio-btn"><input type="radio" name="order_type" checked="checked" value = "subscription" class="showhide-type2" id = "" />{{ trans('login.purchaseonce') }}</label>
                                <label class="radio-btn"><input type="radio" name="order_type" value = "subscription_auto_debet" class="showhide-type2 trigger-show" id = "" />{{ trans('login.regularpurchase') }} (Auto Debet)</label>
                            </div>
                            <div class="row no-border expand-priode">
                                <div class="col">
                                    <label>{{ trans('login.everydate') }}</label>
                                    {{-- <input placeholder="d-m-y"  id = "tanggal"  type="text" name="tanggal"> --}}
                                    {!! Form::selectRange("tanggal",1,27,['id'=>'tanggal']) !!}
                                </div>
                                <div class="col">
                                    <label>{{ trans('login.period') }}</label>
                                    <select name = "tahun">
                                        <option value = "1">1 {{ trans('login.year') }}</option>
                                        <option value = "2">2 {{ trans('login.year') }}</option>
                                        <option value = "3">3 {{ trans('login.year') }}</option>
                                    </select>
                                </div>
                            </div>
                            <div class="row no-border">
                                <div class="disclaimer-box rounded">
                                    <h4>{{ trans('disclaimer.title_transaksi') }}</h4>
                                    <p>
                                        {!! trans('disclaimer.transaksi') !!}
                                    </p>
                                </div>
                            </div>
                            <div class="row no-border a-right">
                                <button type = "button" id = "confirm_topup" class="btn-oval btn-orange">{{ trans('login.confirm') }}</button>
                            </div>

                        </div>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>

</div>
