<table class="table-radius" style="">
    <thead>
    <tr>
        <th>#</th>
        <th>{{ trans('login.code') }}</th>
        <th>AVG</th>
        <th>LAST</th>
        <th>LOT</th>
        <th>SHARE</th>
        <th>VALUE</th>
        <th>PTSL(+/-)</th>
        <th>(%)</th>
    </tr>
    </thead>
    <tbody id = "tbody_stock">
    @php
     $no = 0;
     $totalShare=0;
     $totalValue=0;
     $totalGain=0;
    @endphp
    @foreach($stocks as $s)
    @php
        $no++;
        $totalShare = $totalShare+$s->shares;
        $totalValue = $totalValue+$s->value;
        $totalGain = $totalGain+$s->gain_loss;
    @endphp
    <tr>
        <td>{{ $no }}</td>
        <td style="text-align:left;">{{ $s->item_cd }}</td>
        <td style="text-align:right;">{{ number_format($s->avg) }}</td>
        <td style="text-align:right;">{{ number_format($s->last) }}</td>
        <td style="text-align:right;">{{ number_format($s->lot) }}</td>
        <td style="text-align:right;">{{ number_format($s->shares) }}</td>
        <td style="text-align:right;">(Rp) {{ number_format($s->value) }}</td>
        <td style="color:{{$colorStyle($s->gain_loss)}};text-align:right;">{{ number_format($s->gain_loss) }}</td>
        <td style="color:{{$colorStyle($s->gain_loss_pct)}};text-align:right;">{{  ($s->gain_loss_pct) }}</td>
    </tr>
    @endforeach
    <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td><b>{{ trans('login.total') }}</b></td>
        <td style="text-align:right;">{{ number_format($totalShare) }}</td>
        <td style="text-align:right;">{{ number_format($totalValue) }}</td>
        <td style="text-align:right;">{{ number_format($totalGain) }}</td>
        <td>&nbsp;</td>
    </tr>
    </tbody>
    <!--tfoot-->

    <!--/tfoot-->
</table>

@push('scripts')

<script type="text/javascript">
    $(document).ready(function(){
        $("#filter_stock").on('change',function(){
            acnt_no = $(this).val();
            $.ajax({
                url : "{{ urlLang('user/portfolio/ajax-filter-stock') }}",
                beforeSend : function(){
                    // showPop('loaderpopup');
                },
                data : {
                    userId : acnt_no,
                },
                success : function(respon){
                    $("#tbody_stock").html("");
                    $("#tbody_stock").html(respon.result);
                    // closePop('loaderpopup');
                },
            });
        });
    });
</script>

@endpush
