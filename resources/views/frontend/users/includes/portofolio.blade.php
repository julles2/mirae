@if(count($portofolios)>0)
        @foreach($portofolios as $p)
        <?php
        $fund = $fundMySql($p->fund_cd);
        $information = $porto->information($p->fund_cd,$userId);
        $unitTemporary = $temporary->sumUnit($p->fund_cd,$userId);
        $fundCompany = fundImage($p->fund_cd);
        ?>
        <div class="list-data list_3">
            <div class="inner-list-data">
                <div class="box-title-list">
                    <div class="left">
                        <figure><img src="{{ imageWithEmpty($fundCompany) }}">
                        </figure>
                    </div>
                    <div class="right">
                        <h5>{{ @$fund->fund_name }}</h5>

                        <p>{{ @$fund->im->im_nm }}</p>
                    </div>
                </div>
                <div class="desc-list">
                    <div class="row">
                        <div class="list list_6">
                            <div class="inner-list-data">
                                <label>{{ trans('login.initialinvestmentvalue') }}</label>
                                <strong>IDR {{ formatUang($information->nilai_investasi_semula) }}</strong>
                            </div>
                        </div>
                        <div class="list list_4">
                            <div class="inner-list-data">
                                <label>Return</label>
                                <strong><i class="icwp ic_{{ $information->minus_or_plus }}"></i> {{ valueDecimal($information->return) }}</strong>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="list list_6">
                            <div class="inner-list-data">
                                <label>{{ trans('login.latestnab') }}</label>
                                <span>Per {{ $information->date_last_nab }}</span>
                                <strong>IDR {{ formatUang($information->last_nab) }}</strong>
                            </div>
                        </div>
                        <div class="list list_4">
                            <div class="inner-list-data">
                                <label>{{ trans('login.totalunit') }}</label>
                                <strong>{{ formatUang($information->unit - $unitTemporary) }}</strong>
                                @if($unitTemporary > 0)
                                    <span style="font-size:10px;">{{ valueDecimal($unitTemporary) }} Unit in process</span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="list list_6">
                            <div class="inner-list-data">
                                <label>{{ trans('login.estimatedfinalinvestmentvalue') }}</label>
                                <strong class="">IDR {{ formatUang($information->nilai_investasi_terakhir) }}</strong>
                            </div>
                        </div>
                        <div class="list list_4">
                            <div class="inner-list-data">
                                <label>Gain/Loss</label>
                                <strong>IDR {{ formatUang($information->nilai_investasi_terakhir - $information->nilai_investasi_semula) }}</strong>
                            </div>
                        </div>
                    </div>
                    <div class="row btn-bottom-list">
                        <div class="list list_3">
                            <a class="btn_list btn_darkblue" onclick = "pop_jual('{{ @$fund->fund_cd }}')" href="javascript:void(0)">{{ trans('login.sell') }}</a>
                        </div>
                        <div class="list list_3">
                            <a class="btn_list btn_blue" onclick = "pop_pengalihan('{{ @$fund->fund_cd }}')" href="javascript:void(0)">{{ trans('login.switch') }}</a>
                        </div>
                        <div class="list list_3">
                            <a class="btn_list btn_orange" onclick = "pop_topup('{{ @$fund->fund_cd }}')">{{ trans('login.topup') }}</a>
                        </div>
                    </div>
                    <a class="chart-btn" href="javascript::void(0)" onclick="pop_chart('{{ @$fund->fund_cd }}')">LIHAT CHART</a>
                </div>
            </div>
        </div>
        @endforeach
@else
<h4>{{ trans('login.count_portfolio_message') }}</h4>
@endif
@section('content2')
    @include("frontend.users.pop.portfolio_chart")
    @include("frontend.users.pop.penjualan")
    @include("frontend.users.pop.pengalihan")
    @include("frontend.users.pop.topup")
    @include("frontend.users.scripts.portfolio")
@endsection
