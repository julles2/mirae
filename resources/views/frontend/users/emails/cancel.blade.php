<?php
$trDate = carbon()->parse($model->tr_date)->format("d-M-Y");
?>
<html lang="en-US"><head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta charset="utf-8">
        <link href='//fonts.googleapis.com/css?family=Roboto:400,700' rel='stylesheet' type='text/css'>
    </head>
    <body>
        <table style="font-family:'Calibri';font-size:14px;width: 640px;margin:auto;display: block;line-height: 24px;">
            <tr>
                <td align="right" style="padding: 0 0 30px">
                    <img src="//image.ibb.co/mGtOwa/logo.png" alt="logo" />
                </td>
            </tr>
            <tr>
                <td>
                    <div style="float:left;width:60%;">
                        {{ $model->ref_no }}<br/>
                        Kepada Yth, <b>{{ $user->acnt_nm }}, {{ $user->acnt_no }}</b><br/>
                        <b>{{ $user->mobile1 }}</b><br/>
                    </div>
                    <div style="float:right;max-width:30%;">
                        {{ $trDate }}
                    </div>
                    <div style="clear:both"></div>
                </td>
            </tr>
            <tr>
                <td style="color:#0070c0;padding:40px 0;">
                    <b>Anda telah melakukan pembatalan untuk Instruksi {{ $model->label_type }}</b>
                </td>
            </tr>
            <tr>
                <td style="padding-bottom: 50px;">
                    Berikut merupakan detail {{ $model->label_type }} Reksa Dana anda:
                    <table style="width: 490px;border:1px solid #CCC;border-collapse: collapse;margin:10px 0 0 0">
                        <tr>
                            <td style="border:1px solid #CCC;padding:10px">Reference Number </td>
                            <td style="border:1px solid #CCC;padding:10px">{{ $model->ref_no }}</td>
                        </tr>
                        <tr>
                            <td style="border:1px solid #CCC;padding:10px">Tanggal Order </td>
                            <td style="border:1px solid #CCC;padding:10px">{{ $trDate }}</td>
                        </tr>
                        <tr>
                            <td style="border:1px solid #CCC;padding:10px">Waktu Pembatalan </td>
                            <td style="border:1px solid #CCC;padding:10px">{{ carbon()->parse($model->date_cancel)->format("d-M-Y") }} {{ carbon()->parse($model->time_cancel)->format("H:i:s") }} WIB</td>
                        </tr>
                        <tr>
                            <td style="border:1px solid #CCC;padding:10px">Nama Reksa Dana</td>
                            <td style="border:1px solid #CCC;padding:10px">{{ $model->fund->fund_name }}</td>
                        </tr>
                        <tr>
                            <td style="border:1px solid #CCC;padding:10px">Manajer Investasi </td>
                            <td style="border:1px solid #CCC;padding:10px">{{ $model->fund->im->im_nm }}</td>
                        </tr>
                        <tr>
                            <td style="border:1px solid #CCC;padding:10px">Biaya {{ $model->label_type }}</td>
                            <td style="border:1px solid #CCC;padding:10px">RP {{ formatUang($model->total_fee) }} </td>
                        </tr>
                        <tr>
                            <td style="border:1px solid #CCC;padding:10px">Total {{ $model->label_type }}</td>
                            <td style="border:1px solid #CCC;padding:10px">RP {{ formatUang($model->bill-$model->total_fee) }}</td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <b>Informasi Pembayaran</b><br/>
                    Pastikan dana pada RDN anda mencukupi agar proses pembelian ini dapat dilakukan, apabila dana pada RDN anda tidak mencukupi maka instruksi pembelian Reksa Dana anda tidak dapat diproses (gagal).
                </td>
            </tr>
            <tr>
                <td><p>&nbsp;</p></td>
            </tr>
            <tr>
                <td><b>Untuk informasi lebih lanjut, silahkan hubungi Customer Service kami di:</b></td>
            </tr>
            <tr>
                <td>
                    Pastikan dana pada RDN anda mencukupi agar proses pembelian ini dapat dilakukan, apabila dana pada RDN anda tidak mencukupi maka instruksi pembelian Reksa Dana anda tidak dapat diproses (gagal).
                </td>
            </tr>
            <tr>
                <td style="padding:0 0 70px 0;">
                    <table>
                        <tr>
                            <td>Telp</td>
                            <td>:</td>
                            <td>021 – 2553 1000</td>
                        </tr>
                        <tr>
                            <td>Email</td>
                            <td>:</td>
                            <td><a href="" style="color:#0563c1">customerservice@miraeasset.co.id</a></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="padding:0 0 90px 0;">
                    Terima Kasih,<br/>
                    <b>Mirae Asset Sekuritas Indonesia</b>
                </td>
            </tr>
            <tr>
                <td style="border:1px solid #333;font-size:14px;padding: 10px">
                    <h6 style="font-size: 14px;color:#ff0000;margin:0 0 5px;">Syarat dan Ketentuan:</h6>
                    <ul style="list-style: decimal;list-style-position: outside;margin:0 0 0 -20px">
                        <li>Instruksi pembelian yang tercatat pada sistem PT Mirae Asset Sekuritas Indonesia sebelum pukul 12.00 WIB, akan diproses menggunakan NAB pada hari yang sama.</li>
                        <li>Instruksi pembelian yang tercatat pada sistem PT Mirae Asset Sekuritas Indonesia setelah pukul 12.00 WIB, akan diproses menggunakan NAB hari bursa berikutnya.</li>
                        <li>Instruksi pembelian akan dinyatakan Sah, jika dana pada RDN mencukupi untuk pembelian reksa dana sesuai nominal pembelian yang dipesan (good fund good application).</li>
                        <li>Instruksi Pembelian yang tercatat sebelum pukul 12.00 WIB dapat dilakukan pembatalan sampai dengan pukul 12.00 WIB pada hari bursa yang sama.</li>
                        <li>Instruksi Pembelian yang tercatat setelah pukul 12.00 WIB dapat dilakukan pembatalan sampai dengan pukul 12.00 WIB pada hari bursa berikutnya.</li>
                        <li>Informasi dalam email ini bukan merupakan konfirmasi Transaksi. Konfirmasi Transaksi hanya diterbitkan dari Bank Kustodian.</li>
                        <li>Apabila terdapat perbedaan perhitungan dengan Bank Kustodian, maka akan menggunakan perhitungan yang diterbitkan oleh bank Kustodian</li>
                        <li>Pemilik Akun menyatakan bahwa seluruh Instruksi yang dilakukan pada (MAXFUND) adalah benar dilakukan oleh Pemilik Akun yang sah dan membebaskan PT Mirae Asset Sekuritas Indonesia apabila terjadi penyalahgunaan akun.</li>
                    </ul>
                </td>
            </tr>
        </table>

    </body>
</html>
