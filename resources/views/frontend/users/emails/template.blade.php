
<html lang="en-US"><head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta charset="utf-8">
        <link href='//fonts.googleapis.com/css?family=Roboto:400,700' rel='stylesheet' type='text/css'>
    </head>
    <body>
        <table style="font-family:'Calibri';font-size:14px;width: 640px;margin:auto;display: block;line-height: 24px;">
            <tr>
                <td align="right" style="padding: 0 0 30px">
                    <img src="images/material/logo.png" alt="logo" />
                </td>
            </tr>
            <tr>
                <td >
                    <div style="float:left">
                        Reference Number<br/>
                        Tanggal<br/>
                        YTH, <b>NASABAH (nama)</b><br/>
                        <b>(Cust ID) (Alamat) (No Telp)</b><br/>
                    </div>
                    <div style="float:right">
                        01-05-2005
                    </div>
                    <div style="clear:both"></div>
                </td>
            </tr>
            <tr>
                <td style="color:#0070c0;padding:40px 0;">
                    <b>Instruksi pembelian anda sedang dalam proses.</b>
                </td>
            </tr>
            <tr>
                <td style="padding-bottom: 50px;">
                    Berikut merupakan detail pembelian Reksa Dana anda:
                    <table style="width: 490px;border:1px solid #CCC;border-collapse: collapse;margin:10px 0 0 0">
                        <tr>
                            <td style="border:1px solid #CCC;padding:10px">Reference Number </td>
                            <td style="border:1px solid #CCC;padding:10px">12345678</td>
                        </tr>
                        <tr>
                            <td style="border:1px solid #CCC;padding:10px">Tanggal Order </td>
                            <td style="border:1px solid #CCC;padding:10px">16-Apr-17</td>
                        </tr>
                        <tr>
                            <td style="border:1px solid #CCC;padding:10px">Waktu Order </td>
                            <td style="border:1px solid #CCC;padding:10px">10:00:00 WIB</td>
                        </tr>
                        <tr>
                            <td style="border:1px solid #CCC;padding:10px">Nama Reksa Dana</td>
                            <td style="border:1px solid #CCC;padding:10px">Sucorinvest Equity Fund</td>
                        </tr>
                        <tr>
                            <td style="border:1px solid #CCC;padding:10px">Manajer Investasi </td>
                            <td style="border:1px solid #CCC;padding:10px">Sucorinvest Asset Management, PT</td>
                        </tr>
                        <tr>
                            <td style="border:1px solid #CCC;padding:10px">Nilai Pembelian Bersih</td>
                            <td style="border:1px solid #CCC;padding:10px">RP 250.000,00</td>
                        </tr>
                        <tr>
                            <td style="border:1px solid #CCC;padding:10px">Biaya Pembelian</td>
                            <td style="border:1px solid #CCC;padding:10px">RP 0.00</td>
                        </tr>
                        <tr>
                            <td style="border:1px solid #CCC;padding:10px">Total Pembelian</td>
                            <td style="border:1px solid #CCC;padding:10px">RP 250.000,00</td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td><b>Jika anda memerlukan informasi atau bantuan silahkan hubungi Customer Service kami di:</b></td>
            </tr>
            <tr>
                <td style="padding:0 0 70px 0;">
                    <table>
                        <tr>
                            <td>Telp</td>
                            <td>:</td>
                            <td>021 – 2553 1000</td>
                        </tr>
                        <tr>
                            <td>Email</td>
                            <td>:</td>
                            <td><a href="" style="color:#0563c1">customerservice@miraeasset.co.id</a></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="padding:0 0 90px 0;">
                    Terima Kasih,<br/>
                    <b>Mirae Asset Sekuritas Indonesia</b>
                </td>
            </tr>
            <tr>
                <td style="border:1px solid #333;font-size:14px;padding: 10px">
                    <h6 style="font-size: 14px;color:#ff0000;margin:0 0 5px;">Syarat dan Ketentuan:</h6>
                    <ul style="list-style: decimal;list-style-position: outside;margin:0 0 0 -20px">
                        <li>Instruksi pembelian yang tercatat masuk sistem PT Mirae Asset Sekuritas Indonesia sebelum 12.00 WIB pada hari yang sama akan mengikuti perhitungan NAB pada hari bursa berikutnya.</li>
                        <li>Instruksi pembelian yang tercatat masuk di sistem PT Mirae Asset Sekuritas Indonesia  setelah pukul 12.00 WIB pada hari yang sama akan mengikuti perhitungan NAB pada 2 (dua) hari bursa berikutnya.</li>
                        <li>Instruksi pembelian akan otomatis dibatalkan jika tidak ada konfirmasi pembayaran hingga 3 (tiga) hari bursa berikutnya.</li>
                    </ul>
                </td>
            </tr>
        </table>

    </body>
</html>
