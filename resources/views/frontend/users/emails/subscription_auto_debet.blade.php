<?php
$trDate = parse($model->tr_date)->format("d-M-Y");
$p = explode("-", $model->periode_debet);
$periode = parse($p[0])->format("d F Y").' s/d '.parse($p[1])->format("d F Y");

?>
<html lang="en-US"><head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta charset="utf-8">
        <link href='//fonts.googleapis.com/css?family=Roboto:400,700' rel='stylesheet' type='text/css'>
    </head>
    <body>
        <table style="font-family:'Calibri';font-size:14px;width: 640px;margin:auto;display: block;line-height: 24px;">
            <tr>
                <td align="right" style="padding: 0 0 30px">
                    <img src="//image.ibb.co/mGtOwa/logo_transparent.png" alt="logo" />
                </td>
            </tr>
            <tr>
                <td>
                    <div style="float:left;width:60%;">
                        {{ $model->ref_no }}<br/>
                        Kepada Yth, <b>{{ $user->acnt_nm }}, {{ $user->acnt_no }}</b><br/>
                        <b>{{ $user->mobile1 }}</b><br/>
                    </div>
                    <div style="float:right;max-width:30%;">
                        {{ $trDate }}
                    </div>
                    <div style="clear:both"></div>
                </td>
            </tr>
            <tr>
                <td style="color:#0070c0;padding:40px 0;">
                    <b>Terima kasih atas kepercayaan anda untuk berinvestasi Reksa Dan melalui PT. Mirae Asset Sekuritas Indonesia.</b>
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>

            <tr>
                <td style="color:#0070c0;padding:40px 0;">
                    <b>
                        Permohonan registrasi pembelian Reksa Dana secara berkala Anda di (MAXFUND) telah kami terima. 
                        Berikut adalah detil instruksi pembelian berkala reksa dana anda:

                    </b>
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>

            <tr>
                <td style="padding-bottom: 50px;">

                    <table style="width: 490px;border:1px solid #CCC;border-collapse: collapse;margin:10px 0 0 0">
                        <tr>
                           <td style="border:1px solid #CCC;padding:10p;color:#002060;">Reference Number</td>
                           <td style="border:1px solid #CCC;padding:10px;color:#002060;">{{ $model->ref_no }}</td>
                        </tr>
                        <tr>
                           <td style="border:1px solid #CCC;padding:10px;color:#002060;">Nama Reksa Dana</td>
                           <td style="border:1px solid #CCC;padding:10px;color:#002060;">{{ $model->fund->fund_name }}</td>
                        </tr>
                        <tr>
                            <td style="border:1px solid #CCC;padding:10px;color:#002060;">Manajer Investasi </td>
                            <td style="border:1px solid #CCC;padding:10px;color:#002060;">{{ $model->fund->im->im_nm }}</td>
                        </tr>
                        <tr>
                            <td style="border:1px solid #CCC;padding:10px;color:#002060;">Tanggal Pendebetan</td>
                            <td style="border:1px solid #CCC;padding:10px;color:#002060;">{{ $model->recur_date }}</td>
                        </tr>
                        <tr>
                            <td style="border:1px solid #CCC;padding:10px;color:#002060;">Periode Pendebetan</td>
                            <td style="border:1px solid #CCC;padding:10px;color:#002060;">{{ $periode }}</td>
                        </tr>
                        <tr>
                            <td style="border:1px solid #CCC;padding:10px;color:#002060;">Nilai Pendebetan</td>
                            <td style="border:1px solid #CCC;padding:10px;color:#002060;">RP {{ formatUang($model->bill) }}</td>
                        </tr>
                        <tr>
                           <td style="border:1px solid #CCC;padding:10px;color:#002060;">Biaya Pendebetan</td>
                           <td style="border:1px solid #CCC;padding:10px;color:#002060;">{{ $model->persent }}</td>
                        </tr>
                       <tr>
                           <td style="border:1px solid #CCC;padding:10px;color:#002060;">Nilai Pembelian Bersih</td>
                           <td style="border:1px solid #CCC;padding:10px;color:#002060;">RP {{ formatUang($model->bill-$model->total_fee) }}</td>
                       </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    Mohon pastikan dana pembelian Reksa Dana berkala Anda tersedia di rekening RDN pada tanggal pendebetan yang telah anda pilih. Ketidaktersediaan dana di RDN pada tanggal pendebetan, dapat menyebabkan transaksi pembelian Reksa Dana berkala Anda mengalami kegagalan.
                </td>
            </tr>
            <tr>
                <td><p>&nbsp;</p></td>
            </tr>
            <tr>
                <td><b>Untuk informasi lebih lanjut, silahkan hubungi Customer Service kami di:</b></td>
            </tr>
            <tr>
                <td style="padding:0 0 70px 0;">
                    <table>
                        <tr>
                            <td>Telp</td>
                            <td>:</td>
                            <td>021 – 2553 1000</td>
                        </tr>
                        <tr>
                            <td>Email</td>
                            <td>:</td>
                            <td><a href="mailto:customerservice@miraeasset.co.id" style="color:#0563c1">customerservice@miraeasset.co.id</a></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="padding:0 0 90px 0;">
                    Terima Kasih,<br/>
                    <b>Mirae Asset Sekuritas Indonesia</b>
                </td>
            </tr>
        </table>

    </body>
</html>
