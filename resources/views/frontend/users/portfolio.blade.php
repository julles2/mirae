@extends('frontend.user')
@section('content')
    <div class="content-tab-general">
        <div class="list-tab">
            <div class="title-content after_clear">
                <div class="left">
                    <h3><i class="icwp ic_folder"></i> {{ trans('global.my_portfolio') }}</h3>
                </div>
            </div>
            <div class="row no-border">
                <div class="disclaimer-box rounded">
                    {!! disclaimer('portfolio') !!}
                </div>
            </div>
            <p>&nbsp;</p>
            <div class="title-bg bg-darkblue margbottom">
                <div class="left">
                    <h3>{{ trans('login.estimatedfinalinvestmentvalue') }} :</h3>
                </div>
                <div class="right">
                    <!--h3>IDR 11,567,890,453 (<i class="icwp ic_up"></i> 20.98 %)</h3-->
                    <h3>IDR {{ formatUang($usetNilaiInvestasiTerakhir) }}</h3>
                </div>
            </div>

            <div class="box-list-data after_clear">
                @include('frontend.users.includes.portofolio')
            </div>
            <div class="title-content after_clear">
                <div class="left">
                    <h3><i class="icwp ic_chart_up"></i>{{ trans('login.stockportfolio') }}</h3>
                </div>
                <div class="right">
                    {!! Form::select('acnt_no',$filters,null,['id'=>'filter_stock']) !!}
                </div>
            </div>
                    <div class="overflow">
                            @include('frontend.users.includes.stock_portofolio')
                    </div>
            <div class="box-list-data disclaimer">
                <div class="list-data">
                    <div class="inner-list-data">
                        <div class="disclaimer-box rounded">
                            <h4>{{ trans('disclaimer.title_portfolio') }}</h4>
                            <p>
                                {{ trans('disclaimer.portfolio') }}
                            </p>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

@endsection
@push('content2')
@include('frontend.users.pop.penjualan')
@include('frontend.users.pop.pengalihan')
@include("frontend.users.scripts.portfolio")
@endpush

