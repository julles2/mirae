<?php
/**
 * Created by PhpStorm.
 * User: DanielSimangunsong
 * Date: 5/17/2017
 * Time: 10:20 AM
 */ ?>

@extends('frontend.user')

@section('content')
    <div class="content-tab-general">
        <div class="list-tab">
            <div class="title-content after_clear">
                <h3>{{ trans('login.personaldata') }}</h3>
            </div>
            <hr class="line-blue">
            <div class="box-data-diri">
                <div class="row-data-diri">
                    <div class="col col_33">
                        <label>ID {{ trans('login.user') }}</label>
                        <strong>{{ $model->acnt_no }}</strong>
                    </div>
                    <div class="col col_33">
                        <label>{{ trans('login.fullname') }}</label>
                        <strong>{{ $model->acnt_nm }}</strong>
                    </div>
                    <div class="col col_33">
                        <label>No. {{ $typeIdentitas }}</label>
                        <strong>{{ $model->cid_no }}</strong>
                    </div>
                </div>
                <div class="row-data-diri">
                    <div class="col col_33">
                        <label>{{ trans('login.validityperiod') }} {{ $typeIdentitas }}</label>
                        <strong style="color:{{$expireColor}};">{{ $idenExpire() }}</strong>
                    </div>
                    <div class="col col_33">
                        <label>No. KITAS/KITAP</label>
                        <strong>{{ $model->cid_sect == '4' ? $model->cid_no : '' }}</strong>
                    </div>
                    <div class="col col_33">
                        <label>{{ trans('login.validityperiod') }} KITAS/KITAP</label>
                        <strong>{{ $model->cid_sect == '4' ? $idenExpire() : '-' }}</strong>
                    </div>
                </div>
                <div class="row-data-diri">
                    <div class="col col_33">
                        <label>{{ trans('login.placeofbirth') }}</label>
                        <strong>{{ $model->birth_plc }}</strong>
                    </div>
                    <div class="col col_33">
                        <label>{{ trans('login.dateofbirth') }}</label>
                        <strong>{{ !empty($model->bday) ? carbon()->parse($model->bday)->format("d-M-Y") : '' }}</strong>
                    </div>
                    <div class="col col_33">
                        <label>{{ trans('login.gender') }}</label>
                        <strong>{{ $custInfo->gender($model->sex_sect) }}</strong>
                    </div>
                </div>
                <div class="row-data-diri">
                    <div class="col col_33">
                        <label>{{ trans('login.maritalstatus') }}</label>
                        <strong>{{ $custInfo->wedding($model->wedd_sect) }}</strong>
                    </div>
                    <div class="col col_33">
                        <label>{{ trans('login.mothersname') }}</label>
                        <strong>{{ $model->mother_nm }}</strong>
                    </div>
                    <div class="col col_33">
                        <label>{{ trans('login.nameofthecouple') }}</label>
                        <strong>{{ $model->sps_nm }}</strong>
                    </div>
                </div>
                <div class="row-data-diri">
                    <div class="col col_33">
                        <label>No. Handphone</label>
                        <strong>{{ $model->mobile1 }}</strong>
                    </div>
                    <div class="col col_33">
                        <label>{{ trans('login.address') }} (Sesuai KTP/Paspor)</label>
                        <strong>{{ $model->home_addr }}</strong>
                    </div>
                </div>
                <div class="row-data-diri">
                    <div class="col col_33">
                        <label>NPWP</label>
                        <strong>{{ $model->npwp }}</strong>
                    </div>
                    <div class="col col_33">
                        <label>{{ trans('login.registereddate') }} NPWP</label>
                        <strong>{{ !empty($model->npwp_reg_date) ? carbon()->parse($model->npwp_reg_date)->format("d-M-Y") : '' }}</strong>
                    </div>
                    <div class="col col_33">
                        <label>No. Telp Rumah</label>
                        <strong>{{ $model->homephone }}</strong>
                    </div>
                </div>
                <div class="row-data-diri">
                    <div class="col col_33">
                        <label>E-mail</label>
                        <strong>{{ $model->email }}</strong>
                    </div>
                    <!--div class="col col_33">
                        <label>Alamat E-mail</label>
                        <strong>nuzulferdian@gmail.com</strong>
                    </div-->
                    <div class="col col_33">
                        <label>No. SID</label>
                        <strong>{{ $model->sid }}</strong>
                    </div>
                </div>
                <div class="row-data-diri">
                    <div class="col col_33">
                        <label>{{ trans('login.nationality') }}</label>
                        <strong>{{ @$model->client_info->nationality }}</strong>
                    </div>
                    <div class="col col_33">
                        <label>No. IFUA</label>
                        <strong>{{ @$model->client_info->ifua_cd }}</strong>
                    </div>
                </div>
            </div>
            <br>
            <br>
            <br>
            <br>

            <div class="title-content after_clear">
                <h3>{{ trans('login.employmentandfinancialdata') }}</h3>
            </div>
            <hr class="line-blue">
            <div class="box-data-diri">
                <div class="row-data-diri">
                    <div class="col col_33">
                        <label>{{ trans('login.nameofbankandbranch') }}</label>
                        <strong>{{ @$model->bank->inv_bank_nm }}</strong>
                    </div>
                    <div class="col col_33">
                        <label>{{ trans('login.typeandrekeningnumber') }}</label>
                        <strong>RDI <br/> {{ @$model->bank->inv_acc_no }}</strong>
                    </div>
                    <div class="col col_33">
                        <label>{{ trans('login.lastname') }}</label>
                        <strong>{{ @$model->acnt_nm }}</strong>
                    </div>
                </div>
                <div class="row-data-diri">
                    <div class="col col_33">
                        <label>{{ trans('login.nameofbankandbranch') }}</label>
                        <strong>{{ @$model->bank->out_bank_nm }}</strong>
                    </div>
                    <div class="col col_33">
                        <label>{{ trans('login.typeandrekeningnumber') }}</label>
                        <strong>BANK OUT <br/> {{ @$model->bank->out_bank_acnt_no }}</strong>
                    </div>
                    <div class="col col_33">
                        <label>{{ trans('login.lastname') }}</label>
                        <strong>{{ @$model->bank->out_bank_acnt_nm }}</strong>
                    </div>
                </div>
                <div class="row-data-diri">
                    <div class="col col_33">
                        <label>{{ trans('login.companyname') }}</label>
                        <strong>{{ @$model->job->office_nm }}</strong>
                    </div>
                    <div class="col col_33">
                        <label>{{ trans('login.bussinessfields') }}</label>
                        <strong>{{ @$model->job->comp_type }}</strong>
                    </div>
                    <div class="col col_33">
                        <label>{{ trans('login.companyaddress') }}</label>
                        <strong>{{ @$model->job->addr }}</strong>
                    </div>
                </div>
            </div>
            <div class="title-content after_clear">
                <h3>{{ trans('global.upload_photo') }}</h3>
            </div>
            <div class="box-data-diri">
                {!! Form::open(['files'=>true]) !!}
                <div class="row-data-diri">
                    <div class="col col_33">
                        <label>{{ trans('global.photo') }}</label>
                        {!! Form::file('photo',null) !!} <br/>
                    </div>
                </div>
                <div class="row-data-diri">
                    <div class="col col_33">
                        <input type = "submit" value = "{{ trans("global.upload") }}" class="btn-std" />
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
            <div class="box-list-data disclaimer">
                <div class="list-data">
                    <div class="inner-list-data">
                        <div class="disclaimer-box rounded">
                            <h4>Disclaimer</h4>

                            <p>{{ trans('login.disclaimer') }}</p>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection
