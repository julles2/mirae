@extends('layouts.layout')
@section('content')
    <section class="banner">
        <img src="{{assetFrontend(null)}}images/content/banner-produk.jpg" alt="banner" />
        <div class="caption">
            <h6>{{ trans('login.additionalquestion') }}</h6>
            <h1>{{ trans('global.welcome') }}<br/>
                {{ trans('global.at') }} MaxFund</h1>
            <p>&nbsp;</p>
        </div>
    </section>
    <section class="std-content">
        <div class="wrapper ">
            <h2 class="a-center">Formulir</h2>
            <div class="shadow-spar"></div>
            {!! Form::open() !!}
                <div class='box-form'>
                    @foreach($questions as $q)
                        <div class="row">
                            <div class="left"><label class="require">{{underscore_to_label($q)}}</label></div>
                            <div class="right">
                                    @if($q == 'religion' || $q == 'agama')
                                        {!! Form::select($q,[''=>''] + $religions,null) !!}

                                    @elseif($q == 'birth_country' || $q == 'nationality' || $q=='negara_kelahiran' || $q=='Kewarganegaraan')
                                        {!! Form::select($q,[''=>''] + $countries,null,['class'=>'select2']) !!}
                                    @else
                                        {!! Form::text($q) !!}
                                    @endif
                            </div>
                        </div>
                    @endforeach
                    <div class="row">
                        <div class="left"></div>
                        <div class="right"><input type="submit" value="{{ trans('login.submit') }}" class="btn-std"></div>
                    </div>

                </div>
            {!! Form::close() !!}
        </div>
    </section>
@endsection
