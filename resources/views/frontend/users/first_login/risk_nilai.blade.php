@extends('layouts.layout')
@section('content')
<style>
#customers {
    font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
    border-collapse: collapse;
    width: 100%;
}

#customers td, #customers th {
    border: 1px solid #ddd;
    padding: 8px;
}

#customers tr:nth-child(even){background-color: #f2f2f2;}

#customers tr:hover {background-color: #ddd;}

#customers th {
    padding-top: 12px;
    padding-bottom: 12px;
    text-align: left;
    background-color: #2e76a2;
    color: white;
}
</style>
<section class="banner">
    <img src="{{assetFrontend(null)}}images/content/banner-produk.jpg" alt="banner" />
    <div class="caption">
        <h6>{{ trans('login.Additional-Question') }}</h6>
        <h1>Selamat Datang<br/>
            di MaxFund</h1>
        <p>&nbsp;</p>
    </div>
</section>
<section class="std-content">
    <div class="wrapper ">
        <h2 class="a-center">{{ trans('login.Customer-Risk-Profile') }}</h2>
        <div class="shadow-spar"></div>
        <div class="nilai-pertanyaan">
            <p><b>{{ trans('login.Your-value-in-interpreting-the-risk-profile-to-invest-is') }} : </b></p>
            <h3>{!! $label !!}</h3>
            <p>
                {{ trans('login.Financiers-of-this-type-expect-higher-return-on-investment-and-understand-the-risks-involved-in-the-investment.-Investors-like-this-should-invest-in-Fixed-/-Mixed-Revenue-Funds-and-some-can-be-allocated-to-Equity-Funds.') }}
            </p>
        </div>
        <div class="box-form">
            <div class="row border-dashed a-center">
                @if(!empty($userOracle->ifua_cd))
                    <a href="{{ urlLang("checkout") }}" class="btn-std yellow">Checkout</a>
                @else    
                    <a href="{{ urlLang("user/question/notify") }}" class="btn-std yellow">{{ trans('login.complete') }}</a>
                @endif
            </div>
        </div>
        <div class="h-product">
            <h2>{{ trans('login.Product-Suited-Your-Risk-Profile') }}</h2>
            <div class="list-product has-slider">
                <div class="slider slider-product">
                    @for($s=0;$s<5;$s++)
                    @foreach($funds as $row)
                    <div class="slide">
                        <div class="box">
                            <div class="name">
                                <p>{{ $row->im_nm }}</p>
                            </div>
                            <div class="desc">
                                <div class="ico">
                                    @if(!empty($row->image))
                                    <img src="{{ asset(null) }}contents/{{ $row->image }}" alt="prod" />
                                    @else
                                    <img src="{{ asset('noimage.png') }}" alt="prod" />
                                    @endif

                                </div>
                                <div class="text">
                                    <h5 style="font-size:10px;">{{ $row->fund_name }}</h5>
                                    <p>NAB   <span>{{ valueDecimal($row->nav_per_unit) }}</span></p>
                                </div>
                            </div>
                            <div class="return">
                                <p>Return (%)</p>
                            </div>
                            <div class="float-3">
                                <div>
                                    <p>1 Hr  <span>{{ valueDecimal($row->one_day) }}</span></p>
                                </div>
                                <div>
                                    <p>1 Bln  <span>{{ valueDecimal($row->one_month) }}</span></p>
                                </div>
                                <div>
                                    <p>1 Yr  <span>{{ valueDecimal($row->one_years) }}</span></p>
                                </div>
                            </div>
                            <a onclick="addWatchLists('{{ $row->fund_cd }}')" {!! $repoFund->htmlWatchlist($row->fund_cd,acnt_no()) !!} class="watch-list">Add To Watchlist</a>
                            <div class="btn">
                                <a href = "{{ urlLang('product?pop='.$row->fund_cd) }}">{{ trans('product.buy') }}</a>
                                <a href="javascript::void(0);" onclick="compareHome('{{ $row->fund_cd }}')">{{ trans('product.compare') }}</a>
                            </div>
                        </div>
                    </div>
                    @endforeach
                    @endfor

                </div>
            </div>
        </div>

        <div class="box-form">
            <div class="row border-dashed a-center">
                &nbsp;
            </div>
        </div>

        <div class="h-product">
            <h2>{{ trans('login.Other-Product-Options') }}</h2>
            <div class="list-product">
                @foreach($others as $other)
                <div class="box">
                    <div class="name">
                        <p>{{ $other->im_nm }}</p>
                    </div>
                    <div class="desc">
                        <div class="ico">
                            @if(!empty($other->image))
                            <img src="{{ asset(null) }}contents/{{ $other->image }}" alt="prod" />
                            @else
                            <img src="{{ asset('noimage.png') }}" alt="prod" />
                            @endif

                        </div>
                        <div class="text">
                            <h5 style="font-size:10px;">{{ $other->fund_name }}</h5>
                            <p>NAB   <span>{{ valueDecimal($other->nav_per_unit) }}</span></p>
                        </div>
                    </div>
                    <div class="return">
                        <p>Return (%)</p>
                    </div>
                    <div class="float-3">
                        <div>
                            <p>1 Hr  <span>{{ valueDecimal($other->one_day) }}</span></p>
                        </div>
                        <div>
                            <p>1 Bln  <span>{{ valueDecimal($other->one_month) }}</span></p>
                        </div>
                        <div>
                            <p>1 Yr  <span>{{ valueDecimal($other->one_years) }}</span></p>
                        </div>
                    </div>
                    <a onclick="addWatchLists('{{ $other->fund_cd }}')" {!! $repoFund->htmlWatchlist($other->fund_cd,acnt_no()) !!} class="watch-list">Add To Watchlist</a>
                    <div class="btn">
                        <a href = "{{ urlLang('product?pop='.$other->fund_cd) }}" onclick="popNotes()">{{ trans('product.buy') }}</a>
                        <a href="javascript::void(0);" onclick="compareHome('{{ $other->fund_cd }}')" >{{ trans('product.compare') }}</a>
                    </div>
                </div>
                @endforeach
            </div>
        </div>

        <div class="box-form">
            <div class="row border-dashed a-center">
                <a href="{{ urlLang("product") }}" class="btn-std yellow">{{ trans('login.view-all') }}</a>
            </div>
        </div>

    </div>
    {!! Form::open(['id'=>'form_comare','method'=>'post','url'=>urlLang("compare/store-fund")]) !!}
        {!! Form::hidden('fund_compare[]',null,['id'=>'fund_compare']) !!}
    {!! Form::close() !!}
</section>


@if(@$mileage->mileage > 0)
    @if(lang() == 'id')
        <div class="wrap-popup product-detail info col-2" id="info">
            <div class="head"> 
                <h5>Mileage</h5>
                <a href="javascript:closePop('info')" class="close">close</a>
            </div>
            <div class="content">
                <div class="title-content after_clear">
                    <h3 class="left">Info Mileage</h3>
                </div>
                <div class="myaccount box-content-popup">
                    <div class="box-list-data">
                        <div class="desc-list">
                            <table width="300" height="150" style="font-size:14px;">
                                <tr>
                                    <td>Nama Nasabah</td>
                                    <td>{{ $brokerName }}</td>
                                </tr>
                                <tr>
                                    <td>ID Nasabah</td>
                                    <td>{{ $brokerId }}</td>
                                </tr>
                                <tr>
                                    <td>Jumlah Mileage Point</td>
                                    <td>{{ @$mileage->mileage > 0 ? $mileage->mileage : 0 }}</td>
                                </tr>
                                <tr>
                                    <td>Jumlah Reward</td>
                                    <td>Rp {{ @$mileage->mileage > 0 ? formatUangNoComa($mileage->mileage) : 0 }}</td>
                                </tr>
                                <tr>
                                    <td>Profil Resiko</td>
                                    <td>{{ $labelRiskOnly }}</td>
                                </tr>
                            </table>
                            <hr>
                            <div style="margin-top:20px;">
                                Penukaran Mileage Point: <br><br>
                                <ul>
                                    <li>- Cashback (Harap melakukan penukaran melalui HOTS)</li>
                                    <li>- Voucher Investasi</li>
                                </ul>
                                <br>
                                <div style="font-size:12px;">
                                    <label class="radio-btn">
                                    <input class="aggree" type="checkbox" name="aggree" id = "cek_agree">
                                </label>
                                    Dengan ini saya menyatakan bahwa saya telah setuju dengan <a href = "javascript::void(0)" onclick = "show_term()" style="color:red;">syarat dan ketentuan</a> yang berlaku terkait dengan penukaran mileage point milik saya.
                                </div>
                                <br><br>
                                {{-- <input type="submit" name="" value="KONFIRM" class="btn-oval btn-orange"> --}}
                            </div>
                            <div class="overflow">
                            {!! Form::open(['method'=>'post','id'=>'formna','url'=>urlLang("user/mileage")]) !!}
                                <table id = "customers">
                                    <thead style="background-color: #2e76a2;color:white;">
                                        <tr>
                                            <th>#</th>
                                            <th>Tipe Voucher</th>
                                            <th>REKSA DANA</th>
                                            <th>Profil Risiko</th>
                                            <th>Prospektus</th>
                                            <th>FFS</th>
                                            
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        $model = new \App\Models\Oracle\FundInfo();
                                        ?>
                                        @foreach($products as $type => $codes)
                                            @foreach($codes as $code)
                                            <?php
                                                $fund = $model->where('fund_cd',$code)->first();
                                            ?>

                                                @if(!empty($fund->fund_cd))
                                                    <tr>
                                                        <td>
                                                            <label class="radio-btn">
                                                                <input class="fund" type="radio" name = "fund" value = "{{ $code }}">
                                                            </label>
                                                        </td>
                                                        <td>
                                                            @if($type == 'pemula')
                                                            Voucher Investasi Pemula (Minimum Rp 100,000)
                                                            @else
                                                            Voucher Investasi Ekonomis (Tidak Ada minimum)
                                                            @endif
                                                        </td>
                                                        <td>{{ $fund->fund_name }}</td>
                                                        <td>{{ $fund->result_fund_risk }}</td>
                                                        <td>
                                                            @if(!empty($fund->mysql_fund->id))
                                                            <a href="{{ asset('contents/'.$fund->mysql_fund->prospectus) }}">Prospektus</a>
                                                            @else
                                                            -
                                                            @endif
                                                        </td>
                                                        <td>ffs</td>
                                                        
                                                    </tr>
                                                @endif
                                            @endforeach
                                        @endforeach
                                    </tbody>
                                </table>
                                </br>
                                <div style="font-size:9px;">*Proses penukaran poin akan memakan waktu sampai  dengan 7 hari kerja</div>
                                </br>
                                <a href="javascript::void(0)" class="btn-std yellow" id = "redeem">Tukar</a>
                            {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @else
        <div class="wrap-popup product-detail info col-2" id="info">
            <div class="head"> 
                <h5>Mileage</h5>
                <a href="javascript:closePop('info')" class="close">close</a>
            </div>
            <div class="content">
                <div class="title-content after_clear">
                    <h3 class="left">Info Mileage</h3>
                </div>
                <div class="myaccount box-content-popup">
                    <div class="box-list-data">
                        <br>
                        <div style="font-size:12px;">
                            Mirae Asset Sekuritas Indonesia would like to thank you for your trust to choose us
                            as your investment partner. Please find below the following details regarding your mileage
                            redemptions proccess:
                        </div>
                        <br>
                        <hr>
                        <div class="desc-list">
                            <table width="300" height="150" style="font-size:14px;">
                                <tr>
                                    <td>Name</td>
                                    <td>{{ $brokerName }}</td>
                                </tr>
                                <tr>
                                    <td>Customer ID</td>
                                    <td>{{ $brokerId }}</td>
                                </tr>
                                <tr>
                                    <td>Total Mileage Point</td>
                                    <td>{{ @$mileage->mileage > 0 ? $mileage->mileage : 0 }}</td>
                                </tr>
                                <tr>
                                    <td>Total Reward</td>
                                    <td>Rp {{ @$mileage->mileage > 0 ? formatUangNoComa($mileage->mileage) : 0 }}</td>
                                </tr>
                                <tr>
                                    <td>Type of Reward</td>
                                    <td>Cashback / Invest</td>
                                </tr>
                                <tr>
                                    <td>Profil Resiko</td>
                                    <td>{{ $labelRiskOnly }}</td>
                                </tr>
                            </table>
                            <hr>
                            <div style="margin-top:20px;">
                                <div style="font-size:12px;">
                                    <a href = "{{ urlLang("terms-conditions") }}" style="color:red;">terms & conditions</a> 
                                    regarding my mileage point redemption.
                                </div>
                                <div style="margin-top:10px;">
                                    <div style="text-decoration: underline;font-size: 12px;font-weight: bold;">
                                        Further information regarding Mileage Reward Program, please contact our Customer Service:                                        
                                    </div>
                                    <div style="margin-top: 10px;">
                                        <table style="font-size: 12px;" width="400">
                                            <tr>
                                                <td>Email</td>
                                                <td>:</td>
                                                <td>customerservice@miraeasset.co.id</td>
                                            </tr>
                                            <tr>
                                                <td>Telp</td>
                                                <td>:</td>
                                                <td>021 2553-1000</td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                                <br><br>
                                {{-- <input type="submit" name="" value="KONFIRM" class="btn-oval btn-orange"> --}}
                            </div>
                            
                            {!! Form::open(['method'=>'post','id'=>'formna','url'=>urlLang("user/mileage")]) !!}
                                <table class="" id = "customers">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Voucher Type</th>
                                            <th>Fund Name</th>
                                            <th>Risk Profile</th>
                                            <th>Prospektus</th>
                                            <th>FFS</th>
                                            
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        $model = new \App\Models\Oracle\FundInfo();
                                        ?>
                                        @foreach($products as $type => $codes)
                                            @foreach($codes as $code)
                                            <?php
                                                $fund = $model->where('fund_cd',$code)->first();
                                            ?>

                                                @if(!empty($fund->fund_cd))
                                                    <tr>
                                                        <td>
                                                            <label class="radio-btn">
                                                                <input class="fund" type="radio" name = "fund" value = "{{ $code }}">
                                                            </label>
                                                        </td>
                                                        <td>
                                                            @if($type == 'pemula')
                                                            Voucher Investasi Pemula (Minimum Rp 100,000)
                                                            @else
                                                            Voucher Investasi Ekonomis (Tidak Ada minimum)
                                                            @endif
                                                        </td>
                                                        <td>{{ $fund->fund_name }}</td>
                                                        <td>{{ $fund->result_fund_risk }}</td>
                                                        <td>
                                                            @if(!empty($fund->mysql_fund->id))
                                                            <a href="{{ asset('contents/'.$fund->mysql_fund->prospectus) }}">Prospektus</a>
                                                            @else
                                                            -
                                                            @endif
                                                        </td>
                                                        <td>ffs</td>
                                                        
                                                    </tr>
                                                @endif
                                            @endforeach
                                        @endforeach
                                    </tbody>
                                </table>
                                <br><br>
                                <div style="font-size:12px;margin-top: 20px;">
                                    <label class="radio-btn">
                                        <input class="aggree" type="checkbox" name="aggree" id = "cek_agree">
                                    </label>
                                    Once clicked, there`ll be a pop up regarding details of T & C
                                </div>
                                <br><br>
                                <a href="javascript::void(0)" class="btn-std yellow" id = "redeem">Redeem</a>
                            {!! Form::close() !!}

                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif
@endif

<div class="wrap-popup product-detail term col-2" id="term">
    <div class="head"> 
        <h5>Syarat Dan KetentuanMileage</h5>
        <a href="javascript:closePop('term');showPop('info')" class="close">close</a>
    </div>
    <div class="content">
        <div class="title-content after_clear">
            <h3 class="left">Syarat Dan Ketentuan</h3>
        </div>
        <div class="myaccount box-content-popup">
            <p>
                1.  Proses penukaran poin selambat-lambatnya 7 hari kerja sejak pengajuan permohonan penukaran poin.<br/>
                2.  Penukaran poin  hanya berlaku bagi nasabah aktif PT Mirae Asset Sekuritas Indonesia.<br/>
                3.  Poin tidak dapat ditukarkan sebagian.<br/>
                4.  Poin yang telah dalam proses penukaran tidak dapat dikembalikan atau dibatalkan.<br/>
                5.  Poin yang tidak ditukarkan sampai dengan 2 Juli 2018 akan dianggap hangus dan secara otomatis akan didonasikan untuk kegiatan amal.<br/>
                6.  Nasabah yang memutuskan untuk menukarkan poin mileage menjadi produk reksa dana, menyetujui untuk tunduk terhadap tata cara dan aturan transaksi reksa dana di PT Mirae Asset Sekuritas Indonesia<br/>
                7.  Pajak untuk opsi “Cashback” akan sepenuhnya dibebankan kepada nasabah<br/>
                8.  PT Mirae Asset Sekuritas Indonesia berhak untuk membatalkan permintaan penukaran poin nasabah pada kondisi tertentu.<br/>
                9.  PT Mirae Asset Sekuritas Indonesia sewaktu-waktu berwenang dalam melakukan perubahan kondisi selama atau setelah program ini berlangsung tanpa pemberitahuan sebelumnya.

            </p>
        </div>
    </div>
</div>

@include("frontend.users.first_login.pop.notify")
@endsection
@push('scripts')
    <script type="text/javascript">
        function popNotes()
        {
            showPop('notify');
        }
        function compareHome(fund)
        {
            $("#fund_compare").val(fund);
            $("#form_comare").submit();
        }
        showPop('info')

        function show_term()
        {
            closePop("info");
            showPop('term');
        }

    </script>
    <script type="text/javascript">
    $(document).ready(function(){
        $("#redeem").on('click',function(){
            if ($('#cek_agree').is(':checked')) {
                 var fundSelected = $("input[name='fund']:checked").val();
                   if (fundSelected) {
                        $.ajax({
                            url: '{{ urlLang("user/mileage/ajax-validation") }}',
                            data:{
                                code: fundSelected,
                            },
                            success: function(response){
                                if(response.result == true)
                                {
                                    $("#formna").submit();
                                }else{
                                    alert('Mohon maaf permintaan Anda tidak dapat kami proses dikarenakan jumlah mileage Anda kurang dari Rp 100,000');
                                }
                            },
                        });
                   }else{
                        alert('{{ language("You have not selected yet","anda belum memilih") }}');
                   }
                
            }else{
               alert("{{ language('you have not checked the agreement','anda belum mencentang persetujuan') }}");
            }
                   

        });
    });
</script>
@endpush
