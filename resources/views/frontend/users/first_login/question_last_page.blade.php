@extends('layouts.layout')
@section('content')
    <section class="banner">
        <img src="{{assetFrontend(null)}}images/content/banner-produk.jpg" alt="banner" />
        <div class="caption">
            <h6>Registration Complete</h6>
            <h1>{{ trans('global.welcome') }}<br/>
                {{ trans('global.at') }} MaxFund</h1>
            <p>&nbsp;</p>
        </div>
    </section>
    <section class="std-content gn-content">
        <div class="wrapper ">
            <h2 class="a-center">{{ trans('login.Confirm-Account-Registration') }}</h2>
                {!! trans('login.Registration-of-your-account-at-Mirae-Asset-Express-Fund-we-have-received-our-customer-service-staff-will-contact-you-(on-days-and-hours-of-exchange)-for-the-verification-process.') !!}
            
            <p>
                {{ trans('login.make-sure-the-phone-number-you-enter-is-correct-and-in-active-condition') }}
            </p>

            <p><a href="{{ urlLang('user/profile') }}" class="btn-std">{{ trans('login.back-to-dashboard') }}</a></p>
        </div>
    </section>
@endsection

