@extends('layouts.layout')
@section('content')
    <section class="banner">
        <img src="{{assetFrontend(null)}}images/content/banner-produk.jpg" alt="banner" />
        <div class="caption">
            <h6>Notify</h6>
            <h1>Selamat Datang<br/>
                di MaxFund</h1>
            <p>&nbsp;</p>
        </div>
    </section>
    <section class="std-content">
        <div class="wrapper ">
            <h2 class="a-center">DATA COMPLETED</h2>
            <p>

            </p>
        </div>
    </section>

    @include("frontend.users.first_login.pop.notify")

@endsection
@push('scripts')
    <script type="text/javascript">
        $(document).ready(function(){
            showPop('notify');
        });
    </script>
@endpush
