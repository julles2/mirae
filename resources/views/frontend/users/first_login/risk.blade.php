@extends('layouts.layout')
@section('content')
    <section class="banner">
        <img src="{{assetFrontend(null)}}images/content/banner-produk.jpg" alt="banner" />
        <div class="caption">
            <h6>{{ trans('login.Additional-Question') }}</h6>
            <h1>Selamat Datang<br/>
                di MaxFund</h1>
            <p>&nbsp;</p>
        </div>
    </section>
    <section class="std-content">
    <div class="wrapper ">
        <h2 class="a-center">{{ trans('login.Risk-Profile-Questionnaire') }}</h2>
        <div class="shadow-spar"></div>
        {!! Form::open(['id'=>'form']) !!}
            <div class='box-form pertanyaan'>
                <div class="row">
                    <div class="left">
                        <label><b>{{ trans('login.statement') }} 1</b></label>
                    </div>
                    <div class="right">
                        <div>
                            <label>{{ trans('login.how-old-are-you-?') }}</label>
                        </div>
                        <div class="col-4">
                            <div class="col"><label class="radio-btn">{!!  Form::radio('r1', '5', null)!!}< 35 {{ trans('login.year') }}</label></div>
                            <div class="col"><label class="radio-btn">{!!  Form::radio('r1', '10', null)!!}36 - 45 {{ trans('login.year') }}</label></div>
                            <div class="col"><label class="radio-btn">{!!  Form::radio('r1', '15', null)!!}46 - 60 {{ trans('login.year') }}</label></div>
                            <div class="col"><label class="radio-btn">{!!  Form::radio('r1', '20', null)!!}>61 {{ trans('login.year') }}</label></div>
                        </div>

                    </div>
                </div>
                <div class="row">
                    <div class="left">
                        <label><b>{{ trans('login.statement') }} 2</b></label>
                    </div>
                    <div class="right">
                        <div>
                            <label>{{ trans('login.How-long-does-it-take-to-interact-?') }}</label>
                        </div>

                        <div class="col-4">
                            <div class="col"><label class="radio-btn">{!!  Form::radio('r2', '5', null)!!}< 1 {{ trans('login.year') }}</label></div>
                            <div class="col"><label class="radio-btn">{!!  Form::radio('r2', '10', null)!!}1 s.d 3 {{ trans('login.year') }}</label></div>
                            <div class="col"><label class="radio-btn">{!!  Form::radio('r2', '15', null)!!}3 s.d 5 {{ trans('login.year') }} </label></div>
                            <div class="col"><label class="radio-btn">{!!  Form::radio('r2', '20', null)!!}> 5 {{ trans('login.year') }}</label></div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="left">
                        <label><b>{{ trans('login.statement') }} 3</b></label>
                    </div>
                    <div class="right">
                        <div>
                            <label>{{ trans('login.which-of-the-following-statements-would-suit-your-purpose-in-investing-?') }}</label>
                        </div>

                        <div>
                            <label class="radio-btn">{!!  Form::radio('r3', '5', null)!!}{{ trans('login.to-earn-revenue-on-a-regular-basis') }}</label>
                        </div>
                        <div>
                            <label class="radio-btn">{!!  Form::radio('r3', '10', null)!!}{{ trans('login.seeking-safe-investments,-even-with-relatively-small-returns') }}</label>
                        </div>
                        <div>
                            <label class="radio-btn">{!!  Form::radio('r3', '15', null)!!}{{ trans('login.to-earn-a-profit-in-the-medium-term-with-a-level-of-risk-that-is-not-high') }}</label>
                        </div>
                        <div>
                            <label class="radio-btn">{!!  Form::radio('r3', '20', null)!!}{{ trans('login.to-gain-maximum-growth-in-long-term-investing') }}</label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="left">
                        <label><b>{{ trans('login.statement') }} 4</b></label>
                    </div>
                    <div class="right">
                        <div>
                            <label>{{ trans('login.how-stable-is-your-income-in-the-last-5-(five)-years-?') }}</label>
                        </div>

                        <div>
                            <label class="radio-btn">{!!  Form::radio('r4', '5', null)!!}{{ trans('login.will-rise-above-inflation') }}</label>
                        </div>
                        <div>
                            <label class="radio-btn">{!!  Form::radio('r4', '10', null)!!}{{ trans('login.it-is-likely-to-rise-according-to-inflation') }}</label>
                        </div>
                        <div>
                            <label class="radio-btn">{!!  Form::radio('r4', '15', null)!!}{{ trans('login.the-possibilities-will-be-relatively-the-same') }}</label>
                        </div>
                        <div>
                            <label class="radio-btn">{!!  Form::radio('r4', '20', null)!!}{{ trans('login.the-possibility-will-be-relatively-decreased') }}</label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="left">
                        <label><b>{{ trans('login.statement') }} 5</b></label>
                    </div>
                    <div class="right">
                        <div>
                            <label>{{ trans('login.what-kind-of-investment-do-you-have-today-?') }}</label>
                        </div>

                        <div>
                            <label class="radio-btn">{!!  Form::radio('r5', '5', null)!!}{{ trans('login.savings-and-Deposits') }}</label>
                        </div>
                        <div>
                            <label class="radio-btn">{!!  Form::radio('r5', '10', null)!!}{{ trans('login.savings,-Deposits,-Mutual-Funds,-Gold-and-Bonds') }}</label>
                        </div>
                        <div>
                            <label class="radio-btn">{!!  Form::radio('r5', '15', null)!!}{{ trans('login.savings,-Deposits,-Mutual-Funds,-Bonds,-Gold-and-Stocks') }}</label>
                        </div>
                        <div>
                            <label class="radio-btn">{!!  Form::radio('r5', '20', null)!!}{{ trans('login.savings-and-Gold') }}</label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="left">
                        <label><b>{{ trans('login.statement') }} 6</b></label>
                    </div>
                    <div class="right">
                        <div>
                            <label>{{ trans('login.how-much-is-the-allocation-of-investment-in-Mutual-Funds-from-total-assets-owned-?') }}</label>
                        </div>

                        <div>
                            <label class="radio-btn">{!!  Form::radio('r6', '5', null)!!}{{ trans('login.less-than-20%') }}</label>
                        </div>
                        <div>
                            <label class="radio-btn">{!!  Form::radio('r6', '10', null)!!}{{ trans('login.30%-to-50%') }}</label>
                        </div>
                        <div>
                            <label class="radio-btn">{!!  Form::radio('r6', '15', null)!!}{{ trans('login.between-50%-s.d.-75%') }}</label>
                        </div>
                        <div>
                            <label class="radio-btn">{!!  Form::radio('r6', '20', null)!!}{{ trans('login.more-than-75%') }}</label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="left">
                        <label><b>{{ trans('login.statement') }} 7</b></label>
                    </div>
                    <div class="right">
                        <div>
                            <label>{{ trans('login.to-what-extent-do-you-know-about-the-products-of-the-Mutual-Fund-and-its-industry?') }}</label>
                        </div>

                        <div>
                            <label class="radio-btn">{!!  Form::radio('r7', '5', null)!!}{{ trans('login.just-understand-the-working-mechanism-in-investing-in-Mutual-Funds') }}</label>
                        </div>
                        <div>
                            <label class="radio-btn">{!!  Form::radio('r7', '10', null)!!}{{ trans('login.understand-in-general-about-Mutual-Fund-industry') }}</label>
                        </div>
                        <div>
                            <label class="radio-btn">{!!  Form::radio('r7', '15', null)!!}{{ trans('login.never-invest-in-Mutual-Fund-and-do-not-understand-about-Mutual-Fund') }}</label>
                        </div>
                        <div>
                            <label class="radio-btn">{!!  Form::radio('r7', '20', null)!!}{{ trans('login.understand-well-about-the-types-of-mutual-fund-products-and-working-mechanisms') }}</label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="left">
                        <label><b>{{ trans('login.statement') }} 8</b></label>
                    </div>
                    <div class="right">
                        <div>
                            <label>{{ trans('login.if-in-your-Mutual-Fund-investment-you-are-losing-money,-what-will-you-do?') }}</label>
                        </div>

                        <div>
                            <label class="radio-btn">{!!  Form::radio('r8', '5', null)!!}{{ trans('login.immediate-sales') }}</label>
                        </div>
                        <div>
                            <label class="radio-btn">{!!  Form::radio('r8', '10', null)!!}{{ trans('login.for-sale-when-potential-loss-reaches-5%') }}</label>
                        </div>
                        <div>
                            <label class="radio-btn">{!!  Form::radio('r8', '15', null)!!}{{ trans('login.sold-when-potential-loss-reaches-15%') }}</label>
                        </div>
                        <div>
                            <label class="radio-btn">{!!  Form::radio('r8', '20', null)!!}{{ trans('login.take-it-easy-because-the-loss-is-still-temporary') }}</label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="left">
                        <label><b>{{ trans('login.statement') }} 9</b></label>
                    </div>
                    <div class="right">
                        <div>
                            <label>{{ trans('login.if-you-have-experience-investing-in-Mutual-Fund-products,-what-type-of-Mutual-Fund-are-you-interested-in?') }}</label>
                        </div>

                        <div>
                            <label class="radio-btn">{!!  Form::radio('r9', '5', null)!!}{{ trans('login.protected-Fund') }}</label>
                        </div>
                        <div>
                            <label class="radio-btn">{!!  Form::radio('r9', '10', null)!!}{{ trans('login.money-Market-Funds') }}</label>
                        </div>
                        <div>
                            <label class="radio-btn">{!!  Form::radio('r9', '15', null)!!}{{ trans('login.equity-Fund') }}</label>
                        </div>
                        <div>
                            <label class="radio-btn">{!!  Form::radio('r9', '20', null)!!}{{ trans('login.fixed-Income-Fund-/-Mixed Fund') }}</label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="left">
                        <label><b>{{ trans('login.statement') }} 10</b></label>
                    </div>
                    <div class="right">
                        <div>
                            <label>{{ trans('login.how-much-do-you-know-about-investing-in-the-Capital-Market?') }}</label>
                        </div>
                        <div>
                            <label class="radio-btn">{!!  Form::radio('r10', '5', null)!!}{{ trans('login.not-understanding-at-all') }}</label>
                        </div>
                        <div>
                            <label class="radio-btn">{!!  Form::radio('r10', '10', null)!!}{{ trans('login.have-some-knowledge') }}</label>
                        </div>
                        <div>
                            <label class="radio-btn">{!!  Form::radio('r10', '15', null)!!}{{ trans('login.have-a-good-knowledge-of-the-results-and-risks-that-exist-in-investment-in-the-Capital-Market') }}</label>
                        </div>
                        <div>
                            <label class="radio-btn">{!!  Form::radio('r10', '20', null)!!}
                                {{ trans('login.very-understanding,-especially-about-the-risks-that-exist-in-investasidi-Capital-Market-as-well-strategy-of-how-to-invest-in-Capital-Market') }}
                            </label>
                        </div>
                    </div>
                </div>

                <div class="row border-dashed">
                    <div class="left">
                        <label><b>Disclaimer</b></label>
                    </div>
                    <div class="right">
                        <label class="checkbox-agreement">
                            <input id = "agree" type="checkbox" value="" />
                            {{ trans('login.I-hereby-declare-the-input-data-is-correct') }}
                        </label>
                    </div>
                </div>
                <div class="row bt">
                    <div class="left">
                    </div>
                    <div class="right a-right">
                        <button  type="button" class="btn-std yellow" id = "btn" name="button">{{ trans('login.submit') }}</button>
                    </div>
                </div>
            </div>
        {!! Form::close() !!}
    </div>
</section>
@endsection
@push('scripts')
<script type="text/javascript">
    $(document).ready(function(){
        $("#btn").on('click',function(){
            if($("#agree").is(':checked'))
            {
                $("#form").submit();
            }else{
                swal_error("{{ trans('login.You-have-not-yet-accepted-the-disclaimer') }}");
            }
        });
    });
</script>

@endpush
