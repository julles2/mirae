<div class="wrap-popup product-detail notify col-1 non-close" id="notify">
    <div class="head">
        <h5>{{ trans('global.purchase_of_mutual_fund') }}</h5>
        <?php /*
            <a href="javascript:closePop('pembelian')" class="close">close</a>
        */ ?>
    </div>
    <div class="id"></div>
    <div class="content">
        <div class="title-content after_clear">
            <h3 class="left">{{ trans('global.notes') }}</h3>
        </div>
        <div class="myaccount box-content-popup">
            <div class="box-list-data pembelian-list-data">
                <div class="list-data list_2">
                    <p>
                            {{ trans('global.notify_first_time_login') }}
                    </p>
                    <a class="btn-std" href = "javascript::void(0)">{{ trans('global.data_udpates') }}</a>
                    &nbsp;
                    <a href = "{{ urlLang('user/question/last-page-question') }}" class = "btn-std">{{ trans('global.next') }}</a>
                </div>
            </div>
        </div>
    </div>

</div>
