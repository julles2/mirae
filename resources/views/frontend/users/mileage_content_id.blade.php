@if($cek_user == 'redeem')
<div class="content-tab-general">
    <div class="list-tab">
        <div class="title-content after_clear">
            <h3>
                Mileage
            </h3>	
        </div>
		<div class="disclaimer-box rounded">
           <h4 style="font-size: 18px;">Info</h4>
            <p style="font-size: 18px;margin-top:10px;">
            	Terima kasih, permintaan Anda sudah kami terima. Proses penukaran poin akan memakan waktu sampai  dengan 7 hari kerja.
            </p>
        </div>

    </div>
</div>
@else
<div class="content-tab-general">
    <div class="list-tab">
        <div class="title-content after_clear">
            <h3>
                Mileage
            </h3>
        </div>
        <hr class="line-blue">
            <div class="box-data-diri">
                <div class="row-data-diri">
                    <div class="col col_33">
                        <label>
                            Nama Nasabah
                        </label>
                        <strong>
                            {{ $brokerName }}
                        </strong>
                    </div>
                    <div class="col col_33">
                        <label>
                            ID Nasabah
                        </label>
                        <strong>
                            {{ $brokerId }}
                        </strong>
                    </div>
                    <div class="col col_33">
                        <label>
                            Jumlah Mileage Point
                        </label>
                        <strong>
                            {{ @$mileage->mileage > 0 ? $mileage->mileage : 0  }}
                        </strong>
                    </div>
                </div>
                <div class="row-data-diri">
                    <div class="col col_33">
                        <label>
                            Jumlah Reward
                        </label>
                        <strong>
                            Rp {{ @$mileage->mileage > 0 ? formatUangNoComa($mileage->mileage) : 0 }}
                        </strong>
                    </div>
                    <div class="col col_33">
                        <label>
                            Profil Resiko
                        </label>
                        <strong>
                            {{ $labelRiskOnly }}
                        </strong>
                    </div>
                    
                </div>
            </div>
            <div class="box-list-data disclaimer">
                <div class="list-data">
                    <div class="inner-list-data">
                        <div class="disclaimer-box rounded">
                            <h4>
                                Info Penukaran Point Mileage:
                            </h4>
                            
                            <br>
                                <br>
                                    <ul>
                                        <li>
                                            - Cashback (Harap melakukan penukaran melalui HOTS).
                                        </li>
                                        <li>
                                            - Voucher Investasi (Dapat memilih produk Reksa Dana yang berada di bawah).
                                        </li>
                                        <li>
                                            - Proses Penukaran poin mileage akan memakan waktu hingga 7 (tujuh) hari kerja.
                                        </li>
                                        <li>
                                            - Pajak penukaran poin mileage menjadi cashback akan ditanggung oleh Nasabah.
                                        </li>
                                    </ul>
                                    <br>
                                        {{--
                                        <select id="" name="" style="width:100px;font-size: 12px;margin-top: 10px;">
                                            <option value="">
                                                Pilih
                                            </option>
                                            <option value="yes">
                                                Yes
                                            </option>
                                            <option value="no">
                                                No
                                            </option>
                                        </select>
                                        --}}
                                        <div style="font-size:9px;">
                                            *Proses penukaran poin akan memakan waktu sampai  dengan 7 hari kerja
                                        </div>
                                    </br>
                                </br>
                            </br>
                        </div>
                    </div>
                </div>
            </div>
        </hr>
    </div>
</div>
<div>
	{!! Form::open(['method'=>'post','id'=>'formna']) !!}
    <table class="product product-list-table">
        <thead>
            <tr>
                <th>Tipe Voucher</th>
                <th>REKSA DANA</th>
                <th>Profil Risiko</th>
                <th>Prospektus</th>
                <th>FFS</th>
                <th>#</th>
            </tr>
        </thead>
        <tbody>
        	<?php
        	$model = new \App\Models\Oracle\FundInfo();
        	?>
        	@foreach($products as $type => $codes)
        		@foreach($codes as $code)
        		<?php
        			$fund = $model->where('fund_cd',$code)->first();
        		?>

	        		@if(!empty($fund->fund_cd))
			            <tr>
			            	<td>
			            		@if($type == 'pemula')
			            		Voucher Investasi Pemula (Minimum Rp 100,000)
								@elseif($type == 'capital')
			            		Voucher Investasi Pemula (Minimum Rp 500,000)
								@else
								Voucher Investasi Ekonomis (Tidak Ada minimum)
			            		@endif
			            	</td>
			            	<td>{{ $fund->fund_name }}</td>
			            	<td>{{ $fund->result_fund_risk }}</td>
			            	<td>
			            		@if(!empty($fund->mysql_fund->id))
								<a href="{{ asset('contents/'.$fund->mysql_fund->prospectus) }}">Prospektus</a>
								@else
								-
			            		@endif
			            	</td>
			            	<td>ffs</td>
			            	<td>
			            		<label class="radio-btn">
                                    {{-- <input type = "text" value = '{{ $type }}' id ="tipe{{ $code }}" /> --}}
                                    <input class="fund" type="radio" name = "fund" value = "{{ $code }}">
                                </label>
			            	</td>
			            </tr>
			        @endif
		        @endforeach
	        @endforeach
        </tbody>
    </table>
	<br><br>
	<div style="font-size:12px;margin-top: 20px;">
    	<label class="radio-btn">
            <input class="aggree" type="checkbox" name="aggree" id = "cek_agree">
        </label>
        Dengan ini saya menyatakan bahwa saya telah setuju dengan syarat
            dan ketentuan yang berlaku terkait dengan penukaran mileage point milik saya.
    </div>
	</br></br>
	<a href="javascript::void(0)" class="btn-std yellow" id = "redeem">Tukar</a>
    {!! Form::close() !!}
</div>
@endif
@endsection