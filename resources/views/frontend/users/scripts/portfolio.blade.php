@push('scripts')
<script type="text/javascript">
    $(document).ready(function(){
       $listFund = $("select[name='to_fund']");
        $($listFund).on('change',function(){
            fundCd = $listFund.val();
            $.ajax({
                type : 'get',
                url : '{{ urlLang("user/portfolio/ajax-nab") }}',
                data: {
                    fund_cd : fundCd,
                },
                success : function(respon){
                    $("#ajax_to_nab_last").html(respon.ajax_to_nab_last);
                    $("#ajax_to_date_last").html(respon.ajax_to_date_last);
                    $("#ajax_to_min_investment").html(respon.ajax_to_min_investment);
                    $("#ajax_to_min_increment").html(respon.ajax_to_min_increment);
                    $("#ajax_url_prospektus").attr('href',respon.ajax_url_prospektus);

                },
            });
       });

        $(".overlay-popup").click(function(){
           $(".unit").val("");
           $(".amount").val("");
           $(".unit").removeAttr('readonly');
           $(".unit").css('background-color','white');
           $(".amount").removeAttr('readonly');
           $(".amount").css('background-color','white');
           $('.box_all').removeAttr('checked');
        });

        $("#confirm_topup").on('click',function(){
            $orderType = $("input[name='order_type']:checked").val();
            $.ajax({
                    url : '{{ urlLang("user/portfolio/validate-topup") }}',
                    type : 'get',
                    data : {
                        fund_id : $(".id").html(),
                        nilai : $("#nilai_pembelian").val(),
                        _token : '{{ csrf_token() }}',
                        orderType : $orderType,
                        tanggal : $("#tanggal").val(),
                    },
                    success : function(response){
                        if(response.result == 'false')
                        {
                            swal({
                                type : 'error',
                                title : 'Error',
                                text : '{{ trans('product.Value-Less-than-the-minimum-value-of-subsequent-investments') }}'
                            });
                        }else if(response.result == 'true'){
                            $("#form_pembelian").attr("action", "{{ urlLang('user/portfolio/topup/') }}/" + response.model.id);
                            $("#form_pembelian").submit();
                        }else if(response.result == 'double'){
                            swal({
                              title: "Are you sure?",
                              text: "<p>{{ trans('global.notif_double_subs1') }} <b>"+ response.view.ref_no +"</b> {{ trans('global.notif_double_subs2') }} <b>"+ response.view.tr_date + " "+ response.view.tr_time +"</b>.</p><p>{{ trans('global.notif_double_subs3') }}?</p>",
                              type: "warning",
                              showCancelButton: true,
                              confirmButtonColor: "#DD6B55",
                              confirmButtonText: "Lanjutkan",
                              cancelButtonText: "Batal",
                              closeOnConfirm: false,
                              html: true,
                            },
                            function(){
                                $("#form_pembelian").attr("action", "{{ urlLang('user/portfolio/topup/') }}/" + response.model.id);
                                $("#form_pembelian").submit();

                            });
                        }
                    },
            });

        });



    });

    function pop_jual(fundCd)
    {
        $.ajax({
            url : '{{ urlLang("user/portfolio/ajax-pop-jual") }}',
            data : {
                fundCd : fundCd,
            },
            beforeSend : function(){
                showPop('loaderpopup');
            },
            success : function(respon){
                $.each(respon.result,function(key,value){
                    if(key == 'ajax_fund_cd')
                    {
                        $("."+key).val(value);
                    }else if(key == 'ajax_image'){
                        $('.ajax_image').attr('src',value);
                    }else if(key == 'ajax_minus_plus'){
                        $("#penjualan .icwp").removeClass('ic_up');
                        $("#penjualan .icwp").removeClass('ic_down');

                        if(value ==="up")
                            $("#penjualan .icwp").addClass('ic_up');
                        else
                            $("#penjualan .icwp").addClass('ic_down');
                    }else{
                     $("."+key).html(value);
                    }
                });

                closePop('loaderpopup');
                
                showPop('penjualan');
            },
            async:false,
        });


    }

    function pop_pengalihan(fundCd)
    {
        $.ajax({
            url : '{{ urlLang("user/portfolio/ajax-pop-pengalihan") }}',
            data : {
                fundCd : fundCd,
            },
            beforeSend : function(){
                showPop('loaderpopup');
            },
            success : function(respon){
                $.each(respon.result,function(key,value){
                    if(key == 'ajax_fund_cd')
                    {
                        $("."+key).val(value);
                    }else if(key == 'ajax_image'){
                        $('.ajax_image').attr('src',value);
                    }else if(key == 'ajax_minus_plus'){
                        $("#pengalihan.icwp").removeClass('ic_up');
                        $("#pengalihan .icwp").removeClass('ic_down');

                        if(value ==="up")
                            $("#pengalihan .icwp").addClass('ic_up');
                        else
                            $("#pengalihan .icwp").addClass('ic_down');


                    }else{
                     $("."+key).html(value);
                    }
                });

                $listFund = $("select[name='to_fund']");

                $listFund.html("");

                if(respon.funds == 'false')
                {
                    $listFund.html("<option value = ''>{{ trans('global.no_funds_to_choose_from') }}</option>");
                }else{
                    options = "";
                    $.each(respon.funds,function(key,value){
                        options += "<option value = '"+key+"'>"+value+"</option>";
                    });

                    $listFund.html(options);
                }


                closePop('loaderpopup');
                showPop('pengalihan');
            },
            async:false,
        });
    }

    function pop_topup(fundCd)
    {
       $.ajax({
            url : "{{ urlLang('user/portfolio/ajax-pop-topup') }}",
            success : function(respon){

                $.each(respon.fund,function(key,val){
                    if(isNaN(val) == true)
                    {
                        result = val;
                    }else{
                        result = numeral(val).format('0,0');
                    }

                    $("."+key).html(result);
                });

                $.each(respon.vars,function(key,val){
                   $("."+key).html(val);
                   if(key == 'ajax_image')
                   {
                        $('.ajax_image').attr('src',val);
                   }
                });

                showPop("topup");
            },
            data : {
                fundCd : fundCd,
            },
        });
    }

    function nilaiTrigger(val)
    {
        uangField(val);
    }

    function cancelChangeToall()
    {
        $(".unit").val("");
        $(".amount").val("");
        $(".unit").removeAttr('readonly');
        $(".amount").removeAttr('readonly');
        $(".unit").css('background-color','white');
        $(".amount").css('background-color','white');
        $(".box_all").prop('checked',false);
    }

    function validateForm()
    {
        $unit = $("#formBeli .unit").val();
        $amount = $("#formBeli .amount").val();

        if($unit == "" || $amount=="")
        {
            swal({
                type : 'error',
                html : true,
                title : 'Error',
                text : '{{ trans("global.You_have_not_selected_sales_method_yet") }}',
            });
        }else{
            $fund_cd = $("#penjualan_fund_cd").val();
            $.ajax({
                url : '{{ urlLang("user/portfolio/ajax-validate-penjualan") }}',
                data : {
                    'fund_cd': $fund_cd,
                    'unit': $unit,
                },
                success: function(respon){
                    response = respon;
                    if(respon.result == 'all')
                    {
                        swal({
                              title: "{{ trans('global.are_you_sure') }}",
                              text: "{{ trans('global.notif_all_unit') }}",
                              type: "warning",
                              showCancelButton: true,
                              closeOnConfirm: true,
                              closeOnCancel: true
                            },
                            function(isConfirm){
                              if (isConfirm) {
                                  $(".box_all").prop('checked',true);
                                  changeToall();
                                  $("#formBeli").submit();
                              } else {
                                  cancelChangeToall();
                              }
                        });
                    }else if(respon.result == 'false'){
                        swal_error('{{ trans("units_are_insufficient_to_sell") }}');
                    }else if(respon.result == 'double'){
                        // swal({
                        //   title: "Are you sure?",
                        //   text: "<p>hari ini anda telah melakukan instruksi penjualan dengan produk reksa dana yang sama, dengan nomor transaksi <b>"+ response.view.ref_no +"</b> pada tanggal <b>"+ response.view.tr_date + " "+ response.view.tr_time +"</b>.</p><p>apakah anda tetap akan melakukan penjualan?</p>",
                        //   type: "warning",
                        //   showCancelButton: true,
                        //   confirmButtonColor: "#DD6B55",
                        //   confirmButtonText: "Lanjutkan",
                        //   cancelButtonText: "Batal",
                        //   closeOnConfirm: false,
                        //   html: true,
                        // },
                        // function(){
                        //     $("#formBeli").submit();
                        // });

                        swal({
                          type: 'info',
                          title : 'Info',
                          text : '{{ trans("global.notif_double") }}',
                        });

                    }else if(respon.result == 'true'){
                        $("#formBeli").submit();
                    }else if(respon.result == 'saldo_kurang'){

                        swal({
                              title: "{{ trans('global.are_you_sure') }}",
                              text: "{{ trans('global.minimum_balance_remaining') }} : "+respon.sisaSaldo+", {{ trans('global.are_you_going_to_sell_all_the_units') }}",
                              type: "warning",
                              showCancelButton: true,
                              closeOnConfirm: true,
                              closeOnCancel: true
                            },
                            function(isConfirm){
                              if (isConfirm) {
                                  $(".box_all").prop('checked',true);
                                  changeToall();
                                  $("#formBeli").submit();
                              } else {
                                  cancelChangeToall();
                              }
                        });
                    }else if(respon.result == 'minimum_kurang'){
                        swal_info('{{ trans("global.sales_are_less_than_the_minimum_sales") }}');
                    }else{
                        alert('no action');
                    }

                },
            });

            //$("#form_pengalihan").submit();
        }

    }

    function validateFormPengalihan()
    {
        $toFund = $("select[name='to_fund']").val();
        $unit = $("#form_pengalihan .unit").val();
        $amount = $("#form_pengalihan .amount").val();

        if($toFund == "")
        {
            swal({
                type : 'error',
                html : true,
                title : 'Error',
                text : '{{ trans("global.you_have_not_selected_fund_destination") }}',
            });
        }else if($("#form_pengalihan .unit").val() == ""){
            swal({
                type : 'error',
                html : true,
                title : 'Error',
                text : '{{ trans("global.you_have_not_filled_the_value_to_be_transferred") }}',
            });

        }else{

            $fund_cd = $("#penjualan_fund_cd").val();
            $.ajax({
                url : '{{ urlLang("user/portfolio/ajax-validate-pengalihan") }}',
                data : {
                    'fund_cd': $fund_cd,
                    'unit': $unit,
                    'amount': $amount,
                    'to_fund':$toFund,
                },
                success: function(respon){
                    if(respon.result == 'all')
                    {
                        swal({
                              title: "{{ trans('global.are_you_sure') }}",
                              text: "{{ trans('gloal.notif_all_unit') }}",
                              type: "warning",
                              showCancelButton: true,
                              closeOnConfirm: true,
                              closeOnCancel: true
                            },
                            function(isConfirm){
                              if (isConfirm) {
                                  $(".box_all").prop('checked',true);
                                  changeToall();
                                  $("#form_pengalihan").submit();
                              } else {
                                  cancelChangeToall();
                              }
                        });
                    }else if(respon.result == 'false'){
                        swal_error('{{ trans("global.units_are_insufficient_to_switch") }}');
                    }else if(respon.result == 'true'){
                        $("#form_pengalihan").submit();
                    }else if(respon.result == 'double'){
                        response = respon;
                        swal({
                          title: "{{ trans('global.are_you_sure') }}",
                          text: "<p>{{ trans('global.notif_double_swc1') }}<b>"+ response.view.ref_no +"</b> {{ trans('global.notif_double_swc2') }} <b>"+ response.view.tr_date + " "+ response.view.tr_time +"</b>.</p><p>{{ trans('global.notif_double_swc3') }}?</p>",
                          type: "warning",
                          showCancelButton: true,
                          confirmButtonColor: "#DD6B55",
                          confirmButtonText: "Lanjutkan",
                          cancelButtonText: "Batal",
                          closeOnConfirm: false,
                          html: true,
                        },
                        function(){
                            $("#form_pengalihan").submit();
                        });
                    }else if(respon.result == 'saldo_kurang'){

                        swal({
                              title: "{{ trans('global.are_you_sure') }}",
                              text: "{{ trans('global.minimum_balance_remaining') }} : "+respon.sisaSaldo+", {{ trans('global.are_you_going_to_switch_all_the_units') }} ? ",
                              type: "warning",
                              showCancelButton: true,
                              closeOnConfirm: true,
                              closeOnCancel: true
                            },
                            function(isConfirm){
                              if (isConfirm) {
                                  $(".box_all").prop('checked',true);
                                  changeToall();
                                  //$("#form_pengalihan").submit();
                              } else {
                                  cancelChangeToall();
                              }
                        });
                    }else if(respon.result == 'keluar_kurang'){
                        swal_info('{{ trans("global.minimum_redirect_less") }}');
                    }else if(respon.result == 'masuk_kurang'){
                        swal_info('{{ trans("global.purchase_further_less") }}');
                    }else{
                        alert('no action');
                    }

                },
            });

            //$("#form_pengalihan").submit();
        }
    }

    function changeToAmount(unit)
    {
        $.ajax({
            url : '{{ urlLang("user/portfolio/ajax-change-to-amount") }}',
            data : {
                unit : unit,
                fundCd : $("#penjualan_fund_cd").val(),
            },
            success : function(respon){
                $(".amount").val(respon.result);
            },
        });
    }

    function changeToUnit(amount)
    {
       $.ajax({
            url : '{{ urlLang("user/portfolio/ajax-change-to-unit") }}',
            data : {
                amount : amount,
                fundCd : $("#penjualan_fund_cd").val(),
            },
            success : function(respon){
                $(".unit").val(respon.result);
            },
        });
    }

    function changeToall()
    {
        $box_all = $(".box_all");

        if($box_all.is(":checked"))
        {
            $.ajax({
                url : '{{ urlLang("user/portfolio/ajax-change-to-all") }}',
                data : {
                    fundCd : $("#penjualan_fund_cd").val(),
                },
                success : function(respon){
                    $(".unit").val(respon.unit);
                    $(".amount").val(respon.amount);
                    $(".unit").attr('readonly',true);
                    $(".unit").css('background-color','#f2f5f9');
                    $(".amount").attr('readonly',true);
                    $(".amount").css('background-color','#f2f5f9');
                },
            });
        }else{
            cancelChangeToall();
        }

        function cancelChangeToall()
        {
            $(".unit").val("");
            $(".amount").val("");
            $(".unit").removeAttr('readonly');
            $(".amount").removeAttr('readonly');
            $(".unit").css('background-color','white');
            $(".amount").css('background-color','white');
        }
    }

    function pop_chart(fund_cd)
    {
        $.ajax({
          url: '{{ urlLang("user/portfolio/ajax-chart") }}',
          type: 'get',
          data: {
            fund_cd: fund_cd,
          },
          beforeSend: function(){
            showPop('loaderpopup');
          },
          success: function(data){
            closePop('loaderpopup');
            showPop('popup-chart');
            chart(data.fund_name,data.range,data.series);
          },
        });

    }

    function chart(fund_name,range,series)
    {
      var myChart = Highcharts.chart('chart-1', {
          chart: {
              zoomType: 'xy'
          },
          title: {
              text: '{{ trans('global.mutual_fund_investment_graph') }}',
          },
          credits: 'miraeasset.co.id',
          subtitle: {
              text: fund_name,
          },
          xAxis: {
              categories: range,
              tickInterval: 31,
          },
          plotOptions: {
              series: {
                  marker: {
                      enabled: false
                  }
              }
          },
          yAxis: {
              title: {
                  text: '{{ trans('global.mutual_fund_investment_graph') }}'
              }
          },
          series: series
      });
    }
</script>
@include("backend.common.sweet_flashes")
@endpush
