@push('scripts')
<script type="text/javascript">


$(document).ready(function(){
    $("#fund_id").on('change',function(){
        fundId = $(this).val();
        $.ajax({
            url: '{{ urlLang("user/debit/ajax-fund") }}',
            data: {
                fund_id: fundId,
            },
            success: function(respon){
                $.each(respon,function(key,val){
                    $("."+key).html(val);
                });
            },
        });
    });
});

function getDetail(ref_no,tr_type)
{
    $.ajax({
        url: '{{ urlLang("user/order/ajax-detail-pop") }}',
        data: {
            ref_no: ref_no,
            type: tr_type
        },
        success: function(respon){
            $.each(respon.params,function(key,val){
                $("#"+key).val(val);
            });
            $("#table_pop").html(respon.view);
        },
    });
    showPop('detail');

}

function getDebit()
{
    showPop('pop-debet');
}

function confirms()
{
    $fund = $("#fund_id").val();
    $tanggal = $("#tanggal").val();
    $nilai = $("#nilai").val();

    if($fund == "")
    {
        swal_error('{{ trans("global.not_filled_fund") }}');
    }else if(parseInt($tanggal) > 31){
        swal_error('{{ trans("global.date_wrong") }}');
    }else{

        $.ajax({
            url: '{{ urlLang("user/debit/ajax-validate") }}',
            data: {
                fundId: $fund,
                nilai: $nilai,
            },
            success: function(respon){
                if(respon.result == "true")
                {
                    $generateUrl = '{{ urlLang("product/pembelian") }}/' + $fund + '/user-debit';
                    $("#form-debit").attr('action',$generateUrl);
                    $("#form-debit").submit();
                }else{
                    swal_error('{{ trans("global.minimum_purchase_value_less") }}');
                }
            }
        });

    }
}

function cancelDebit(ref_no,tr_type)
{

    cek = confirm("{{ trans('global.are_you_sure_want_to_cancel_transaction') }}");
    $reject_msg = $("#rejt_msg").val();
    if(cek)
    {
        if($("#rejt_msg").length)
        {
            if($reject_msg != '')
            {
                url = "{{ urlLang('user/order/cancel?') }}";
                url = url + "ref_no="+ref_no + "&type=" + tr_type + "&msg="+$reject_msg;
                document.location.href=url;
            }else{
                swal_error('{{ trans("global.message_required") }}');
            }

        }

    }
}
</script>

@endpush
