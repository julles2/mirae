@push('scripts')
<script type="text/javascript">
function getDetail(ref_no,tr_type)
{
    $.ajax({
        url: '{{ urlLang("user/order/ajax-detail-pop") }}',
        data: {
            ref_no: ref_no,
            type: tr_type
        },
        beforeSend: function(){
            showPop('loaderpopup');
        },
        success: function(respon){
                closePop('loaderpopup');
                if(respon.params.fail != "fail")
                {
                    $.each(respon.params,function(key,val){
                        if(key == 'hidden_ref_no' || key == 'hidden_tr_type')
                        {
                            $("#"+key).val(val);
                        }
                    });

                    $("#detail_box").html(respon.view);
                    setTimeout(function(){
                        showPop('detail')
                    },500);

                }else{
                    alert('Data di oracle tidak ditemukan');
                }


        },
    });

}

$(document).ready(function(){
    $("#filter").on('click',function(){
        showPop('pop_filter');
    });
});

function val(val)
{
    return $("#" + val).val();
}

function filter()
{
    paramFilter = paramFilter(val("select_date"),val("select_instruksi"),val("text_reksa_dana"),val('select_status'),val('start_date'),val('end_date'));

    document.location.href="{{ urlLang('user/transaction') }}" + paramFilter;
}

function paramFilter(date="",instruksi="",reksa_dana="",status="",start_date="",end_date="")
{

    $result = "?date=" + date + "&instruksi="+instruksi + "&reksa_dana="+reksa_dana + "&status="+status + "&start_date="+start_date+"&end_date="+end_date;

    return $result;
}

</script>

@endpush
