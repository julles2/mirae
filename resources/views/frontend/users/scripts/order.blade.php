@push('scripts')
<script type="text/javascript">
    $(document).ready(function(){
        $("#filter").on('click',function(){
            showPop('pop_filter');
        });
    });

    function val(val)
    {
        return $("#" + val).val();
    }

    function filter()
    {
        paramFilter = paramFilter(val("select_date"),val("select_instruksi"),val("text_reksa_dana"),val('select_status'),val('start_date'),val('end_date'));

        document.location.href="{{ urlLang('user/order') }}" + paramFilter;
    }

    function paramFilter(date="",instruksi="",reksa_dana="",status="",start_date="",end_date="")
    {

        $result = "?date=" + date + "&instruksi="+instruksi + "&reksa_dana="+reksa_dana + "&status="+status + "&start_date="+start_date+"&end_date="+end_date;

        return $result;
    }

    function getRejectMessage(ref_no,tr_type)
    {
        $.ajax({
            url: '{{ urlLang("user/order/ajax-detail-pop") }}',
            data: {
                ref_no: ref_no,
                type: tr_type
            },
            beforeSend: function(){
                showPop('loaderpopup');
            },
            success: function(respon){
                closePop('loaderpopup');

                if(respon.params.fail != "fail")
                {
                    $.each(respon.params,function(key,val){
                        if(key == 'hidden_ref_no' || key == 'hidden_tr_type')
                        {
                            $("#"+key).val(val);
                        }
                    });

                    $("#rejected_box").html(respon.view);
                    setTimeout(function(){
                        showPop('rejected')
                    },500);

                }else{
                    alert('Data di oracle tidak ditemukan');
                }
            },
        });
    }

    function getCancel(ref_no,tr_type)
    {
        $.ajax({
            url: '{{ urlLang("user/order/ajax-detail-pop") }}',
            data: {
                ref_no: ref_no,
                type: tr_type
            },
            beforeSend: function(){
                showPop('loaderpopup');
            },
            success: function(respon){
                closePop('loaderpopup');

                if(respon.params.fail != "fail")
                {
                    $.each(respon.params,function(key,val){
                        if(key == 'hidden_ref_no' || key == 'hidden_tr_type')
                        {
                            $("#"+key).val(val);
                        }
                    });

                    $("#cancel_box").html(respon.view);
                    setTimeout(function(){
                        showPop('cancel')
                    },500);

                }else{
                    alert('Data di oracle tidak ditemukan');
                }
            },
        });


    }

    function getOther(ref_no,tr_type)
    {
        $.ajax({
            url: '{{ urlLang("user/order/ajax-detail-pop") }}',
            data: {
                ref_no: ref_no,
                type: tr_type
            },
            beforeSend: function(){
                showPop('loaderpopup');
            },
            success: function(respon){
                closePop('loaderpopup');

                if(respon.params.fail != "fail")
                {
                    $.each(respon.params,function(key,val){
                        if(key == 'hidden_ref_no' || key == 'hidden_tr_type')
                        {
                            $("#"+key).val(val);
                        }
                    });

                    $("#other_box").html(respon.view);
                    setTimeout(function(){
                        showPop('other')
                    },500);

                }else{
                    alert('Data di oracle tidak ditemukan');
                }
            },
        });


    }

    function getCancelAction(ref_no,tr_type)
    {

        cek = confirm("Are you sure want to cancel this transaction ?");

        if(cek)
        {
            url = "{{ urlLang('user/order/cancel?') }}";

            url = url + "ref_no="+ref_no + "&type=" + tr_type;

            document.location.href=url;
        }
    }

</script>
@endpush
