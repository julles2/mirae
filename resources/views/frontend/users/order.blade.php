@extends('frontend.user')

@section('content')
    <div class="content-tab-general">
                <div class="list-tab">
                    <div class="title-content after_clear">
                        <div class="left">
                            <h3><i class="icwp ic_money"></i> {{ trans('menu.order_status') }}</h3>
                        </div>
                        <div class="right">
                            <a href="javascript:void(0)" class="btn-std left" id = "filter">Filter</a>
                        </div>
                    </div>
                    <div class="overflow order-overflow">
                            <table class="table-radius">
                                <thead>
                                    <tr>
                                        <th style="width: 100px;">{{ trans('login.time') }}</th>
                                        <th>{{ strtoupper(trans('login.mutualfundname')) }}</th>
                                        <th>{{ strtoupper(trans('login.intructions')) }}</th>
                                        <th>{{ strtoupper(trans('login.totalunit')) }}</th>
                                        <th>{{ strtoupper(trans('login.ordervalue')) }}</th>
                                        <th>{{ strtoupper(trans('login.transactionfee'))}}</th>
                                        <th>{{ strtoupper(trans('global.nav')) }}</th>
                                        <th>{{ strtoupper(trans('login.status')) }}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php
                                $trType = function($row){
                                    $stats = [
                                        'R'=>'Redemption',
                                        'S'=>'Subscription',
                                        'swc'=>'Switching',
                                        'SA'=>'Subscription <br/> (Auto Debet)'
                                    ];

                                    if(array_key_exists($row->tr_type, $stats))
                                    {
                                        return $stats[$row->tr_type];
                                    }
                                };
                                ?>
                                @if($listing->count() > 0)
                                    @foreach($listing as $row)
                                    <?php
                                    $procTime = !empty($row->proc_time) ? substr($row->proc_time,0,6) : '000000';
                                    $fee = Mirae::resultPercentageOrAmount($row->fee,$row->amount);
                                    $totalNilai = $row->amount - $fee;
                                    $statusReject = $modelTrans->statusReject($row->proc_cd);
                                    ?>
                                        <tr>
                                            <td>{{ carbonLocalize(parse($row->tr_date)->format("d-F-Y")) }} {{ carbon()->parse($procTime)->format("H:i:s") }}</td>
                                            @if($row->out != '0')
                                                <td><b>Dari</b> <br/>{{ $fund($row->out) }} <br/> <b>Ke</b>  <br/>{{ $fund($row->fund_cd) }}</td>
                                            @else
                                                <td>{{ $fund($row->fund_cd) }}</td>
                                            @endif
                                            <td>{!! $trType($row) !!}</td>
                                            <td>{{ $row->proc_cd >= 60 ? $row->unit : '-' }}</td>
                                            <td>IDR {{ formatUang($row->amount) }}</td>
                                            <td>IDR {{ formatUang($fee) }}</td>
                                            {{-- <td>IDR {{ formatUang($totalNilai) }}</td> --}}
                                            <td>{{ $row->proc_cd >= 60 ? $row->ord_nav : '-' }}</td>
                                            <td>
                                                <?php
                                                if($statusReject !=false)
                                                {
                                                    $onclick = "return getRejectMessage('".$row->ref_no."','".$row->tr_type."')";
                                                }else{
                                                    if(in_array($row->proc_cd,[0,10]))
                                                    {
                                                        $onclick = "return getCancel('".$row->ref_no."','".$row->tr_type."')";
                                                    }else{
                                                        $onclick = "return getOther('".$row->ref_no."','".$row->tr_type."')";
                                                    }
                                                }
                                                ?>
                                                    <a style="text-decoration:underline !important;" href = "javascript:" onclick="{{$onclick}}" class="link link_{{ $modelTrans->statusColor($row->proc_cd) }}">
                                                        {{ $modelTrans->status($row->proc_cd) }}
                                                    </a>
                                            </td>
                                        </tr>
                                    @endforeach

                                    @else
                                    <tr>
                                        <td colspan="8" style="text-align: center;color:blue;">{{ trans('login.datanotfound') }}</td>
                                    </tr>

                                    @endif

                                </tbody>
                            </table>
                    </div>
                    <?php
                    /*
                    <div class="table-nav">
                        <div class="entries left">
                            {{trans('login.show')}}
                            <select>
                                <option>15</option>
                            </select>
                            {{ trans('login.allentries') }}
                        </div>
                        <div class="info right">
                            {{trans('login.showing')}} 1 to 15 of 157
                        </div>
                    </div>
                    */
                    ?>
                </div>
            </div>

            <div class="box-list-data disclaimer">
                <div class="list-data">
                    <div class="inner-list-data">
                        <div class="row no-border">
                            <div class="disclaimer-box rounded">
                                <h4>{{ trans('disclaimer.title_transaksi') }}</h4>
                                <p>
                                    {!! trans('disclaimer.transaksi') !!}
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>



@endsection
@section('content2')
    <div class="wrap-popup product-detail pop-notif" id="pop_filter">
        <div class="head">
            <a href="javascript:closePop('pop_filter')" class="close">{{trans('login.cancel')}}</a>
        </div>
        <div class="content">
            <div class="myaccount after_clear">
                <h3 class="torange">Filter</h3>
                <div class="box-form">
                    <div class="row">
                    {{ trans('login.date') }} : <br/>

                        {!! Form::select('date',['' => trans('login.sortbydate') ,'asc'=>trans('login.oldest'),'desc'=>trans('login.newest')] , get('date') , ['id'=>'select_date']) !!}
                        
                        <p>&nbsp;</p>
                        {{ trans('login.start_date') }}
                        {!! Form::text('start_date', get('start_date') , ['id'=>'start_date','class'=>'datepicker','readonly'=>true,'style'=>'background-color:white;']) !!}

                        <p>&nbsp;</p>
                        {{ trans('login.end_date') }}
                        {!! Form::text('end_date', get('end_date') , ['id'=>'end_date','class'=>'datepicker','readonly'=>true,'style'=>'background-color:white;']) !!}


                        <p>&nbsp;</p>
                        {{ trans('login.instruction') }}
                        {!! Form::select('instruksi',[''=>trans('login.choose').' '.trans('login.instruction'),'S'=>'Subscription','SWC'=>'Switching','R'=>'Redeem'] , get('instruksi') , ['id'=>'select_instruksi']) !!}

                        <p>&nbsp;</p>
                        {{ trans('login.fund') }}
                        {!! Form::text('reksa_dana' , get('reksa_dana') , ['id'=>'text_reksa_dana','placeholder'=>'']) !!}

                        <p>&nbsp;</p>
                        Status:
                        {!! Form::select('status',[''=>trans('login.choose').' '.trans('login.status'),'pending'=>'Pending','reject'=>'Reject','confirm'=>'Confirm'] , get('status') , ['id'=>'select_status']) !!}


                    </div>
                    <div class="row">
                        <a href = "javascript:" onclick = "return filter()" class="btn-std ">Filter</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="wrap-popup product-detail pop-notif" id="rejected">
        <div class="head">
            <a href="javascript:closePop('rejected')" class="close">{{trans('login.close')}}</a>
        </div>
        <div class="content">
            <div class="myaccount">
                <h3 class="torange">{{trans('login.rejected')}}</h3>
                <div id = "rejected_box">

                </div>
            </div>
        </div>
    </div>

    <div class="wrap-popup product-detail penjualan pop-notif  col-1" id="other">
        <div class="head">
            <a href="javascript:closePop('other')" class="close">{{trans('login.close')}}</a>
        </div>
        <div class="content">
            <div class="myaccount">
                <h3 class="torange">{{trans('login.orderstatus')}}</h3>
                <input type = "hidden" id = "hidden_ref_no"/>
                <input type = "hidden" id = "hidden_tr_type"/>

                <div id = "other_box">

                </div>

            </div>
            <p>&nbsp;</p>
			<button type="button" class = "btn-std" onclick="window.print('other_box');">{{trans('login.download')}}</button>
            <button type="button" class = "btn-std" onclick="closePop('other')">{{trans('login.close')}}</button>

        </div>
    </div>

    <div class="wrap-popup product-detail penjualan pop-notif  col-1" id="cancel">
        <div class="head">
            <a href="javascript:closePop('cancel')" class="close">{{trans('login.close')}}</a>
        </div>
        <div class="content">
            <div class="myaccount">
                <h3 class="torange">{{trans('login.orderstatus')}}</h3>
                <input type = "hidden" id = "hidden_ref_no"/>
                <input type = "hidden" id = "hidden_tr_type"/>

                <div id = "cancel_box">

                </div>

            </div>
            <p>&nbsp;</p>
            <button type="button" class = "btn-std" onclick="getCancelAction($('#hidden_ref_no').val(),$('#hidden_tr_type').val())">{{trans('login.cancel')}}</button>
            <button type="button" class = "btn-std" onclick="closePop('cancel')">{{trans('login.close')}}</button>

        </div>
    </div>
@endsection
@include("frontend.users.scripts.order")
