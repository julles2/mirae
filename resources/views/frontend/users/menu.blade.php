<?php
/**
 * Created by PhpStorm.
 * User: DanielSimangunsong
 * Date: 5/17/2017
 * Time: 1:57 PM
 */ ?>
<div class="nav-tab-general">
    <div class="list-tab{{ isset($vMenu) && 'dashboard' === $vMenu ? ' active' : '' }}">
        <a href="{{ url(App::getLocale() . '/user/dashboard') }}">
            {{ ucwords(trans('menu.dashboard')) }}
        </a>
    </div>
    <div class="list-tab{{ isset($vMenu) && 'portfolio' === $vMenu ? ' active' : '' }}">
        <a href="{{ url(App::getLocale() . '/user/portfolio') }}">
            {{ ucwords(trans('menu.portfolio')) }}
        </a>
    </div>
    <div class="list-tab{{ isset($vMenu) && 'order' === $vMenu ? ' active' : '' }}">
        <a href="{{ url(App::getLocale() . '/user/order') }}">
            {{ ucwords(trans('menu.order_status')) }}
        </a>
    </div>
    <div class="list-tab{{ isset($vMenu) && 'transaction' === $vMenu ? ' active' : '' }}">
        <a href="{{ url(App::getLocale() . '/user/transaction') }}">
            {{ ucwords(trans('menu.transaction')) }}
        </a>
    </div>
    <div class="list-tab{{ isset($vMenu) && 'debit' === $vMenu ? ' active' : '' }}">
        <a href="{{ url(App::getLocale() . '/user/debit') }}">
            {{ ucwords(trans('menu.autodebit')) }}
        </a>
    </div>
    <div class="list-tab{{ isset($vMenu) && 'profile' === $vMenu ? ' active' : '' }}">
        <a href="{{ url(App::getLocale() . '/user/profile') }}">
            {{ trans('login.personaldata') }}
        </a>
    </div>
    
    <div class="list-tab{{ isset($vMenu) && 'mileage' === $vMenu ? ' active' : '' }}">
        <a href="{{ url(App::getLocale() . '/user/mileage') }}">
            Mileage
        </a>
    </div>
    <div class="list-tab">
        <a href="{{ urlLang('user/logout') }}">
            {{ ucwords(trans('menu.logout')) }}
        </a>
    </div>
</div>
