@extends('frontend.user')
@section('content')

@include("frontend.users.mileage_content_".lang())

@push('scripts')
<script type="text/javascript">
	$(document).ready(function(){
		$("#redeem").on('click',function(){
            $jml_mileage = '{{ $mileage->mileage }}';

            if($jml_mileage > 0)
            {
                if ($('#cek_agree').is(':checked')) {
                         var fundSelected = $("input[name='fund']:checked").val();
                           if (fundSelected) {
                                $.ajax({
                                    url: '{{ urlLang("user/mileage/ajax-validation") }}',
                                    data:{
                                        code: fundSelected,
                                    },
                                    success: function(response){
                                        if(response.result == true)
                                        {
                                            $("#formna").submit();
                                        }else{
                                            alert('Mohon maaf permintaan Anda tidak dapat kami proses dikarenakan jumlah mileage Anda kurang dari Dana Minimum');
                                        }
                                    },
                                });
                           }else{
                                alert('{{ language("You have not selected yet","anda belum memilih") }}');
                           }
                        
                    }else{
                       alert("{{ language('you have not checked the agreement','anda belum mencentang persetujuan') }}");
                    }
            }else{
                swal("{{ language('your mileage 0, you can not continue the transaction ','mileage anda 0, anda tidak bisa melanjutkan transaksi') }}");
            }
                    
    			   

		});
	});
</script>
@endpush
