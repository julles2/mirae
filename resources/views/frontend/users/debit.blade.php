@extends('frontend.user')

@section('content')
    <div class="content-tab-general">
        <div class="list-tab">
            <div class="title-content after_clear">
                <div class="left">
                    <h3><i class="icwp ic_atm"></i> {{ ucwords(trans('menu.autodebit')) }}</h3>
                </div>
                <div class="right">
                    <a href="javascript::void(0)" onclick = "getDebit()" class="btn-oval btn-orange"><!--i class="icwp ic_plus nomarg"></i--> Register {{ ucwords(trans('menu.autodebit')) }}</a>
                </div>
            </div>
            @if(count($listing)>0)
            <div class="overflow">

                        <table class="table-radius">
                            <thead>
                            <tr>
                                <th>{{ strtoupper(trans('login.mutualfundname')) }}</th>
                                <th>{{ strtoupper(trans('login.startdate')) }}</th>
                                <th>{{ strtoupper(trans('login.enddate')) }}</th>
                                <th>{{ strtoupper(trans('login.timeperiod')) }}</th>
                                <th>{{ strtoupper(trans('login.autodebitvaluepermonth')) }}</th>
                                <th>{{ strtoupper(trans('login.transactionstatus')) }}</th>
                                <th>{{ strtoupper(trans('login.nextautodebet')) }}</th>
                                <th>{{ strtoupper(trans('login.action')) }}</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php

                            foreach($listing as $row)
                            {
                                $from = carbon()->parse($row->from_date);
                                $end = carbon()->parse($row->to_date);
                                $jangka = $end->diffForHumans($from);
                            ?>
                                @if(!empty($row->ref_no))
                                    <tr>
                                        <td>{{ $fund($row->fund_cd) }}</td>
                                        <td>{{ carbonLocalize($from->format("d-F-Y")) }}</td>
                                        <td>{{ carbonLocalize($end->format("d-F-Y")) }}</td>
                                        <td>{{ Mirae::diffHumanIndo($jangka) }}</td>
                                        <td>IDR {{ formatUang($row->ord_amt) }}</td>
                                        <td>{{ $trans->status($row->proc_cd) }}</td>
                                        <td>{{ $nextPayment($row) }}</td>
                                        <td><a href="javascript::void(0)" onclick = "getDetail('{{ $row->ref_no }}','SA')">{{ trans('login.detail') }}</a></td>
                                    </tr>
                                @endif
                            <?php
                            }
                            ?>

                            </tbody>
                        </table>
            </div>
            @else
                <h4>{{ trans('login.count_debet') }}</h4>
            @endif
            <!--div class="table-nav">
                <div class="entries left">
                    Show
                    <select>
                        <option>15</option>
                    </select>
                    All entries
                </div>
                <div class="info right">
                    Showing 1 to 15 of 157 entries
                </div>
            </div-->

            <div class="box-list-data disclaimer">
                <div class="list-data">
                    <div class="inner-list-data">
                        <div class="row no-border">
                            <div class="disclaimer-box rounded">
                                <h4>{{ trans('disclaimer.title_transaksi') }}</h4>
                                <p>
                                    {!! trans('disclaimer.transaksi') !!}
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection
@section('content2')
    <div class="wrap-popup product-detail pengalihan col-1" id="pop-debet">
    <div class="head">
        <h5>Auto Debet {{ trans('login.mutualfund') }}</h5>
        <?php /*
        <a href="javascript:closePop('popup-debet')" class="close">{{ trans('login.close') }}</a>
        */
        ?>
    </div>
    <div class="content">
        <div class="title-content after_clear">
            <h3 class="left">{{ trans("login.autodebet") }}</h3>
        </div>
        <div class="myaccount box-content-popup">
            <div class="box-list-data">
            {!! Form::open(['id'=>'form-debit']) !!}
                <div class="list-data list_2 std-content">
                    <div class="desc-list row_btm">
                        <div class="row no-border">
                            {{ trans('login.mutualfunddestination') }}
                        </div>
                        <div class="row no-border in_full">
                            {!! Form::select('fund_id',$funds,null,['class'=>'select2','style'=>'width:100%;','id'=>"fund_id"]) !!}
                        </div>
                        <div class="row no-border bg-transorange">
                            <div class="left">
                                <label>{{ trans('login.latestnab') }}</label><br>

                                <span class="tsmall ajax_tanggal">Per -</span><br/><br/>
                                <label>Min Investment</label><br>
                            </div>

                            <div class="right a-right">
                                <h5 class="ajax_unit">-</h5>
                                <h5>&nbsp;</h5>
                                <h5 class="ajax_investment">-</h5>
                            </div>
                        </div>
                        <div class="row no-border">
                            <div class="left">{{ trans('login.inputvalue') }}</div>
                            <div class="right">

    <!--                                <label class="radio-btn"><input type="radio" name="rnil"/>Nominal</label>
                                <label class="radio-btn"><input type="radio" name="rnil"/>Unit</label>-->
                            </div>
                        </div>
                        <div class="row no-border">
                            <input type="text" name="nilai" id = "nilai" onkeyup="uangField(this)" class="a-right">
                        </div>
    <!--                        <div class="row no-border">
                            <label class="radio-btn"><input type="radio" name="rnil"/>Semua Unit</label>
                        </div>-->
                        <input type="hidden" name = "order_type" value="subscription_auto_debet">
                        <div class="row no-border">
                            <div class="col">
                                <label>{{ trans('login.everydate') }}</label>
                                {{-- <input type="text"  onkeypress="return onlyNumber(event)" maxlength="2" id = "tanggal" name="tanggal"> --}}
                                {!! Form::selectRange("tanggal",1,27,['id'=>'tanggal']) !!}
                            </div>
                            <div class="col">
                                <label>{{ trans('login.period') }}</label>
                                <select name = "tahun">
                                    <option value = "1">1 {{ trans('login.year') }}</option>
                                    <option value = "2">2 {{ trans('login.year') }}</option>
                                    <option value = "3">3 {{ trans('login.year') }}</option>
                                </select>
                            </div>
                        </div>
                        <div class="row no-border a-right">
                            <button onclick = "confirms()" type="button" class="btn-oval btn-orange">{{ trans('login.confirm') }}</button>
                        </div>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
        </div>

    </div>


    <div class="wrap-popup product-detail penjualan pop-notif  col-1" id="detail">
        <div class="head">
            <a href="javascript:closePop('detail')" class="close">{{ trans('login.close') }}</a>
        </div>
        <div class="content">
            <div class="myaccount">
                <h3 class="torange">{{ trans('login.orderstatus') }}</h3>
                <input type = "hidden" id = "hidden_ref_no"/>
                <input type = "hidden" id = "hidden_tr_type"/>
                <div id = "table_pop">
                        <table class="fill-notif">
                            <tr>
                                <th>{{ trans('login.intruction') }}</th>
                                <td id = "td_instruksi"></td>
                            </tr>
                            <tr>
                                <th>{{ trans('login.fund') }}</th>
                                <td id = "td_fund">Test filer</td>
                            </tr>
                            <tr>
                                <th>Unit</th>
                                <td id = "td_unit">Lorem 312dasdf</td>
                            </tr>
                            <tr>
                                <th>{{ trans('login.amount') }}</th>
                                <td id = "td_amount">Test filer</td>
                            </tr>
                            <tr>
                                <th>{{ trans('login.date') }}</th>
                                <td id = "td_date">Test filer</td>
                            </tr>
                        </table>

                </div>
            </div>
        </div>
    </div>

@endsection
@include("frontend.users.scripts.debit")
