<?php
$allUnit = $model->all_unit == 1 ? ' (All Unit)' : '';
$unit = $model->unit.$allUnit;
?>
<table class="fill-notif">
    <tr>
        <th>{{ trans('login.reference-no') }}</th>
        <td id = "td_instruksi">{{ $model->ref_no }}</td>
    </tr>
    <tr>
        <th>{{ trans('login.instruction') }}</th>
        <td id = "td_instruksi">{{ $instruksi }}</td>
    </tr>
    <tr>
        <th>{{ trans('login.im-name') }}</th>
        <td id = "td_fund">{{ $model->out_fund->im->im_nm }}</td>
    </tr>
    <tr>
        <th>{{ trans('login.fund') }}</th>
        <td id = "td_fund">{{ $model->out_fund->fund_name }}</td>
    </tr>
    <tr>
        <th>{{ trans('login.total') }}</th>
        <td id = "td_unit">{{ $unit }} Unit</td>
    </tr>
    <tr>
        <th>{{ trans('global.out_nav_date') }}</th>
        <td id = "td_amount">{{ mirae_date($model->out_nav_date) }}</td>
    </tr>
    <tr>
        <th>{{ trans('global.our_order_nav') }}</th>
        <td id = "td_date">{{ $model->proc_cd >= 70 ? $model->out_ord_nav : '-' }}</td>
    </tr>
    <tr>
        <th>{{ trans('login.outorderamount') }}</th>
        <td id = "td_date">{{ formatUang($model->out_ord_amt) }}</td>
    </tr>
    <tr>
        <th>{{ trans('login.out-fee') }}</th>
        <td id = "td_date">{{ $model->out_fee_money }}{{ $model->fee_persent }}</td>
    </tr>
    <tr>
        <th>In {{ trans('login.fund') }}</th>
        <td id = "td_date">{{ $model->in_fund->fund_name }}</td>
    </tr>
    <tr>
        <th>{{ trans('login.im-name') }}</th>
        <td id = "td_date">{{ $model->in_fund->im->im_nm }}</td>
    </tr>
    <tr>
        <th>In {{ trans('login.amount') }}</th>
        <td id = "td_date">{{ formatUang($model->in_ord_amt) }}</td>
    </tr>
    <tr>
        <th>{{ trans('login.in-order-nav') }}</th>
        <td id = "td_date">{{ $model->proc_cd >= 70 ? $model->in_ord_nav : '-' }}</td>
    </tr>
    <tr>
        <th>{{ trans('login.in-order-unit') }}</th>
        <td id = "td_date">{{ $model->in_ord_unit }}</td>
    </tr>
    <tr>
        <th>{{ trans('login.in-fund-fee') }}</th>
        <td id = "td_date">-</td>
    </tr>
    <tr>
        <th>{{ trans('login.proccesstime') }}</th>
        <td id = "td_date">{{ mirae_date($model->tr_date) }} {{ mirae_time($model->proc_time) }} WIB</td>
    </tr>
    <tr>
        <th>{{ trans('login.approvaltime') }}</th>
        <td id = "td_date">{{ mirae_date($model->sa_appr_date) }} {{ mirae_time($model->sa_appr_time) }} WIB</td>
    </tr>
    <tr>
        <th>Status</th>
        <td>{{ $swc->status($model->proc_cd) }}</td>
    </tr>
    @if(!empty($model->reject_msg))
        <tr>
            <th>{{ trans('login.message') }}</th>
            <td>{{ $model->reject_msg }}</td>
        </tr>
    @endif
</table>
