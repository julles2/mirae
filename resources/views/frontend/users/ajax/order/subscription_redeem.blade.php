<?php
$allUnit = $model->all_unit == 1 ? ' (All Unit)' : '';
$unit = $model->unit.$allUnit;

?>
<table class="fill-notif">
    <tr>
        <th>{{ trans('login.reference-no') }}</th>
        <td id = "td_instruksi">{{ $model->ref_no }}</td>
    </tr>
    <tr>
        <th>{{ trans('login.instruction') }}</th>
        <td id = "td_instruksi">{{ $instruksi }}</td>
    </tr>
    <tr>
        <th>{{ trans('login.im-name') }}</th>
        <td id = "td_fund">{{ $model->fund->im->im_nm }}</td>
    </tr>
    <tr>
        <th>{{ trans('login.fund') }}</th>
        <td id = "td_fund">{{ $model->fund->fund_name }}</td>
    </tr>
    {{-- <tr>
        <th>{{ trans('login.total') }}</th>
        <td id = "td_unit">{{ $unit }} Unit</td>
    </tr> --}}
    <tr>
        <th>{{ trans('login.nav-date') }}</th>
        <td id = "td_amount">{{ mirae_date($model->tr_date) }}</td>
    </tr>
    <tr>
        <th>{{ trans('login.order-nav') }}</th>
        <td id = "td_date">{{ $model->proc_cd >= 70 ? $model->ord_nav : '-' }}</td>
    </tr>
    <tr>
        <th>{{ trans('login.fee') }}</th>
        <td id = "td_date">{{ $model->fee_money }}{{ $model->fee_persent }}</td>
    </tr>
    <tr>
        <th>Net</th>
        <td id = "td_date">{{ formatUang($model->total_order) }}</td>
    </tr>
    <tr>
        <th>Unit</th>
        <td id = "td_date">{{ $model->unit }}</td>
    </tr>
    <tr>
        <th>Status</th>
        <td>{{ $model->result_status }}</td>
    </tr>
    @if(!empty($model->reject_msg))
        <tr>
            <th>{{ trans('login.message') }}</th>
            <td>{{ $model->reject_msg }}</td>
        </tr>
    @endif
    <?php
        $type = !empty(get('type')) ? get('type') : $model->tr_type;
    ?>
    @if($model->proc_cd == 0 && $type == 'SA') <!-- jika auto debit -->
        <tr>
            <th>{{ trans('login.reject-message') }}</th>
            <td>
                <textarea maxlength="100" name="" id = "rejt_msg" ></textarea>
            </td>
        </tr>
    @endif

</table>

@if($model->proc_cd == 0 && $type == 'SA') <!-- jika auto debit -->
    <a style="margin-top:30px;" href="javascript::void(0)" onclick = "return cancelDebit('{{ $model->ref_no }}','{{$type}}')" class="btn-std">{{ trans('login.cancel') }}</a>
@endif
