<?php
/**
 * Created by PhpStorm.
 * User: DanielSimangunsong
 * Date: 5/16/2017
 * Time: 12:20 PM
 */ ?>
@extends('layouts.layout')

@section('content')
    <section class="banner">
        <img src="{{ asset('frontend/images/content/banner-produk.jpg') }}" alt="banner" />
        <div class="caption">
            <h6>LOGIN</h6>
            <h1>{!! trans('login.welcome') !!}</h1>
            <p>&nbsp;</p>
        </div>
    </section>
    <section class="std-content">
        <div class="wrapper login-page">
            <h2 class="a-center">Member Login</h2>
            <div class="shadow-spar"></div>
            {!! Form::open(['url' => $url]) !!}
                <div class='box-form login'>
                    <div class="row">
                        <div class="left">
                            <label>User ID</label>
                        </div>
                        <div class="right">
                            <input name="uid" value = "" class="required" type='text' />
                        </div>
                    </div>
                    <div class="row">
                        <div class="left">
                            <label>Password</label>
                        </div>
                        <div class="right">
                            <input name="pwd" value = "" class="required" type='password' />
                        </div>
                    </div>
                    <div class="row btn">
                        <div class="right">
                            <div class="col">
                                <input type="submit" value="{{ trans('login.signin') }}" class="btn-std sea"/>
                            </div>
                            <?php /*
                            <div class="col">
                                <a href="oa-individu-1.php" class="btn-std yellow">{{ trans('login.register') }}</a>
                            </div>
                            */
                           ?>
                        </div>
                    </div>
                </div>
            {!! Form::close() !!}
        </div>
    </section>
@endsection
@push('scripts')
<script type="text/javascript">
    $(document).ready(function(){
        $.ajax({
            url: '{{ urlLang("user/cek-login") }}',
            success: function(response){
                if(response.result == 'true')
                {
                    document.location.href='{{ urlLang("user/portfolio") }}';
                }
            },
        });
    });
</script>
@if(!empty($errorMsg))
    <script type="text/javascript">
        swal_error('{{$errorMsg}}');
    </script>
@endif

@endpush
