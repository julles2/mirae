@extends('frontend.user')

@section('content')
    <div class="content-tab-general">
        <div class="list-tab">
            <div class="title-content after_clear">
                <div class="left">
                    <h3><i class="icwp ic_bag"></i> {{ trans('login.latest_transaction') }}</h3>
                </div>
                <div class="right">
                    <a href="javascript:void(0)" class="btn-std left" id = "filter">Filter</a>
                </div>
            </div>
                <div class="overflow">
                @if(count($transactions) > 0)
                        <table class="table-radius">
                            <thead>
                            <tr>
                                <th style="width: 100px;">{{ strtoupper(trans('login.time')) }}</th>
                                <th>{{ strtoupper(trans('login.mutualfundname')) }}</th>
                                <th>{{ strtoupper(trans('login.intructions')) }}</th>
                                <th>NAV</th>
                                <th>{{ strtoupper(trans('login.totalunit')) }}</th>
                                <th>{{ strtoupper(trans('login.totalvalue')) }}</th>
                                <th>{{ strtoupper(trans('login.certificate')) }}</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($transactions as $trans)
                            <?php
                                $row = (object)$trans;

                                $fundName = $fund($row->fund_cd);
                                $procTime = !empty($row->proc_time) ? substr($row->proc_time, 0, 6) : '000000';
                                $trType = function () use ($row) {
                                    $stats = [
                                        'R'=>'Redemption',
                                        'S'=>'Subscription',
                                        'swc'=>'Switching',
                                    ];

                                    if (array_key_exists($row->tr_type, $stats)) {
                                        $result = $stats[$row->tr_type];
                                        if ($row->tr_type == 'S') {
                                            $result = $row->source == 2 ? 'Auto Debet' : 'Subscription';
                                        }

                                        return $result;
                                    }
                                };
                                $fee = Mirae::resultPercentageOrAmount($row->fee, $row->amount);
                                $totalNilai = $row->amount - $fee;
                            ?>
                            <tr>
                                <td>{{ carbonLocalize(parse($row->tr_date)->format("d-F-Y")) }} {{ carbon()->parse($procTime)->format("H:i:s") }}</td>
                                @if($row->out != '0')
                                    <td>{{ $fund($row->out) }} <b>TO</b> {{ $fund($row->fund_cd) }}</td>
                                @else
                                    <td>{{ $fundName }}</td>
                                @endif
                                <td>{{ $trType() }}</td>
                                <td>{{ $row->ord_nav }}</td>
                                <td>{{ $row->unit }}</td>
                                <td>IDR {{ formatUang($totalNilai) }}</td>
                                <td><a href="javascript::void(0)" onclick = "getDetail('{{ $row->ref_no }}','{{ $row->tr_type }}')">Details</a></td>
                            </tr>
                            @endforeach
                            </tbody>
                        </table>
                @else
                    <h4>{{ trans('login.count_transaction') }}</h4>
                @endif
                </div>
            <!--div class="table-nav">
                <div class="entries left">
                    Show
                    <select>
                        <option>15</option>
                    </select>
                    All entries
                </div>
                <div class="info right">
                    Showing 1 to 15 of 157 entries
                </div>
            </div-->
            <div class="box-list-data disclaimer">
                <div class="list-data">
                    <div class="inner-list-data">
                        <div class="row no-border">
                            <div class="disclaimer-box rounded">
                                <h4>{{ trans('disclaimer.title_transaksi') }}</h4>
                                <p>
                                    {!! trans('disclaimer.transaksi') !!}
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection
@section('content2')
    
    <div class="wrap-popup product-detail pop-notif" id="pop_filter">
        <div class="head">
            <a href="javascript:closePop('pop_filter')" class="close">{{trans('login.cancel')}}</a>
        </div>
        <div class="content">
            <div class="myaccount after_clear">
                <h3 class="torange">Filter</h3>
                <div class="box-form">
                    <div class="row">
                    {{ trans('login.date') }} : <br/>

                        {!! Form::select('date',['' => trans('login.sortbydate') ,'asc'=>trans('login.oldest'),'desc'=>trans('login.newest')] , get('date') , ['id'=>'select_date']) !!}
                        
                        <p>&nbsp;</p>
                        Start Date:
                        {!! Form::text('start_date', get('start_date') , ['id'=>'start_date','class'=>'datepicker','readonly'=>true,'style'=>'background-color:white;']) !!}

                        <p>&nbsp;</p>
                        End Date:
                        {!! Form::text('end_date', get('end_date') , ['id'=>'end_date','class'=>'datepicker','readonly'=>true,'style'=>'background-color:white;']) !!}


                        <p>&nbsp;</p>
                        Instruksi:
                        {!! Form::select('instruksi',[''=>trans('login.choose').''.trans('login.intructions'),'S'=>'Subscription','SWC'=>'Switching','R'=>'Redeem'] , get('instruksi') , ['id'=>'select_instruksi']) !!}

                        <p>&nbsp;</p>
                        Reksa Dana:
                        {!! Form::text('reksa_dana' , get('reksa_dana') , ['id'=>'text_reksa_dana','placeholder'=>'Nama Reksa Dana']) !!}

                        <p>&nbsp;</p>
                        Status:
                        {!! Form::select('status',[''=>trans('login.choose').' '.trans('login.status'),'pending'=>'Pending','reject'=>'Reject','confirm'=>'Confirm'] , get('status') , ['id'=>'select_status']) !!}


                    </div>
                    <div class="row">
                        <a href = "javascript:" onclick = "return filter()" class="btn-std ">Filter</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include("frontend.users.pop.transaction_detail")
@endsection
@include("frontend.users.scripts.transaction")
