@if($cek_user == 'redeem')
<div class="content-tab-general">
    <div class="list-tab">
        <div class="title-content after_clear">
            <h3>
                Mileage
            </h3>	
        </div>
		<div class="disclaimer-box rounded" ">
            <h4 style="font-size: 18px;">Info</h4>
            <p style="font-size: 18px;margin-top:10px;">
            	Thank you, your request has been processed. The redemption process will take up to 7 working days.
            </p>
        </div>

    </div>
</div>
@else
<div class="content-tab-general">
    <div class="list-tab">
        <div class="title-content after_clear">
            <h3>
                Mileage
            </h3>
        </div>
        <hr class="line-blue">
            <div class="box-data-diri">
                <div class="row-data-diri">
                    <div class="col col_33">
                        <label>
                            Name
                        </label>
                        <strong>
                            {{ $brokerName }}
                        </strong>
                    </div>
                    <div class="col col_33">
                        <label>
                            Customer ID
                        </label>
                        <strong>
                            {{ $brokerId }}
                        </strong>
                    </div>
                    <div class="col col_33">
                        <label>
                            Total Mileage Point
                        </label>
                        <strong>
                            {{ @$mileage->mileage > 0 ? $mileage->mileage : 0  }}
                        </strong>
                    </div>
                </div>
                <div class="row-data-diri">
                    <div class="col col_33">
                        <label>
                            Total Reward
                        </label>
                        <strong>
                            Rp {{ @$mileage->mileage > 0 ? formatUangNoComa($mileage->mileage) : 0 }}
                        </strong>
                    </div>
                    <div class="col col_33">
                        <label>
                            Customer Risk Profile
                        </label>
                        <strong>
                            {{ $labelRiskOnly }}
                        </strong>
                    </div>
                </div>
            </div>
            <div class="box-list-data disclaimer">
                <div class="list-data">
                    <div class="inner-list-data">
                        <div class="disclaimer-box rounded">
                            <h4>
                                Info
                            </h4>
                            Reward Option
                            <br>
                                <br>
                                    <ul>
                                        <li>
                                            - Cashback
                                        </li>
                                        <li>
                                            - Voucher Investment
                                        </li>
                                    </ul>
                                    <br>
                                       {{--  <div style="font-size:12px;">
                                            Dengan ini saya menyatakan bahwa saya telah setuju dengan syarat
                                            <br>
                                                dan ketentuan yang berlaku terkait dengan penukaran mileage point milik saya.
                                            </br>
                                        </div> --}}
                                        {{--
                                        <select id="" name="" style="width:100px;font-size: 12px;margin-top: 10px;">
                                            <option value="">
                                                Pilih
                                            </option>
                                            <option value="yes">
                                                Yes
                                            </option>
                                            <option value="no">
                                                No
                                            </option>
                                        </select>
                                        --}}
                                        <div style="font-size:9px;">
                                            *  The redemption proccess will take up to 7 working days
                                        </div>
                                    </br>
                                </br>
                            </br>
                        </div>
                    </div>
                </div>
            </div>
        </hr>
    </div>
</div>
<div>
	{!! Form::open(['method'=>'post','id'=>'formna']) !!}
    <table class="product product-list-table">
        <thead>
            <tr>
                <th>Voucher Type</th>
                <th>Fund Name</th>
                <th>Risk Profile</th>
                <th>Prospektus</th>
                <th>FFS</th>
                <th>#</th>
            </tr>
        </thead>
        <tbody>
        	<?php
        	$model = new \App\Models\Oracle\FundInfo();
        	?>
        	@foreach($products as $type => $codes)
        		@foreach($codes as $code)
        		<?php
        			$fund = $model->where('fund_cd',$code)->first();
        		?>

	        		@if(!empty($fund->fund_cd))
			            <tr>
			            	<td>
			            		@if($type == 'pemula')
			            		Voucher Investasi Pemula (Minimum Rp 100,000)
								@elseif($type == 'capital')
			            		Voucher Investasi Pemula (Minimum Rp 500,000)
								@else
								Voucher Investasi Ekonomis (Tidak Ada minimum)
			            		@endif
			            	</td>
			            	<td>{{ $fund->fund_name }}</td>
			            	<td>{{ $fund->result_fund_risk }}</td>
			            	<td>
			            		@if(!empty($fund->mysql_fund->id))
								<a href="{{ asset('contents/'.$fund->mysql_fund->prospectus) }}">Prospektus</a>
								@else
								-
			            		@endif
			            	</td>
			            	<td>ffs</td>
			            	<td>
			            		<label class="radio-btn">
                                    {{-- <inpyt type = "hidden" value = '{{ $type }}' id ="tipe{{ $code }}" /> --}}
                                    <input class="fund" type="radio" name = "fund" value = "{{ $code }}">
                                </label>
			            	</td>
			            </tr>
			        @endif
		        @endforeach
	        @endforeach
        </tbody>
    </table>
	<br><br>
    <div style="font-size:12px;margin-top: 20px;">
        <label class="radio-btn">
            <input class="aggree" type="checkbox" name="aggree" id = "cek_agree">
        </label>
        Once clicked, there`ll be a pop up regarding details of T & C
    </div>
    <br><br>
	<a href="javascript::void(0)" class="btn-std yellow" id = "redeem">Redeem</a>
    {!! Form::close() !!}
</div>
@endif
@endsection