<?php
/**
 * Created by PhpStorm.
 * User: DanielSimangunsong
 * Date: 5/17/2017
 * Time: 3:24 PM
 */ ?>
@extends('frontend.user')

@section('content')
    <div class="content-tab-general">
        <div class="list-tab">
            <div class="title-bg bg-darkblue">
                <div class="left">
                    <h3><i class="icwp ic_money_white"></i>{{ trans('login.mycash') }}</h3>
                </div>
                <div class="right">
                    <h3>IDR {{ formatUang(injectOracle('VCashBal')->balance()) }}</h3>
                </div>
            </div>

            <div class="title-content after_clear">
                <div class="left">
                    <h3><i class="icwp ic_folder"></i>{{ trans('login.myportfolio') }}</h3>
                </div>
                <div class="right">
                    <a href="{{ url(App::getLocale() . '/user/portfolio') }}" class="btn-oval btn-trans-orange">{{ trans('login.viewallportfolio') }}</a>
                </div>
            </div>

            <div class="box-list-data after_clear">
                @include('frontend.users.includes.portofolio')
            </div>

            <div class="title-content after_clear">
                <div class="left">
                    <h3><i class="icwp ic_chart_up"></i> {{ trans('login.stockportfolio') }}</h3>
                </div>
                <div class="right">
                    <a href="{{ urlLang('user/portfolio') }}" class="btn-oval btn-trans-orange">{{ trans('login.viewallportfolio') }}</a>
                </div>
            </div>

            @include('frontend.users.includes.stock_portofolio')

            <div class="title-content after_clear">
                <div class="left">
                    <h3><i class="icwp ic_bag"></i> {{ trans('login.latesttransaction') }}</h3>
                </div>
                <div class="right">
                    <a href="{{ url(App::getLocale() . '/user/transaction') }}" class="btn-oval btn-trans-orange">{{ trans('login.latesttransaction') }}</a>
                </div>
            </div>
            <table class="table-radius">
                <thead>
                <tr>
                    <th style="width: 100px;">{{ strtoupper(trans('login.time')) }}</th>
                    <th>{{ strtoupper(trans('login.mutualfundname')) }}</th>
                    <th>{{ strtoupper(trans('login.intructions')) }}</th>
                    <th>NAV</th>
                    <th>{{ strtoupper(trans('login.totalunit')) }}</th>
                    <th>{{ strtoupper(trans('login.totalvalue')) }}</th>
                    <th>{{ strtoupper(trans('login.certificate')) }}</th>
                </tr>
                </thead>
                <tbody>
                @foreach($transactions as $trans)
                <?php
                    $row = (object)$trans;

                    $fundName = $fundMySql($row->fund_cd)->fund_name;
                    $procTime = !empty($row->proc_time) ? substr($row->proc_time,0,6) : '000000';
                    $trType = function()use($row){
                        $stats = [
                            'R'=>'Redeem',
                            'S'=>'Subscription',
                            'swc'=>'Switching',
                        ];

                        if(array_key_exists($row->tr_type, $stats))
                        {
                            return $stats[$row->tr_type];
                        }
                    };
                    $fee = Mirae::resultPercentageOrAmount($row->fee,$row->amount);
                    $totalNilai = $row->amount - $fee;
                ?>
                <tr>
                    <td>{{ carbon()->parse($row->tr_date)->format("d-F-Y") }} {{ carbon()->parse($procTime)->format("H:i:s") }}</td>
                    @if($row->out != '0')
                        <td>{{ $fundMySql($row->out)->fund_name }} TO {{ $fundMySql($row->fund_cd)->fund_name }}</td>
                    @else
                        <td>{{ $fundName }}</td>
                    @endif
                    <td>{{ $trType() }}</td>
                    <td>{{ $row->ord_nav }}</td>
                    <td>{{ $row->unit }}</td>
                    <td>IDR {{ formatUang($totalNilai) }}</td>
                    <td><a href="javascript::void(0)" onclick = "getDetail('{{ $row->ref_no }}','{{ $row->tr_type }}')">Detail</a></td>
                </tr>
                @endforeach
                </tbody>
            </table>
            <div class="box-list-data">
                <div class="list-data list_3 type-2">
                    <div class="title-content after_clear type-2">
                        <h3><i class="icwp ic_money"></i> {{ trans('login.latestorderstatus') }}</h3>
                    </div>
                    <div class="inner">
                        <div class="box-head-list after_clear">
                            <div class="left"><strong>NO. REF</strong></div>
                            <div class="right">{{ @$latestTransaction->ref_no }}</div>
                        </div>
                        <div class="box-title-list">
                            <div class="left">
                                <figure><img src="{{ @$latestTransaction->sqlFund->result_image }}"></figure>
                            </div>
                            <div class="right">
                                <h5>{{ @$latestTransaction->fund->fund_name }}</h5>

                                <p>{{ @$latestTransaction->fund->im->im_nm }}</p>
                            </div>
                        </div>
                        <div class="desc-list">
                            <div class="row">
                                <div class="list">
                                    <div class="row_inner">
                                        <div class="left">
                                            <label>Instruksi</label>
                                        </div>
                                        <div class="right">
                                            <b>
                                                @if(@$latestTransaction->tr_type == 'R')
                                                    Redeem
                                                @elseif(@$latestTransaction->tr_type == 'S')
                                                    Subscription ({{ $modelTrans->source(@$latestTransaction->source) }})
                                                @else
                                                    Switching
                                                @endif
                                            </b>
                                        </div>
                                    </div>
                                    <div class="row_inner">
                                        <div class="left">
                                            <label>Jumlah Unit</label>
                                        </div>
                                        <div class="right">
                                            <b>{{ @$latestTransaction->unit }}</b>
                                        </div>
                                    </div>
                                    <div class="row_inner">
                                        <div class="left">
                                            <label>{{ trans('login.netordervalue') }}</label>
                                        </div>
                                        <div class="right">
                                            <b>IDR {{ formatUang(@$latestTransaction->amount) }}</b>
                                        </div>
                                    </div>
                                    <div class="row_inner">
                                        <div class="left">
                                            <label>{{ trans('login.transactionfee') }}</label>
                                        </div>
                                        <div class="right">
                                            <b>IDR {{ formatUang(@$latestTransaction->fee_money) }}</b>
                                        </div>
                                    </div>
                                    <div class="row_inner">
                                        <div class="left">
                                            <label>{{ trans('login.totalorder') }}</label>
                                        </div>
                                        <div class="right">
                                            <b>IDR {{ formatUang(@$latestTransaction->total_order) }}</b>
                                        </div>
                                    </div>
                                    <div class="row_inner">
                                        <div class="left">
                                            <label>{{ trans('login.status') }}</label>
                                        </div>
                                        <div class="right">
                                            <b class="b-completed">{{ strtoupper(@$latestTransaction->result_status) }}</b>
                                        </div>
                                    </div>
                                    <div class="row_inner row_btn_inner">
                                        <a href="{{ url(App::getLocale() . '/user/order') }}"
                                           class="btn-oval btn-trans-orange">{{ trans('login.viewallorder') }}</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="list-data list_6">
                    <div class="title-content after_clear">
                        <div class="left">
                            <h3><i class="icwp ic_eye"></i>{{ trans('login.mywatchlist') }}</h3>
                        </div>
                        <!--div class="right">
                            <a href="" class="btn-oval btn-trans-orange">VIEW ALL</a>
                        </div-->
                    </div>
                    
                    <table class="table-radius acc-tbl-rd">
                        <thead>
                        <tr>
                            <th>&nbsp;</th>
                            <th>
                                <label class="opt"><input id = "check_all_watch" type="checkbox"></label>
                            </th>
                            <th>{{ strtoupper(trans('login.mutualfundname')) }}</th>
                            <th>{{ strtoupper(trans('login.type')) }}</th>
                            <th><a>NAB <i class="icwp ic_arr_topbtm"></i></a></th>
                            <th>1{{ strtoupper(trans('global.dy')) }}</th>
                            <th>1{{ strtoupper(trans('global.mt')) }}</th>
                            <th>AUM</th>
                            <th>&nbsp;-</th>
                        </tr>
                        </thead>
                        <tbody>
                        {!! Form::open(['id'=>'form_watch','method'=>'post','url'=>urlLang('user/dashboard/delete-check-watch')]) !!}
                        @php($no=0)
                        @foreach($watchlists as $row)
                        @php($no++)
                            <tr>
                                <td>{{ $no }}</td>
                                <td>
                                    <label class="opt"><input type="checkbox" name = "watch_id[]" value="{{ $row->id }}"></label>
                                </td>
                                <td>{{ $row->fund->fund_name }}</td>
                                <td>{{ $row->fund->result_type }}</td>
                                <td>{{ $row->fund->last_nab }}</td>
                                <td>{{ @valueDecimal($row->fund->daily_nav->one_day) }}</td>
                                <td>{{ @valueDecimal($row->fund->daily_nav->one_month) }}</td>
                                <td>{{ formatUang(@valueDecimal(@$row->fund->daily_nav->aum + 0)) }}</td>
                                <td style="">
                                        <a href="{{ urlLang('product?pop='.$row->fund_cd) }}">{{ trans('login.buy') }}</a>
                                        |
                                        <a onclick = "return confirm('{{ trans('global.ask_delete') }}')" href="{{ urlLang('user/dashboard/delete-watch/'.$row->id) }}">{{ strtoupper(trans('global.delete')) }}</a>
                                </td>
                            </tr>
                        @endforeach
                        {!! Form::close() !!}
                        </tbody>
                    </table>
                    <div class="right" >
                        <a id = "delete-check-whatch" style="margin-top:20px;" href="javascript::void(0);" class="btn-oval btn-trans-orange">{{ trans('global.delete') }}</a>                    
                    </div>
                </div>
            </div>
            <div class="box-list-data disclaimer">
                <div class="list-data">
                    <div class="inner-list-data">
                        <div class="disclaimer-box rounded">
                            <h4>{{ trans('disclaimer.title_portfolio') }}</h4>
                            <p>
                                {{ trans('disclaimer.portfolio') }}
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @if(session()->has('success'))

    @endif
@endsection
@push('content2')

@include('frontend.users.pop.penjualan')
@include('frontend.users.pop.pengalihan')
@include("frontend.users.pop.transaction_detail")
@include("frontend.users.scripts.transaction")
@include("frontend.users.scripts.dashboard")
@endpush
