@extends('backend.layouts.layout')
@section('content')

<div id="app_header_shadowing"></div>
<div id="app_content">
    <div id="content_header">
        <h3 class="user">About Us</h3>
    </div>
        <div id="content_body">
            
            <div class = 'row'>

                <div class = 'col-md-6'>

                    @include('backend.common.errors')

                     {!! Form::model($model,['files'=>true]) !!} 
                     
                        <!-- NEW CATEGORIES -->
                        <div id="MainAboutUs">
                      
                          <div class="form-group">
                            <label>Status</label> :
                            {!! Form::select('status' , ['' => '-- Select Status --', 'y' => 'Publish' , 'n' => 'Un Publish'] , @$model->status ,['class' => 'form-control']) !!}
                          </div>

                          <div class="form-group">
                            <label>Content</label>
                              <div id="tabs">
                                  <ul>
                                  @foreach(langs() as $lang => $val)
                                    <li><a href="#tabs-{{$lang}}">{{$val}}</a></li>
                                  @endforeach
                                  </ul>

                                  <?php $no = 0; ?>
                                  @if(!empty($companyTranslations))

                                      @foreach(langs() as $lang => $val)
                                            <?php
                                              $details = $detail($lang);
                                            ?>

                                             <div id="tabs-{{ $lang }}">
                                                  <div class="form-group">
                                                    <label>Title</label> :
                                                    {!! Form::text('title['.$lang.']' , @$details->title ,['class' => 'form-control']) !!}
                                                  </div>

                                                  <div class="form-group">
                                                    <label>Description</label> :
                                                    {!! Form::textarea('description['.$lang.']' , @$details->description ,['class' => 'form-control ckeditor']) !!}
                                                    {!! Form::hidden('id['.$lang.']' , @$details->id ,['class' => 'form-control']) !!}
                                                    {!! Form::hidden('lang['.$lang.']' , $lang ,['class' => 'form-control']) !!}
                                                  </div>

                                              </div>
                                        <?php $no++;?>
                                        @endforeach

                                  @else

                                      @foreach(langs() as $lang => $val)
                                          <div id="tabs-{{ $lang }}">
                                                <div class="form-group">
                                                  <label>Title</label> :
                                                  {!! Form::text('title['.$lang.']' , @$details->title ,['class' => 'form-control']) !!}
                                                </div>

                                                <div class="form-group">
                                                  <label>Description</label> :
                                                  {!! Form::textarea('description['.$lang.']' , null ,['class' => 'form-control ckeditor']) !!}
                                                  {!! Form::hidden('id['.$lang.']' , null ,['class' => 'form-control']) !!}
                                                  {!! Form::hidden('lang['.$lang.']' , $lang ,['class' => 'form-control']) !!}
                                                </div>
                                          </div>
                                        <?php $no++;?>
                                        @endforeach

                                  @endif

                                </div>
                          </div>

                        </div>

                      <br><br>


                      <button type="submit" class="btn btn-primary">{{ !empty($model->id) ? 'Update' : 'Save' }}</button>
                    
                    {!! Form::close() !!}

                </div>

            </div>

        </div>
    </div>
@endsection

@section('script')
{!! JsValidator::formRequest('App\Http\Requests\Backend\CompanyRequest') !!}
<script type="text/javascript">
        
        $(document).ready(function(){
            $( "#tabs" ).tabs();
        });


    </script>

@endsection