@extends('backend.layouts.layout')
@section('content')

<div id="app_header_shadowing"></div>
<div id="app_content">
    <div id="content_header">
        <h3 class="user"> News</h3>
    </div>
        <div id="content_body">
            
            <div class = 'row'>

                <div class = 'col-md-6'>

                    @include('backend.common.errors')

                     {!! Form::model($model,['files'=>true]) !!} 
                     
                        <!-- NEW CATEGORIES -->
                        <div id="MainNewsCategories">
                        
                          <div class="form-group">
                            <label>Category</label> :
                            {!! Form::select('category_id' , $categories_dropdown , @$model->category_id ,['class' => 'form-control']) !!}
                          </div>

                          <div class="form-group">
                            <label>Status</label> :
                            {!! Form::select('status' , ['' => '-- Select Status --', 'y' => 'Publish' , 'n' => 'Un Publish'] , @$model->status ,['class' => 'form-control']) !!}
                          </div>

                          <div class="form-group">
                            <label>Date</label> :
                            {!! Form::text('date' , @$model->date ,['class' => 'form-control','id' => 'date']) !!}
                          </div>

                          <div class="form-group">
                            <label>Image Intro (370 x 210)</label>
                            {!! Webarq::htmlImage('image_intro', @$model->image_intro, $model) !!}
                          </div>

                          <div class="form-group">
                            <label>Image Detail (1920 x 550)</label>
                            {!! Webarq::htmlImage('image_detail', @$model->image_detail, $model) !!}
                          </div>
                          <div class="form-group">
                            <label>Pinned News</label> :
                            {!! Form::select('pinned' , ['n'=>'No','y'=>'Yes'] , null ,['class' => 'form-control']) !!}
                          </div>

                          <div class="form-group">
                            <label>Content</label>
                              <div id="tabs">
                                  <ul>
                                  @foreach(langs() as $lang => $val)
                                    <li><a href="#tabs-{{$lang}}">{{$val}}</a></li>
                                  @endforeach
                                  </ul>
                                  <?php $i=0;?>
                                  @if(!empty($newsTranslations))

                                      @foreach(langs() as $lang => $val)
                                            <?php
                                              $details = $detail($lang);
                                            ?>

                                             <div id="tabs-{{ $lang }}">

                                                  <div class="form-group">
                                                    <label>Title</label> :
                                                    {!! Form::text('title['.$lang.']' , @$details->title ,['class' => 'form-control']) !!}
                                                    {!! Form::hidden('id['.$lang.']' , @$details->id ,['class' => 'form-control']) !!}
                                                    {!! Form::hidden('lang['.$lang.']' , $lang ,['class' => 'form-control']) !!}
                                                  </div>

                                                  <div class="form-group">
                                                    <label>Intro</label> :
                                                    {!! Form::text('intro['.$lang.']' , @$details->intro ,['class' => 'form-control']) !!}
                                                  </div>

                                                  <div class="form-group">
                                                    <label>Description</label> :
                                                    {!! Form::textarea('description['.$lang.']' , @$details->description ,['class' => 'form-control ckeditor']) !!}
                                                  </div>
                                              </div>
                                              <?php $i++; ?>
                                        @endforeach

                                  @else

                                      @foreach(langs() as $lang => $val)
                                          <div id="tabs-{{ $lang }}">
                                              <div class="form-group">
                                                  <label>Title</label> :
                                                  {!! Form::text('title['.$lang.']' , null ,['class' => 'form-control']) !!}
                                                  {!! Form::hidden('id['.$lang.']' , null ,['class' => 'form-control']) !!}
                                                  {!! Form::hidden('lang['.$lang.']' , $lang ,['class' => 'form-control']) !!}
                                                </div>

                                                <div class="form-group">
                                                  <label>Intro</label> :
                                                  {!! Form::text('intro['.$lang.']' , null ,['class' => 'form-control']) !!}
                                                </div>

                                                <div class="form-group">
                                                  <label>Description</label> :
                                                  {!! Form::textarea('description['.$lang.']' , null ,['class' => 'form-control ckeditor']) !!}
                                                </div>
                                            </div>
                                            <?php $i++; ?>
                                        @endforeach

                                  @endif

                                </div>
                          </div>

                        </div>

                      <br><br>


                      <button type="submit" class="btn btn-primary">{{ !empty($model->id) ? 'Update' : 'Save' }}</button>
                    
                    {!! Form::close() !!}

                </div>

            </div>

        </div>
    </div>
@endsection

@section('script')
{!! JsValidator::formRequest('App\Http\Requests\Backend\NewsRequest') !!}
<script type="text/javascript">
        
        $(document).ready(function(){
            $( "#tabs" ).tabs();

            $( "#date" ).datepicker({dateFormat: "yy-mm-dd"});
        });
</script>

@endsection