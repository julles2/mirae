@extends('backend.layouts.layout')
@section('content')

<div id="app_header_shadowing"></div>
<div id="app_content">
    <div id="content_header">
        <h3 class="user">{{ webarq::titleActionForm() }}</h3>
    </div>
    <div id="content_body">

        @include('backend.common.flashes')

        <div class = 'row'>
           <div class = 'col-md-12'>
                <div id="tabs">
                    <ul>
                        <li><a href="#tabs-listing">Listing</a></li>
                        <li><a href="#tabs-setting">Setting List</a></li>
                        <li><a href="#tabs-compare">Setting Compare</a></li>
                    </ul>

                    <!-- LISTING -->
                    <div id="tabs-listing">
                            <table class = 'table' id = 'table'>
                                <thead>
                                    <tr>
                                        <th>Code</th>
                                        <th>Fund Name</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                            </table>
                    </div>

                    <!-- SETTING -->
                    <div id="tabs-setting">
                    
                        {!! Form::model($setting,['files'=>true]) !!} 
                     
                        <div id="setting">
                      
                          <div class="form-group">
                            <label>Content</label>
                              <div id="tabs2">
                                  <ul>
                                  @foreach(langs() as $lang => $val)
                                    <li><a href="#tabs-{{$lang}}">{{$val}}</a></li>
                                  @endforeach
                                  </ul>
                                  {!! Form::hidden('jenis' , 'list' ,['class' => 'form-control']) !!}
                                  <?php $no = 0; ?>
                                      @foreach(langs() as $lang => $val)
                                        <?php
                                          $settings = $detail($lang);
                                        ?>
 
                                           <div id="tabs-{{ $lang }}">

                                                <div class="form-group">
                                                  <label>Title Caption</label> :
                                                  {!! Form::text('title_caption['.$lang.']' , @$settings->title_caption ,['class' => 'form-control','id' => 'date']) !!}
                                                  {!! Form::hidden('lang['.$lang.']' , $lang ,['class' => 'form-control']) !!}
                                                </div>

                                                <div class="form-group">
                                                  <label>Title Main</label> :
                                                  {!! Form::text('title_main['.$lang.']' , @$settings->title_main ,['class' => 'form-control','id' => 'date']) !!}
                                                </div>
 
                                                <div class="form-group">
                                                  <label>Sub Title</label> :
                                                  {!! Form::text('sub_title['.$lang.']' , @$settings->sub_title ,['class' => 'form-control','id' => 'date']) !!}
                                                </div>

                                                <div class="form-group">
                                                  <label>Banner Image (1920 x 550)</label>
                                                  <!-- {!! Webarq::htmlImageFree('banner_image['.$lang.']', @$settings->banner_image, @$details,'banner_image') !!} -->
                                                  {!! Webarq::htmlImageBanner('banner_image['.$lang.']' , 'contents/'.@$settings->banner_image, @$details,'value','manajer-investasi','banner_image',$lang) !!}
                                                </div>

                                                <div class="form-group" style="display: none;">
                                                  <label>Title Content</label> :
                                                  {!! Form::text('title_content['.$lang.']' , @$settings->title_content ,['class' => 'form-control']) !!}
                                                </div>

                                            </div>
                                      <?php $no++;?>
                                      @endforeach

                                </div>
                          </div>

                        </div>
                        <br><br>
                        <button type="submit" class="btn btn-primary">{{ !empty($model->id) ? 'Update' : 'Save' }}</button>
                    
                        {!! Form::close() !!}

                    </div>

                    <!-- SETTING -->
                    <div id="tabs-compare">
                    
                        {!! Form::model($settingCompare,['files'=>true]) !!} 
                     
                        <div id="compare">
                      
                          <div class="form-group">
                            <label>Content</label>
                              <div id="tabs3">
                                  <ul>
                                  @foreach(langs() as $lang => $val)
                                    <li><a href="#tabs-{{$lang}}">{{$val}}</a></li>
                                  @endforeach
                                  </ul>
                                  {!! Form::hidden('jenis' , 'compare' ,['class' => 'form-control']) !!}
                                  <?php $no = 0; ?>
                                      @foreach(langs() as $lang => $val)
                                        <?php
                                          $settingCompares = $detailCompare($lang);
                                        ?>
 
                                           <div id="tabs-{{ $lang }}">

                                                <div class="form-group">
                                                  <label>Title Caption</label> :
                                                  {!! Form::text('title_caption['.$lang.']' , @$settingCompares->title_caption ,['class' => 'form-control','id' => 'date']) !!}
                                                  {!! Form::hidden('lang['.$lang.']' , $lang ,['class' => 'form-control']) !!}
                                                </div>

                                                <div class="form-group">
                                                  <label>Title Main</label> :
                                                  {!! Form::text('title_main['.$lang.']' , @$settingCompares->title_main ,['class' => 'form-control','id' => 'date']) !!}
                                                </div>
 
                                                <div class="form-group">
                                                  <label>Sub Title</label> :
                                                  {!! Form::text('sub_title['.$lang.']' , @$settingCompares->sub_title ,['class' => 'form-control','id' => 'date']) !!}
                                                </div>

                                                <div class="form-group">
                                                  <label>Banner Image (1920 x 550)</label>
                                                  <!-- {!! Webarq::htmlImageFree('banner_image['.$lang.']', @$settings->banner_image, @$details,'banner_image') !!} -->
                                                  {!! Webarq::htmlImageBanner('banner_image['.$lang.']' , 'contents/'.@$settingCompares->banner_image, @$details,'value','manajer-investasi','banner_image',$lang) !!}
                                                </div>

                                                <div class="form-group" style="display: none;">
                                                  <label>Title Content</label> :
                                                  {!! Form::text('title_content['.$lang.']' , @$settingCompares->title_content ,['class' => 'form-control']) !!}
                                                </div>

                                            </div>
                                      <?php $no++;?>
                                      @endforeach

                                </div>
                          </div>

                        </div>
                        <br><br>
                        <button type="submit" class="btn btn-primary">{{ !empty($model->id) ? 'Update' : 'Save' }}</button>
                    
                        {!! Form::close() !!}

                    </div>
            </div>              

            </div>

        </div>

        


    </div>
</div>
@endsection



@section('script')
    
    <script type="text/javascript">
        
        $(document).ready(function(){
            $('#table').DataTable({
                processing: true,
                serverSide: true,
                ajax: '{{ urlBackendAction("data") }}',
                "iDisplayLength": 50,
                columns: [
                    { data: 'fund_cd', name: 'fund_cd' },
                    { data: 'fund_name', name: 'fund_name'},
                    { data: 'action', name: 'action' , searchable: false},
                ]
            })
            .on( 'error.dt', function ( e, settings, techNote, message ) {
                alert("Your Login is Expired");
                document.location.href='{{ url('login/logout') }}';
            });

            $('#tabs').tabs();
            $('#tabs2').tabs();
            $('#tabs3').tabs();
        });

    </script>

@endsection
