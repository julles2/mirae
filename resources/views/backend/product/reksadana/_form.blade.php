@extends('backend.layouts.layout')
@section('content')

<div id="app_header_shadowing"></div>
<div id="app_content">
    <div id="content_header">
        <h3 class="user"> Reksa Dana</h3>
    </div>
        <div id="content_body">

            <div class = 'row'>

                <div class = 'col-md-12'>

                    @include('backend.common.errors')

                     {!! Form::model($model,['files'=>true]) !!}

                     <div class="form-group">
                      <label>Code</label> :
                      {{ $model->fund_cd }}
                    </div>

                    <div class="form-group">
                      <label>Name</label> :
                      {{ $model->fund_name }}
                    </div>

                     <div id="tabsMain">
                         <ul>
                            <li><a href="#MainInfoProduk">Info Produk</a></li>
                            <li><a href="#MainInfoPerusahaan">Info Perusahaan</a></li>
                         </ul>
                        <div class="row" id="MainInfoProduk">
                            <div class="col-md-12">

                                <div class="form-group">
                                  <label>Image Detail (251 x 85)</label>
                                  {!! Webarq::htmlImage('image',@$cekImage->image,!empty($cekImage->id) ? $cekImage : $model) !!}
                                </div>

                                <div class="form-group">
                                  <label>File Fund Fact Sheet</label>
                                  {!! Webarq::htmlFile('fund_fact_sheet',asset(null).'contents',$model,'fund_fact_sheet',$model->id) !!}
                                  <span style="color: red;">File Type: PDF</span><br>
                                  <span style="color: red;">Max File Size : 5 Mb</span>
                                </div>

                                <div class="form-group">
                                  <label>File Prospectus</label>
                                  {!! Webarq::htmlFile('prospectus',asset(null).'contents',$model,'prospectus',$model->id) !!}
                                  <span style="color: red;">File Type: PDF</span><br>
                                  <span style="color: red;">Max File Size : 5 Mb</span>
                                </div>
                            </div>
                            <p>&nbsp;</p>
                            <div class="col-md-12">
                                <a href="javascript::void(0)" style="color:white;" onclick = "showRow()" class="btn btn-success btn-sm">Add Row</a>
                                <table class="table" id = "table_row">
                                    <thead>
                                        <tr>
                                            <th width="80%">Title</th>
                                            <th width="20%">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($model->info_details()->whereLang('id')->get() as $row)
                                            <tr>
                                                <td>{{ $row->title1 }}</td>
                                                <td>
                                                    <a style="color:white;" onclick = "showUpdateRow('{{ $row->header_id }}')" href="javascript::void(0)" class="btn btn-info btn-sm">Edit</a>
                                                    <a style="color:white;" href="{{ urlBackendAction('delete-row/'.$row->header_id) }}" onclick="return confirm('are you sure want to delete?')" class="btn btn-danger btn-sm">Delete</a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>

                        <!-- INFO PERUSAHAAN -->
                         <div id="MainInfoPerusahaan">

                            <div id="tabsCompany">
                              <ul>
                              @foreach(langs() as $lang => $val)
                                <li><a href="#tabsCompany-{{$lang}}">{{$val}}</a></li>
                              @endforeach
                              </ul>
                                <?php $no = 0; ?>
                                @foreach(langs() as $lang => $val)
                                  <?php
                                    $detailsCompany = $model->company_details()->whereLang($lang)->first();
                                  ?>

                                   <div id="tabsCompany-{{ $lang }}">
                                        <div class="form-group">
                                          <label>Image Detail (251 x 85)</label>
                                          {!! Webarq::htmlImageFree('imageCompany'.$no,'contents/'.@$detailsCompany->image, @$detailsCompany,'image') !!}
                                          {!! Form::hidden('idCompany[]' , @$detailsCompany->id ,['class' => 'form-control']) !!}
                                          {!! Form::hidden('langCompany[]' , $lang ,['class' => 'form-control']) !!}
                                        </div>

                                        <div class="form-group">
                                          <label>Description</label> :
                                          {!! Form::textarea('descriptionCompany[]' , @$detailsCompany->description ,['class' => 'form-control ckeditor','id'=>'descriptionCompany'.$lang]) !!}
                                        </div>

                                    </div>
                                    <?php $no++;?>
                              @endforeach

                            </div>
                        </div>
                    </div>

                    <p>&nbsp;</p>
                    <button type="submit" class="btn btn-primary">{{ !empty($model->id) ? 'Update' : 'Save' }}</button>

                    {!! Form::close() !!}

                </div>

            </div>

        </div>
    </div>

    <!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Row</h4>
      </div>
      <div class="modal-body">
            <div class="row">
                <div class="col-md-12">
                    {!! Form::open(['id'=>'form_pop','url'=>urlBackendAction("save-row/".$model->id)]) !!}

                    <div id="tabs_row">
                        <ul>
                            @foreach(langs() as $key=>$val)
                                <li><a href="#tabs-{{ $key }}">{{ $val }}</a></li>
                            @endforeach
                        </ul>
                        @foreach(langs() as $key=>$val)
                            <div id="tabs-{{ $key }}">
                                <div class="form-group">
                                    {!! Form::label('title','Title') !!}
                                    {!! Form::text('title1['.$key.']',null,['class'=>'form-control','id'=>'title1_'.$key]) !!}
                                </div>
                                <div class="form-group">
                                    {!! Form::label('title2','Sub Title') !!}
                                    {!! Form::text('title2['.$key.']',null,['class'=>'form-control','id'=>'title2_'.$key]) !!}
                                </div>
                                <div class="form-group">
                                    {!! Form::label('description','Description') !!}
                                    {!! Form::textarea('description['.$key.']',null,['class'=>'ckeditor','id'=>'description_'.$key]) !!}
                                </div>
                            </div>
                        @endforeach

                        <div class="well-lg">
                            <button id = "cmd" type="submit" class="btn btn-primary"></button>
                        </div>

                        {!! Form::close() !!}
                </div>
            </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

@endsection

@section('script')
{!! JsValidator::formRequest('App\Http\Requests\Backend\ReksaDanaRequest') !!}
<script type="text/javascript">

        $(document).ready(function(){
            $( "#tabs" ).tabs();
            $( "#tabs_row" ).tabs();

            $( "#tabsCompany" ).tabs();

            $( "#tabsMain" ).tabs();

            $("#table_row").DataTable({
                 "bLengthChange": false,
                 "bInfo" : false,
                 ordering:false,
            });

        });

        function showRow()
        {
            $("#cmd").html("Save");
            $('#form_pop').trigger("reset");
            $("#form_pop").attr("action","{{urlBackendAction('save-row/'.$model->id)}}");
            $("#myModal").modal();
        }

        function showUpdateRow(header_id)
        {
            $.ajax({
                url: '{{ urlBackendAction("show-update-row") }}/' + header_id,
                type: 'get',
                success: function(respon){
                    $("#cmd").html("Edit");
                    $("#form_pop").attr("action","{{urlBackendAction('save-row/'.$model->id)}}?edit="+header_id);
                    $.each(respon.result,function(key,val){
                        $.each(val,function(a,b){
                            id = "#" + a + "_" + key;
                            if(a != 'description')
                            {
                                $(id).val(b);
                            }

                        });
                    });
                    CKEDITOR.instances.description_id.setData( respon.result.id.description );
                    CKEDITOR.instances.description_en.setData( respon.result.en.description );
                    $("#myModal").modal();
                },
            });
        }

        // DELETE ACCORDION NYA
        function delPDF(id,jenis)
        {
          var konfirmasi = confirm('Are you sure wants delete this file ?');
          if (konfirmasi) {
            if (id != null) {
              $.ajax({
                  url: '{{ urlBackendAction("hapuspdf") }}/' + id + '/' + jenis,
                  type: 'GET',
                  success: function( hasil ) {
                      if ( hasil.status === 'success' ) {
                          alert(hasil.message);
                          $("#" + jenis).val("");
                          $("#" + jenis).attr('src','');
                          $("#div_image_" + jenis).hide();
                      }
                  },
                  error: function( data ) {
                      if ( data.status === 'failed' ) {
                          alert(hasil.message);
                      }
                  }
              });
            }
          }
        }



    </script>

@endsection
