@extends('backend.layouts.layout')
@section('content')

<div id="app_header_shadowing"></div>
<div id="app_content">
    <div id="content_header">
        <h3 class="user">{{ webarq::titleActionForm() }}</h3>
    </div>
    <div id="content_body">

        @include('backend.common.flashes')

        <div class = 'row'>

            <div class = 'col-md-12'>
                <div>
                     <div class="form-group">
                        <label>Name</label> :
                        {{ $model->name }}
                    </div>

                    <div class="form-group">
                        <label>Email</label> :
                        {{ $model->email }}
                    </div>

                    <div class="form-group">
                        <label>Message</label> :
                        {!! $model->message !!}
                    </div>

                    <div class="form-group">
                        <a href="{{ urlBackendAction('index') }}" class="btn btn-primary">Back</a>
                    </div>
                </div>

             </div>

        </div>

    </div>
</div>
@endsection