@extends('backend.layouts.layout')
@section('content')

<div id="app_header_shadowing"></div>
<div id="app_content">
    <div id="content_header">
        <h3 class="user">{{ webarq::titleActionForm() }}</h3>
    </div>
    <div id="content_body">

        @include('backend.common.flashes')

        <div class = 'row'>

            <div class = 'col-md-12'>
                <div id="tabs">
                    <ul>
                        <li><a href="#tabs-listing">Listing</a></li>
                        <li><a href="#tabs-setting">Setting</a></li>
                    </ul>

                    <!-- LISTING -->
                    <div id="tabs-listing">

                            <div class="col-md-4">
                                <a href="{{ urlBackendAction('exportdata') }}"><img src="{{ asset('frontend/images/material/download.png') }}" /> Download</a>
                            </div>

                            <br>
                            <br>

                            <table class='table' id='table'>
                                <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Message</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                
                            </table>

                    </div>

                    <!-- SETTING -->
                    <div id="tabs-setting">
                    
                        {!! Form::model($setting,['files'=>true]) !!} 
                     
                        <div>
                      
                             <div class="form-group">
                                <label>Send Email</label> :
                                {!! Form::checkbox('send_email',$isCheck,$isCheck) !!}
                            </div>
                            <div class="form-group">
                                <label>Email</label> :
                                {!! Form::text('email',@$email->value,['class' => 'form-control']) !!}
                            </div>
                        </div>

                      <br><br>
                      <button type="submit" class="btn btn-primary">{{ !empty($model->id) ? 'Update' : 'Save' }}</button>
                    
                    {!! Form::close() !!}

                    </div>

                </div>

             </div>

        </div>

    </div>
</div>
@endsection

@section('script')
    
    <script type="text/javascript">
        
        $(document).ready(function(){
            $('#table').DataTable({
                processing: true,
                serverSide: true,
                ajax: '{{ urlBackendAction("data") }}',
                columns: [
                    { data: 'name', name: 'name'},
                    { data: 'email', name: 'email '},
                    { data: 'message', name: 'message' },
                    { data: 'action', name: 'action' , searchable: false },
                ]
            });

             $( "#tabs" ).tabs();
        });

    </script>

@endsection
