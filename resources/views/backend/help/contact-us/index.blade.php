@extends('backend.layouts.layout')
@section('content')

<div id="app_header_shadowing"></div>
<div id="app_content">
    <div id="content_header">
        <h3 class="user">{{ webarq::titleActionForm() }}</h3>
    </div>
    <div id="content_body">

        @include('backend.common.flashes')

        <div class = 'row'>
           <div class = 'col-md-12'>

                <div id="tabs">
                    <ul>
                        <li><a href="#tabs-listing">Listing</a></li>
                        <li><a href="#tabs-setting">Setting</a></li>
                    </ul>

                    <div id="tabs-listing">


                            <div class="col-md-4">
                                <a href="{{ urlBackendAction('exportdata') }}"><img src="{{ asset('frontend/images/material/download.png') }}" /> Download</a>
                            </div>

                            <br>
                            <br>

                            <table class='table' id='table'>
                                <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Date</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                            </table>

                    </div>

                    <!-- SETTING -->
                    <div id="tabs-setting">
                    
                        @include('backend.common.errors')

                         {!! Form::model($setting,['files'=>true]) !!} 
                         
                            <div id="setting">

                              <div class="form-group">
                                <label>Email Setting</label>
                                <div>
                                  <div class="form-group">
                                      <span>Send Email</span> :
                                      {!! Form::checkbox('send_email',@$isCheck,@$isCheck) !!}
                                  </div>

                                  <div class="form-group">
                                      <label>Email</label> :
                                      {!! Form::text('email',@$email->value,['class' => 'form-control']) !!}
                                  </div>
                                </div>
                              </div>
                          
                              <div class="form-group">
                                <label>Content</label>
                                  <div id="tabs2">
                                      <ul>
                                      @foreach(langs() as $lang => $val)
                                        <li><a href="#tabs-{{$lang}}">{{$val}}</a></li>
                                      @endforeach
                                      </ul>

                                          @foreach(langs() as $lang => $val)
                                            <?php
                                              $settings = $detail($lang);
                                            ?>
     
                                               <div id="tabs-{{ $lang }}">

                                                    <div class="form-group">
                                                      <label>Title Caption</label> :
                                                      {!! Form::text('title_caption['.$lang.']' , @$settings->title_caption ,['class' => 'form-control']) !!}
                                                       {!! Form::hidden('lang['.$lang.']' , $lang ,['class' => 'form-control']) !!}
                                                    </div>

                                                    <div class="form-group">
                                                      <label>Title Main</label> :
                                                      {!! Form::text('title_main['.$lang.']' , @$settings->title_main ,['class' => 'form-control']) !!}
                                                    </div>

                                                    <div class="form-group">
                                                      <label>Su Title</label> :
                                                      {!! Form::text('sub_title['.$lang.']' , @$settings->sub_title ,['class' => 'form-control']) !!}
                                                    </div>

                                                    <div class="form-group">
                                                      <label>Banner Image (1920 x 550)</label>
                                                      {!! Webarq::htmlImageFree('banner_image['.$lang.']', 'contents/'.@$settings->banner_image, @$settings,'banner_image') !!}
                                                    </div>

                                                    <div class="form-group">
                                                      <label>Title Content</label> :
                                                      {!! Form::text('title_content['.$lang.']' , @$settings->title_content ,['class' => 'form-control']) !!}
                                                    </div>

                                                    <div class="form-group">
                                                      <label>Title Contact</label> :
                                                      {!! Form::text('title_contact['.$lang.']' , @$settings->title_contact ,['class' => 'form-control']) !!}
                                                    </div>
     
                                                    <div class="form-group">
                                                      <label>Sub Title Contact</label> :
                                                      {!! Form::text('sub_title_contact['.$lang.']' , @$settings->sub_title_contact ,['class' => 'form-control']) !!}
                                                    </div>

                                                    <div class="form-group">
                                                      <label>Address Wording</label> :
                                                      {!! Form::textarea('address['.$lang.']' , @$settings->address ,['class' => 'form-control ckeditor']) !!}
                                                    </div>

                                                    <div class="form-group">
                                                      <label>Maps</label> :
                                                      {!! Form::text('maps['.$lang.']' , @$settings->maps ,['class' => 'form-control']) !!}
                                                    </div>

                                                </div>
                                          @endforeach

                                    </div>
                              </div>

                            </div>

                          <br><br>


                          <button type="submit" class="btn btn-primary">{{ !empty($model->id) ? 'Update' : 'Save' }}</button>
                        
                        {!! Form::close() !!}

                    </div>

                </div>

            </div>

        </div>

        


    </div>
</div>
@endsection

@section('script')
{!! JsValidator::formRequest('App\Http\Requests\Backend\MenuSettingsRequest') !!}
    <script type="text/javascript">
        
        $(document).ready(function(){
            $('#table').DataTable({
                processing: true,
                serverSide: true,
                ajax: '{{ urlBackendAction("data") }}',
                columns: [
                    { data: 'name', name: 'name' },
                    { data: 'email', name: 'email' },
                    { data: 'created_at', name: 'created_at' },
                    { data: 'action', name: 'action' , searchable: false},
                ]
            });

            $( "#tabs" ).tabs();
            $( "#tabs2" ).tabs();
        });

    </script>

@endsection
