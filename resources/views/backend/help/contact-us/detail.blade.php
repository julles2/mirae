@extends('backend.layouts.layout')
@section('content')

<div id="app_header_shadowing"></div>
<div id="app_content">
    <div id="content_header">
        <h3 class="user"> Contact Us </h3>
    </div>
        <div id="content_body">
            
            <div class = 'row'>

                <div class = 'col-md-6'>

                    <form>
                        
                        <div class="form-group">
                          <label>Name</label> :
                          {{ @$model->name }}
                        </div>

                        <div class="form-group">
                          <label>Email</label> :
                          {{ @$model->email }}
                        </div>

                        <div class="form-group">
                          <label>Date</label> :
                          {{ @$model->created_at }}
                        </div>

                        <div class="form-group">
                          <label>Message</label> :
                          {{ @$model->message }}
                        </div>

                        <a class="btn btn-primary" href="{{ urlBackendAction('index') }}"> Back </a>

                    </form>

                </div>

            </div>

        </div>
    </div>
@endsection