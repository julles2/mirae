@extends('backend.layouts.layout')
@section('content')
<div id="app_header_shadowing">
</div>
<div id="app_content">
    <div id="content_header">
        <h3 class="user">
            Menu
        </h3>
    </div>
    <div id="content_body">
        <div class="row">
            <div class="col-md-8">
                @include('backend.common.errors')
				@include('backend.common.flashes')
                {!! Form::model($model,['files' => true]) !!}
                <div class="form-group">
                    <label>
                        Disclaimer
                    </label>
                    {!! Form::textarea('portfolio' , @$data->portfolio ,['class' => 'form-control','id'=>'description']) !!}
                </div>
                <button class="btn btn-primary" type="submit">
                   Update
                </button>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
<script type="text/javascript">
  window.onload = function()
  {
      CKEDITOR.replace( 'description',{
      filebrowserBrowseUrl: '{{ urlBackend("image/lib")}}'});
  }
</script>
@endsection
