
@extends('backend.layouts.layout')
@section('content')
<?php $dir = "packages/barryvdh/elfinder"; ?>

<link rel="stylesheet" type="text/css" media="screen" href="{{ asset(null) }}backend//elfinder/css/elfinder_bootstrap.css">
<div id="app_header_shadowing"></div>
<div id="app_content">
    <div id="content_header">
        <h3 class="user"> {{ webarq::titleActionForm() }}</h3>
    </div>
        <div id="content_body">
            
            <div class = 'row'>

                <div class = 'col-md-12'>

                    <div id = 'elfinder'>
                      
                    </div>

                </div>

            </div>

        </div>
    </div>
@endsection
@section('script')
<script type="text/javascript">
            $().ready(function() {
                $('#elfinder').elfinder({
                    // set your elFinder options here
                    customData: { 
                        _token: '{{ csrf_token() }}'
                    },
                    url : '{{ route("elfinder.connector") }}',  // connector URL
                    soundPath: '{{ asset($dir.'/sounds') }}'
                });
            });
        </script>
@endsection
