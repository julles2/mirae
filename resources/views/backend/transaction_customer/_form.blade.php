@extends('backend.layouts.layout')
@section('content')

<div id="app_header_shadowing"></div>
<div id="app_content">
    <div id="content_header">
        <h3 class="user"> {{ webarq::titleActionForm() }}</h3>
    </div>
        <div id="content_body">
            <div class = 'row'>
            {!! Form::model($model,['id'=>'form_approve']) !!}
                <div class = 'col-md-6'>
                    <h2>Data</h2>
                    @include('backend.common.errors')

                      <div class="form-group">
                        <label>Acnt No</label>
                        {!! Form::text('acnt_no' , null ,['class' => 'form-control','readonly'=>true]) !!}
                      </div>
                      <div class="form-group">
                        <label>Name</label>
                        {!! Form::text('name' , null ,['class' => 'form-control','readonly'=>true]) !!}
                      </div>
                      <div class="form-group">
                        <label>Status</label>
                        {!! Form::text('status' , $model->status=='y'?'Approve' : 'Pending' ,['class' => 'form-control','readonly'=>true]) !!}
                      </div>
                          <div class="form-group">
                            <label>Created At</label>
                            {!! Form::text('created_at' , null ,['class' => 'form-control','readonly'=>true]) !!}
                          </div>
                      <div class="form-group">
                        <label>Nilai</label>
                        {!! Form::text('risk_total' , null,['class' => 'form-control','readonly'=>true]) !!}
                      </div>
                      @if($model->status == 'r')
                        <div class="form-group">
                          <label>Rejected Message</label>
                          {!! Form::textarea('msg_rejected' , null ,['class' => 'form-control','readonly'=>true]) !!}
                        </div>
                      @endif
                      @if(in_array($model->status,$statusRejectedApproved))
                        <a href = "{{ urlBackendAction('index') }}" class="btn btn-primary btn-sm">Back To Listing</a>
                      @else
                        <button type="button" id = "btn" class="btn btn-success">Approve</button>
                        <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#myModal">Reject</button>
                      @endif
                </div>

                <div class = 'col-md-6'>
                    <h2>Additional Question</h2>
                    @foreach(json_decode($model->data_question) as $key => $val)

                        <?php
                        if($key == 'religion')
                        {
                            $val = $repo->religions()[$val];
                        }elseif($key == 'birth_country' || $key == 'nationality'){
                            $val = $cekCountry($val);
                        }
                        ?>

                        <div class="form-group">
                          <label>{{ underscore_to_label($key) }}</label>
                          {!! Form::text($key , $val ,['class' => 'form-control','readonly'=>true]) !!}
                        </div>
                    @endforeach
                    

                </div>

            {!! Form::close() !!}
            </div>

        </div>
    </div>

    <!-- Modal -->
      <div id="myModal" class="modal fade" role="dialog">
        <div class="modal-dialog">

          <!-- Modal content-->
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title">Reject Message</h4>
            </div>
            <div class="modal-body">
              {!! Form::open(['method'=>'post','url'=>urlBackendAction('reject/'.$model->id),'id'=>'form_reject']) !!}
                <p>
                  <div class="form-group">
                    <textarea name="msg_rejected" id = "msg_rejected" class="form-control"></textarea>
                  </div>
                </p>
              {!! Form::close() !!}
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-danger" id = "btn_reject">Reject This User</button>
            </div>
          </div>

        </div>
      </div>

@endsection
@section('script')
<script>
    $(document).ready(function(){
        $("#btn").on('click',function(){
            con = confirm("Are you sure want to approve this customer ?");

            if(con == true)
            {
                $("#form_approve").submit();
            }
        });

        $("#btn_reject").on('click',function(){
            $msg = $("#msg_rejected");

            if($msg.val() == "")
            {
              alert('Message Cannot be empty');
            }else{
              $("#form_reject").submit();
            }
        });
    });
</script>
@endsection
