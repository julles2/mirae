@extends('backend.layouts.layout')
@section('content')

<div id="app_header_shadowing"></div>
<div id="app_content">
    <div id="content_header">
        <h3 class="user">{{ webarq::titleActionForm() }}</h3>
    </div>
    <div id="content_body">
        @include('backend.common.flashes')
        <div class = 'row'>
           <div class = 'col-md-12'>
               {!! webarq::buttonCreate() !!}
                <p>&nbsp;</p>
                <p>&nbsp;</p>

                <table class = 'table' id = 'table'>
                    <thead>
                        <tr>
                            <th width = ''>Acnt No</th>
                            <th width = ''>Name</th>
                            <th width = ''>Status</th>
                            <th width = ''>Action</th>
                        </tr>
                    </thead>

                </table>

            </div>

        </div>
    </div>
</div>
@endsection

@section('script')

    <script type="text/javascript">

        $(document).ready(function(){
            $('#table').DataTable({
                processing: true,
                serverSide: true,
                ajax: '{{ urlBackendAction("data") }}',
                "pageLength": 100,
                ordering: "false",
                columns: [
                    { data: 'acnt_no', name: 'acnt_no' },
                    { data: 'name', name: 'name' },
                    { data: 'status', name: 'status' },
                    { data: 'action', name: 'action' , searchable: false},

                ]
            }).on( 'error.dt', function ( e, settings, techNote, message ) {
                alert("Your Login is Expired");
                document.location.href='{{ url('login/logout') }}';
            });
        });

    </script>

@endsection
