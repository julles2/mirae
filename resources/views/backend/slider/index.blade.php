@extends('backend.layouts.layout')
@section('content')

<div id="app_header_shadowing"></div>
<div id="app_content">
    <div id="content_header">
        <h3 class="user">{{ webarq::titleActionForm() }}</h3>
    </div>
    <div id="content_body">

        @include('backend.common.flashes')

        <div class = 'row'>

            <div class = 'col-md-12'>
                {!! webarq::buttonCreate() !!}

                <br>
                <br>

                <table class='table' id='table'>
                    <thead>
                        <tr>
                            <th>Title</th>
                            <th>Wording</th>
                            <th>Status</th>
                            <th>Order</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                </table>
             </div>

</div>

    </div>
</div>
@endsection

@section('script')
    
    <script type="text/javascript">
        
        $(document).ready(function(){
            $('#table').DataTable({
                processing: true,
                serverSide: true,
                ajax: '{{ urlBackendAction("data") }}',
                columns: [
                    { data: 'title', name:  'slider_translations.title'},
                    { data: 'wording', name: 'slider_translations.wording '},
                    { data: 'status', name: 'status' },
                    { data: 'order', name: 'order' },
                    { data: 'action', name: 'action' , searchable: false },
                ]
            }).on( 'error.dt', function ( e, settings, techNote, message ) {
                alert("Your Login is Expired");
                document.location.href='{{ url('login/logout') }}';
            });
        });

    </script>

@endsection
