@extends('backend.layouts.layout')
@section('content')

<div id="app_header_shadowing"></div>
<div id="app_content">
    <div id="content_header">
        <h3 class="user">Home Slider</h3>
    </div>
        <div id="content_body">
            
            <div class = 'row'>

                <div class = 'col-md-12'>

                    @include('backend.common.errors')

                     {!! Form::model($model,['files'=>true]) !!} 
                     
                        <!-- NEW CATEGORIES -->
                        <div id="MainAboutUs">
                      
                          <div class="form-group">
                            <label>Status</label> :
                            {!! Form::select('status' , ['' => '-- Select Status --', 'y' => 'Publish' , 'n' => 'Un Publish'] , @$model->status ,['class' => 'form-control']) !!}
                          </div>

                          <div class="form-group">
                            <label>Image (1425 x 749)</label>
                            {!! Webarq::htmlImage('image', @$model->image, $model) !!}
                          </div>

                          <div class="form-group">
                            <label>Content</label>
                              <div id="tabs">
                                  <ul>
                                  @foreach(langs() as $lang => $val)
                                    <li><a href="#tabs-{{$lang}}">{{$val}}</a></li>
                                  @endforeach
                                  </ul>

                                  <?php $no = 0; ?>
                                  @if(!empty($sliderTranslations))

                                      @foreach(langs() as $lang => $val)
                                            <?php
                                              $details = $detail($lang);
                                            ?>

                                             <div id="tabs-{{ $lang }}">
                                                  <div class="form-group">
                                                    <label>Title</label> :
                                                    {!! Form::text('title['.$lang.']' , @$details->title ,['class' => 'form-control']) !!}
                                                    {!! Form::hidden('id['.$lang.']' , @$details->id ,['class' => 'form-control']) !!}
                                                    {!! Form::hidden('lang['.$lang.']' , $lang ,['class' => 'form-control']) !!}
                                                  </div>

                                                  <div class="form-group">
                                                    <label>Wording</label> :
                                                    {!! Form::textarea('wording['.$lang.']' , @$details->wording ,['class' => 'form-control ckeditor']) !!}
                                                  </div>

                                                  <div class="form-group">
                                                    <label>Use Button</label> :
                                                    {!! Form::select('statusbutton['.$lang.']' , ['' => '-- Select --', 'y' => 'Yes' , 'n' => 'No'] , @$details->status ,['class' => 'form-control','id' => 'status'.$lang]) !!}
                                                  </div>

                                                  <div class="form-group" id="divstatus2{{$lang}}">
                                                    <label>Button Caption</label> :
                                                    {!! Form::text('button_caption['.$lang.']' , @$details->button_caption ,['class' => 'form-control']) !!}
                                                  </div>

                                                  <div class="form-group" id="divstatus{{$lang}}">
                                                    <label>Url</label> :
                                                    {!! Form::text('url['.$lang.']' , @$details->url ,['class' => 'form-control']) !!}
                                                  </div>

                                                  <div class="form-group">
                                                    <label>Order</label> :
                                                    {!! Form::text('order['.$lang.']' , @$details->order ,['class' => 'form-control']) !!}
                                                  </div>
                                              </div>
                                        <?php $no++;?>
                                        @endforeach

                                  @else

                                      @foreach(langs() as $lang => $val)
                                          <div id="tabs-{{ $lang }}">
                                                <div class="form-group">
                                                  <label>Title</label> :
                                                  {!! Form::text('title['.$lang.']' , @$details->title ,['class' => 'form-control']) !!}
                                                  {!! Form::hidden('id['.$lang.']' , null ,['class' => 'form-control']) !!}
                                                  {!! Form::hidden('lang['.$lang.']' , $lang ,['class' => 'form-control']) !!}
                                                </div>

                                                <div class="form-group">
                                                  <label>Wording</label> :
                                                  {!! Form::textarea('wording['.$lang.']' , null ,['class' => 'form-control ckeditor']) !!}
                                                </div>

                                                <div class="form-group">
                                                  <label>Use Button</label> :
                                                    {!! Form::select('statusbutton['.$lang.']' , ['' => '-- Select --', 'y' => 'Yes' , 'n' => 'No'] , null ,['class' => 'form-control','id' => 'status'.$lang]) !!}
                                                </div>

                                                <div class="form-group" id="divstatus2{{$lang}}">
                                                    <label>Button Caption</label> :
                                                    {!! Form::text('button_caption['.$lang.']' , null ,['class' => 'form-control']) !!}
                                                  </div>

                                                <div class="form-group show" id="divstatus{{$lang}}">
                                                  <label>Url</label> :
                                                  {!! Form::text('url['.$lang.']' , null ,['class' => 'form-control']) !!}
                                                </div>

                                                <div class="form-group">
                                                  <label>Order</label> :
                                                  {!! Form::text('order['.$lang.']' , null ,['class' => 'form-control']) !!}
                                                </div>
                                          </div>
                                        <?php $no++;?>
                                        @endforeach

                                  @endif

                                </div>
                          </div>

                        </div>

                      <br><br>


                      <button type="submit" class="btn btn-primary">{{ !empty($model->id) ? 'Update' : 'Save' }}</button>
                    
                    {!! Form::close() !!}

                </div>

            </div>

        </div>
    </div>
@endsection

@section('script')
{!! JsValidator::formRequest('App\Http\Requests\Backend\SliderRequest') !!}
<script type="text/javascript">
        
        $(document).ready(function(){
            $( "#tabs" ).tabs();

            var useButtonen = $("#statusen").val();
            var useButtonid = $("#statusid").val();
            var useButtonen2 = $("#status2en").val();
            var useButtonid2 = $("#status2id").val();

            console.log(useButtonen);

            if (useButtonen == 'y') {
                $("#divstatusen").removeClass("hidden");
                $("#divstatusen").addClass("show");
                $("#divstatus2en").removeClass("hidden");
                $("#divstatus2en").addClass("show");
            }else{
                $("#divstatusen").removeClass("show");
                $("#divstatusen").addClass("hidden");
                $("#divstatus2en").removeClass("show");
                $("#divstatus2en").addClass("hidden");
            }

            if (useButtonid == 'y') {
                $("#divstatusid").removeClass("hidden");
                $("#divstatusid").addClass("show");
                $("#divstatus2id").removeClass("hidden");
                $("#divstatus2id").addClass("show");
            }else{
                $("#divstatusid").removeClass("show");
                $("#divstatusid").addClass("hidden");
                $("#divstatus2id").removeClass("show");
                $("#divstatus2id").addClass("hidden");
            }

            $("#statusen").change(function () {
                var status = this.value;
                if (status == 'y') {
                    $("#divstatusen").removeClass("hidden");
                    $("#divstatusen").addClass("show");
                    $("#divstatus2en").removeClass("hidden");
                    $("#divstatus2en").addClass("show");
                }else{
                    $("#divstatusid").removeClass("show");
                    $("#divstatusid").addClass("hidden");
                    $("#divstatus2id").removeClass("show");
                    $("#divstatus2id").addClass("hidden");
                }
            });

            $("#statusid").change(function () {
                var status = this.value;
                if (status == 'y') {
                    $("#divstatusid").removeClass("hidden");
                    $("#divstatusid").addClass("show");
                    $("#divstatus2id").removeClass("hidden");
                    $("#divstatus2id").addClass("show");
                }else{
                    $("#divstatusid").removeClass("show");
                    $("#divstatusid").addClass("hidden");
                    $("#divstatus2id").removeClass("show");
                    $("#divstatus2id").addClass("hidden");
                }
            });
        });

    </script>

@endsection