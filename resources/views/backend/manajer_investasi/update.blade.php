@extends('backend.layouts.layout')
@section('content')
<div id="app_header_shadowing"></div>
<div id="app_content">
    <div id="content_header">
        <h3 class="user"> {{ webarq::titleActionForm() }}</h3>
    </div>
        <div id="content_body">

            <div class = 'row'>

                <div class = 'col-md-6'>

                    @include('backend.common.errors')

                     {!! Form::model($model,['files'=>true]) !!}
                     <div class="form-group">
                        <label>IM</label>
                        {!! Form::text('im',$model->im->im_nm,['class' => 'form-control','readonly']) !!}
                      </div>
                      <div class="form-group">
                        <label>Bulan</label>
                        {!! Form::selectMonth('bulan',null,['class' => 'form-control']) !!}
                      </div>
                      <div class="form-group">
                        <label>Tahun</label>
                        {!! Form::text('tahun',null,['class' => 'form-control']) !!}
                      </div>
                      <div class="form-group">
                        <label>AUM IDR</label>
                        {!! Form::text('aum_idr',null,['class'=>'form-control']) !!}
                      </div>
                      <div class="form-group">
                        <label>AUM USD</label>
                        {!! Form::text('aum_usd',null,['class'=>'form-control']) !!}
                      </div>
                      <div class="form-group">
                        <label>UNIT IDR</label>
                        {!! Form::text('unit_idr',null,['class'=>'form-control']) !!}
                      </div>
                      <div class="form-group">
                        <label>UNIT USD</label>
                        {!! Form::text('unit_usd',null,['class'=>'form-control']) !!}
                      </div>

                      <button type="submit" id = 'button' class="btn btn-primary">{{ !empty($model->id) ? 'Update' : 'Save' }}</button>

                    {!! Form::close() !!}

                </div>

            </div>

        </div>
    </div>
@endsection
