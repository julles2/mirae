@extends('backend.layouts.layout')
@section('content')

<div id="app_header_shadowing"></div>
<div id="app_content">
    <div id="content_header">
        <h3 class="user">{{ webarq::titleActionForm() }}</h3>
    </div>
    <div id="content_body">

        @include('backend.common.flashes')

        <div class = 'row'>
           <div class = 'col-md-12'>
            <?php /*
                {!! Form::open(['method'=>'get']) !!}
                    <div class="row">
                        <div class="col-md-6">
                            <label>Periode</label>
                            {!! Form::text('periode',null,['class' => 'form-control datepicker']) !!}

                            <button type="submit" style="margin-bottom:20px;margin-top:10px;" id = 'button' class="btn btn-primary">Filter</button>
                        </div>
                    </div>
                {!! Form::close() !!}
                <hr>
            */ ?>
                {!! webarq::buttonCreate() !!}
                <p>&nbsp;</p>

                <table class = 'table' id = 'table'>
                    <thead>
                        <tr>
                            <th width = '20%'>Periode</th>
                            <th width = '20%'>Code</th>
                            <th width = '40%'>Manajer Investasi</th>
                            <th width = '20%'>Action</th>
                        </tr>
                    </thead>

                </table>

            </div>
        </div>
    </div>
</div>
@endsection

@section('script')

    <script type="text/javascript">

        $(document).ready(function(){
            $('#table').DataTable({
                processing: true,
                serverSide: true,
                "ajax": {
                    "url": '{{ urlBackendAction("data") }}',
                    "data": function ( d ) {
                        d.periode = "{{get('periode')}}";
                        // d.custom = $('#myInput').val();
                        // etc
                    }
                },
                ordering:false,
                columns: [
                    { data: 'periode', name: 'im_datas.periode' },
                    { data: 'im_cd', name: 'im_cd' },
                    { data: 'im_nm', name: 'im_nm' },
                    { data: 'action', name: 'action' , searchable: false},
                ]
            }).on( 'error.dt', function ( e, settings, techNote, message ) {
                alert("Your Login is Expired");
                document.location.href='{{ url('login/logout') }}';
            });
        });

    </script>

@endsection
