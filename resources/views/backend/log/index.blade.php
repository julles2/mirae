@extends('backend.layouts.layout')
@section('content')

<div id="app_header_shadowing"></div>
<div id="app_content">
    <div id="content_header">
        <h3 class="user">{{ webarq::titleActionForm() }}</h3>
    </div>
    <div id="content_body">

        @include('backend.common.flashes')

        <div class = 'row'>
           <div class = 'col-md-12'>
                <div id="tabs">
                    <ul>
                        <li><a href="#tabs-1">Success</a></li>
                        <li><a href="#tabs-2">Failed</a></li>
                    </ul>
                    <div id="tabs-1">
                        <table class = 'table' id = 'table'>
                            <thead>
                                <tr>
                                    <th width = '80%'>Message</th>
                                    <th width = '20%'>Created</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                    <div id="tabs-2">
                        <table class = 'table' id = 'table2'>
                            <thead>
                                <tr>
                                    <th width = '80%'>Message</th>
                                    <th width = '20%'>Created</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
    
    <script type="text/javascript">
        
        $(document).ready(function(){

            $( "#tabs" ).tabs();

            $('#table').DataTable({
                processing: true,
                serverSide: true,
                ajax: '{{ urlBackendAction("data") }}',
                ordering:false,
                columns: [
                    { data: 'message', name: 'message' },
                    { data: 'created_at', name: 'created_at' },
                ]
            }).on( 'error.dt', function ( e, settings, techNote, message ) {
                alert("Your Login is Expired");
                document.location.href='{{ url('login/logout') }}';
            });

            $('#table2').DataTable({
                processing: true,
                serverSide: true,
                ajax: '{{ urlBackendAction("data-failed") }}',
                ordering:false,
                columns: [
                    { data: 'message', name: 'message' },
                    { data: 'created_at', name: 'created_at' },
                ]
            }).on( 'error.dt', function ( e, settings, techNote, message ) {
                alert("Your Login is Expired");
                document.location.href='{{ url('login/logout') }}';
            });
        });

    </script>

@endsection