@extends('backend.layouts.layout')
@section('content')

<div id="app_header_shadowing"></div>
<div id="app_content">
    <div id="content_header">
        <h3 class="user">{{ webarq::titleActionForm() }}</h3>
    </div>
    <div id="content_body">

        @include('backend.common.flashes')

        <div class = 'row'>
           <div class = 'col-md-12'>

                    <div class="col-md-4">
                        <a href="{{ urlBackendAction('exportdata') }}"><img src="{{ asset('frontend/images/material/download.png') }}" /> Download</a>
                    </div>

                    <br>
                    <br>

                    <table class='table' id='table'>
                        <thead>
                            <tr>
                                <th>Email</th>
                                <th>Register Date</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        
                    </table>
            </div>

        </div>

        


    </div>
</div>
@endsection

@section('script')
    <script type="text/javascript">
        
        $(document).ready(function(){
            $('#table').DataTable({
                processing: true,
                serverSide: true,
                ajax: '{{ urlBackendAction("data") }}',
                columns: [
                    { data: 'email', name: 'email' },
                    { data: 'created_at', name: 'created_at' },
                    { data: 'action', name: 'action' , searchable: false},
                ]
            }).on( 'error.dt', function ( e, settings, techNote, message ) {
                alert("Your Login is Expired");
                document.location.href='{{ url('login/logout') }}';
            });
        });

    </script>

@endsection
