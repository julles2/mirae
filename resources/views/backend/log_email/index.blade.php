@extends('backend.layouts.layout')
@section('content')

<div id="app_header_shadowing"></div>
<div id="app_content">
    <div id="content_header">
        <h3 class="user">{{ webarq::titleActionForm() }}</h3>
    </div>
    <div id="content_body">

        @include('backend.common.flashes')

        <div class = 'row'>
           <div class = 'col-md-12'>
                <table class = 'table' id = 'table'>
                    <thead>
                        <tr>
                            <th width = ''>From</th>
                            <th width = ''>To</th>
                            <th width = ''>System Message</th>
                            <th width = ''>Ref No</th>
                            <th width = ''>Transaction Type</th>
                            <th width = ''>Created At</th>
                            <th width = ''>Status</th>
                        </tr>
                    </thead>
                    
                </table>

            </div>

        </div>
    </div>
</div>
@endsection

@section('script')
    
    <script type="text/javascript">
        
        $(document).ready(function(){
            $('#table').DataTable({
                processing: true,
                serverSide: true,
                ajax: '{{ urlBackendAction("data") }}',
                ordering:false,
                columns: [
                    { data: 'from', name: 'from' },
                    { data: 'to', name: 'to' },
                    { data: 'message', name: 'message' },
                    { data: 'transaction_id', name: 'transaction_id' },
                    { data: 'transaction_type', name: 'transaction_type' },
                    { data: 'created_at', name: 'created_at' },
                    { data: 'status', name: 'status' },
                ]
            });
        });

    </script>

@endsection