@extends('backend.layouts.layout')
@section('content')

<div id="app_header_shadowing"></div>
<div id="app_content">
    <div id="content_header">
        <h3 class="user">{{ webarq::titleActionForm() }}</h3>
    </div>
    <div id="content_body">

        @include('backend.common.flashes')

        <div class = 'row'>

            <div class = 'col-md-12'>
                {!! Form::model($setting,['files'=>true]) !!} 
                 <div id="setting">
                          
                      <div class="form-group">
                          <div id="tabs">
                              <ul>
                              @foreach(langs() as $lang => $val)
                                <li><a href="#tabs-{{$lang}}">{{$val}}</a></li>
                              @endforeach
                              </ul>

                              <?php $no = 0; ?>
                                  @foreach(langs() as $lang => $val)
                                      <div id="tabs-{{ $lang }}">

                                            <div class="form-group">
                                              <label>URL Embed Youtube</label> :
                                              {!! Form::text('url['.$lang.']' , @$setting['url_'.$lang] ,['class' => 'form-control','id' => 'date']) !!}
                                              {!! Form::hidden('lang['.$lang.']' , $lang ,['class' => 'form-control']) !!}
                                            </div>
                                            <div class="form-group">
                                              <label>Description</label> :
                                              {!! Form::textarea('description['.$lang.']' , @$setting['description_'.$lang] ,['class' => 'form-control ckeditor']) !!}
                                              {!! Form::hidden('lang['.$lang.']' , $lang ,['class' => 'form-control']) !!}
                                            </div>

                                        </div>
                                  <?php $no++;?>
                                  @endforeach

                            </div>
                        </div>

                    </div>

                <button type="submit" class="btn btn-primary">{{ !empty($model->id) ? 'Update' : 'Save' }}</button>
                    
                {!! Form::close() !!}

            </div>

        
</div>

    </div>
</div>
@endsection

@section('script')
    
    <script type="text/javascript">
        
        $(document).ready(function(){
            $( "#tabs" ).tabs();
        });

    </script>

@endsection
