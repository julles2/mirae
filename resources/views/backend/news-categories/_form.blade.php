@extends('backend.layouts.layout')
@section('content')

<div id="app_header_shadowing"></div>
<div id="app_content">
    <div id="content_header">
        <h3 class="user"> New Categories</h3>
    </div>
        <div id="content_body">
            
            <div class = 'row'>

                <div class = 'col-md-6'>

                    @include('backend.common.errors')

                     {!! Form::model($model) !!} 
                     
                        <!-- NEW CATEGORIES -->
                        <div id="MainNewsCategories">
                        
                          <div id="tabs">
                            <ul>
                            @foreach(langs() as $lang => $val)
                              <li><a href="#tabs-{{$lang}}">{{$val}}</a></li>
                            @endforeach
                            </ul>

                            @if(!empty($categories))

                                @foreach(langs() as $lang => $val)
                                      <?php
                                        $details = $detail($lang);
                                      ?>

                                       <div id="tabs-{{ $lang }}">
                                            <div class="form-group">
                                              <label>Category</label> :
                                              {!! Form::text('category['.$lang.']' , @$details->category ,['class' => 'form-control']) !!}
                                              {!! Form::hidden('id['.$lang.']' , @$details->id ,['class' => 'form-control']) !!}
                                              {!! Form::hidden('lang['.$lang.']' , $lang ,['class' => 'form-control']) !!}
                                            </div>
                                        </div>
                                  @endforeach

                            @else

                                @foreach(langs() as $lang => $val)
                                    <div id="tabs-{{ $lang }}">
                                        <div class="form-group">
                                          <label>Category</label> : 
                                          {!! Form::text('category['.$lang.']' , null ,['class' => 'form-control']) !!}
                                          {!! Form::hidden('id['.$lang.']' , null ,['class' => 'form-control']) !!}
                                          {!! Form::hidden('lang['.$lang.']' , $lang ,['class' => 'form-control']) !!}
                                        </div>
                                    </div>
                                  @endforeach

                            @endif
                          
                          </div>
                          <p>&nbsp;</p>
                          <div class="form-group">
                            <label>Color</label> : 
                            {!! Form::color('color' , null ,['class' => '']) !!}
                          </div>
                        </div>

                      <br><br>


                      <button type="submit" class="btn btn-primary">{{ !empty($model->id) ? 'Update' : 'Save' }}</button>
                    
                    {!! Form::close() !!}

                </div>

            </div>

        </div>
    </div>
@endsection

@section('script')
{!! JsValidator::formRequest('App\Http\Requests\Backend\NewsCategoriesRequest') !!}
<script type="text/javascript">
        
        $(document).ready(function(){
            $( "#tabs" ).tabs();
        });

    </script>

@endsection