@extends('backend.layouts.layout')
@section('content')
<div id="app_header_shadowing"></div>
<div id="app_content">
    <div id="content_header">
        <h3 class="user"> {{ webarq::titleActionForm() }}</h3>
    </div>
        <div id="content_body">

            <div class = 'row'>

                <div class = 'col-md-12'>
                  @include('backend.common.errors')
                  {!! Form::model($model,['files'=>true]) !!}
                    <div id="tabs">
                        <ul>
                            <li><a href="#general-info">General Info</a></li>
                            <li><a href="#company-info">Company Profile</a></li>
                        </ul>

                        <div id="general-info">
                            

                            <div class="form-group">

                                  <div class="form-group">
                                      <div id="tabs2">
                                          <ul>
                                          @foreach(langs() as $lang => $val)
                                            <li><a href="#tabs-{{$lang}}">{{$val}}</a></li>
                                          @endforeach
                                          </ul>

                                              @foreach(langs() as $lang => $val)
                                                    <?php
                                                      $details = $detailGeneral($lang,$model->im_cd);
                                                    ?>

                                                     <div id="tabs-{{ $lang }}">

                                                          <div class="form-group">
                                                            <label>Description</label> :
                                                            {!! Form::textarea('description['.$lang.']' , @$details->description ,['class' => 'form-control ckeditor']) !!}
                                                            {!! Form::hidden('id['.$lang.']' , @$details->id ,['class' => 'form-control']) !!}
                                                            {!! Form::hidden('lang['.$lang.']' , $lang ,['class' => 'form-control']) !!}
                                                          </div>

                                                          <div class="form-group">
                                                            <label>Pengelolaan</label> :
                                                            {!! Form::text('pengelolaan['.$lang.']' , @$details->pengelolaan ,['class' => 'form-control']) !!}
                                                          </div>

                                                          <div class="form-group">
                                                            <label>Modal Dasar</label> :
                                                            {!! Form::text('modal_dasar['.$lang.']' , @$details->modal_dasar ,['class' => 'form-control']) !!}
                                                          </div>

                                                          <div class="form-group">
                                                            <label>Modal Disetor</label> :
                                                            {!! Form::text('modal_disetor['.$lang.']' , @$details->modal_disetor ,['class' => 'form-control']) !!}
                                                          </div>

                                                          <div class="form-group">
                                                            <label>Kepemilikan</label> :
                                                            {!! Form::text('kepemilikan['.$lang.']' , @$details->kepemilikan ,['class' => 'form-control']) !!}
                                                          </div>

                                                          <div class="form-group">
                                                            <label>Izin Usaha</label> :
                                                            {!! Form::text('izin_usaha['.$lang.']' , @$details->izin_usaha ,['class' => 'form-control']) !!}
                                                          </div>

                                                          <div class="form-group">
                                                            <label>Status</label> :
                                                            {!! Form::select('status['.$lang.']' , ['' => '-- Select Status --', 'y' => 'Publish' , 'n' => 'Un Publish'] , @$details->status ,['class' => 'form-control']) !!}
                                                          </div>
                                                      </div>

                                                @endforeach
                                        </div>
                                  </div>
                            </div>

                            <div class="form-group">
                              <label>Department</label>
                              <div>
                                  <a id='button' class="btn btn-primary addDepartment" style="color: #fff !important;">Add</a>
                                  <table class="table" id="departmentTable">
                                      <thead>
                                        <th>NAMA</th>
                                        <th>JABATAN</th>
                                      </thead>
                                      <tbody>
                                      @foreach($detailDepartment as $index => $value)
                                        <tr>
                                          <td>{!! Form::text('d_nama['.$index.']' , $value->nama ,['class' => 'form-control']) !!}</td>
                                          <td>{!! Form::select('d_jabatan['.$index.']' ,['komisaris'=>'Komisaris','direktur'=>'Direktur'],@$value->jabatan ,['class' => 'form-control']) !!}</td>
                                          <td>
                                            {!! Form::hidden('d_id['.$index.']' , $value->id ,['class' => 'form-control']) !!}
                                            <a id="{{ $value->id }}" class="btn btn-danger btn-sm hapusDepartment"><span class="glyphicon glyphicon-trash"></span></a>
                                          </td>
                                        </tr>
                                      @endforeach
                                      </tbody>
                                    </table>

                              </div>
                            </div>
                        </div>

                        <div id="company-info">
                            <div class="form-group">

                                  <div class="form-group">
                                      <div id="tabs3">
                                          <ul>
                                          @foreach(langs() as $lang => $val)
                                            <li><a href="#tabs-{{$lang}}">{{$val}}</a></li>
                                          @endforeach
                                          </ul>

                                              @foreach(langs() as $lang => $val)
                                                    <?php
                                                      $details = $detailCompany($lang, $model->im_cd);
                                                    ?>

                                                     <div id="tabs-{{ $lang }}">

                                                          <div class="form-group">
                                                            <label>Image Company (250px x 85px)</label> :
                                                            {!! Webarq::htmlImageFree('image['.$lang.']',@$detailsCompany->image, @$detailsCompany,'image') !!}
                                                          </div>

                                                          <div class="form-group">
                                                            <label>Description</label> :
                                                            {!! Form::textarea('description_company['.$lang.']' , @$details->description ,['class' => 'form-control ckeditor']) !!}
                                                            {!! Form::hidden('id['.$lang.']' , @$details->id ,['class' => 'form-control']) !!}
                                                            {!! Form::hidden('lang['.$lang.']' , $lang ,['class' => 'form-control']) !!}
                                                          </div>

                                                      </div>

                                                @endforeach
                                        </div>
                                  </div>
                            </div>
                        </div>

                    <div class="form-group col-md-12">
                      <br>
                      <br>
                      <button type="submit" id = 'button' class="btn btn-primary">{{ !empty($model->id) ? 'Update' : 'Save' }}</button>
                    </div>
                    {!! Form::close() !!}
                    </div>

                    

                </div>

            </div>

        </div>
    </div>
@endsection
@section('script')
{!! JsValidator::formRequest('App\Http\Requests\Backend\MIInfoRequest') !!}
    <script type="text/javascript">

        $(document).ready(function(){
            $('#tabs').tabs();
            $('#tabs2').tabs();
            $('#tabs3').tabs();

            $(document).on('click', '.hapusDepartment', function(e) {
                  var tr = $(this).closest('tr');
                  del_id = $(this).attr('id');

                  var konfirmasi = confirm('Are You sure want to delete this data?');

                  if(konfirmasi)
                  {

                    if ((del_id != null) && (del_id != "")) {
                        $.ajax({
                            url: "{{ urlBackend('mi-info/deletedepartment') }}/" + del_id,
                            success:function(result){
                              alert('Data has been deleted.');

                                tr.fadeOut(1000, function(){
                                    $(this).remove();
                                });
                            }
                        });
                    }else{
                        alert('Data has been deleted.');

                        tr.fadeOut(1000, function(){
                            $(this).remove();
                        });
                    }
                    
                  }
              });


            $(document).on('click', '.addDepartment', function(e) {
                var tr = $(this).closest('hidden');
                var clonedRow = "<tr>" +
                                  "<td><input class='form-control' name='d_nama[]' type='text' value=''></td>" +
                                  // "<td><input class='form-control' name='d_jabatan[]' type='text' value=''></td>" +
                                  "<td><select class = 'form-control' name = 'd_jabatan'><option value = 'komisaris'>Komisaris</option><option value = 'direktur'>Direktur</option></select></td>" +
                                  "<td>"+
                                      "<input class='form-control' name='d_id[]' type='hidden' value=''>"+
                                      "<a id='' class='btn btn-danger btn-sm hapusDepartment'><span class='glyphicon glyphicon-trash'></span></a>" +
                                  "</td>" +
                                "</tr>";
                $("#departmentTable").append(clonedRow); //add the row back to the table

            });

        });

    </script>

@endsection
