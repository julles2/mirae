<?php

return [
    'eq'=>'Ekuitas',
    'mx'=>'Campuran',
    'fi'=>'Pendapatan Tetap',
    'mm'=>'Pasar Uang',
    'sr'=>'Syariah',
    'all'=>'Semua',
];
