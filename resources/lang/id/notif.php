<?php
    return [
    	'information' => 'Informasi',
    	'success!' => 'Sukses!',
        'transaction_cancel'=>'Transaksi anda berhasil di batalkan',
        'add_to_cart'=>'Mohon periksa Chart anda, dan Klik tombol Checkout untuk menyelesaikan transaksi',
        'tanggal_30'=>'Format Tanggal Salah',
        'max_compare'=>'Maksimal 3 produk untuk perbandingan',
        'checkout_validation'=>'Checkout tidak bisa di lanjutkan, silahkan cek kesalahan input',
    ];
?>