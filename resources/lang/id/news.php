<?php
return [
    'category' => 'Kategori',
    'sortby' => 'Urutkan Berdasarkan',
    'readmore' => 'Selengkapnya',
    'back' => 'Kembali',
    'moreonnews' => 'Selengkapnya di Berita',
    'shareon' => 'Bagikan',
    'newest' => 'Terbaru',
    'oldest' => 'Terlama',
    'all' => 'Semua',
];
