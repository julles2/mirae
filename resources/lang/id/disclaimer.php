<?php

return [
    'title_performa'=>'Disclaimer',
    'performa'=>'Setiap keputusan investasi merupakan keputusan nasabah, sehingga tanggung jawab ada pada masing-masing nasabah yang membuat keputusan investasi tersebut. PT Mirae Asset Sekuritas Indonesia tidak bertanggung jawab atas segala keputusan investasi yang dilakukan oleh siapapun baik yang memberikan keuntungan ataupun kerugian dengan segala kondisi dan situasi apapun juga. Semua data yang tertera pada artikel merupakan data dari pihak luar dan tidak dapat digunakan sebagai jaminan atas dasar perhitungan untuk membeli atau menjual suatu efek. Data-data tersebut merupakan data historis yang menggambarkan kinerja di masa lalu dan bukan merupakan jaminan atas kinerja efek tersebut di masa mendatang. Investor wajib membaca, memahami prospektus dan ringkasan reksa dana yang terdapat pada halaman ini.',

    'title_portfolio'=>'Penyangkalan Portofolio',
    'portfolio'=>'Nilai yang tercantum pada halaman ini
                bukan acuan yang resmi. Nilai resmi yang
                dapat dijadikan acuan adalah nilai yang
                tertera pada dokumen yang diterbitkan
                oleh Bank Kustodian produk reksa dana.
                Nilai yang ditampilkan pada halaman ini bisa berbeda dengan yang tertera pada
                dokumen Bank Kustodian',

    'title_transaksi'=>'Penyangkalan Transaksi',
    'transaksi'=>'Reksa Dana adalah produk pasar modal dan
                    bukan merupakan produk bank. Produk ini
                    tidak dijamin oleh bank dan tidak termasuk
                    simpanan yang merupakan objek program
                    penjaminan pemerintah atau penjaminan
                    simpanan. <br/><br/>

                    Reksa dana bukan merupakan produk PT
                    Mirae Asset Sekuritas Indonesia akan tetapi
                    merupakan produk yang dikelola oleh
                    Manajer Investasi <br/><br/>

                    PT Mirae Asset Sekuritas Indonesia hanya
                    berkedudukan sebagai agen penjual efek
                    reksa dana. PT Mirae Asset Sekuritas Indonesia tidak
                    bertanggung jawab atas keterlambatan
                    penyampaian instruksi nasabah ke Manajer
                    Investasi yang diakibatkan oleh gangguan
                    jaringan atau hal-hal yang berkaitan
                    dengan keadaan di luar kemampuan (<i>force
                    majeure</i>). <br/><br/>

                    Berinvestasi di reksa dana mengandung
                    resiko. Calon pemodal wajib untuk
                    membaca, memahami dan menyetujui
                    prospektus sebelum memutuskan untuk
                    berinvestasi melalui reksa dana. Kinerja
                    masa lalu tidak bisa dijadikan acuan
                    sebagai kinerja masa datang. <br/><br/>

                    PT Mirae Asset Sekuritas tidak bertanggung
                    jawab atas segala kerugian, kehilangan,
                    tuntutan maupun gugatan yang dialami Nasabah yang mungkin timbul sebagai
                    akibat dari penyampaian informasi
                    Nasabah yang tidak lengkap termasuk
                    tetapi tidak terbatas pada pembatalan atau
                    perubahan instruksi yang disampaikan
                    kepada PT Mirae Asset Sekuritas Indonesia. <br/><br/>

                    Dengan menggunakan layanan jual dan beli
                    reksa dana melalui PT Mirae Asset Sekuritas
                    Indonesia sebagai agen penjual efek Reksa
                    Dana, maka Nasabah bertanggung jawab
                    penuh atas segala tindakan yang dilakukan
                    serta membebaskan PT Mirae Asset
                    Sekuritas Indonesia, termasuk anak
                    Perusahaan, afiliasi, pemegang saham,
                    direksi, pejabat, karyawan sepenuhnya dari
                    segala tuntutan dan kerugian, pembayaran,
                    maupun ongkos apapun yang timbul di
                    kemudian hari yang diajukan kepada PT
                    Mirae Asset Sekuritas Indonesia baik secara
                    langsung maupun tidak langsung,
                    termasuk tetapi tidak terbatas pada biaya
                    penasehat hukum dan biaya perkara, yang
                    berhubungan dengan dilaksanakannya
                    atau tidak dilaksanakannya instruksi dari
                    Nasabah oleh Manager Investasi.',
];
