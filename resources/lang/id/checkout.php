<?php 

return [
	'transaction'=>'Transaksi',
	'risk_notes'=>'Anda hendak memilih produk investasi yang memiliki risiko lebih tinggi dari profil risiko Anda. Apabila Anda tetap ingin melanjutkan transaksi?',
	'yes'=>'YA',
	'no'=>'TIDAK',
	'dalam_rupiah'=>'Semua nilai dalam Rupiah (IDR) kecuali disebutkan lain.',
];