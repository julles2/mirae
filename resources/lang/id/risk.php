<?php
return [
    'low' => 'Rendah',
    'moderate' => 'Rendah - Sedang',
    'low_to_moderate' => 'Rendah - Sedang',
    'moderate_to_high' => 'Moderat - Tinggi',
    'high' => 'Tinggi',
];