<?php
    return [
    	'information' => 'information',
    	'success!' => 'Success!',
        'transaction_cancel'=>'Data has been canceled',
        'add_to_cart'=>'Please check your Chart, and Click Checkout button to complete the transaction',
        'tanggal_30'=>'Incorrect date',
        'max_compare'=>'Maximum 3 product for comparison',
        'checkout_validation'=>'Checkout can not proceed, please check input error',
    ];
?>
