<?php
return [
    'low' => 'Low',
    'moderate' => 'Low to Moderate',
    'low_to_moderate' => 'Moderate',
    'moderate_to_high' => 'Moderate to High',
    'high' => 'High',
];
