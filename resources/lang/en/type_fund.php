<?php

return [
    'eq'=>'Equity',
    'mx'=>'Balance',
    'fi'=>'Fixed Income',
    'mm'=>'Money Market',
    'sr'=>'Syariah',
    'all'=>'All',
];
