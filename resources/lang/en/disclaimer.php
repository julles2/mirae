<?php

return [
    'title_performa'=>'Risk and Disclaimer Peformance',
    'performa'=>'Every investment decision is the customer
                decision, therefore responsibility on each
                customer who made investment decisions. PT Mirae Asset Sekuritas Indonesia does
                not responsible for every investment
                decision made by anyone which giving
                profit or loss in every any condition and
                situation. Every data contained in the article is data
                from outside parties and cannot be used as
                collateral for basic calculation to buy or sell
                any securities. Those data are historical
                data that describes past performance and
                can-not guarantee future performance of
                these securities. Investor should read,
                understand and agree with the prospectus
                before deciding to invest through mutual funds.',

    'title_portfolio'=>'Disclaimer Portfolio',
    'portfolio'=>'The value listed on this page not an official
                    reference. The official value which can be
                    used as reference is the value stated in the
                    document issued by the custodian bank of
                    the mutual fund product. The value
                    displayed on this page may be different from those contained in the Custodian Bank documents.',

    'title_transaksi'=>'Disclaimer Transaction',
    'transaksi'=>'Mutual Fund is a capital market product and
                not a bank product. This product is not
                guaranteed by the bank and does not
                classify as deposit which is the object of
                government guarantee program or deposit
                insurance. <br/><br/>

                Mutual funds are not a product of PT Mirae
                Asset Sekuritas Indonesia but are products
                managed by the Investment Manager. <br/><br/>

                PT Mirae Asset Sekuritas Indonesia is only a
                mutual fund selling agent. <br/><br/>

                PT Mirae Asset Sekuritas Indonesia is not
                responsible for any delay in delivering
                customer instruction to Investment
                Manager due to network malfunction or
                other things related to force majaeure. <br/><br/>

                Investing in mutual fund carries risks.
                Investors must read, understand, and agree
                with the prospectus before deciding to
                invest through mutual funds. The past
                performance cannot be used as benchmark
                to see the future performance. <br/><br/>

                PT Mirae Asset Sekuritas Indonesia is not
                responsible for any damages, losses, claims
                or lawsuits suffered by the Customer that
                may arise as a result from incomplete
                information submission including but not
                limited to instruction cancellation or
                changing instruction delivered to PT Mirae
                Asset Sekuritas Indonesia <br/><br/>

                By using the mutual fund buying and selling
                service through PT Mirae Asset Sekuritas
                Indonesia as Mutual Fund selling agent, the
                Customer consciously willing to take full
                responsibility for all actions committed and
                release PT Mirae Asset Sekuritas Indonesia,
                include subsidiary, affiliate, shareholders,
                directors, official, employee from all
                demands and losses arising submitted to PT
                Mirae Asset Sekuritas Indonesia in the
                future both directly and indirectly, including
                but not limited to the costs of legal advisor
                and court fees, in relation to the execute or
                non-execute instruction from the Customer
                by the Investment Manager.

                ',
];
