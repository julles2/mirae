<?php 

return [
	'transaction'=>'Transaction',
	'risk_notes'=>'You want to choose an investment product that has a higher risk than your risk profile. Should you still want to continue the transaction?',
	'yes'=>'YES',
	'no'=>'NO',
	'dalam_rupiah'=>'All Values are in Rupiah (IDR) unless stated otherwise',
];