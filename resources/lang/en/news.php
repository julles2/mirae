<?php
return [
    'category' => 'Category',
    'sortby' => 'Sort By',
    'readmore' => 'Read More',
    'back' => 'Back',
    'moreonnews' => 'More on News',
    'shareon' => 'Share on',
    'newest' => 'Newest',
    'oldest' => 'Oldest',
    'all' => 'All',
];
