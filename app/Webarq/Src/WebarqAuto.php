<?php

function webarq()
{
	return new App\Webarq\Src\Webarq;
}

function parse($date)
{
    return carbon()->parse($date);
}

function injectModel($model)
{
	$inject = "App\Models\\".$model;
	return new $inject;
}

function injectOracle($model)
{
	$inject = "App\Models\Oracle\\".$model;
	return new $inject;
}

function urlBackend($slug)
{
	return url(webarq()->backendUrl.'/'.$slug);
}

function urlBackendAction($action)
{
	return urlBackend(request()->segment(2).'/'.$action);
}

function getUser()
{
	return Auth::user();
}

function randomImage($str = "")
{
	return str_random(6).date("YmdHis");
}

function assetFrontend($file="")
{
	return !empty($file) ?  asset('frontend/'.$file) : asset('frontend').'/';
}

function assetContent($file="")
{
    return !empty($file) ?  asset('contents/'.$file) : asset('contents').'/';
}

function lang()
{
    $langs = ['id','en'];
    $lang = request()->segment(1);
    if(in_array($lang,$langs))
    {
        return $lang;
    }else{
        return 'id';
    }
}

function urlLang($params)
{
	return url(lang().'/'.$params);
}

function setLang($locale, $url)
{
    return url($locale.'/'.$url);
}

function notEmpty($param,$notEmpty="",$empty="")
{
	$param = !empty($notEmpty) ? $notEmpty : $param;
	$paramDefault = !empty($default) ? $default : 0;

	return !empty($param) ? $param : $paramDefault;
}

function valueDecimal($param,$paramSub="")
{
	$param = str_replace(",","",$param);
	$ex = explode(".", $param);
    $sub = !empty($paramSub) ? $paramSub : 2;
	if(!empty($ex[1]))
	{
		//return round($ex[0].'.'.substr($ex[1],0,$sub),$sub);
        return round($param,$sub);
	}else{
		return round($param,$sub);
	}
}


function formatUang($uang)
{
	return !empty($uang) ? number_format($uang,2,".",",") : 0;
}

function formatUangNoComa($uang)
{
    return !empty($uang) ? str_replace(",",".",number_format($uang)) : 0;
}

function formatInteger($formatUang)
{
	return !empty($formatUang) ? str_replace(",", "", $formatUang) : 0;
}

function formatUangPop($uang)
{
    return !empty($uang) && is_numeric($uang) ? $numerFormat = number_format($uang,2,",",".") : 0;
}

function formatUangKoma2($uang)
{
    return !empty($uang) && is_numeric($uang) ? $numerFormat = number_format($uang,2,",",".") : 0;
}

function makeCode($var)
{
	return md5(md5('!@#$%^&*()'.$var));
}

function carbon()
{
	return new Carbon\Carbon();
}

function get($var)
{
	return request()->get($var);
}

function acnt_no()
{
	$user = auth()->guard('broker');
    if($user->check())
    {
        return $user->user()->a31OI01GridOutRecVo[0]->o_acnt_no;
    }else{
        return null;
    }

    //return $user->check() ? $user->user()->accountData->accountNum : 'd';
}


function langs()
{
	 return [
	 	'id'=>'Bahasa',
	 	'en'=>'English',
	 ];
}

function datelangs()
{
     return [
        'id'=>'id_ID',
        'en'=>'en_EN',
     ];
}

function mirae_date($date="")
{
   return !empty($date) ? carbon()->parse($date)->format("d-M-Y") : "-";
}

function mirae_date_oracle($date="")
{
	return !empty($date) ? carbon()->parse($date)->format("Ymd") : "";
}

function mirae_time($time)
{
	if(!empty($time))
	{
		$result = substr($time,0,-2);

		return carbon()->parse($result)->format("H:i:s");
	}
}

// GET SETTING MENU DATA
function menu_setting($menu,$lang)
{
	$menuSettings = new App\Models\MenuSettings;

	if (!empty($menu)) {
		$setting = $menuSettings
                ->select(
                    DB::raw('
                        id,
                        lang,
                        menu,
                        (SELECT a.value FROM menu_settings a WHERE a.key="title_caption" AND a.menu=menu_settings.menu AND a.lang=menu_settings.lang) as title_caption,
                        (SELECT a.value FROM menu_settings a WHERE a.key="title_main" AND a.menu=menu_settings.menu AND a.lang=menu_settings.lang) as title_main,
                        (SELECT a.value FROM menu_settings a WHERE a.key="sub_title" AND a.menu=menu_settings.menu AND a.lang=menu_settings.lang) as sub_title,
                        (SELECT a.value FROM menu_settings a WHERE a.key="banner_image" AND a.menu=menu_settings.menu AND a.lang=menu_settings.lang) as banner_image,
                        (SELECT a.value FROM menu_settings a WHERE a.key="title_content" AND a.menu=menu_settings.menu AND a.lang=menu_settings.lang) as title_content
                    '))
                ->where('menu',$menu);

        if (!empty($lang)) {

        	$setting = $setting->where('lang',$lang);

        }

        $setting = $setting->get();

        return $setting;
	}

	return '';
}

// GET SETTING MENU DATA
function menu_setting2($menu,$lang)
{
    $menuSettings = new App\Models\MenuSettings;

    if (!empty($menu)) {
        $setting = $menuSettings
                ->select(
                    DB::raw('
                        id,
                        lang,
                        menu,
                        (SELECT a.value FROM menu_settings a WHERE a.key="title_caption" AND a.menu=menu_settings.menu AND a.lang=menu_settings.lang) as title_caption,
                        (SELECT a.value FROM menu_settings a WHERE a.key="title_main" AND a.menu=menu_settings.menu AND a.lang=menu_settings.lang) as title_main,
                        (SELECT a.value FROM menu_settings a WHERE a.key="sub_title" AND a.menu=menu_settings.menu AND a.lang=menu_settings.lang) as sub_title,
                        (SELECT a.value FROM menu_settings a WHERE a.key="banner_image" AND a.menu=menu_settings.menu AND a.lang=menu_settings.lang) as banner_image,
                        (SELECT a.value FROM menu_settings a WHERE a.key="title_content" AND a.menu=menu_settings.menu AND a.lang=menu_settings.lang) as title_content,
                        (SELECT a.value FROM menu_settings a WHERE a.key="title_contact" AND a.menu=menu_settings.menu AND a.lang=menu_settings.lang) as title_contact,
                        (SELECT a.value FROM menu_settings a WHERE a.key="sub_title_contact" AND a.menu=menu_settings.menu AND a.lang=menu_settings.lang) as sub_title_contact,
                        (SELECT a.value FROM menu_settings a WHERE a.key="address" AND a.menu=menu_settings.menu AND a.lang=menu_settings.lang) as address,
                        (SELECT a.value FROM menu_settings a WHERE a.key="maps" AND a.menu=menu_settings.menu AND a.lang=menu_settings.lang) as maps
                    '))
                ->where('menu',$menu);

        if (!empty($lang)) {

            $setting = $setting->where('lang',$lang);

        }

        $setting = $setting->get();

        return $setting;
    }

    return '';
}

// SAVE SETTING MENU DATA
function save_setting($model,$menu,$request)
{
	$inputs = $request->all();

    // TEXT NAME MUST BE SAME WITH THIS ARRAY
    $keyArray = array('title_caption','title_main','sub_title', 'banner_image','title_content');
    // Title Caption = Text
    // Title Main = Text
    // Sub Title = Text
    // Banner Image = Image / File Image
    // Title Content = Text

    foreach (langs() as $key => $value) {
        for ($x=0; $x < count($keyArray); $x++) {

            if (!empty($inputs['lang'][$key])) {

                $upRow = $model
                            ->where('lang',$inputs['lang'][$key])
                            ->where('key',$keyArray[$x])
                            ->where('menu',$menu)
                            ->first();

                $data = [
                    'lang'=> $inputs['lang'][$key],
                    'key'=> $keyArray[$x],
                    'menu'=> $menu,
                ];

                if ($keyArray[$x] == 'banner_image') {

                    if (!empty($inputs['banner_image'][$key])) {

                        $data['value'] = image_setting($inputs['banner_image'][$key],$model,$request);

                    }

                }else{

                    $data['value'] = $inputs[$keyArray[$x]][$key];
                }

                !empty($upRow->id) ? $upRow->update($data) : $model->create($data);

            }else{

                $upRow = $model
                            ->where('key',$keyArray[$x])
                            ->where('menu',$menu)
                            ->first();

                $data = [
                    'key'=> $keyArray[$x],
                    'menu'=> $menu,
                    'value'=> $inputs[$keyArray[$x]][$key],
                ];

                !empty($upRow->id) ? $upRow->update($data) : $model->create($data);

            }

        }
    }


}

// SAVE IMAGE SETTING DATA
function image_setting($nameFile, $model, $request)
{
    $image = $nameFile;

    if(!empty($image))
    {
        $model = $model->where('menu','milestone')
                       ->where('key','banner_image')->first();
        if(!empty($model->value))
        {
            @unlink(public_path('contents/'.$model->value));
            //@unlink(public_path('frontend/images/contents/'.$model->value));
        }

        $imageName = randomImage().'.'.$image->getClientOriginalExtension();

        $image = \Image::make($image);

        $image = $image->resize(1920,550);

        $image = $image->save(public_path('contents/'.$imageName));

        $image = $imageName;

    }else{

        $image = $model->value;
    }

    return $image;
}

function kFormat($num) {
    
    if($num != '0.00000000')
    {
      $x = round($num);
      $x_number_format = number_format($x);
      $x_array = explode(',', $x_number_format);
      $x_parts = array('k', 'm', 'b', 't');
      $x_count_parts = count($x_array) - 1;
      $x_display = $x;
      $x_display = $x_array[0] . ((int) $x_array[1][0] !== 0 ? '.' . $x_array[1][0] : '');
      $x_display .= $x_parts[$x_count_parts - 1];
      return $x_display;
    }
}

function get_help($model, $menu)
{
    $modelnya = $model->select('bantuans.id',
                                    \DB::raw('(CASE WHEN bantuans.status = "y" THEN "Publish" ELSE "Un Publish" END) AS status'),
                                    'bantuans.order',
                                    'bantuan_translations.lang',
                                    'bantuan_translations.title',
                                    'bantuan_translations.description'
                                    )
        ->join('bantuan_translations','bantuan_translations.bantuan_id','=','bantuans.id')
        ->where('bantuans.menu',$menu)
        ->where('bantuan_translations.lang','id');

    $data = \Table::of($modelnya)
    ->addColumn('action' , function($modelnya){
        return \webarq::buttons($modelnya->id);
    })
    ->make(true);

    return $data;
}

function save_help($request,$model,$modelTranslations,$menu)
{
    $model = $model;

    $inputs = $request->all();

    $model->status = $inputs['status'];

    $model->order = $inputs['order'];

    $model->menu = $menu;

    $model->save();

    $bantuanTranslations = $modelTranslations;

    foreach (langs() as $key => $value) {
        $data = [
            'bantuan_id'=>$model->id,
            'lang'=>$inputs['lang'][$key],
            'title'=>$inputs['title'][$key],
            'description'=>$inputs['description'][$key],
        ];

        $modelTranslations->create($data);
    }
}

function update_help($request,$model,$modelTranslations,$id)
{
    $model = $model->findOrFail($id);

    $inputs = $request->all();

    $dataBantuan = [
                'status' => $inputs['status'],
                'order' => $inputs['order'],
    ];

    $model->update($dataBantuan);

    foreach (langs() as $key => $value) {
        $upRow = $modelTranslations->find($inputs['id'][$key]);

        $data = [
            'lang'=>$inputs['lang'][$key],
            'title'=>$inputs['title'][$key],
            'description'=>$inputs['description'][$key],
        ];

        if (!empty($upRow->id)) {

            $upRow->update($data);

        }else{

             $modelTranslations->create($data);

        }
    }
}

function delete_help($model,$modelTranslations,$id)
{
    $model = $model->findOrFail($id);

    $bantuanTranslations = $modelTranslations->where('bantuan_id',$model->id);

    $bantuanTranslations->delete();

    $model->delete();
}

function fundImage($fundCd)
{
    $model = new \App\Models\FundImage();

    $result =  $model->where('fund_cd',$fundCd)->first();

    if(!empty($result->image))
    {
        return $result->image;
    }else{
        return $result;
    }
}

function changeQueryString($arr,$queryString="")
{
    $url = !empty($queryString) ? htmlspecialchars_decode($queryString) : $_SERVER['QUERY_STRING'];
    parse_str($url, $query_string);

    foreach($arr as $key => $val)
    {
        $query_string[$key] = $val;
    }
    return http_build_query($query_string);
}

function checkWishList($fundCd,$acnt)
{
    $model = new \App\Models\Watchlists();

    $checkWishlist = function($fundCd, $acnt){

        $data = $model->where('fund_cd',$fundCd)
                                ->where('acnt_no', $acnt)
                                ->first();
        if (!empty($data->id)) {
            return 'style="pointer-events: none;cursor: default;"';
        }else{
            return "false";
        }
    };
}

function condition($var)
{
    if(!empty($var))
    {
        return $var;
    }else{

        return 0;
    }
}

function cekSatOrSun($date)
{
	return \Mirae::cekSatOrSun($date);
}

function substrText ($text,$max) {
     $totalStr = strlen($text);
     $totalStr > $max ? $returnText = substr($text, 0, $max).'...' : $returnText = $text;
     return $returnText;
}

function share($route,$description, $social)
{
    return App\shareSocialMedia()->shareSocial($route,$description, $social);
}

function underscore_to_label($var)
{
	$result = strtolower($var);
	$result = str_replace("_"," ",$result);
	$result = ucwords($result);
	return $result;
}

function imageWithEmpty($image="")
{
    if(!empty($image))
    {
        $path = asset('contents/'.$image);
    }else{
        $path = asset('noimage.png');
    }

    return $path;
}

function labelCart($type)
{
    $data = [
        'subscription'=>'Subscription',
        'subscription_auto_debet'=>'Auto Debet',
        'redeem'=>'Redemption',
        'switch'=>'Switching',
    ];

    if(array_key_exists($type,$data))
    {
        return $data[$type];
    }

}

function cekTanggal($date)
{
    $length = strlen($date);
}

function carbonLocalize($date)
{
    $date= parse($date)->format('l d F Y');
    if(lang() == 'id')
    {
        $d = explode(" ", $date);
        $date = str_replace($d[0], \Mirae::listDayIndo()[$d[0]], $date);
        $date = str_replace($d[2], \Mirae::listMonthIndo()[$d[2]], $date);

    }
    return $date;
}

function zeroDate($var)
{
    $strlen = strlen($var);

    return $strlen==1 ? "0".$var : $var;
}

function real_ip()
{
    $client  = @$_SERVER['HTTP_CLIENT_IP'];
    $forward = @$_SERVER['HTTP_X_FORWARDED_FOR'];
    $remote  = $_SERVER['REMOTE_ADDR'];

    if(filter_var($client, FILTER_VALIDATE_IP))
    {
        $ip = $client;
    }
    elseif(filter_var($forward, FILTER_VALIDATE_IP))
    {
        $ip = $forward;
    }
    else
    {
        $ip = $remote;
    }

    return $ip;
}

function visitor($ip)
{
    $url = "http://ip-api.com/json/".$ip;
    $json = file_get_contents($url);
    $arr = json_decode($json);
    return (array)$arr;
}

function futureValue($rate, $nper, $pmt, $pv = 0, $type = 0)
{

}

function language($en,$id){
    if(lang() == 'en')
    {
        return $en;
    }else{
        return $id;
    }
}

function getSegments($url)
{
    $parse = parse_url($url);
    $path = str_replace("/public","",$parse['path']);
    $segments = $path.'?'.@$parse['query'];
    return $segments;
}

function changeLang($segments)
{
    return url("change-lang?segments=".$segments);
}

function makeEmptyTable($table)
{
    \DB::statement("DELETE FROM $table");
    \DB::statement("ALTER TABLE $table AUTO_INCREMENT = 1");
}

function x100($val)
{
    return $val * 100;
}


function isJson($string)
{
    return is_string($string) && is_array(json_decode($string, true)) && (json_last_error() == JSON_ERROR_NONE) ? true : false;
}

function disclaimer($key)
{
    $model = new \App\Models\StaticModel();
    return $model->disclaimer($key);
}

function youtube_id($url)
{
    $result = "";
    if(!empty($url))
    {
        $exp = explode("/",$url);
        $result = end($exp);
    }
    
    return str_replace("?rel=0","",$result);
}

function youtube_thumbnail($url)
{
    $id = youtube_id($url);
    return "https://img.youtube.com/vi/$id/maxresdefault.jpg";
}

function carbon_lang($date,$format="")
{
    setlocale(LC_TIME, lang());
    $format = !empty($format) ? $format : "%d-%b-%Y";
    return carbon()->parse($date)->formatLocalized($format);  
}

function bagi_milyar($int)
{
    return bagi_angka($int,1000000000);
}

function bagi_juta($int)
{
    return bagi_angka($int,1000000);
}

function bagi_ribu($int)
{
    return bagi_angka($int,1000);
}

function bagi_angka($int,$angka)
{
    $result = 0;
    if(!empty($int))
    {
        $bagi = $int / $angka;
        $result = formatUangKoma2($bagi);
    }

    return $result;
}
