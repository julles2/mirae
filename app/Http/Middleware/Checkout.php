<?php

namespace App\Http\Middleware;

use Closure;
use App\Repository\User as UserRepository;
class Checkout
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $guard = auth()->guard('broker')->user();
        $sessionPin = session()->get('pin');
        if($guard != null)
        {
            if(!empty($sessionPin))
            {
                $user = new UserRepository();
                if($user->status_user->status == false)
                {
                    return redirect(urlLang('user/profile'))->with('info_global','Account anda belum aktif, anda belum bisa melanjutkan checkout');
                }else{
                    if(empty($user->statusUser()->risk_profile))
                    {
                       return redirect(urlLang('user/question/risk'));
                    }
                    
                    return $next($request);
                }
            }else{
                return redirect(urlLang('user/pin-login?to=checkout'));
            }
        }else{
            return redirect(urlLang('user/login?to=checkout'));
        }
    }
}
