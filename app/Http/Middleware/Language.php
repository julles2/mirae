<?php

namespace App\Http\Middleware;

use Closure;

class Language
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $langs = config('webarq.languages');
        if(!in_array(lang(), $langs))
        {
            abort(404);
        }
        $broker = auth()->guard('broker')->user();
        view()->share('broker',$broker);
        return $next($request);
    }
}
