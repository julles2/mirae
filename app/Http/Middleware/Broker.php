<?php

namespace App\Http\Middleware;


use Closure;
use Illuminate\Support\Facades\Auth;
use App\Presenters\TrackLoginPresenter;

class Broker
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @param  string|null $guard
     * @param string|null $secret
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null, $secret = null)
    {
        //dd(Auth::guard($guard)->check());
        if (Auth::guard($guard)->check()) {
            $member = Auth::guard($guard)->user();
            if (isset($secret) && 'broker' === $guard) {
                if (null === $member->getData('accountData')) {
//                    Set redirect session
                    \Session::put('redirect-url', \Request::fullUrl());

                    return redirect(lang() . '/user/pin-login');
                }
            }

            // if(!empty($member->getData('accountData')))
            // {
            //     $track = new TrackLoginPresenter();
            //     if($track->check() == false)
            //     {
            //         return redirect(urlLang('user/logout'))->withInfo('dupager');
            //     }
            // }
                

            return $next($request);
        } else {
            switch ($guard) {
                case 'broker':
                    if ('login' !== \Request::segment(3)) {
                        return redirect(lang() . '/user/login');
                    }

                default:
                    return $next($request);
            }
        }
    }
}
