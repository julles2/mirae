<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as BaseVerifier;

class VerifyCsrfToken extends BaseVerifier
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        'id/compare/store-fund',
        'en/compare/store-fund',
        'id/watchlists/addwatchlists',
        'en/watchlists/addwatchlists',
    ];
}
