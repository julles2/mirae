<?php

namespace App\Http\Middleware;
use App\Presenters\TrackLoginPresenter;
use Closure;

class LoginTimeMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */

    public function handleTimeLogin()
    {
        $trackLogin = new TrackLoginPresenter();
        return $trackLogin = $trackLogin->handleExpire();
    }

    public function handle($request, Closure $next)
    {
        if ($this->handleTimeLogin() == 'expired') {
            return redirect(urlLang('user/logout'));
        }

        return $next($request);
    }
}
