<?php

// Admin Routes

Route::group(['middleware' => ['web']], function () {

    Route::get('/', [
            'uses' => 'BaseController@getIndex',
            'as' => 'base.index'
    ]);

    Route::controller('login', 'Backend\LoginController');

    Route::get('admin-cp', function () {
        return redirect('login');
    });

    if (request()->segment(1) == webarq()->backendUrl) {
        include __DIR__ . '/../backendRoutes.php';
    }

});
