<?php

Route::get('test-email',function(){
    \Mail::send('tes_email',[], function ($m) {
        $m->from(env('MAIL_FROM'), 'Your Application');
        $m->to("chrisna@miraeasset.co.id")->subject('Test email Mirae');
        // $m->to("yoshuaadi@hotmail.com")->subject('Test email Mirae');
    });
});

Route::controller('test','TestController');

Route::get('mase/{file}','FileController@index');


Route::get('ora',function(){
    try
    {
        $model = new \App\Models\Oracle\VCustInfo();
        $sql = $model->limit(3)->get();

        foreach($sql as $row)
        {
            echo $row->acnt_no." - ".$row->acnt_nm."<br/>";
        }
        echo "Connection Success";

    }catch(\Exception $e){
        echo "Connection Gagal : ".$e->getMessage();
    }

});
