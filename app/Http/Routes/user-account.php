<?php
/**
 * Created by PhpStorm.
 * User: DanielSimangunsong
 * Date: 5/16/2017
 * Time: 10:43 AM
 */

Route::group(['middleware' =>['web'], 'prefix' => '{lang}'], function () {
    App::setLocale(lang());

    Route::controller('user/question','Users\QuestionController');

    Route::group(['middleware'=>'login-time'] , function(){
        Route::controller('user/dashboard', 'Users\DashboardController');

        Route::controller('user/portfolio', 'Users\PortfolioController');

        Route::controller('user/order', 'Users\OrderController');

        Route::controller('user/transaction', 'Users\TransactionController');

        Route::controller('user/debit', 'Users\DebitController');
        
        Route::controller('user/mileage', 'Users\MileageController');
        
    });

    Route::post('user/login', [
            'uses' => 'UserController@postLogin',
            'as' => 'user.login'
    ]);

    Route::controller('user', 'UserController');
});
