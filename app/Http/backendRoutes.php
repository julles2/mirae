<?php
	Route::group(['prefix' => webarq()->backendUrl , 'middleware' => ['auth','right']] , function(){

		if(\Schema::hasTable('menus'))
		{
			if(\Cache::has('queryRoute'))
			{
				$queryRoute = \Cache::get('queryRoute');
			}else{
				$queryRoute = injectModel('Menu')->where('controller','!=','#')->get();
				\Cache::put('queryRoute',$queryRoute,5);
			}
			foreach($queryRoute as $row)
			{	
				$path = app_path('Http/Controllers/Backend/'.$row->controller.'.php');
				Route::controller('mutual-fund','Backend\MutualFundController');
				if(file_exists($path))
				{
					Route::controller($row->slug,'Backend\\'.$row->controller);
				}
					
			}

		}
	});

?>