<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Models\Company;
use App\Models\CompanyTranslations;

use DB;
use Table;

class AboutUsController extends Controller
{
    public function __construct()
    {
        \App::setLocale(lang());

        $this->company = new Company();
        $this->companyTranslations = new CompanyTranslations();
    }

    public function dataCompany()
    {
        $about = $this->company->select('company_translations.title', 'company_translations.description', 'company.id')
            ->join('company_translations', 'company_translations.company_id', '=', 'company.id')
            ->where('company_translations.lang', lang())
            ->where('company.status', 'y')
            ->where('category', 'aboutus');

        return $about;
    }

    public function getIndex()
    {
        $about = $this->dataCompany()->first();

        $setting = menu_setting('aboutus', lang())->first();

        return view('aboutus.index', compact('about', 'setting'));
    }
}
