<?php

namespace App\Http\Controllers;

use App\Http\Controllers\BaseController;
use App\Models\FundInfo;
use App\Models\LogEmail;
use App\Models\Oracle\ClientInfo;
use App\Models\Oracle\DailyNav;
use App\Models\Oracle\Portofolio;
use App\Models\Oracle\Transaction;
use App\Models\Oracle\TransactionAutoDebet;
use App\Models\Oracle\TransactionSwitching;
use App\Models\Oracle\VCashBal;
use App\Models\TransactionTemporary;
use Cart;
use Illuminate\Http\Request;
use Mail;
use App\Presenters\CartPresenter;

class CheckoutController extends BaseController
{
    use CartPresenter;

    public $model;

    public $path;

    public function __construct(FundInfo $model)
    {
        $this->middleware('checkout', ['except'=>'getDeleteCart']);
        parent::__construct();
        $this->model                = $model;
        $this->path                 = 'checkout.';
        $this->vCashBal             = new VCashBal();
        $this->user                 = auth()->guard('broker')->user();
        $this->clientInfo           = new ClientInfo();
        $this->transaction          = new Transaction();
        $this->transactionAutoDebet = new TransactionAutoDebet();
        $this->transactionTemporary = new TransactionTemporary();
        $this->date                 = date("Ymd");
        $this->time                 = date("His");
        $this->timeOracle           = substr(date("Hisms"), 0, -2);
        $this->dailyNav             = new DailyNav();
        $this->transSwt             = new TransactionSwitching();
        $this->portofolio           = new Portofolio();
    }

    public function getIndex()
    {
        $carts = Cart::content();
        $fund = function ($id, $field = "") {
            if (empty($field)) {
                $model = $this->model->find($id);
            } else {
                $model = $this->model->where($field, $id)->first();
            }
            return $model;
        };

        $balance = $this->vCashBal->balance();

        $checked = function ($type, $arg) {
            return $type == $arg ? 'checked=""' : 'disabled';
        };

        $periode = [];

        for ($i = 1; $i <= 3; $i++) {
            $periode[$i] = $i . " Tahun";
        }

        $user = $this->clientInfo->where('acnt_no', acnt_no())->first();

        return view($this->path . 'index', [
            'carts'   => $carts,
            'fund'    => $fund,
            'model'   => $this->model,
            'balance' => $balance,
            'userId'  => $this->user->accountData->accountNum,
            'checked' => $checked,
            'periode' => $periode,
            'user' => $user,
        ]);
    }

    public function getValidatePembelian()
    {
        $nilai         = request()->get('nilai');
        $fundId        = request()->get('fund_id');
        $counter_nilai = request()->get('counter_nilai');
        $nilai         = formatInteger($nilai);
        $cekMinimum    = $this->model->findOrFail($fundId);
        $balance       = $this->vCashBal->getBalance(acnt_no());
        if (formatInteger($nilai) > $balance) {
            $result = 'false_cash';
        } else {
            $minimal = $this->portofolio->investment_or_increment($cekMinimum->fund_cd);

            if (($nilai - $this->model->fee($cekMinimum, $nilai)) < $cekMinimum->$minimal) {
                dd($nilai - $this->model->fee($cekMinimum, $nilai));
                $result = 'false';
            } else {
                $result = "true";
            }
        }

        return response()->json([
            'result'        => $result,
            'model'         => $cekMinimum,
            'counter_nilai' => $counter_nilai,
        ]);
    }

    public function pembelianSekali($modelFund, $client, $bill, $feeType, $userId, $params = [])
    {
        $im         = $modelFund->im;
        $feeType    = 0;
        $dataOracle = [
            'tr_date'    => $this->date,
            'acnt_no'    => $userId,
            'ifua_cd'    => $client->ifua_cd,
            'fund_cd'    => $modelFund->fund_cd,
            'ref_no'     => $this->transaction->makeRefNo(),
            'proc_cd'    => '10',
            'tr_type'    => 'S',
            'im_cd'      => $im->im_cd,
            'ord_type'   => '1',
            'amount'     => $bill,
            'fee_type'   => $feeType,
            'fee'        => 0,
            //'fee'=>!empty($modelFund->fee_subs) ? $modelFund->fee_subs : 0,
            'source'     => '1',
            'input_user' => $this->user->user_id,
            'proc_time'  => $this->timeOracle,
        ];

        \DB::connection('oracle')->table('F0T200P')->insert($dataOracle);
    }

    public function pembelianAutoDebet($modelFund, $client, $bill, $feeType, $userId, $data = [])
    {
        $ref_no = $this->transactionAutoDebet->makeRefNo();

        $im         = $modelFund->im;
        $feeType    = 0;
        $dataOracle = [
            'tr_date'    => $this->date,
            'acnt_no'    => $userId,
            'ifua_cd'    => $client->ifua_cd,
            'ref_no'     => $ref_no,
            'fund_cd'    => $modelFund->fund_cd,
            'recur_date' => $data['recur_date'],
            'proc_cd'    => 0,
            'im_cd'      => $im->im_cd,
            'ord_amt'    => $bill,
            'fee_type'   => $feeType,
            'fee'        => 0,
            'cncl_sect'  => 0,
            'from_date'  => $data['from_date'],
            'to_date'    => $data['to_date'],
            'input_user' => $this->user->user_id,
            'proc_time'  => $this->timeOracle,
            'ord_date'   => null,
        ];
        $cek = $this->transactionAutoDebet->where('tr_date', $this->date)->where('acnt_no', $userId)
            ->where('ifua_cd', $client->ifua_cd)->where('recur_date', $data['recur_date'])->first();
        if (!empty($cek->tr_date)) {
            $cek->update($dataOracle);
        } else {
            \DB::connection('oracle')->table('F0T202P')->insert($dataOracle);
        }
    }

    public function saveToPembelian($request)
    {
        $count  = count($request->fund_cd);
        $userId = $this->user->accountData->accountNum;
        $client = $this->clientInfo->where('ACNT_NO', $userId)->first();
        $tr     = "?token=" . makeCode($this->user->user_id);

        $transId = [];
        $tglLoop = 0;
        for ($a = 0; $a < $count; $a++) {
            $tglLoop++;
            $bill           = formatInteger($request->nilai[$a]);
            $modelFund      = $this->model->where('fund_cd', $request->fund_cd[$a])->first();
            $feeType        = $this->model->percentageOrAmount($modelFund, 'fee_subs');
            $totalFee       = $this->model->fee($modelFund, $bill);
            $type           = $request->type[$a] == 'subscription' ? 's' : 'sa';
            $lastNav        = $this->dailyNav->nabLast($modelFund->fund_cd);
            $totalInvestasi = $bill - $totalFee;
            $unit           = $totalInvestasi > 0 ? (round($totalInvestasi / $lastNav)) : 0;

            $risk_profile_request = "risk_profile".$modelFund->id;

            $risk_profile = null;

            if (!empty($request->{$risk_profile_request})) {
                $risk_profile = json_encode($request->{$risk_profile_request});
            }

            $dataTemporary = [
                'tr_date'   => $this->date,
                'tr_time'   => $this->time,
                'acnt_no'   => $userId,
                'ifua_cd'   => $client->ifua_cd,
                'fund_cd'   => $request->fund_cd[$a],
                'ref_no'    => $type == 's' ? $this->transaction->makeRefNo() : $this->transactionAutoDebet->makeRefNo(),
                'bill'      => $bill,
                'type'      => $type,
                'fee'       => !empty($modelFund->fee_subs) ? $modelFund->fee_subs : 0,
                'total_fee' => $totalFee,
                'last_nav'  => $lastNav,
                'unit'      => valueDecimal($unit, 4),
                'source'    => '1',
                'risk'  => $risk_profile,
            ];

            $modelTemporary = $this->transactionTemporary->create($dataTemporary);

            $transId[] = $modelTemporary->id;

            if ($type == 's') {
                $params = [];
                $this->pembelianSekali($modelFund, $client, $bill, $feeType, $userId, $params);
            } else {
                $requestTanggal = date("Y-m-") . $request->tanggal[$a];
                $tanggalParse   = carbon()->parse($requestTanggal);
                $recurDate      = $tanggalParse->format("d");
                $fromDate       = $tanggalParse->format("Ymd");
                $toDate         = $tanggalParse->addYears($request->tahun[$a])->format("Ymd");
                $params         = [
                    'recur_date' => $recurDate,
                    'from_date'  => $fromDate,
                    'to_date'    => $toDate,
                ];
                $modelTemporary->update(['periode_debet' => $fromDate . '-' . $toDate]);
                $updateTemporary = $this->transactionTemporary->find($modelTemporary->id)->update([
                    'recur_date' => $recurDate,
                ]);

                $autoDebet = $this->pembelianAutoDebet($modelFund, $client, $bill, $feeType, $userId, $params);
            }
        }

        request()->session()->remove('trans');

        request()->session()->push('trans', collect($transId));

        return $tr;
    }

    public function penjualanUnit()
    {
    }

    public function saveToPenjualan($request)
    {
        $count   = count($request->penjualan_fund_cd);
        $userId  = $this->user->accountData->accountNum;
        $client  = $this->clientInfo->where('ACNT_NO', $userId)->first();
        $tr      = "";
        $inputs  = $request->all();
        $transId = [];
        for ($a = 0; $a < $count; $a++) {
            $b         = $a + 1;
            $modelFund = $this->model->where('fund_cd', $request->penjualan_fund_cd[$a])->first();
            $im        = $modelFund->im;
            $bill      = $request->penjualan_amount[$a];
            $unit      = $request->penjualan_nilai[$a];

            $allUnit = ($request->penjualan_box_all[$a]) == 'all' ? 1 : 2;
            // dd($request->penjualan_box_all[$a]);
            $feeType = $this->model->percentageOrAmount($modelFund, 'fee_redm');

            $fee = !empty($modelFund->fee_redm) ? $modelFund->fee_redm : 0;

            $lastNav = $this->dailyNav->nabLast($modelFund->fund_cd);

            $feeBefore = function () use ($fee, $modelFund, $lastNav) {
                $cekPersent = \Mirae::percentageOrAmount($fee);
                if ($cekPersent == 1) { // persent
                    $result = $fee / 100 * $lastNav;
                } else {
                    $result = $fee;
                }

                return $result;
            };

            $feeBefore = $feeBefore();

            $totalFee = $feeBefore * $request->penjualan_nilai[$a];

            $dataTemporary = [
                'tr_date'   => $this->date,
                'tr_time'   => $this->time,
                'acnt_no'   => $userId,
                'ifua_cd'   => $client->ifua_cd,
                'fund_cd'   => $request->penjualan_fund_cd[$a],
                'ref_no'    => $this->transaction->makeRefNo(),
                'bill'      => formatInteger($bill),
                'type'      => 'r',
                'fee'       => $fee,
                'total_fee' => $totalFee,
                'source'    => '1',
                'unit'      => valueDecimal($unit, 4),
                'last_nav'  => $lastNav,

            ];

            $modelTemporary = $this->transactionTemporary->create($dataTemporary);

            $dataOracle = [
                'tr_date'    => $this->date,
                'acnt_no'    => $userId,
                'ifua_cd'    => $client->ifua_cd,
                'fund_cd'    => $modelFund->fund_cd,
                'ref_no'     => $this->transaction->makeRefNo(),
                'proc_cd'    => '10',
                'tr_type'    => 'R',
                'im_cd'      => $im->im_cd,
                'ord_type'   => 2,
                //'amount'=>formatInteger($bill),
                'amount'     => null,
                'unit'       => valueDecimal($unit, 4),
                'all_unit'   => formatInteger($allUnit),
                'fee_type'   => null,
                //'fee'=>$fee,
                'source'     => '1',
                'input_user' => $this->user->user_id,
                'proc_time'  => $this->timeOracle,
            ];

            \DB::connection('oracle')->table('F0T200P')->insert($dataOracle);

            $transId[] = $modelTemporary->id;
        }

        request()->session()->push('trans', collect($transId));

        return $tr;
    }

    public function saveToPengalihan($request)
    {
        $count   = count($request->from_fund);
        $userId  = $this->user->accountData->accountNum;
        $client  = $this->clientInfo->where('ACNT_NO', $userId)->first();
        $tr      = "";
        $inputs  = $request->all();
        $transId = [];

        for ($a = 0; $a < $count; $a++) {
            $b                = $a + 1;
            $requestOrderType = $inputs['order_type' . $b];
            $fromFund         = $this->model->where('fund_cd', $request->from_fund[$a])->first();
            $toFund           = $this->model->where('fund_cd', $request->to_fund[$a])->first();
            $im               = $fromFund->im;
            $toIm             = $toFund->im;

            $orderType = $this->transaction->convertOrderType($requestOrderType);
            $bill      = formatInteger($request->pengalihan_amount[$a]);
            $unit      = $request->pengalihan_nilai[$a];
            $allUnit   = $request->pengalihan_box_all[$a] == 'all' ? 1 : 2;

            $feeType = $this->model->percentageOrAmount($fromFund, 'fee_swtc');

            $fee = !empty($fromFund->fee_swtc) ? $fromFund->fee_swtc : 0;

            $lastNav = $this->dailyNav->nabLast($fromFund->fund_cd);

            $feeBefore = function () use ($fee, $lastNav) {
                $cekPersent = \Mirae::percentageOrAmount($fee);
                if ($cekPersent == 1) { // persent
                    $result = $fee / 100 * $lastNav;
                } else {
                    $result = $fee;
                }

                return $result;
            };

            $feeBefore = $feeBefore();

            $totalFee = $feeBefore * $request->pengalihan_nilai[$a];

            $dataTemporary = [
                'tr_date'   => $this->date,
                'tr_time'   => $this->time,
                'acnt_no'   => $userId,
                'ifua_cd'   => $client->ifua_cd,
                'fund_cd'   => $request->from_fund[$a],
                'to_fund'   => $request->to_fund[$a],
                'ref_no'    => $this->transSwt->makeRefNo(),
                'bill'      => formatInteger($bill),
                'type'      => 'sw',
                'source'    => '1',
                'last_nav'  => $lastNav,
                'unit'      => valueDecimal($unit, 4),
                'fee'       => $fee,
                'total_fee' => $totalFee,

            ];

            $modelTemporary = $this->transactionTemporary->create($dataTemporary);
            $outNavDate     = $this->dailyNav->lastDayByFundCd($fromFund->fund_cd);
            //$outNabLast = $this->dailyNav->nabLast($fromFund->fund_cd);

            $inNavDate = $this->dailyNav->lastDayByFundCd($toFund->fund_cd);
            //$inNabLast = $this->dailyNav->nabLast($toFund->fund_cd);
            //dd('ON PROGRESS');
            $dataOracle = [
                'tr_date'      => $this->date,
                'acnt_no'      => $userId,
                'ifua_cd'      => $client->ifua_cd,
                'out_fund_cd'  => $fromFund->fund_cd,
                'ref_no'       => $this->transSwt->makeRefNo(),
                'proc_cd'      => 10,
                'out_im_cd'    => $im->im_cd,
                // 'out_nav_date'=>$outNavDate,
                //'out_ord_nav'=>$outNabLast,
                'out_ord_type' => $orderType,
                'out_ord_amt'  => null,
                //'out_ord_amt'=>formatInteger($bill),
                'out_ord_unit' => valueDecimal($unit, 4),
                'out_all_unit' => $allUnit,
                'in_fund_cd'   => $toFund->fund_cd,
                'in_im_cd'     => $toIm->im_cd,
                // 'in_nav_date'=>$inNavDate,
                //'in_ord_nav'=>$inNabLast,
                //'in_ord_type'=>$client->ifua_cd,
                //'in_ord_amt'=>$client->ifua_cd,
                //'in_ord_unit'=>$client->ifua_cd,
                'fee_type'     => null,
                //'fee'=>$client->ifua_cd,
                //'in_fund_fee'=>$client->ifua_cd,
                'source'       => 1,
                'input_user'   => $this->user->user_id,
                //'abstr_cd'=>null,
                'proc_time'    => $this->timeOracle,

            ];

            \DB::connection('oracle')->table('F0T201P')->insert($dataOracle);

            $transId[] = $modelTemporary->id;
        }

        request()->session()->push('trans', collect($transId));

        return $tr;
    }

    public function clear_cache()
    {
        header("Cache-Control: no-cache, must-revalidate");
        header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
        header("Content-Type: application/xml; charset=utf-8");
    }

    public function postIndex(Request $request, $lang)
    {
        if (empty($this->user->accountData->accountNum)) {
            return redirect(urlLang('user/pin-login?to=checkout'));
        }

        $pembelian  = $this->saveToPembelian($request);
        $penjualan  = $this->saveToPenjualan($request);
        $pengalihan = $this->saveToPengalihan($request);

        $tr = $pembelian;

        $tr .= $penjualan;

        $tr .= $pengalihan;

        Cart::destroy();
        $this->clear_cache();
        $sessionTrans = request()->session()->get('trans');

        $transIds = array_flatten($sessionTrans);

        $model    = TransactionTemporary::whereIn('id', $transIds)->get();
        $mailFrom = env('MAIL_FROM');
        try {
            foreach ($model as $row) {
                $user   = $row->user;
                $params = [
                    'model' => $row,
                    'user'  => $user,
                ];

                if ($row->type == 's') {
                    \Mail::send('frontend.users.emails.subscription', $params, function ($m) use ($user, $row, $mailFrom) {
                        $m->from($mailFrom, 'Subscription - ' . $row->ref_no);
                        $m->to(trim($user->email))->subject('Subscription Mirae Asset - ' . $row->ref_no);
                    });
                } elseif ($row->type == 'sa') {
                    \Mail::send('frontend.users.emails.subscription_auto_debet', $params, function ($m) use ($user, $row, $mailFrom) {
                        $m->from($mailFrom, 'Subscription Auto Debet- ' . $row->ref_no);
                        $m->to(trim($user->email))->subject('Subscription Auto Debet Mirae Asset - ' . $row->ref_no);
                    });
                } elseif ($row->type == 'r') {
                    \Mail::send('frontend.users.emails.redeem', $params, function ($m) use ($user, $row, $mailFrom) {
                        $m->from($mailFrom, 'Redeem - ' . $row->ref_no);
                        $m->to(trim($user->email))->subject('Redeem Mirae Asset - ' . $row->ref_no);
                    });
                } elseif ($row->type == 'sw') {
                    \Mail::send('frontend.users.emails.swc', $params, function ($m) use ($user, $row) {
                        $m->from(env('MAIL_FROM'), 'Switching - ' . $row->ref_no);
                        $m->to(trim($user->email))->subject('Switching Mirae Asset - ' . $row->ref_no);
                    });
                }
            }
            $logStatus  = 'success';
            $logMessage = "Email Telah berhasil di kirim";
        } catch (\Exception $e) {
            $logStatus  = 'failed';
            $logMessage = "Email Gagal dikirim , Error Message : " . $e->getMessage();
        }

        $dataLog = [
            'transaction_id'   => $row->ref_no,
            'transaction_type' => $row->type,
            'message'          => $logMessage,
            'from'             => $mailFrom,
            'to'               => $user->email,
            'status'           => $logStatus,
        ];
        LogEmail::create($dataLog);
        Cart::destroy();
        return redirect(urlLang('checkout/thanks' . $tr));
    }

    public function getThanks($lang)
    {
        $token  = request()->get('token');
        $userId = $this->user->user_id;

        if (makeCode($userId) != $token) {
            abort(404);
        }

        $sessionTrans = request()->session()->get('trans');

        $transIds = array_flatten($sessionTrans);

        $model = TransactionTemporary::whereIn('id', $transIds)->get();

        return view($this->path . 'thank', [
            'model'                => $model,
            'transaction'          => new Transaction(),
            'transactionAutoDebet' => new TransactionAutoDebet(),
            'fund'                 => $this->model,
            'dailyNav'             => $this->dailyNav,
            'userId'               => $userId = $this->user->accountData->accountNum,
            'transactionSwt'       => $this->transSwt,
        ]);
    }

    public function getCekCashBalance()
    {
        $userId      = request()->get('userId');
        $totalNilai  = request()->get('total_nilai');
        $cashBalance = $this->vCashBal->balance(acnt_no());

        $result = ($totalNilai <= $cashBalance) ? 'true' : 'false';

        return response()->json([
            'result' => $result,
        ]);
    }

    public function getDeleteCart($lang, $rowId)
    {
        Cart::remove($rowId);
        $this->cartStoreToDb();
        return redirect()->back();
    }

    public function getCancel()
    {
        Cart::destroy();

        return redirect()->back();
    }
}
