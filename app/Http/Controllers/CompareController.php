<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Repository\Listing;
use App\Repository\Compare;
use Cache;

class CompareController extends Controller
{

	public function __construct()
	{
		$this->compare = new Compare();

		$this->view = 'compare.';

		$this->list = new Listing();

        view()->share('fieldShare',$this->compare->defaultField);
        view()->share('monthShare',$this->compare->month);
        view()->share('compareFunds',$this->compare->data);
        //dd(Cache::get('compare_funds'.request()->ip()));
    }

    public function postCompareFund(Request $request)
    {
        $funds = $request->fund_compare;
    	$funds = array_where($funds, function ($key, $value) {
		    return !empty($value);
		});

    	$var = "?";
    	$no=0;
    	foreach($funds as $f)
    	{
            $f = trim($f);
    		$no++;
    		$var.="fund$no=".$f."&";
    	}

        $var.="m=1&f=nav";

    	return redirect(urlLang('compare/compare-fund'.$var));
    }

	public function getAjaxChangeReksaDana()
	{
		$imId = get('im_id');
		$type = get('type');
		$query = $this->list->selectFund($imId,$type);
		$option = "";
		foreach($query as $key => $val)
		{
			$option.="<option value='$key'>$val</option>";
		}

		return response()->json([
			'result'=>$option,
		]);
	}

    public function getAjaxRangeFund()
    {
        $from = mirae_date_oracle(get('from'));
        $to = mirae_date_oracle(get('to'));
        $queryString = get('queryString');
        $result = changeQueryString(['m'=>$from.'-'.$to],$queryString);
        return response()->json([
            'result'=>$result,
        ]);
    }

    public function postStoreFund(Request $request)
    {
        $param = 'compare_funds'. request()->ip();
        Cache::pull($param);
        $value = Cache::rememberForever($param, function() use ($request,$param) {
            return [
                'funds'=>!empty($request->fund_compare) ? $request->fund_compare : $request->fund_cd,
                'fund_field'=>!empty($request->fund_field) ? $request->fund_field : 'nav',
                'fund_month'=>!empty($request->fund_month) ? $request->fund_month : 12,
            ];
        });

        return redirect(urlLang("compare/compare-fund"));
    }

    public function getAjaxChart()
    {
        $dates = json_encode($this->compare->dates());
        $data = json_encode($this->compare->data($this->compare->handleFundCodeCache()));
        $interval = round(count($this->compare->dates()) / 2);

        return response()->json([
            'dates'=>$dates,
            'data'=>$data,
            'interval'=>$interval,
        ]);
        
    }

	public function getCompareFund()
    {
        $dateTextBox = $this->compare->dateTextBox();
        $funds = $this->compare->fundByCode(array_flatten($this->compare->handleFundCodeCache()));
        $dates = json_encode($this->compare->dates());
        // $data = $this->compare->data($this->compare->handleFundCodeCache());

        // $interval = round(count($this->compare->dates()) / 2);
        $setting = menu_setting('produk-compare',lang())->first();

        return view($this->view.'compare_fund',[
			'funds'=>$funds,
            // 'dates'=>$dates,
            // 'data'=>json_encode($data),
            // 'interval'=>$interval,
            'periode'=>$this->compare->periode,
			'selectedIm'=>$this->compare->handleSelectedIm(),
            'setting' => $setting,
            'dateTextBox' => $dateTextBox,
            'filterTypes'=>$this->compare->filterTypes(),
		]);
	}

    
}
