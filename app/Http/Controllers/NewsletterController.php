<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Models\Newsletters;
use Illuminate\Support\Facades\Validator;

class NewsletterController extends Controller
{
    public function __construct()
    {
        $this->subscribe = new Newsletters();
    }

    public function getNewsletter()
    {
        $request = request();

    	$email = $request->email;

		$newsletter = $this->subscribe;

    	$validator = Validator::make($request->all(), [
		    'email' => 'email|required|unique:newsletters|max:50|',
		]);

		if ($validator->fails()) {

			$errors = $validator->errors();

			$kata = trans('home.newsletter');
            return response()->json([
	    		'status'=>'failed',
	            'success'=> "email sudah terdaftar",
	        ]);

		}else{
			$newsletter->email = $email;

			$newsletter->save();

			$kata = trans('home.newslettersuccess');

	    	return response()->json([
	    		'status'=>'success',
	            'success'=> $kata,
	        ]);
		}
    }
}
