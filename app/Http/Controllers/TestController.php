<?php

namespace App\Http\Controllers;

use App\Models\Feed\FundAum;
use App\Models\FundInfo as Fund;
use Mirae;
use Mirae\Console\FundPerformancePop;
use Jenssegers\Agent\Agent;
use App\Presenters\TrackLoginPresenter;

class TestController extends Controller
{
    public function __construct()
    {
        $this->console = new FundPerformancePop();
        $this->track = new TrackLoginPresenter('11');
    }

    public function getIndex()
    {
        dd(strlen('Aksi Beli Investor Asing Dorong IHSG ke Zona Hijau'));
        // IIM02FISIHAJJS00
        $result = [];
        foreach (Fund::all() as $fund) {
            if ($fund->fund_cd == 'IIM02FISIHAJJS00') {
                $dailyNav      = $fund->daily_nav;
                $date          = $dailyNav->tr_date;
                $feedAum = new FundAum();
                $tes           = $dailyNav->one_month;
                dd(Mirae::oneMonthAgo($date));
                dd($feedAum->manipulateAum($fund->fund_cd, Mirae::oneMonthAgo($date), $date));
                $result[] = [
                    // 'id'               => $no,
                    'fund_id'          => $fund->id,

                    'one_month'        => $dailyNav->one_month,
                    'three_months'     => $dailyNav->three_months,
                    'six_months'       => $dailyNav->six_months,
                    'one_year'         => $dailyNav->one_year,
                    'three_year'       => $dailyNav->three_year,
                    'ytd'              => $dailyNav->ytd,

                    'aum_one_month'    => $feedAum->manipulateAum($fund->fund_cd, Mirae::oneMonthAgo($date), $date),
                    'aum_three_months' => $feedAum->manipulateAum($fund->fund_cd, Mirae::threeMonthsAgo($date), $date),
                    'aum_six_months'   => $feedAum->manipulateAum($fund->fund_cd, Mirae::sixMonthsAgo($date), $date),
                    'aum_one_year'     => $feedAum->manipulateAum($fund->fund_cd, Mirae::yearsAgo($date, 1), $date),
                    'aum_three_year'   => $feedAum->manipulateAum($fund->fund_cd, Mirae::yearsAgo($date, 3), $date),
                    'aum_ytd'          => $feedAum->manipulateAum($fund->fund_cd, Mirae::desemberYearsAgo($date), $date),

                    'ou_one_month'     => $feedAum->manipulateUnit($fund->fund_cd, Mirae::oneMonthAgo($date), $date),
                    'ou_three_months'  => $feedAum->manipulateUnit($fund->fund_cd, Mirae::threeMonthsAgo($date), $date),
                    'ou_six_months'    => $feedAum->manipulateUnit($fund->fund_cd, Mirae::sixMonthsAgo($date), $date),
                    'ou_one_year'      => $feedAum->manipulateUnit($fund->fund_cd, Mirae::yearsAgo($date, 1), $date),
                    'ou_three_year'    => $feedAum->manipulateUnit($fund->fund_cd, Mirae::yearsAgo($date, 3), $date),
                    'ou_ytd'           => $feedAum->manipulateUnit($fund->fund_cd, Mirae::desemberYearsAgo($date), $date),
                    'tr_date'          => $date,
                ];
            }

        }
        dd($result);
    }

    public function getLogin()
    {
        // $agent = new Agent();
        // $model = \Cache::put('dupag',acnt_no().'_'.$agent->browser().'_'.request()->ip(),1);
        // dd($this->track->store_cache());
    }

    public function getCache()
    {
        dd(\Cache::get('online_016018'));
    }

    public function getStoreCookie()
    {
        \Cookie::queue('dupager', 'rezas', 1);
    }

    public function getCookie()
    {
       $c = \Cookie::get('dupager');
       dd($c);
    }

    public function getDestroyCookie()
    {
        \Cookie::queue(\Cookie::forget('dupager'));
    }

    public function getCart()
    {
        dd(\Cart::content());
    }

    public function getCart2()
    {
        $model = \App\Models\TransactionTemporary::where('acnt_no','058753')->orderby('created_at','desc')->get();

        dd($model);
    }

    public function getDestroyCart()
    {
        \Cart::destroy();
    }

    public function fund_aum()
    {

    }

    public function getSendEmail()
    {
         \Mail::send('tes_email', [], function ($m){
            $m->from('hello@app.com', 'Test Email From website 1');

            $m->to('reza.wikrama3@gmail.com', 'nuniekmaudya@gmail.com')->subject('Test Email From website');
        });
    }

    public function getStr($lang,$slug)
    {
        return strtolower(str_replace(" ", "_", $slug));
    }

    public function getDate()
    {
        echo carbon_lang("1994-02-11");
    }

    public function getTime()
    {
        $trackLogin = new TrackLoginPresenter();
        dd($trackLogin->handleExpire());
        // dd($login_time);
        // dd($cek);
    }

    public function getCompareTime()
    {
        $now = parse("2018-04-04 14:12:31");
        $expire = parse("2018-04-04 14:17:31");
        dd($expire->diffInMinutes($now));
    }
}
