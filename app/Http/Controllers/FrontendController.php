<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class FrontendController extends Controller
{
	public $view;

    public function __construct()
    {
    	$this->view = '';
    }

    public function makeView($file,$data=[])
    {
    	return view($this->view.$file,$data);
    }
}
