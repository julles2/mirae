<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Models\Bantuan;
use App\Models\BantuanTranslation;

use Illuminate\Pagination\BootstrapThreePresenter;

use DB;
use Table;

class GlossaryController extends Controller
{
	public function __construct()
	{
		\App::setLocale(lang());

	    $this->bantuan = new Bantuan();
	    $this->bantuanTranslations = new BantuanTranslation();
	}

    public function dataIndex($lang, $sort)
    {
        $help = $this->bantuan->select('bantuans.id',
                                        'bantuans.order',
                                        'bantuans.status',
                                        'bantuan_translations.lang',
                                        'bantuan_translations.title',
                                        'bantuan_translations.description'
                                        )
            ->join('bantuan_translations','bantuan_translations.bantuan_id','=','bantuans.id')
            ->where('bantuan_translations.lang',lang())
            ->where('bantuans.status','y')
            ->where('bantuans.menu','glossary');

            if (!empty($sort)) {
                $help->where('title','like',$sort.'%');
            }

            $help = $help->paginate(9)->appends(['sort'=>$sort]);

        return $help;
    }

    public function getIndex($lang,$sort=null)
    {
        $sort = empty(get('sort')) ? 'A' : get('sort');

        $help = $this->dataIndex($lang, $sort);

        $setting = menu_setting('glossary',lang())->first();

        return view('help.glossary.index',compact('help','setting','sort'));
    }
}
