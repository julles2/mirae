<?php

namespace App\Http\Controllers;

use App\Http\Controllers\BaseController;
use App\Models\News;
use App\Models\NewsCategories;
use App\Models\NewsCategoryTranslations;
use App\Models\NewsTranslation;

class NewsController extends BaseController
{
    public function __construct()
    {
        \App::setLocale(lang());
        parent::__construct();
        $this->news                 = new News();
        $this->newsTranslations     = new NewsTranslation();
        $this->category             = new NewsCategories;
        $this->categoryTranslations = new NewsCategoryTranslations;
    }

    public function dataNews($category = "", $sort = "", $slug = "")
    {
        $news = $this->news->select('news.id',
            'news_categories.color',
            'news.date',
            'news.image_intro',
            'news.image_detail',
            'news_category_translations.category',
            'news_translations.lang',
            'news_translations.title',
            'news_translations.intro',
            'news_translations.slug',
            'news_translations.description'
        )
            ->join('news_translations', 'news_translations.news_id', '=', 'news.id')
            ->join('news_category_translations', 'news_category_translations.category_id', '=', 'news.category_id')
            ->join('news_categories', 'news_categories.id', '=', 'news_category_translations.category_id')
            ->where('news_translations.lang', lang())
            ->where('news.status', 'y')
            ->where('news_category_translations.lang', lang());

        if (!empty($category)) {
            $news = $news->where('news.category_id', $category);
        }

        if (!empty($sort)) {
            $news = $news->orderBy('news.date', $sort);
        } else {
            $news = $news->orderBy('news.date', 'desc');
        }

        if (!empty($slug)) {
            $news = $news->where('news_translations.slug', $slug);
        }

        return $news;
    }

    public function getIndex($lang, $category = null, $sort = null)
    {
        $pinned               = $this->dataNews()->wherePinned('y')->get();
        $new                  = $this->dataNews(get('category'), get('sort'), null)->where('pinned', '=', 'n')->paginate(9);
        $categoryTranslations = ['' => trans('news.all')] + $this->categoryTranslations
            ->where('lang', lang())
            ->lists('category', 'category_id')
            ->toArray();

        $setting = menu_setting('news', lang())->first();
        $char = function($words){
            return strlen($words) > 36 ? substr($words,0,36).'...' : $words;
        };
        return view('news.index', compact('new', 'categoryTranslations', 'setting', 'pinned','char'));
    }

    public function getRead($lang, $slug)
    {
        $details  = $this->dataNews(null, null, $slug)->first();
        $newsMore = $this->dataNews(null, null, null)->limit(3)->get();

        return view('news.detail', compact('details', 'newsMore'));
    }

}
