<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Models\Bantuan;
use App\Models\BantuanTranslation;

use Illuminate\Pagination\BootstrapThreePresenter;

use DB;
use Table;

class MutualfundController extends Controller
{
	public function __construct()
	{
		\App::setLocale(lang());

	    $this->bantuan = new Bantuan();
	    $this->bantuanTranslations = new BantuanTranslation();
	}

    public function dataIndex($lang)
    {
        $help = $this->bantuan->select('bantuans.id',
                                        'bantuans.order',
                                        'bantuans.status',
                                        'bantuan_translations.lang',
                                        'bantuan_translations.title',
                                        'bantuan_translations.description'
                                        )
            ->join('bantuan_translations','bantuan_translations.bantuan_id','=','bantuans.id')
            ->where('bantuan_translations.lang',lang())
            ->where('bantuans.status','y')
            ->where('bantuans.menu','mutual_fund')
            ->first();

        return $help;
    }

    public function getIndex($lang)
    {
        $help = $this->dataIndex($lang);

        $setting = menu_setting('mutual_fun',lang())->first();

        return view('help.mutual-fund.index',compact('help','setting'));
    }
}
