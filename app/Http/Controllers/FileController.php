<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class FileController extends Controller
{
    public function index($file)
    {
        return response()->file(public_path('contents/pdf/'.$file));
    }
}
