<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Backend\WebarqController;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Company;
use App\Models\CompanyTranslations;
use App\Models\MenuSettings;
use Table;
use webarq;
use DB;


class AboutUsController extends WebarqController
{
    public function __construct(Company $model, CompanyTranslations $companyTranslations, MenuSettings $menuSettings)
    {
    	parent::__construct();
        $this->model = $model;
        $this->companyTranslations = $companyTranslations;
        $this->menuSettings = $menuSettings;
        $this->path = 'backend.aboutus.';
    }

    public function getData()
    {
    	$model = $this->model->select('company.id',
    								  DB::raw('(CASE WHEN company.status = "y" THEN "Publish" ELSE "Un Publish" END) AS status'),
    								  'company_translations.lang',
    								  'company_translations.title',
    								  'company_translations.description')
    						->from('company')
    						 ->join('company_translations','company_translations.company_id' ,'=', 'company.id')
    						 ->where('company_translations.lang','id')
                             ->where('company.category','aboutus');

    	$data = Table::of($model)
        ->addColumn('title' , function($model){
            return substrText($model->title,80);
        })
        ->addColumn('description' , function($model){
            return substrText($model->description,80);
        })
		->addColumn('action' , function($model){
    		return \webarq::buttons($model->id);
    	})
		->make(true);

		return $data;
    }

    public function getIndex()
    {
        $setting = menu_setting('aboutus',null);

        $detail = function($lang)use($setting){
          
            $setting = $setting->where('lang',$lang)->first();

            if(!empty($setting->id))
            {
                return $setting;
            }

        };

        $model = $this->model->whereCategory('aboutus')->get();

        $allowAdd = count($model) > 0 ? false : true;

    	return view($this->path.'index',compact('setting','detail','allowAdd'));
    }

    public function postIndex(Requests\Backend\MenuSettingsRequest $request)
    {
        $save_setting = save_setting($this->menuSettings,'aboutus', $request);

        return redirect(urlBackendAction('index'))->with('success','Data has been saved');
    }

    public function getCreate()
    {
    	$model = $this->model;

    	return view($this->path.'_form',compact('model'));
    }

    public function postCreate(Requests\Backend\CompanyRequest $request)
    {

    	$model = $this->model;

		$inputs = $request->all();

		$model->category = 'aboutus';

		$model->status = $inputs['status'];

		$model->save();

		$companyTranslations = $this->companyTranslations;

        foreach (langs() as $key => $value) {
            $data = [
                    'company_id'=>$model->id,
                    'title'  => $inputs['title'][$key],
                    'lang'=>$inputs['lang'][$key],
                    'description'=>$inputs['description'][$key],
                ];

            $this->companyTranslations->create($data);
        }

        return redirect(urlBackendAction('index'))->with('success','Data has been saved');
    }

    public function getUpdate($id)
    {
    	$model = $this->model->findOrFail($id);
    
	    $companyTranslations = $this->companyTranslations->where('company_id',$model->id)->get();

        $detail = function($lang)use($companyTranslations){
          
            $companyTranslations = $companyTranslations
                ->where('lang',$lang)
                ->first();

            if(!empty($companyTranslations->id))
            {
                return $companyTranslations;
            }

        };

    	return view($this->path.'_form',compact('model','companyTranslations','detail'));
    }

    public function postUpdate(Requests\Backend\CompanyRequest $request,$id)
    {
    	$model = $this->model->findOrFail($id);

		$inputs = $request->all();

		$dataModel = ['status' => $inputs['status']];

		$model->update($dataModel);

        foreach (langs() as $key => $value) {
            $upRow = $this->companyTranslations->find($inputs['id'][$key]);

                $data = [
                    'title'=>$inputs['title'][$key],
                    'description'=>$inputs['description'][$key],
                ];

            if (!empty($upRow->id)) {

                $upRow->update($data);

            }else{

                 $this->companyTranslations->create($data);

            }
        }

        return redirect(urlBackendAction('index'))->with('success','Data has been saved');
    }

   public function getDelete($id)
    {
       $model = $this->model->findOrFail($id);

       $companyTranslations = $this->companyTranslations->where('company_id',$model->id);

       $companyTranslations->delete();

        return $this->delete($model,[]);
    }
}
