<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Backend\WebarqController;
use Table;
use DB;
use App\Models\Im;
use App\Models\ImData;
use Excel;

class ManajerInvestasiController extends WebarqController
{
    public function __construct(ImData $model,Im $im)
    {
    	parent::__construct();

    	$this->model = $model;

        $this->im = $im;

    	$this->view .= 'manajer_investasi.';
    }

    public function getData()
    {
        $periode = mirae_date_oracle(get('periode'));

    	$model = $this->im->select('im.id','im.im_cd','im.im_nm','im_datas.id as im_data_id','im_datas.periode','im_datas.id as aidi')
            ->join('im_datas','im_datas.im_cd','=','im.im_cd')
            ->orderBy('periode','desc');

        if(!empty(get('periode')))
        {
            $model = $model->where('periode',$periode);
        }
        $model = $model->orderBy('periode','asc');
        $tables = Table::of($model)
        ->addColumn('periode' , function($model){
            if(!empty($model->periode)){
    		  return carbon()->parse($model->periode)->format("d-M-Y");
            }else{
                return '-';
            }
    	})
    	->addColumn('action' , function($model){
    		return \webarq::buttons($model->aidi);
    	})
        ->make(true);

    	return $tables;
    }

    public function getIndex()
    {
        return $this->view('index');
    }

    public function getCreate()
    {
        $model = $this->model;
        return $this->view('create',[
            'model'=>$model,
        ]);
    }

    public function postCreate(Requests\Backend\ManajerInvestasiRequest $request)
    {
        $periode = parse($request->tahun.'-'.$request->bulan.'-01')->endOfMonth()->format("Ymd");
        $cekPeriode = $this->model->where('periode',$periode)->count();

        if($cekPeriode > 0)
        {
            $deletePeriode = $this->model->where('periode',$periode)->delete();
        }

        $file = $request->file('excel');

        $im = $this->im->select('im_cd')->get()->toArray();

        $listIm = array_flatten($im);

        DB::beginTransaction();

        try
        {
            $prosesExcel = Excel::load($file, function($reader)use($listIm,$request,$periode) {
                                $model = $this->model;
                                $data = [];
                                foreach($reader->get() as $row)
                                {
                                  if(in_array($row->im_cd,$listIm))
                                   {

                                        $data[] = [
                                            'im_cd'=>$row->im_cd,
                                            'periode'=>$periode,
                                            'aum_idr'=>$row->aum_idr,
                                            'aum_usd'=>$row->aum_usd,
                                            'unit_idr'=>$row->unit_idr,
                                            'unit_usd'=>$row->unit_usd,

                                        ];
                                   }
                                }

                                $model->insert($data);
                            });
            DB::commit();
            return redirect(urlBackendAction('index'))
                ->with('success','Data has been imported');
        }catch(\Exception $e){
            DB::rollback();

            dd('PROSES IMPORT EXCEL ERROR,HUBUNGI ADMINISTRATOR :'.$e->getMessage());
        }
    }

    public function getUpdate($id)
    {
        $model = $this->model->findOrFail($id);
        
        return $this->view('update',[
            'model'=>$model,
        ]);
    }

    public function postUpdate(Request $request,$id)
    {
        $inputs = $request->all();
        $inputs['periode'] = parse($request->tahun.'-'.$request->bulan.'-01')->endOfMonth()->format("Ymd");
        unset($inputs['im']);
        $model = $this->model->findOrFail($id);
        return $this->insertOrUpdate($model,$inputs);
    }

    public function getDelete($id)
    {
        $model = $this->model->findOrFail($id)->firstOrFail();

        return $this->delete($model);
    }
}
