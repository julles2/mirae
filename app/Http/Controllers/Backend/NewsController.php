<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Backend\WebarqController;
use App\Http\Requests;
use App\Models\MenuSettings;
use App\Models\News;
use App\Models\NewsCategories;
use App\Models\NewsCategoryTranslations;
use App\Models\NewsTranslation;
use DB;
use Table;
use webarq;

class NewsController extends WebarqController
{
    public function __construct(News $model, NewsTranslation $newsTranslations, NewsCategories $categories, NewsCategoryTranslations $categoriesTranslations, MenuSettings $menuSettings)
    {
        parent::__construct();
        $this->model                  = $model;
        $this->newsTranslations       = $newsTranslations;
        $this->categories             = $categories;
        $this->categoriesTranslations = $categoriesTranslations;
        $this->menuSettings           = $menuSettings;
        $this->path                   = 'backend.news.';
    }

    private function dropdownCategories()
    {
        $model               = $this->model;
        $categories_dropdown = ['' => '-- Select Category --'] +
        $this->categories
            ->select('news_categories.id', 'news_category_translations.category', 'news_category_translations.lang')
            ->join('news_category_translations', 'news_category_translations.category_id', '=', 'news_categories.id')
            ->where('news_category_translations.lang', 'id')
            ->orderBy('news_category_translations.category', 'asc')
            ->lists('category', 'id')
            ->toArray();

        return $categories_dropdown;
    }

    public function getData()
    {

        $model = $this->model->select('news.id',
            DB::raw('(CASE WHEN news.status = "y" THEN "Publish" ELSE "Un Publish" END) AS status'),
            'news.date',
            'news.pinned',
            'news_category_translations.category',
            'news_translations.lang',
            'news_translations.title',
            'news_translations.intro'
        )
            ->join('news_translations', 'news_translations.news_id', '=', 'news.id')
            ->join('news_category_translations', 'news_category_translations.category_id', '=', 'news.category_id')
            ->where('news_translations.lang', 'id')
            ->where('news_category_translations.lang', 'id');
        $data = Table::of($model)
            ->addColumn('title', function ($model) {
                return \substrText($model->title, 50);
            })
            ->addColumn('intro', function ($model) {
                return \substrText($model->intro, 50);
            })
            ->addColumn('pinned', function ($model) {
                return $model->pinned == 'y' ? '<small style = "color:green;">Pinned</small>' : '';
            })
            ->addColumn('action', function ($model) {
                return \webarq::buttons($model->id);
            })
            ->make(true);

        return $data;
    }

    public function getIndex()
    {
        $setting = menu_setting('news', null);

        $detail = function ($lang) use ($setting) {

            $setting = $setting->where('lang', $lang)->first();

            if (!empty($setting->id)) {
                return $setting;
            }

        };

        return view($this->path . 'index', compact('setting', 'detail'));
    }

    public function postIndex(Requests\Backend\MenuSettingsRequest $request)
    {
        $save_setting = save_setting($this->menuSettings, 'news', $request);

        return redirect(urlBackendAction('index'))->with('success', 'Data has been saved');
    }

    public function getCreate()
    {
        $model               = $this->model;
        $categories_dropdown = $this->dropdownCategories();

        return view($this->path . '_form', compact('model', 'categories_dropdown'));
    }

    public function postCreate(Requests\Backend\NewsRequest $request)
    {
        $model = $this->model;

        $inputs = $request->all();

        $model->category_id = $inputs['category_id'];

        $model->status = $inputs['status'];

        $model->date = $inputs['date'];

        $model->pinned = $inputs['pinned'];

        if (!empty($inputs['image_intro'])) {

            $inputs['image_intro'] = $this->handleUpload($request, $model, 'image_intro', [370, 210]);

            $model->image_intro = $inputs['image_intro'];
        }

        if (!empty($inputs['image_detail'])) {

            $inputs['image_detail'] = $this->handleUpload($request, $model, 'image_detail', [1920, 550]);

            $model->image_detail = $inputs['image_detail'];
        }

        $model->save();

        $newsTranslations = $this->newsTranslations;

        foreach (langs() as $key => $value) {
            $data = [
                'news_id'     => $model->id,
                'lang'        => $inputs['lang'][$key],
                'title'       => $inputs['title'][$key] = null ? '' : $inputs['title'][$key],
                'intro'       => $inputs['intro'][$key] = null ? '' : $inputs['intro'][$key],
                'description' => $inputs['description'][$key],
            ];

            $this->newsTranslations->create($data);
        }

        return redirect(urlBackendAction('index'))->with('success', 'Data has been saved');
    }

    public function getUpdate($id)
    {
        $model = $this->model->findOrFail($id);

        $categories_dropdown = $this->dropdownCategories();

        $newsTranslations = $this->newsTranslations->where('news_id', $model->id)->get();

        $detail = function ($lang) use ($newsTranslations) {

            $newsTranslations = $newsTranslations
                ->where('lang', $lang)
                ->first();

            if (!empty($newsTranslations->id)) {
                return $newsTranslations;
            }

        };

        return view($this->path . '_form', compact('model', 'categories_dropdown', 'newsTranslations', 'detail'));
    }

    public function postUpdate(Requests\Backend\NewsRequest $request, $id)
    {
        $model = $this->model->findOrFail($id);

        $inputs = $request->all();

        $inputs['image_intro'] = $this->handleUpload($request, $model, 'image_intro', [370, 210]);

        $inputs['image_detail'] = $this->handleUpload($request, $model, 'image_detail', [1920, 550]);

        $dataNews = [
            'category_id'  => $inputs['category_id'],
            'status'       => $inputs['status'],
            'pinned'       => $inputs['pinned'],
            'date'         => $inputs['date'],
            'image_intro'  => $inputs['image_intro'],
            'image_detail' => $inputs['image_detail'],
        ];

        $model->update($dataNews);

        foreach (langs() as $key => $value) {
            $upRow = $this->newsTranslations->find($inputs['id'][$key]);

            $data = [
                'lang'        => $inputs['lang'][$key],
                'title'       => $inputs['title'][$key],
                'intro'       => $inputs['intro'][$key],
                'description' => $inputs['description'][$key],
            ];

            if (!empty($upRow->id)) {

                $upRow->update($data);

            } else {

                $this->newsTranslations->create($data);

            }
        }
        return redirect(urlBackendAction('index'))->with('success', 'Data has been saved');
    }

    public function getDelete($id)
    {
        $model = $this->model->findOrFail($id);

        $newsTranslations = $this->newsTranslations->where('news_id', $model->id);

        $newsTranslations->delete();

        return $this->delete($model, []);
    }
}
