<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Backend\WebarqController;
use App\Models\MenuSettings;
use Illuminate\Http\Request;

class VideoController extends WebarqController
{
    public function __construct(MenuSettings $model)
    {
        parent::__construct();
        $this->model = $model;
        $this->path  = 'backend.video.';
    }

    public function data()
    {
        $model = $this->model->whereMenu('video')->get();
        $data = [];
        foreach($model as $row)
        {
            $data[$row->key.'_'.$row->lang]=$row->value;
        }
        return $data;
    }

    public function getIndex()
    {
        $setting = $this->data();
        $detail = function ($lang) use ($setting) {

            $setting = $setting->where('lang', $lang)->first();

            if (!empty($setting->id)) {
                return $setting;
            }

        };

        return view($this->path . 'index', compact('setting', 'detail'));
    }

    public function postIndex(Request $request)
    {
        // $save_setting = save_setting($this->model,'video', $request);

        $inputs = $request->all();
        $model = $this->model->whereMenu('video')->delete();
        $data = [];
        foreach($inputs as $key => $val)
        {
            if(is_array($val))
            {
                foreach($val as $lang => $lang)
                {
                     $this->model->create([
                        'menu'=>'video',
                        'key'=>$key,
                        'value'=>$val[$lang],
                        'lang'=>$lang,
                    ]);
                }
            }else{
                $this->model->create([
                    'menu'=>'video',
                    'key'=>$key,
                    'value'=>$val,
                ]);                
            }
        }

        return redirect()->back()->with('success','Data has been updated');
    }

    
}
