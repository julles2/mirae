<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Backend\WebarqController;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Bantuan;
use App\Models\BantuanTranslation;
use App\Models\MenuSettings;
use Table;
use Image;
use webarq;
use DB;

class GlossaryController extends WebarqController
{
    public function __construct(Bantuan $model, BantuanTranslation $bantuanTranslations,MenuSettings $menuSettings)
    {
    	parent::__construct();
        $this->model = $model;
        $this->bantuanTranslations = $bantuanTranslations;
        $this->menuSettings = $menuSettings;
        $this->path = 'backend.help.glossary.';
    }

   
    public function getData()
    {

    	$data = get_help($this->model,'glossary');

		return $data;
    }

    public function getIndex()
    {
        $setting = menu_setting('glossary',null);

        $detail = function($lang)use($setting){
          
            $setting = $setting->where('lang',$lang)->first();

            if(!empty($setting->id))
            {
                return $setting;
            }

        };

    	return view($this->path.'index',compact('setting','detail'));
    }

    public function postIndex(Requests\Backend\MenuSettingsRequest $request)
    {
        $save_setting = save_setting($this->menuSettings,'glossary', $request);

        return redirect(urlBackendAction('index'))->with('success','Data has been saved');
    }

    public function getCreate()
    {
    	$model = $this->model;

    	return view($this->path.'_form',compact('model'));
    }

    public function postCreate(Requests\Backend\HelpRequest $request)
    {

    	$save_data = save_help($request,$this->model,$this->bantuanTranslations,'glossary');

        return redirect(urlBackendAction('index'))->with('success','Data has been saved');
    }

    public function getUpdate($id)
    {
    	$model = $this->model->findOrFail($id);

        $bantuanTranslations = $this->bantuanTranslations->where('bantuan_id',$model->id)->get();

        $detail = function($lang)use($bantuanTranslations){
          
            $bantuanTranslations = $bantuanTranslations
                ->where('lang',$lang)
                ->first();

            if(!empty($bantuanTranslations->id))
            {
                return $bantuanTranslations;
            }

        };

    	return view($this->path.'_form',compact('model','bantuanTranslations','detail'));
    }

    public function postUpdate(Requests\Backend\HelpRequest $request,$id)
    {
    	$update_data = update_help($request,$this->model,$this->bantuanTranslations,$id);

        return redirect(urlBackendAction('index'))->with('success','Data has been saved');
    }

   public function getDelete($id)
    {
        $delete_data = delete_help($this->model,$this->bantuanTranslations,$id);

        return redirect(urlBackendAction('index'))->with('success','Data has been deleted');
    }
}
