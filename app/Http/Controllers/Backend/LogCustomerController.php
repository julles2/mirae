<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Backend\WebarqController;
use App\Models\LogCustomer;

use Admin;
use Table;

class LogCustomerController extends WebarqController
{
    public function __construct(LogCustomer $model)
    {
    	parent::__construct();

    	$this->model = $model;
	}

	public function getData()
    {
    	$model = $this->model->select('id','acnt_no','information','created_at','name','username','status')
    		->orderBy('created_at','desc');

    	$tables = Table::of($model)
    	->addColumn('status',function($model){
    		$class = $model->status == 'success' ? "alert alert-success" : "alert alert-danger";
    		return "<label class = '$class'>".$model->status."</label>";
    	})
    	->make(true);

    	return $tables;
    }

    public function getIndex()
    {
    	return view('backend.log_customer.index');
    }

}
