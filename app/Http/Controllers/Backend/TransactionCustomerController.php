<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Backend\WebarqController;
use App\Models\Oracle\ClientInfo;
use App\Models\Oracle\VCustInfo;

use Illuminate\Http\Request;
use App\Models\Question;
use App\Repository\Question as QuestionRepo;
use webarq;
use Table;
use Image;

class TransactionCustomerController extends WebarqController
{
    public function __construct(Question $model)
    {
    	parent::__construct();
        $this->model = $model;
        $this->questionRepo = new QuestionRepo();
    }

    public function getData()
    {
    	$model = $this->model->select('id','acnt_no','name','status','msg_rejected')
            ->orderByRaw("status = 'n' desc");

    	$data = Table::of($model)
            ->addColumn('status',function($model){
                $label = '<label style = "color:green;">Approve</label>';
                if($model->status == 'n')
                {
                    $label = '<label style="color:blue;">Pending</label>';
                }
                
                if($model->status == 'r')
                {
                    $label = '<label style="color:red;">Rejected</label>';
                }

                return $label;
            })
    		->addColumn('action',function($model){
                if($model->status == 'n')
                {
                    return webarq::buttons($model->id);
                }
                
                $status = ['r','y'];
                
                if(in_array($model->status,$status))
                {
                    return '<a href = "'.urlBackendAction('update/'.$model->id).'">View</a>';
                }

    		})
    		->make(true);

    	return $data;
    }

    public function getIndex()
    {
    	return view('backend.transaction_customer.index');
    }

    public function postReject(Request $request,$id)
    {
        $model = $this->model->findOrFail($id);
        $msg = $request->msg_rejected;
        if(empty($msg))
        {
            abort(404);
        }
        
        $model->update([
            'status'=>'r',
            'msg_rejected'=>$msg,
        ]);

        return redirect(urlBackendAction('index'))->withSuccess('Data has been rejected');
    }

    public function getUpdate($id)
    {
        $model = $this->model->findOrFail($id);
        $statusRejectedApproved = ['r','y'];

        $cekCountry = function($code){
            $model = new \App\Models\Country();

            $result = $model->whereCode($code)->first();

            return $result->country;
        };

        return view('backend.transaction_customer._form',[
            'model'=>$model,
            'cekCountry'=>$cekCountry,
            'repo'=>$this->questionRepo,
            'statusRejectedApproved'=>$statusRejectedApproved,
        ]);
    }
    //021574
    public function postUpdate(Request $request,$id)
    {
        $model = $this->model->where('status','n')->findOrFail($id);

        $modelClient = new ClientInfo();

        $data = json_decode($model->data_question);
        $riskProfile = $this->questionRepo->riskRange($model->risk_total);

        if (filter_var($model->email, FILTER_VALIDATE_EMAIL)) {
            \Mail::send('backend.transaction_customer.mail',[],function($m)use($model){
                $m->from(env('MAIL_FROM'), 'Verification');
                $m->to(trim($model->email))->cc('masagideveloper@gmail.com')->subject('Verification');
            });
        }

        

        $modelCustInfo = VCustInfo::select('sid')->where('acnt_no',$model->acnt_no)->first();
        $sid = null;
        if(!empty($modelCustInfo->sid))
        {
            $sid = $modelCustInfo->sid;
        }
        if(!empty($data->Kewarganegaraan))
        {
            $modelClient->create([
                'acnt_no'=>$model->acnt_no,
                'nationality'=>@$data->Kewarganegaraan,
                'birth_country'=>@$data->negara_kelahiran,
                'religion'=>@$data->agama,
                'addr'=>@$data->alamat_saat_ini,
                'risk_profile'=>$riskProfile,
                'sid'=>$sid,
            ]);
        }else{
            $modelClient->create([
                'acnt_no'=>$model->acnt_no,
                'nationality'=>@$data->nationality,
                'birth_country'=>@$data->birth_country,
                'religion'=>@$data->religion,
                'addr'=>@$data->current_address,
                'risk_profile'=>$riskProfile,
                'sid'=>$sid,
            ]);
        }

            

        $model->update(['status'=>'y']);

        return redirect(urlBackendAction('index'))->withSuccess('Data has been approved');
    }

    public function getSendEmail()
    {
        // \Mail::send('backend.transaction_customer.mail',[],function($m){
        //     $m->from('mirae@mirae.com', 'Verification');
        //     $m->to('masagideveloper@gmail.com')->subject('Verification');
        // });
    }
}
