<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Backend\WebarqController;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Milestones;
use App\Models\MilestonesTranslations;
use App\Models\MenuSettings;
use Table;
use Image;
use webarq;
use DB;


class MilestoneController extends WebarqController
{
    public function __construct(Milestones $model, MilestonesTranslations $milestonesTranslations, MenuSettings $menuSettings)
    {
    	parent::__construct();
        $this->model = $model;
        $this->milestonesTranslations = $milestonesTranslations;
        $this->menuSettings = $menuSettings;

        $this->path = 'backend.milestone.';
    }

    public function getData()
    {

    	$model = $this->model->select('milestones.id',
    									DB::raw('(CASE WHEN milestones.status = "y" THEN "Publish" ELSE "Un Publish" END) AS status'),
    									'milestones_translations.year',
                                        'milestones_translations.title',
    									'milestones.order',
    								  	'milestones_translations.lang',
    								  	'milestones_translations.description'
    								  	)
			->join('milestones_translations','milestones_translations.milestone_id','=','milestones.id')
			->where('milestones_translations.lang','id');
		$data = Table::of($model)
		->addColumn('action' , function($model){
    		return \webarq::buttons($model->id);
    	})
		->make(true);

		return $data;
    }

    public function dropdownYear()
    {

        $years = array();
        $year = 1990;

        for ($i=$year; $i <= date("Y") ; $i++) {
            $years[$i] = $i;
        }
    	// $years = array_combine(range(date("Y"), 2000), range(date("Y"), 2000));

		return $years;
    }

    public function getIndex()
    {
        $setting = menu_setting('milestone',null);

        $detail = function($lang)use($setting){
          
            $setting = $setting->where('lang',$lang)->first();

            if(!empty($setting->id))
            {
                return $setting;
            }

        };


    	return view($this->path.'index',compact('setting','detail'));
    }

    public function postIndex(Requests\Backend\MenuSettingsRequest $request)
    {
        $save_setting = save_setting($this->menuSettings,'milestone', $request);

        return redirect(urlBackendAction('index'))->with('success','Data has been saved');
    }

    public function getCreate()
    {
    	$model = $this->model;
    	$ddYear = $this->dropdownYear();

    	return view($this->path.'_form',compact('model','ddYear'));
    }

    public function postCreate(Requests\Backend\MilestonesRequest $request)
    {

    	$model = $this->model;

		$inputs = $request->all();

		$model->status = $inputs['status'];

		$model->order = $inputs['order'];

		$model->save();

		$milestonesTranslations = $this->milestonesTranslations;

		foreach (langs() as $key => $value) {
            
            $image = $request->file('image')[$key];

            if(!empty($image))
            {

                $imageName = randomImage().'.'.$image->getClientOriginalExtension();

                $image = \Image::make($image);

                $image = $image->resize(700,260);

                $image = $image->save(public_path('contents/'.$imageName));

                $image = $imageName;

                $data = [
                    'milestone_id'=>$model->id,
                    'year'  => $inputs['year'],
                    'title'  => $inputs['title'][$key],
                    'lang'=>$inputs['lang'][$key],
                    'image'=> $image,
                    'description'=>$inputs['description'][$key],
                ];

            }else{

                $data = [
                    'milestone_id'=>$model->id,
                    'year'  => $inputs['year'],
                    'title'  => $inputs['title'][$key],
                    'lang'=>$inputs['lang'][$key],
                    'description'=>$inputs['description'][$key],
                ];
            }

            $this->milestonesTranslations->create($data);

        }

        return redirect(urlBackendAction('index'))->with('success','Data has been saved');
    }

    public function getUpdate($id)
    {
    	$model = $this->model->findOrFail($id);

    	$ddYear = $this->dropdownYear();

        $milestonesTranslations = $this->milestonesTranslations->where('milestone_id',$model->id)->get();

        $detail = function($lang)use($milestonesTranslations){
          
            $milestonesTranslations = $milestonesTranslations
                ->where('lang',$lang)
                ->first();

            if(!empty($milestonesTranslations->id))
            {
                return $milestonesTranslations;
            }

        };

    	return view($this->path.'_form',compact('model','ddYear','milestonesTranslations','detail'));
    }

    public function postUpdate(Requests\Backend\MilestonesRequest $request,$id)
    {
    	$model = $this->model->findOrFail($id);

		$inputs = $request->all();

		$dataNews = [
					'status' => $inputs['status'],
					'order' => $inputs['order'],
		];

		$model->update($dataNews);

		foreach (langs() as $key => $value) {
            
            $upRow = $this->milestonesTranslations->find($inputs['id'][$key]);

            $image = $request->file('image')[$key];

            if(!empty($image))
            {
                 if(!empty($upRow->image))
                    {
                        @unlink(public_path('contents/'.$upRow->image));
                    }

                $imageName = randomImage().'.'.$image->getClientOriginalExtension();

                $image = \Image::make($image);

                $image = $image->resize(700,260);

                $image = $image->save(public_path('contents/'.$imageName));

                $image = $imageName;

                $data = [
                    'lang'=>$inputs['lang'][$key],
                    'year'=>$inputs['year'],
                    'title'=>$inputs['title'][$key],
                    'image'=>$image,
                    'description'=>$inputs['description'][$key],
                ];

            }else{

                $image = $upRow->image;

                $data = [
                    'lang'=>$inputs['lang'][$key],
                    'year'=>$inputs['year'],
                    'title'=>$inputs['title'][$key],
                    'image'=>$image,
                    'description'=>$inputs['description'][$key],
                ];
            }

            if (!empty($upRow->id)) {

                $upRow->update($data);

            }else{

                 $this->milestonesTranslations->create($data);

            }

        }

        return redirect(urlBackendAction('index'))->with('success','Data has been saved');
    }

   public function getDelete($id)
    {
       $model = $this->model->findOrFail($id);

       $milestonesTranslations = $this->milestonesTranslations->where('milestone_id',$model->id);

       $milestonesTranslations->delete();

        return $this->delete($model,[]);
    }

    
}
