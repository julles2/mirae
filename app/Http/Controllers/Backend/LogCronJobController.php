<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Backend\WebarqController;
use App\Models\LogCronjob;
use Table;

class LogCronJobController extends WebarqController
{
    public function __construct(LogCronJob $model)
    {
    	parent::__construct();

    	$this->model = $model;
	}

	public function getData()
    {
    	$model = $this->model->select('id','message','created_at')->where('status','y')
    	->orderBy('created_at','desc');

    	$tables = Table::of($model)
		->make(true);

    	return $tables;
    }

    public function getDataFailed()
    {
        $model = $this->model->select('id','message','created_at')->where('status','n')
        ->orderBy('created_at','desc');

        $tables = Table::of($model)
        ->make(true);

        return $tables;
    }

    public function getIndex()
    {
    	return view('backend.log.index');
    }
}
