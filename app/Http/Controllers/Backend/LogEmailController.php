<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Backend\WebarqController;
use App\Models\LogEmail;

use Admin;
use Table;

class LogEmailController extends WebarqController
{
    public function __construct(LogEmail $model)
    {
    	parent::__construct();

    	$this->model = $model;
	}

	public function getData()
    {
    	$model = $this->model->select('id','transaction_id','transaction_type','created_at','message','from','to','status')
    		->orderBy('created_at','desc');

    	$tables = Table::of($model)
    	->addColumn('status',function($model){
    		$class = $model->status == 'success' ? "alert alert-success" : "alert alert-danger";
    		return "<label class = '$class'>".$model->status."</label>";
    	})
    	->make(true);

    	return $tables;
    }

    public function getIndex()
    {
    	return view('backend.log_email.index');
    }

}
