<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Backend\WebarqController;
use App\Models\Im;
use App\Models\ImData;
use App\Models\ImInfoDetails;
use App\Models\ImDetailCompany;
use App\Models\MenuSettings;
use App\Models\ImDepartments;
use App\Models\ImImages;

use Table;
use DB;

class MiInfoController extends WebarqController
{
    public function __construct(ImData $model,Im $im,MenuSettings $menuSettings, ImDetailCompany $company , ImInfoDetails $generalInfo, ImDepartments $department, ImImages $ImImages)
    {
    	parent::__construct();

    	$this->model = $model;

        $this->im = $im;

        $this->menuSettings = $menuSettings;

        $this->company = $company;

        $this->generalInfo = $generalInfo;

        $this->department = $department;

        $this->ImImages = $ImImages;

    	$this->view .= 'im-info.';
    }

    public function getData()
    {
    	$model = $this->im->select('im.id','im.im_cd','im.im_nm');

    	$tables = Table::of($model)

    	->addColumn('action' , function($model){
    		return \webarq::buttons($model->im_cd);
    	})

    	->make(true);

    	return $tables;
    }

    public function getIndex()
    {
        $setting = menu_setting('manajer-investasi',null);

        $detail = function($lang)use($setting){

            $setting = $setting->where('lang',$lang)->first();

            if(!empty($setting->id))
            {
                return $setting;
            }

        };

        $settingCompare = menu_setting('manajer-compare',null);

        $detailCompare = function($lang)use($settingCompare){

            $settingCompare = $settingCompare->where('lang',$lang)->first();

            if(!empty($settingCompare->id))
            {
                return $settingCompare;
            }

        };

        return $this->view('index',compact('setting','detail','settingCompare','detailCompare'));
    }

    public function postIndex(Requests\Backend\MenuSettingsRequest $request)
    {
        $inputs = $request->all();

        if ($inputs['jenis'] == 'list') {
            $save_setting = save_setting($this->menuSettings,'manajer-investasi', $request);
        }else{
            $save_setting = save_setting($this->menuSettings,'manajer-compare', $request);
        }


        return redirect(urlBackendAction('index'))->with('success','Data has been saved');
    }

    public function getUpdate($imCd)
    {
        //dd($imCd);
        $model = $this->im->where('im_cd',$imCd)->firstOrFail();

        $detailGeneral = function($lang, $im_cd)
        {
        	$generalinfo = $this->generalInfo
        							->where('im_cd',$im_cd)
        							->where('lang',$lang)->first();

        	if (!empty($generalinfo->id)) {

        		return $generalinfo;

        	}
        };

        $detailCompany = function($lang, $im_cd)
        {
        	$company = $this->company
        							->where('im_cd',$im_cd)
        							->whereLang($lang)->first();

        	if (!empty($company->id)) {

        		return $company;

        	}
        };

        $detailDepartment = $this->department->where('im_cd',$imCd)->get();

        return $this->view('_form',['model'			=> $model,
        							'detailGeneral' => $detailGeneral,
        							'detailCompany' => $detailCompany,
        							'detailDepartment' => $detailDepartment,]);
    }

    public function postUpdate(Requests\Backend\MIInfoRequest $request,$imCode)
    {
        $inputs = $request->all();

        $model = $this->im->where('im_cd',$imCode)->firstOrFail();

        // ===== GENERAL INFO ===== //

        foreach (langs() as $key => $value) {

        	$upRow = $this->generalInfo
        						->whereIm_cd($model->im_cd)
        						->whereLang($key)->first();

            $data = [
                    'im_cd'         => $model->im_cd,
                    'lang'          => $inputs['lang'][$key],
                    'description'   => $inputs['description'][$key],
                    'pengelolaan'   => $inputs['pengelolaan'][$key],
                    'modal_dasar'   => $inputs['modal_dasar'][$key],
                    'modal_disetor' => $inputs['modal_disetor'][$key],
                    'kepemilikan'   => $inputs['kepemilikan'][$key],
                    'status'        => $inputs['status'][$key],
                    'izin_usaha'    => $inputs['izin_usaha'][$key],
                ];

            if (!empty($upRow->id)) {

                $upRow->update($data);

            }else{

                 $this->generalInfo->create($data);

            }
        }


        // === DEPARTMENT === //
        if (!empty($inputs['d_nama'])) {
            for ($i=0; $i < count($inputs['d_nama']) ; $i++) {

                $dataDepartment = $this->department->whereId($inputs['d_id'][$i])->first();

                $depart['im_cd'] = $imCode;
                $depart['nama'] = @$inputs['d_nama'][$i];
                $depart['jabatan'] = @$inputs['d_jabatan'][$i];

                if (!empty($dataDepartment->id)) {
                    $dataDepartment->update($depart);
                }else{
                    $this->department->create($depart);
                }
            }
        }


        // ===== END GENERAL INFO ===== //

        // ===== COMPANY INFO ===== //

        foreach (langs() as $key => $value) {

        	$upRow = $this->company
        						->whereIm_cd($model->im_cd)
        						->whereLang($key)->first();

            $data = [
                    'im_cd'         => $model->im_cd,
                    'lang'          => $inputs['lang'][$key],
                    'description'   => $inputs['description_company'][$key],
                ];

            if (!empty($inputs['image'][$key]))
            {
                $data['image'] = $this->uploadImageLang($request, $key, 250, 85);


                $imageInfo['image'] = $this->uploadImageLang($request, $key, 250, 85);
            }

            if (!empty($upRow->id)) {

                $upRow->update($data);

            }else{

                 $this->company->create($data);

            }

            $imageIM = $this->ImImages
        						->whereIm_cd($model->im_cd)
        						->first();

        	$imageInfo['im_cd'] = $model->im_cd;

        	if (!empty($inputs['image'][$key]))
            {
                $imageInfo['image'] = $this->uploadImageLang($request, $key, 250, 85);
            }

        	if (!empty($imageIM->id)) {

                $imageIM->update($imageInfo);

            }else{

                 $this->ImImages->create($imageInfo);

            }
        }

        // ===== END COMPANY INFO ===== //
        return redirect(urlBackendAction('index'))
            ->with('success','Data has been saved');
    }

    private function uploadImageLang($request, $key, $imagewidth, $imageHeight)
    {
    	$image = $request->file('image')[$key];

	    $imageName = randomImage().'.'.$image->getClientOriginalExtension();

	    $image = \Image::make($image);

	    $image = $image->resize($imagewidth,$imageHeight);

	    $image = $image->save(public_path('contents/'.$imageName));

	    $image = $imageName;

	    return $image;
    }

    public function getDeletedepartment($id)
    {
       $model = $this->department->findOrFail($id);

        return $this->delete($model,[]);
    }
}
