<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Newsletters;
use Table;
use Excel;

class NewsletterController extends WebarqController
{
    public function __construct(Newsletters $newsletters)
    {
    	parent::__construct();
        $this->newsletters = $newsletters;
        $this->path = 'backend.newsletters.';
    }

    public function getData()
    {
    	$model = $this->newsletters->get();
		$data = Table::of($model)
		->addColumn('action' , function($model){
    		return \webarq::buttons($model->id);
    	})
		->make(true);

		return $data;
    }

    public function getIndex()
    {
    	return view($this->path.'index');
    }

    public function getDelete($id)
    {
    	$model = $this->newsletters->findOrFail($id);
    	
        return $this->delete($model,[]);
    }

    public function getExportdata()
    {
        $model = $this->newsletters->select('email','created_at','updated_at')->get();

        Excel::create('Newsletters', function($excel) use($model) {

            $excel->sheet('user', function($sheet) use($model) {

                $sheet->fromArray($model);

            });

        })->export('xls');
    }	
}
