<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Backend\WebarqController;
use App\Http\Requests;
use App\Models\Sliders;
use App\Models\SliderTranslations;
use DB;
use Table;
use Validator;
use webarq;

class SliderController extends WebarqController
{
    public function __construct(Sliders $model, SliderTranslations $sliderTranslations)
    {
        parent::__construct();
        $this->model              = $model;
        $this->sliderTranslations = $sliderTranslations;
        $this->path               = 'backend.slider.';
    }

    public function getData()
    {
        $model = $this->model->select('sliders.id',
            'sliders.status',
            'slider_translations.lang',
            'slider_translations.title',
            'slider_translations.order',
            'slider_translations.wording')
            ->from('sliders')
            ->join('slider_translations', 'slider_translations.slider_id', '=', 'sliders.id')
            ->where('slider_translations.lang', 'id');

        $data = Table::of($model)
            ->addColumn('action', function ($model) {
                return \webarq::buttons($model->id);
            })
            ->make(true);

        return $data;
    }

    public function getIndex()
    {
        return view($this->path . 'index');
    }

    public function getCreate()
    {
        $model = $this->model;

        return view($this->path . '_form', compact('model'));
    }

    public function postCreate(Requests\Backend\SliderRequest $request)
    {

        $model = $this->model;

        $inputs = $request->all();

        $rules = array(
            'image' => 'required',

        );

        $messsages = array(
            'image.required' => 'Image cannot be empty',
        );

        $validator = Validator::make($request->all(), $rules, $messsages);

        if ($validator->fails()):
            $this->throwValidationException($request, $validator);
        endif;

        $inputs['image'] = $this->handleUpload($request, $model, 'image', [1425, 749]);

        $model->status = $inputs['status'];

        $model->image = $inputs['image'];

        $model->save();

        $sliderTranslations = $this->sliderTranslations;

        foreach (langs() as $key => $value) {
            $data = [
                'slider_id'      => $model->id,
                'title'          => $inputs['title'][$key],
                'lang'           => $inputs['lang'][$key],
                'wording'        => $inputs['wording'][$key],
                'status'         => $inputs['statusbutton'][$key],
                'url'            => $inputs['url'][$key],
                'order'          => $inputs['order'][$key],
                'button_caption' => $inputs['button_caption'][$key],
            ];

            $this->sliderTranslations->create($data);
        }

        return redirect(urlBackendAction('index'))->with('success', 'Data has been saved');
    }

    public function getUpdate($id)
    {
        $model = $this->model->findOrFail($id);

        $sliderTranslations = $this->sliderTranslations->where('slider_id', $model->id)->get();

        $detail = function ($lang) use ($sliderTranslations) {

            $sliderTranslations = $sliderTranslations
                ->where('lang', $lang)
                ->first();

            if (!empty($sliderTranslations->id)) {
                return $sliderTranslations;
            }

        };

        return view($this->path . '_form', compact('model', 'sliderTranslations', 'detail'));
    }

    public function postUpdate(Requests\Backend\SliderRequest $request, $id)
    {
        $model = $this->model->findOrFail($id);

        $inputs = $request->all();

        $inputs['image'] = $this->handleUpload($request, $model, 'image', [1425, 749]);

        $dataModel = array();

        $dataModel['status'] = $inputs['status'];

        if (!empty($inputs['image'])) {

            $dataModel['image'] = $inputs['image'];

        }

        $model->update($dataModel);

        foreach (langs() as $key => $value) {
            $upRow = $this->sliderTranslations->find($inputs['id'][$key]);

            $data = [
                'title'          => $inputs['title'][$key],
                'wording'        => $inputs['wording'][$key],
                'status'         => $inputs['statusbutton'][$key],
                'url'            => $inputs['url'][$key],
                'order'          => $inputs['order'][$key],
                'button_caption' => $inputs['button_caption'][$key],
            ];

            if (!empty($upRow->id)) {

                $upRow->update($data);

            } else {

                $this->sliderTranslations->create($data);

            }
        }
        return redirect(urlBackendAction('index'))->with('success', 'Data has been saved');
    }

    public function getDelete($id)
    {
        $model = $this->model->findOrFail($id);

        $sliderTranslations = $this->sliderTranslations->where('slider_id', $model->id);

        $sliderTranslations->delete();

        return $this->delete($model, []);
    }
}
