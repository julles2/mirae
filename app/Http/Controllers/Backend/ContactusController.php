<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Backend\WebarqController;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\MenuSettings;
use App\Models\CustomerVoice;
use webarq;
use Table;
use DB;
use Excel;

class ContactusController extends WebarqController
{
    public function __construct(CustomerVoice $model, MenuSettings $menuSettings)
    {
    	parent::__construct();
        $this->model = $model;
        $this->menuSettings = $menuSettings;
        $this->path = 'backend.help.contact-us.';
    }

    public function getData()
    {
    	$model = $this->model->get();

    	$data = Table::of($model)
		->addColumn('action' , function($model){
    		return \webarq::buttons($model->id);
    	})
		->make(true);

		return $data;
    }

    public function getIndex()
    {
    	$model = $this->model->get();

        $setting = $this->menuSettings
			                ->select(
			                    DB::raw('
			                        id,
			                        lang,
			                        (SELECT a.value FROM menu_settings a WHERE a.key="title_caption" AND a.menu=menu_settings.menu AND a.lang=menu_settings.lang) as title_caption,
			                        (SELECT a.value FROM menu_settings a WHERE a.key="title_main" AND a.menu=menu_settings.menu AND a.lang=menu_settings.lang) as title_main,
			                        (SELECT a.value FROM menu_settings a WHERE a.key="sub_title" AND a.menu=menu_settings.menu AND a.lang=menu_settings.lang) as sub_title,
			                        (SELECT a.value FROM menu_settings a WHERE a.key="banner_image" AND a.menu=menu_settings.menu AND a.lang=menu_settings.lang) as banner_image,
			                        (SELECT a.value FROM menu_settings a WHERE a.key="title_content" AND a.menu=menu_settings.menu AND a.lang=menu_settings.lang) as title_content,
			                        (SELECT a.value FROM menu_settings a WHERE a.key="title_contact" AND a.menu=menu_settings.menu AND a.lang=menu_settings.lang) as title_contact,
			                        (SELECT a.value FROM menu_settings a WHERE a.key="sub_title_contact" AND a.menu=menu_settings.menu AND a.lang=menu_settings.lang) as sub_title_contact,
			                        (SELECT a.value FROM menu_settings a WHERE a.key="address" AND a.menu=menu_settings.menu AND a.lang=menu_settings.lang) as address,
			                        (SELECT a.value FROM menu_settings a WHERE a.key="maps" AND a.menu=menu_settings.menu AND a.lang=menu_settings.lang) as maps
			                    '))
			                ->whereMenu('contactus')->get();

        $detail = function($lang)use($setting){
          
            $settings = $setting->where('lang',$lang)->first();

            if(!empty($settings->id))
            {
                return $settings;
            }

        };

        $settingMail = $this->menuSettings->whereMenu('contactus')
        								->whereKey('send_email')
        								->first();

        $email = $this->menuSettings->whereMenu('contactus')->whereKey('email')->first();

    	$isCheck = null;

    	if (!empty($settingMail)) {
    		if ($settingMail->value == '1') {
	    		$isCheck = true;
	    	}
    	}

    	return view($this->path.'index',compact('setting','detail','model','isCheck','email'));
    }

    public function getView($id)
    {
    	$model = $this->model->find($id);

    	return view($this->path.'detail',compact('model'));

    }

    public function postIndex(Requests\Backend\MenuSettingsRequest $request)
    {

    	$inputs = $request->all();

    	$menuSettings = $this->menuSettings->whereMenu('contactus')
                                            ->whereKey('send_email')
                                            ->first();

        $menuSettings2 = $this->menuSettings->whereMenu('contactus')
                                            ->whereKey('email')
                                            ->first();

        if (!empty($inputs['send_email'])) {
            $inputs['send_email'] = true;
        }else{
            $inputs['send_email'] = false;
        }

        $data = ['key'  => 'send_email',
                'value' => $inputs['send_email'],
                'menu'  => 'contactus'];

        $data2 = ['key'  => 'email',
                'value' => $inputs['email'],
                'menu'  => 'contactus'];

        !empty($menuSettings->id) ? $menuSettings->update($data) : $this->menuSettings->create($data);

        !empty($menuSettings2->id) ? $menuSettings2->update($data2) : $this->menuSettings->create($data2);

	    $menu = 'contactus';

	    // TEXT NAME MUST BE SAME WITH THIS ARRAY
	    $keyArray = array('title_caption','title_main','sub_title', 'banner_image','title_content','title_contact','sub_title_contact', 'address','maps');

	    foreach (langs() as $key => $value) {
	    	
	    	for ($x=0; $x < count($keyArray); $x++) { 

	            if (!empty($inputs['lang'][$key])) {
	                
	                $upRow = $this->menuSettings
	                            ->where('lang',$inputs['lang'][$key])
	                            ->where('key',$keyArray[$x])
	                            ->where('menu',$menu)
	                            ->first();

	                $data = [
	                    'lang'=> $inputs['lang'][$key],
	                    'key'=> $keyArray[$x],
	                    'menu'=> $menu,
	                ];

	                if ($keyArray[$x] == 'banner_image') {

	                    if (!empty($inputs['banner_image'][$key])) {

	                        $data['value'] = image_setting($inputs['banner_image'][$key],$this->menuSettings,$request);   
	                        
	                    }

	                }else{

	                    $data['value'] = $inputs[$keyArray[$x]][$key];
	                }

	                !empty($upRow->id) ? $upRow->update($data) : $this->menuSettings->create($data);

	            }else{

	                $upRow = $menuSettings
	                            ->where('key',$keyArray[$x])
	                            ->where('menu',$menu)
	                            ->first();

	                $data = [
	                    'key'=> $keyArray[$x],
	                    'menu'=> $menu,
	                    'value'=> $inputs[$keyArray[$x]][$key],
	                ];

	                !empty($upRow->id) ? $upRow->update($data) : $this->menuSettings->create($data);

	            }

	        }

	    }
        return redirect(urlBackendAction('index'))->with('success','Data has been saved');
    }

   public function getDelete($id)
    {
       $model = $this->model->findOrFail($id);

        return $this->delete($model,[]);
    }

    public function getExportdata()
    {
        $model = $this->model->select('name','email','message','created_at')->get();

        Excel::create('Contact Form Data', function($excel) use($model) {

            $excel->sheet('Contact Data', function($sheet) use($model) {

                $sheet->fromArray($model);

            });

        })->export('xls');
    }	
}
