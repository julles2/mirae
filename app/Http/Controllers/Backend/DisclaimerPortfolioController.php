<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Backend\WebarqController;
use App\Models\StaticModel;

class DisclaimerPortfolioController extends WebarqController
{
	public function __construct(StaticModel $model)
	{
		parent::__construct();
		$this->model = $model;
		$this->group = 'disclaimer';
	}

    public function getIndex()
    {
    	$data = $this->model->getByGroup($this->group);
    	return view('backend.disclaimer.portfolio',[
    		'model'=>$this->model,
    		'data'=>$data,
    	]);
    }

    public function postIndex(Request $request)
    {
    	$this->updateStaticPage($request->all(),$this->group);
    	return $this->backSuccess();
    }
}
