<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Http\Requests;
use App\Http\Controllers\Backend\WebarqController;
use App\Models\FundInfo;
use App\Models\FundImage;
use App\Models\FundInfoDetail;
use App\Models\FundDetailCompany;
use App\Models\MenuSettings;
use Table;
use Image;
use webarq;

class ReksaDanaController extends WebarqController
{
	public function __construct(FundInfo $model,FundInfoDetail $detail,FundImage $img,FundDetailCompany $company,MenuSettings $menuSettings)
    {
    	parent::__construct();
        $this->model = $model;
        $this->detail = $detail;
        $this->path = 'backend.product.reksadana.';
        $this->img = $img;
        $this->company = $company;
        $this->menuSettings = $menuSettings;
    }

    public function getData()
    {
        $model = $this->model->select('id','fund_cd','fund_name');

        $data = Table::of($model)
        ->addColumn('action' , function($model){

            return \webarq::buttons($model->id);

        })
        ->make(true);

        return $data;
    }

    public function getIndex()
    {
        $setting = menu_setting('produk',null);

        $settingCompare = menu_setting('produk-compare',null);

        $detail = function($lang)use($setting){

            $setting = $setting->where('lang',$lang)->first();

            if(!empty($setting->id))
            {
                return $setting;
            }

        };

        $detailCompare = function($lang)use($settingCompare){

            $settingCompare = $settingCompare->where('lang',$lang)->first();

            if(!empty($settingCompare->id))
            {
                return $settingCompare;
            }

        };

        return view($this->path.'index',compact('setting','detail','settingCompare','detailCompare'));
    }

    public function postIndex(Requests\Backend\MenuSettingsRequest $request)
    {
        $inputs = $request->all();

        if ($inputs['jenis'] == 'list') {
            $save_setting = save_setting($this->menuSettings,'produk', $request);
        }else{
            $save_setting = save_setting($this->menuSettings,'produk-compare', $request);
        }

        return redirect(urlBackendAction('index'))->with('success','Data has been saved');
    }

    public function getUpdate($id)
    {
        $model = $this->model->findOrFail($id);

        $cekImage = $model->get_image;
        $detail = $this->detail
                    ->where('fund_id',$model->id)
                    ->where('lang','en')
                    ->orderBy('lang','asc')
                    ->get();

        return view($this->path.'_form',
                    compact('model','cekImage')
        );
    }


    public function postUpdate(Requests\Backend\ReksaDanaRequest $request,$id)
    {
        $model = $this->model->findOrFail($id);

        $inputs = $request->all();

        if(!empty($model->id))
        {
            if (!empty($inputs['fund_fact_sheet']) && $inputs['fund_fact_sheet']!= null)
            {
                $inputs['fund_fact_sheet'] = $this->filePDF($request,'fund_fact_sheet',$model,'fund_fact_sheet');
                $dataModel['fund_fact_sheet'] = $inputs['fund_fact_sheet'];
            }

            if (!empty($inputs['prospectus']) && $inputs['prospectus'] != null)
            {
                $inputs['prospectus'] = $this->filePDF($request,'prospectus',$model,'prospectus');
                $dataModel['prospectus'] = $inputs['prospectus'];
            }

            if (!empty($dataModel))
            {
                if (!empty($model->id))
                {
                    $model->update($dataModel);
                }else{
                    $this->model->create($dataModel);
                }
            }
        }



        $cekImage = $this->img->where('fund_cd',$model->fund_cd)->first();

        if (!empty($inputs['image']))
        {
            $inputs['image'] = $this->handleUpload($request,$cekImage,'image',[251,85]);
        }

        // ==== FUND IMAGES ==== //
        if (!empty($inputs['image']))
        {
            if(!empty($cekImage->id))
            {
                $cekImage->update([
                    'image'=>$inputs['image'],
                ]);
            }else{
                $this->img->create([
                    'fund_cd'=>$model->fund_cd,
                    'image'=>$inputs['image'],
                ]);
            }
        }

        // ==== INFO PERUSAHAAN / COMPANY INFO ==== //
        if(!empty($inputs['idCompany']))
        {
            $jumlah = count($inputs['idCompany']);
            for ($i=0; $i < $jumlah; $i++) {

                $companyUpRow = $this->company->find($inputs['idCompany'][$i]);

                $image = $request->file('imageCompany'.$i);

                if(!empty($image))
                {
                     if(!empty($companyUpRow->image))
                        {
                            @unlink(public_path('contents/'.$companyUpRow->image));
                        }

                    $imageName = randomImage().'.'.$image->getClientOriginalExtension();

                    $image = \Image::make($image);

                    $image = $image->resize(251,85);

                    $image = $image->save(public_path('contents/'.$imageName));

                    $image = $imageName;

                }else{

                    if(!empty($companyUpRow->image))
                    {
                        $image = $companyUpRow->image;
                    }else{
                        $image = null;
                    }
                }

                $dataCompany = [
                    'fund_id'=>$model->id,
                    'lang'=>$inputs['langCompany'][$i],
                    'image'=>$image,
                    'description'=>$inputs['descriptionCompany'][$i],
                ];

                if (!empty($companyUpRow->id)) {

                    $companyUpRow->update($dataCompany);

                }else{

                     $this->company->create($dataCompany);

                }

            }
        }

        return redirect(urlBackendAction('index'))->with('success','Data has been saved');
    }

    public function getHapus($id)
    {
        try {

            $details = $this->detail->findOrFail($id);

            $details = $this->detail->whereFund_id($details->fund_id)
                                    ->whereId_seq($details->id_seq);

            $details->delete();

            $response = array();
            $response['status'] = 'success';
            $response['message'] = 'Data Has Been Deleted';

        } catch (Exception $e) {

            $response = array();
            $response['status'] = 'failed';
            $response['message'] = 'Data Cannot Be Deleted';

        }


        return $response;
    }

    public function getHapuspdf($id, $jenis)
    {
        try {

            $model = $this->model->findOrFail($id);

            @unlink(public_path('contents/pdf/'.$model->$jenis));

            $data = [$jenis => ''];

            $model->update($data);

            $response = array();
            $response['status'] = 'success';
            $response['message'] = 'Data Has Been Deleted';

        } catch (Exception $e) {

            $response = array();
            $response['status'] = 'failed';
            $response['message'] = 'Data Cannot Be Deleted';

        }


        return $response;
    }

    private function filePDF($request,$nameFile,$model,$field)
    {
        $file = $request->file($nameFile);

        if(!empty($file))
        {
            if(!empty($model->field))
            {
                @unlink(public_path('contents/pdf/'.$model->field));
            }

            $fileName = str_random(6).date('Ymdhis').'.'.$file->getClientOriginalExtension();

            $file = $file->move(public_path('contents/pdf'),$fileName);

            $file = $fileName;

        }else{

            if(!empty($model->field))
            {
                $file = $model->field;
            }else{
                $file = null;
            }
        }

        return $file;

    }

    public function postSaveRow(Request $request,$id)
    {
        $edit = get('edit');

        $model = $this->model->findOrFail($id);
        $header_id = str_random(10);
        foreach(langs() as $key => $val)
        {
            if(empty($edit))
            {
                $create = $model->info_details()->create([
                    'lang'=>$key,
                    'title1'=>$request->title1[$key],
                    'title2'=>$request->title2[$key],
                    'description'=>$request->description[$key],
                    'header_id'=>$header_id,
                ]);
            }else{
                $update = $model->info_details()->where("header_id",$edit)->where('lang',$key)->first();
                $update->update([
                    'lang'=>$key,
                    'title1'=>$request->title1[$key],
                    'title2'=>$request->title2[$key],
                    'description'=>$request->description[$key],
                    'header_id'=>get('edit'),
                ]);
            }

        }
        $action = !empty(get('edit')) ? "Updated" : "Saved";
        return redirect()->back()->with('success','Row has been '.$action);
    }

    public function getShowUpdateRow($headerId)
    {
        $model = $this->detail->where('header_id',$headerId)->get();
        $result = [];
        foreach($model as $row)
        {
            $result[$row->lang] = [
                'title1'=>$row->title1,
                'title2'=>$row->title2,
                'description'=>$row->description,
            ];
        }

        return response()->json([
            'result'=>$result,
        ]);
    }

    public function getDeleteRow($headerId)
    {
        $model = $this->detail->where('header_id',$headerId)->delete();

        return redirect()->back()->with('success','Row has been deleted');
    }

}
