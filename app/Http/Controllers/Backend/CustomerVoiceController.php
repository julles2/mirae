<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Backend\WebarqController;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\CustomerVoice;
use App\Models\MenuSettings;
use Table;
use webarq;
use DB;
use Excel;

class CustomerVoiceController extends WebarqController
{
    public function __construct(CustomerVoice $model,MenuSettings $menuSettings)
    {
    	parent::__construct();
        $this->model = $model;
        $this->menuSettings = $menuSettings;
        $this->path = 'backend.customer_voice.';
    }

    public function getData()
    {
    	$model = $this->model->get();

    	$data = Table::of($model)
		->addColumn('action' , function($model){
    		return \webarq::buttons($model->id);
    	})
		->make(true);

		return $data;
    }

    public function getIndex()
    {
    	$setting = $this->menuSettings->whereMenu('customer_v')->whereKey('send_email')->first();
        $email = $this->menuSettings->whereMenu('customer_v')->whereKey('email')->first();

    	$isCheck = null;

    	if (@$setting->value == '1') {
    		$isCheck = true;
    	}

    	return view($this->path.'index',compact('setting','isCheck', 'email'));
    }

    public function postIndex(Request $request)
    {
    	$inputs = $request->all();

    	$menuSettings = $this->menuSettings->whereMenu('customer_v')
    										->whereKey('send_email')
    										->first();

        $menuSettings2 = $this->menuSettings->whereMenu('customer_v')
                                            ->whereKey('email')
                                            ->first();

    	if (!empty($inputs['send_email'])) {
    		$inputs['send_email'] = true;
    	}else{
    		$inputs['send_email'] = false;
    	}

    	$data = ['key' 	=> 'send_email',
    			'value' => $inputs['send_email'],
    			'menu' 	=> 'customer_v'];

        $data2 = ['key'  => 'email',
                'value' => $inputs['email'],
                'menu'  => 'customer_v'];

        !empty($menuSettings->id) ? $menuSettings->update($data) : $this->menuSettings->create($data);

        !empty($menuSettings2->id) ? $menuSettings2->update($data2) : $this->menuSettings->create($data2);

        return redirect(urlBackendAction('index'))->with('success','Data has been saved');
    }

    public function getView($id)
    {
    	$model = $this->model->whereId($id)->first();

    	return view($this->path.'detail',compact('model'));
    }

    public function getDelete($id)
    {
    	$model = $this->model->find($id);

    	$model->delete();

    	return redirect(urlBackendAction('index'))->with('succes','Data has been deleted');
    }

    public function getExportdata()
    {
        $model = $this->model->select('name','email','message','created_at')->get();

        Excel::create('Customer Voice', function($excel) use($model) {

            $excel->sheet('Data Customer Voice', function($sheet) use($model) {

                $sheet->fromArray($model);

            });

        })->export('xls');
    }   
}
