<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Backend\WebarqController;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\NewsCategoryTranslations;
use App\Models\NewsCategories;
use Table;
use Image;
use webarq;

class NewsCategoriesController extends WebarqController
{
    public function __construct(NewsCategories $model, NewsCategoryTranslations $categories)
    {
    	parent::__construct();
        $this->model = $model;
        $this->categories = $categories;
        $this->path = 'backend.news-categories.';
    }

    public function getData()
    {

    	$model = $this->model->select('news_categories.id','news_category_translations.category','news_category_translations.lang')
			->join('news_category_translations',
						'news_category_translations.category_id','=','news_categories.id')
			->where('news_category_translations.lang','id');
		$data = Table::of($model)
		
		->addColumn('action' , function($model){
    		return \webarq::buttons($model->id);
    	})
		->make(true);

		return $data;
    }

    public function getIndex()
    {
    	return view($this->path.'index');
    }

	public function getCreate()
	{

		$model = $this->model;
	
		return view($this->path.'_form',compact('model'));
	}

	public function postCreate(Requests\Backend\NewsCategoriesRequest $request)
	{
        $model = $this->model;
		$categories = $this->categories;
        $model->color = $request->color;
		$model->created_at = date("Y-m-d H:i:s");
		$model->save();

		$inputs = $request->all();

        foreach (langs() as $key => $value) {
            $upRow = $this->categories->find($inputs['id'][$key]);

            $data = [
                'category_id'=>$model->id,
                'lang'=>$inputs['lang'][$key],
                'category'=>$inputs['category'][$key],
            ];

            $this->categories->create($data);
        }

        return redirect(urlBackendAction('index'))->with('success','Data has been saved');

	}

	public function getUpdate($id)
	{
		$model = $this->model->findOrFail($id);
		$categories = $this->categories->where('category_id',$model->id)->get();

		$detail = function($lang)use($categories){
          
            $categories = $categories
                ->where('lang',$lang)
                ->first();

            if(!empty($categories->id))
            {
                return $categories;
            }

        };

		return view($this->path.'_form',compact('model','categories','detail'));
	}

	public function postUpdate(Requests\Backend\NewsCategoriesRequest $request,$id)
	{
		$model = $this->model->findOrFail($id);
        $model->color = $request->color;
        $model->update();
		$inputs = $request->all();

		foreach (langs() as $key => $value) {
            $upRow = $this->categories->find($inputs['id'][$key]);

            $data = [
                'lang'=>$inputs['lang'][$key],
                'category'=>$inputs['category'][$key],
            ];

            if (!empty($upRow->id)) {

                $upRow->update($data);

            }else{

                 $this->detail->create($data);

            }
        }

        return redirect(urlBackendAction('index'))->with('success','Data has been updated');
	}

	public function getDelete($id)
    {
       $model = $this->model->findOrFail($id);

       $categories = $this->categories->where('category_id',$model->id);

       $categories->delete();

        return $this->delete($model,[]);
    }
}
