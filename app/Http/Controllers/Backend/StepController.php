<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Backend\WebarqController;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Step;
use App\Models\StepTranslations;
use Table;
use webarq;
use DB;


class StepController extends WebarqController
{
    public function __construct(Step $model, StepTranslations $stepTranslations)
    {
    	parent::__construct();
        $this->model = $model;
        $this->stepTranslations = $stepTranslations;
        $this->path = 'backend.step.';
    }

    public function getData()
    {
    	$model = $this->model->select('steps.id',
    								  DB::raw('(CASE WHEN steps.status = "y" THEN "Publish" ELSE "Un Publish" END) AS status'),
    								  'step_translations.lang',
    								  'step_translations.title',
    								  'step_translations.wording')
    						->from('steps')
    						 ->join('step_translations','step_translations.step_id' ,'=', 'steps.id')
    						 ->where('step_translations.lang','id');

    	$data = Table::of($model)
		->addColumn('action' , function($model){
    		return \webarq::buttons($model->id);
    	})
		->make(true);

		return $data;
    }

    public function getIndex()
    {
        $model = $this->model->get();

        $allowAdd = count($model) == 3 ? false : true;

    	return view($this->path.'index',compact('allowAdd'));
    }

    public function getCreate()
    {
    	$model = $this->model;

    	return view($this->path.'_form',compact('model'));
    }

    public function postCreate(Requests\Backend\StepRequest $request)
    {

    	$model = $this->model;

		$inputs = $request->all();

		$model->status = $inputs['status'];

        if (!empty($inputs['icon'])) {

            $inputs['icon'] = $this->handleUpload($request,$model,'icon',[70,70]);

            $model->icon = $inputs['icon'];
        }

		$model->save();

		$stepTranslations = $this->stepTranslations;

		foreach (langs() as $key => $value) {
            $data = [
                    'step_id'=>$model->id,
                    'title'  => $inputs['title'][$key],
                    'lang'=>$inputs['lang'][$key],
                    'wording'=>$inputs['wording'][$key],
                ];

            $this->stepTranslations->create($data);
        }

        return redirect(urlBackendAction('index'))->with('success','Data has been saved');
    }

    public function getUpdate($id)
    {
    	$model = $this->model->findOrFail($id);
    
	    $stepTranslations = $this->stepTranslations->where('step_id',$model->id)->get();

        $detail = function($lang)use($stepTranslations){
          
            $stepTranslations = $stepTranslations
                ->where('lang',$lang)
                ->first();

            if(!empty($stepTranslations->id))
            {
                return $stepTranslations;
            }

        };

    	return view($this->path.'_form',compact('model','stepTranslations','detail'));
    }

    public function postUpdate(Requests\Backend\StepRequest $request,$id)
    {
    	$model = $this->model->findOrFail($id);

		$inputs = $request->all();

		$dataModel['status']  = $inputs['status'];

        if (!empty($inputs['icon'])) {

            $inputs['icon'] = $this->handleUpload($request,$model,'icon',[70,70]);

            $dataModel['icon'] = $inputs['icon'];
        }

		$model->update($dataModel);

		foreach (langs() as $key => $value) {
            
            $upRow = $this->stepTranslations->find($inputs['id'][$key]);

                $data = [
                    'title'=>$inputs['title'][$key],
                    'wording'=>$inputs['wording'][$key],
                ];

            if (!empty($upRow->id)) {

                $upRow->update($data);

            }else{

                 $this->stepTranslations->create($data);

            }

        }

        return redirect(urlBackendAction('index'))->with('success','Data has been saved');
    }

   public function getDelete($id)
    {
       $model = $this->model->findOrFail($id);

       $stepTranslations = $this->stepTranslations->where('step_id',$model->id);

       $stepTranslations->delete();

        return $this->delete($model,[]);
    }
}
