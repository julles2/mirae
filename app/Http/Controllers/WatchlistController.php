<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Models\Watchlists;

class WatchlistController extends Controller
{
	public function __construct()
    {
        \App::setLocale(lang());

        $this->watch = new Watchlists();
    }

    public function getAddwatchlists(Request $request)
    {
    	$fund_cd = $request->fund_cd;

		$acnt_no = acnt_no();

		if(!empty($acnt_no))
		{

			$data = $this->watch->where('acnt_no',$acnt_no)
								->where('fund_cd',$fund_cd)
								->first();

			if (!empty($data)) {

				return response()->json([
		    		'status'=>'success',
		            'success'=>'Item exist in your watchlist',
		            'fund_cd'=>$fund_cd,
		            'url'=>'',
		        ]);

			}else{

				$data = $this->watch;

		        $data->fund_cd = $fund_cd;
				$data->acnt_no = $acnt_no;
				$data->save();


		    	return response()->json([
		    		'status'=>'success',
		            'success'=>'Item has been add to Watchlist',
		            'fund_cd'=>$fund_cd,
		            'url'=>'',
		        ]);

			}
		}else{

			$url = urlLang('user/login');

			return response()->json([
					'status'=>'failed',
		            'success'=>$acnt_no,
		            'url'=>$url,
		        ]);

		}
    }

    public function getCheckwishlist(Request $request)
    {
    	$fund_cd = get('fund_cd');

		$acnt_no = acnt_no();

    	$data = $this->watch->where('acnt_no',$acnt_no)
							->where('fund_cd',$fund_cd)
							->first();

		if (!empty($data->id)) {
			return "true";
		}else{
			"false";
		}
    }
}
