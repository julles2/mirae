<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use App\Http\Requests;
use App\Models\Company;
use App\Models\CompanyTranslations;

use DB;
use Table;

class AssetController extends Controller
{
    public function __construct()
    {
        \App::setLocale(lang());

        $this->company = new Company();
        $this->companyTranslations = new CompanyTranslations();
    }

    public function dataCompany()
    {
    	$asset = $this->company->select('company_translations.title','company_translations.description','company.id')
			->join('company_translations','company_translations.company_id','=','company.id')
			->where('company_translations.lang',lang())
			->where('company.status','y')
			->where('category','miraeasset');

		return $asset;
    }

    public function getIndex()
    {
        $asset = $this->dataCompany()->first();

        $setting = menu_setting('miraeasset',lang())->first();

        return view('asset.index',compact('asset','setting'));
    }
}
