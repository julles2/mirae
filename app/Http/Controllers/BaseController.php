<?php
/**
 * Created by PhpStorm.
 * User: DanielSimangunsong
 * Date: 5/16/2017
 * Time: 10:46 AM
 */

namespace App\Http\Controllers;


use Request;
use View;

class BaseController extends Controller
{
    protected $layout;

    protected $broker;

    public function __construct()
    {
        $this->broker = auth()->guard('broker')->user();

        view()->share([
                'broker' => $this->broker,
                'vLang' => lang()
        ]);
    }

    // public function getIndex($var1="",$var2="",$var3="")
    // {
    //     return view('home.index');
    // }

    /**
     * @param $view
     * @param array $params
     * @return mixed
     */
    protected function makeView($view, array $params = [])
    {
        return view($this->layout . '.' . $view, $params);
    }
}