<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Models\NewsTranslation;

class ChangeLangController extends Controller
{
    public function handleNews()
    {

    }

    public function index()
    {

        if(empty(get('to')))
        {
            abort(404);
        }

        if(!array_key_exists(get('to'),langs()))
        {
            abort(404);
        }

        $segments = get('segments');
        $explode = explode("/",$segments);
        $explode[1] = get('to');

        $route = $explode[2];

        if($route == 'news')
        {
            $slug = $explode[4];
            $slug = str_replace("?","",$slug);
            $newsBefore = NewsTranslation::where('slug',$slug)->first();
            $newsNew = $newsBefore->news->trans()->where('lang',get('to'))->firstOrFail();
            $explode[4] = $newsNew->slug;
        }

        $implode = implode("/",$explode);

        $fixUrl = url($implode);

        return redirect($fixUrl);
    }
}
