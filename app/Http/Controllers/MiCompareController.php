<?php

namespace App\Http\Controllers;

use App\Repository\MiCompare;

class MiCompareController extends Controller
{
    public function __construct()
    {
        $this->view = 'manajer_investasi.compare.';
        $this->repo = new MiCompare();
    }

    public function getIndex()
    {
        $range = collect($this->repo->rangeMonth());
        $data    = $this->repo->collectDataMonthly();
        $setting = menu_setting('manajer-compare', lang())->first();
        return view($this->view . 'index', [
            'selectMi'    => $this->repo->select_mi(),
            'range'       => $range->toJson(),
            'dataAum'     => $data->aum->toJson(),
            'dataAumUsd'  => $data->aum_usd->toJson(),
            'dataUnit'    => $data->unit->toJson(),
            'dataUnitUsd' => $data->unit_usd->toJson(),
            'repo'        => $this->repo,
            'setting'     => $setting,
        ]);
    }

    public function getFilterRange()
    {
        $startDate = parse(get('start_year') . zeroDate(get('start_month')) . '01');
        $endDate   = parse(get('end_year') . zeroDate(get('end_month')) . '01');
        $month     = $endDate->diffInMonths($startDate);
        $url       = urlLang('manager-investment-comare?' . $this->repo->queryStringCode() . '&month=' . $month . '&start=' . $startDate->format("Ymd") . '&end=' . $endDate->format("Ymd"));

        return redirect($url);
    }
}
