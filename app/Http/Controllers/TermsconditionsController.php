<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use App\Http\Requests;
use App\Models\Bantuan;
use App\Models\BantuanTranslation;

use DB;
use Table;

class TermsconditionsController  extends Controller
{
    public function __construct()
    {
        \App::setLocale(lang());

        $this->bantuan = new Bantuan();
        $this->bantuanTranslations = new BantuanTranslation();
    }

    public function dataTerms()
    {
    	$help = $this->bantuan->select('bantuans.id',
                                        'bantuans.order',
                                        'bantuans.status',
                                        'bantuan_translations.lang',
                                        'bantuan_translations.title',
                                        'bantuan_translations.description'
                                        )
            ->join('bantuan_translations','bantuan_translations.bantuan_id','=','bantuans.id')
            ->where('bantuan_translations.lang',lang())
            ->where('bantuans.status','y')
            ->where('bantuans.menu','terms-condition')
            ->first();

        return $help;
    }

    public function getIndex()
    {
        $about = $this->dataTerms();

        $setting = menu_setting('terms-condition',lang())->first();

        return view('global.termscondition',compact('about','setting'));
    }
}
