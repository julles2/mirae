<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Models\Milestones;
use App\Models\MilestonesTranslations;

use DB;
use Table;

class MilestonesController extends Controller
{
    public function __construct()
    {
        \App::setLocale(lang());

        $this->milestones = new Milestones();
        $this->milestonesTranslations = new MilestonesTranslations();
    }

    public function dataMilestone()
    {
    	$data = $this->milestones->select('milestones.id',
    									'milestones.order',
    								  	'milestones_translations.lang',
                                        'milestones_translations.title',
                                        'milestones_translations.image',
    								  	'milestones_translations.year',
    								  	'milestones_translations.slug',
    								  	'milestones_translations.description'
    								  	)
			->join('milestones_translations','milestones_translations.milestone_id','=','milestones.id')
			->where('milestones_translations.lang',lang())
			->where('milestones.status','y')
            ->orderBy('milestones.order','asc');

		return $data;
    }

    public function getIndex()
    {
        $milestone = $this->dataMilestone()->get();

        $setting = menu_setting('milestone',lang())->first();

        return view('milestone.index',compact('milestone','setting'));
    }
}
