<?php

namespace App\Http\Controllers;
use Mirae;
use App\Models\FundImage;
use App\Models\FundInfo;
use App\Models\Im;
use App\Models\ImData;
use App\Models\ImDepartments;
use App\Models\ImDetailCompany;
use App\Models\ImImages;
use App\Models\ImInfoDetails;
use App\Models\ImStakeholders;
use App\Repository\Mi;
use Illuminate\Http\Request;

class ManajerInvestasiController extends Controller
{
    public function __construct()
    {

        $this->view             = 'manajer_investasi.';
        $this->data             = new ImData();
        $this->im               = new Im();
        $this->details          = new ImInfoDetails();
        $this->companies        = new ImDetailCompany();
        $this->stakeholder      = new ImStakeholders();
        $this->department       = new ImDepartments();
        $this->modelFund        = new FundInfo();
        $this->funImage         = new FundImage();
        $this->imImage          = new ImImages();
        $this->mi               = new Mi();
        $this->limit            = 15;
        $this->defaultStartDate = carbon()->parse(Mirae::oneYearAgo(date("Y-m-d")))->format("Y-m");
        $this->defaultEndDate   = date("Y-m");
    }

    public function fundList($im_cd)
    {
        $model = $this->modelFund
            ->select('fund_infos.fund_cd', 'im_nm', 'fund_name', 'nav_per_unit', 'one_day', 'one_month', 'one_years')
            ->join('im', 'im.id', '=', 'fund_infos.im_id')
            ->join('daily_navs', 'daily_navs.fund_id', '=', 'fund_infos.id')
            ->where('im_cd', $im_cd)
            ->get();
        return $model;
    }

    public function queryReload($imName = "", $limit = "", $offset = "")
    {
        $data = $this->im
            ->where('im_nm', 'like', '%' . $imName . '%')
            ->limit($limit)
            ->offset($offset)
            ->get()
            ->unique('im_cd');
        return $data;
    }

    public function getReloadTable()
    {
        $limit  = get('limit');
        $offset = get('offset');
        $imName = get('im_name');

        $data = $this->queryReload($imName, $limit, $offset);

        $view = view($this->view . 'ajax.listing', compact('data'))->render();
        return response()
            ->json([
                'listing' => $view,
            ]);
    }

    public function getIndex()
    {
        $limit = $this->limit;
        $count = $this->mi->listing()->get()->count();

        $setting = menu_setting('manajer-investasi', lang())->first();
        return view($this->view . 'index', compact('setting', 'limit', 'count'));
    }

    public function getAjaxReloadChart()
    {
        $from  = get('from');
        $to    = get('to');
        $im_cd = get('im_cd');

        return response()->json([
            'chartDates' => $this->mi->dateFromTo($from, $to),
            'chartIdr'   => $this->mi->charts($im_cd, $from, $to, "idr"),
            'chartUsd'   => $this->mi->charts($im_cd, $from, $to, "usd"),
        ]);
    }

    public function getAjaxPop()
    {
        $im_cd = request()->get('im_cd');

        $im = $this->im->whereIm_cd($im_cd)->first();

        $modelInfoCompany = $this->companies
            ->whereLang(lang())
            ->whereIm_cd($im_cd)->first();

        $infoCompany = view('manajer_investasi.ajax.info_company', [
            'modelInfoCompany' => $modelInfoCompany,
        ])
            ->render();

        // === GENERAL INFO POPUP === //
        $modelInfoGeneral = $this->details
            ->whereLang(lang())
            ->whereIm_cd($im_cd)->first();

        // === GENERAL INFO STAKEHOLDER === //
        $modelStakeHolder = $this->stakeholder->whereIm_cd($im_cd)->get();


        // === GENERAL INFO DEPARTMENT === //
        $modelDepartment = $this->department->whereIm_cd($im_cd)->get();
        $imName = function ($im_cd) use ($modelInfoCompany) {

            $imMaster = $this->im
                ->whereIm_cd($im_cd)
                ->first();

            if (!empty($imMaster->id)) {
                return $imMaster->im_nm;
            }

        };

        $infoGeneral = view('manajer_investasi.ajax.info_general', [
            'modelInfoGeneral' => $modelInfoGeneral,
            'stakeHolder'      => $modelStakeHolder,
            'department'       => $modelDepartment,
            'imName'           => $imName,
        ])
            ->render();

        // === FUND LIST === //
        $fundList = $this->fundList($im_cd);
        //$fundList = $im->funds;
        $fundImage = function ($fundCd) {
            return $this->funImage->where('fund_cd', $fundCd)->first();
        };

        $infoFundList = view('manajer_investasi.ajax.info_fund_list', [
            'modelFundList' => $fundList,
            'fundImage'     => $fundImage,
        ])
            ->render();

        $imageIm = $this->imImage->where('im_cd', $im_cd)->first();
        $imgIm   = !empty($imageIm->image) ? asset('contents/' . $imageIm->image) : asset('noimage.png');
        $vars    = [
            'div_info_companies' => $infoCompany,
            'div_info_general'   => $infoGeneral,
            'div_info_fund'      => $infoFundList,
            'imImage'            => $imgIm,
            'im_Name'            => @$im->im_nm,
            'im_cd'              => @$im->im_cd,
        ];

        return response()->json([
            'vars'       => $vars,
            'chartDates' => $this->mi->dateFromTo($this->defaultStartDate, $this->defaultEndDate),
            'chartIdr'   => $this->mi->charts($im_cd, $this->defaultStartDate, $this->defaultEndDate, ""),
            'chartUsd'   => $this->mi->charts($im_cd, $this->defaultStartDate, $this->defaultEndDate, "usd"),
        ]);
    }
}
