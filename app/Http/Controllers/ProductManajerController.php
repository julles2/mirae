<?php

namespace App\Http\Controllers;

use App\Models\Oracle\InvestmentManagerInfo;

class ProductManajerController extends Controller
{
    public $model;

    public $view;

    public function __construct(InvestmentManagerInfo $model)
    {
        $this->model = $model;

        $this->view = 'product_manajer.';
    }

    public function getAjaxListingManajer()
    {
        $queryLists = $this->model
            ->limit(15)
            ->get();

        $listing = view($this->view . 'ajax.listing', [
            'lists' => $queryLists,
        ])
            ->render();

        return response()->json([
            'listing' => $listing,
        ]);
    }

    public function getIndex()
    {
        return view($this->view . 'index');
    }

}
