<?php

namespace App\Http\Controllers;

use App\Http\Controllers\BaseController;
use App\Http\Requests;
use App\Models\CustomerVoice;
use App\Models\FundImage;
use App\Models\MenuSettings;
use App\Models\News;
use App\Models\NewsCategoryTranslations;
use App\Models\NewsTranslation;
use App\Models\Sliders;
use App\Models\SliderTranslations;
use App\Models\Step;
use App\Models\StepTranslations;
use App\Models\Watchlists;
use App\Repository\Home;
use Mail;

class HomeController extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        \App::setLocale(lang());

        $this->repo               = new Home();
        $this->mainSlider         = new Sliders();
        $this->SliderTranslations = new SliderTranslations();

        $this->mainNews         = new News();
        $this->newsTranslations = new NewsTranslation();

        $this->categoryNews = new NewsCategoryTranslations();

        $this->mainStep         = new Step();
        $this->stepTranslations = new StepTranslations();

        $this->funImage = new FundImage();

        $this->contact = new CustomerVoice();

        $this->watchlists = new Watchlists();

        $this->menu_setting = new MenuSettings();
    }

    public function getIndex()
    {
        $repo                = $this->repo;
        $selectTypeReksaDana = ['-' => trans('home.mutual_fund_type'),'' => trans('global.all')] + $repo->selectTypeFund();

        $mainSlider = $this->mainSlider->whereStatus('y')->get();

        $slider = function ($lang, $sliderID) use ($mainSlider) {
            $slider = $this->SliderTranslations
                ->whereLang($lang)
                ->whereSlider_id($sliderID)
                ->first();

            if (!empty($slider->id)) {
                return $slider;
            }
        };

        $mainNews = $this->mainNews
            ->select('news.date', 'news.category_id', 'news.status', 'news.id', 'news.image_intro','news_categories.color','news_translations.title','news_translations.intro','news_category_translations.category','news_translations.slug')
            ->join('news_translations', 'news.id', '=', 'news_translations.news_id')
            ->join('news_categories','news_categories.id','=','news.category_id')
            ->join('news_category_translations','news_category_translations.category_id','=','news_categories.id')
            ->where('news.status', 'y')
            ->where('news_category_translations.lang',lang())
            ->where('news_translations.lang', lang())
            ->limit(3)
            ->orderBy('news.date', 'desc')
            ->get();
        // dd($mainNews);
        $mainStep = $this->mainStep->whereStatus('y')->limit(3)->get();

        $steps = function ($lang, $stepID) use ($mainStep) {
            $steps = $this->stepTranslations
                ->whereLang($lang)
                ->whereStep_id($stepID)
                ->first();

            if (!empty($steps->id)) {
                return $steps;
            }
        };

        $fundImage = function ($fundCd) {
            return $this->funImage->where('fund_cd', $fundCd)->first();
        };

        $checkWishlist = function ($fundCd, $acnt) {
            $data = $this->watchlists->where('fund_cd', $fundCd)
                ->where('acnt_no', $acnt)
                ->first();
            if (!empty($data->id)) {
                return 'style="pointer-events: none;cursor: default;"';
            } else {
                return "false";
            }
        };

        $video = $this->menu_setting->display_by_lang('video');
        
        return view('home.index', [
            'returnHarian'        => $repo->listingReturnHarian(),
            'selectTypeReksaDana' => $selectTypeReksaDana,
            'repo'                => $repo,
            'mainSlider'          => $mainSlider,
            'slider'              => $slider,
            'mainNews'            => $mainNews,
            'news'                => $mainNews,
            'mainStep'            => $mainStep,
            'steps'               => $steps,
            'fundImage'           => $fundImage,
            'checkWishlist'       => $checkWishlist,
            'video'               => $video,
        ]);
    }

    public function getAjaxFund()
    {
        $type  = get('type');
        $im_id = get('im_id');
        $model = $this->repo->selectFund($im_id, $type);
        $html  = "";
        foreach ($model as $key => $val) {
            $html .= "<option value = '" . $key . "'>" . $val . "</option>";
        }

        return response()->json([
            'result' => $html,
        ]);
    }

    public function getAjaxChangeType()
    {
        $repo                      = $this->repo;
        $repo->typeReksadanaHarian = get('type');
        $model                     = $repo->performaReksaDanaHarian();

        $fundImage = function ($fundCd) {
            return $this->funImage->where('fund_cd', $fundCd)->first();
        };
        $view = view('home.ajax.performa_reksa_dana_harian', [
            'model'     => $model,
            'fundImage' => $fundImage,
        ])
            ->render();

        return response()->json([
            'result' => $view,
        ]);
    }

    public function postIndex(Requests\ContactusRequest $request, $lang)
    {
        $contact = $this->contact;

        $inputs = $request->all();

        $contact->name = $inputs['name'];

        $contact->email = $inputs['email'];

        $contact->message = $inputs['message'];

        $setting = $this->menu_setting
            ->whereMenu('customer_v')
            ->whereKey('send_email')
            ->first();

        if ($setting->value) {
            $email = $this->menu_setting
                ->whereMenu('customer_v')
                ->whereKey('email')
                ->first();

            $emailReceiver = "customerservice@miraeasset.co.id";

            if (!empty($email)) {
                $emailReceiver = $email->value;
            }

            Mail::send(
                'help.email',
                array(
                    'name'         => $request->get('name'),
                    'email'        => $request->get('email'),
                    'body_message' => $request->get('message')),
                function ($message) use ($emailReceiver) {
                    $message->from('customerservice@miraeasset.co.id');

                    $message->to($emailReceiver);

                    $message->subject("Mirrae Customer Voice");
                }
            );
        }

        $contact->save();

        return redirect(urlLang('home'))->with('success', trans('home.data-has-been-sent'));
    }
}
