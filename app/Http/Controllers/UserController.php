<?php
namespace App\Http\Controllers;

use App;
use App\Models\Customer;
use App\Models\Oracle\ClientInfo;
use App\Models\Oracle\VCustInfo;
use App\Models\Question;
use App\Presenters\CartPresenter;
use App\Presenters\TrackLoginPresenter;
use Request;
use Session;

class UserController extends BaseController
{
    use CartPresenter;

    protected $layout = 'frontend.users';

    protected $view = 'profile';

    protected $broker;

    public $custInfo;

    public function __construct()
    {
        $this->middleware('broker:broker', ['except' => ['getLogout','getLogoutMid'], 'guard' => 'broker']);
        parent::__construct();
        $this->custInfo    = new VCustInfo();
        $this->clientInfo  = new ClientInfo();
        $this->question    = new Question();
        $this->track_login = new TrackLoginPresenter();
        view()->share('statusUser', $this->statusUser());
        view()->share('imageUser', $this->imageUser());
        view()->share('globalRiskProfile', $this->riskProfile());
    }

    public function riskProfile()
    {
        $clientInfo = ClientInfo::where('acnt_no', acnt_no())->first();

        if (!empty($clientInfo->risk_profile)) {
            return $clientInfo->result_risk_profile;
        }
    }

    public function imageUser()
    {
        $result = null;
        if (auth()->guard('broker')->check()) {
            $model  = Customer::where('acnt_no', @acnt_no())->first();
            $result = empty($model->photo) ? asset('frontend/default_user.png') : asset('contents/' . $model->photo);
        }
        return $result;
    }

    /**
     * @return mixed
     */

    public function getIndex()
    {
       return $this->getProfile();
    }

    public function checkQuestion()
    {
        $auth = auth()->guard('broker')->user();
        dd($auth);
    }

    public function getProfile()
    {
        // dd($this->track_login->handleExpire());
        if($this->track_login->handleExpire() == "expired")
        {
            return redirect(urlLang('user/logout'));
        }

        $acnt_no = "";

        if (empty($this->broker->accountData->accountNum)) {
            return redirect(urlLang('user/pin-login'));
        }

        $acnt_no = $this->broker->accountData->accountNum;

        $model = $this->custInfo->where('acnt_no', $acnt_no)
            ->first();

        $typeIdentitas = $this->custInfo->tipe_identitas($model->cid_sect);

        $idenExpire = function () use ($model) {
            if ($model->expr_date == '99991231') {
                $result = "SEUMUR HIDUP";
            } elseif ($model->expr_date == '00000000') {
                $result = '-';
            } else {
                $result = !empty($model->expr_date) ? carbon()->parse($model->expr_date)->format("d-M-Y") : '-';
            }

            return $result;
        };

        $expireColor = function () use ($model) {
            $result = 'black';
            if ($model->expr_date <= date("Ymd")) {
                $result = 'red';
            }

            return $result;
        };

        return $this->makeView($this->view, [
            'model'         => $model,
            'typeIdentitas' => $typeIdentitas,
            'idenExpire'    => $idenExpire,
            'expireColor'   => $expireColor(),
            'custInfo'      => $this->custInfo,
        ]);
    }

    public function postProfile()
    {
        $request = request();
        $this->validate($request, ['photo' => 'required|image']);
        $strName   = str_random(15);
        $image     = $request->file('photo');
        $imageName = $strName . "." . $image->getClientOriginalExtension();
        \Image::make($image)->resize(90, 90)->save(public_path('contents/' . $imageName));
        $cek = Customer::where('acnt_no', acnt_no())->first();
        if (!empty($cek->id)) {
            $cek->update(['photo' => $imageName]);
        } else {
            Customer::create(['acnt_no' => acnt_no(), 'photo' => $imageName]);
        }
        return redirect()->back()->with('success', 'Data has been updated');
    }

    /**
     * @param $view
     * @param array $params
     * @return mixed
     */
    public function makeView($view, array $params = [])
    {
        return parent::makeView($view, $params)->with('vMenu', $this->view);
    }

    /**
     * Log out handler
     *
     * @param string $lang
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function getLogout($lang = 'en')
    {
        // Session::pull('loginMember');
        // session()->forget('status_user');
        $this->cartStoreToDb();
        $this->track_login->destroy(acnt_no());
        \Cookie::queue(\Cookie::forget('login_time'));
        Session::flush();
        \Cache::flush();
        return redirect('/' . $lang . '/user/login');
    }

    public function getLogoutMid($lang='en')
    {

    }

    public function firstLoginOrNot($broker)
    {
        session()->forget('user_status');
        $data    = $broker->a31OI01GridOutRecVo[0];
        $acnt_no = $data->o_acnt_no;
        $name    = $data->o_acnt_nm;

        $model = $this->clientInfo
            ->where('acnt_no', $acnt_no)
            ->first();

        $result        = "not_completed";
        $questionMySql = $this->question->where('acnt_no', $acnt_no)->first();
        if (!empty($model->acnt_no)) {
            if (!empty($model->ifua_cd)) {
                $result = 'completed';
            } else {
                $result = 'pending';
            }
        } else {
            if (!empty($questionMySql->id)) {
                $result = "pending";
                if ($questionMySql->status == 'r') {
                    $result = 'not_completed';
                }
            } else {
                $result = 'not_completed';
            }
        }
        $data = (object) [
            'acnt_no' => $acnt_no,
            'status'  => $result,
            'name'    => $name,
            'email'   => $broker->email,
        ];

        session(['user_status' => $data]);

        return $result;
    }

    public function handleNotIfua()
    {
        dd(2);
    }

    /**
     * Login process handler
     *
     * @param string $lang
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */

    public function postLogin($lang = 'en')
    {
        $this->track_login->setTimeExpire();
        $to = request()->get('to');
        auth()->guard('broker')->attempt(['uid' => Request::input('uid'), 'pwd' => Request::input('pwd')]);
        $broker = auth()->guard('broker')->user();
        if (!$broker) {
            \Mirae::logCustomer((object) [
                'acnt_no'  => "unknown",
                'name'     => "unknown",
                'username' => Request::input('uid'),
                'status'   => 'failed',
            ]);

            return $this->getLogin()
                ->with('errorMsg', 'Please check your user ID or Password, login failed!');
        } else {
            $acnt_no         = !empty($broker->a31OI01GridOutRecVo[0]->o_acnt_no) ? $broker->a31OI01GridOutRecVo[0]->o_acnt_no : null;
            
            // Cache
                $cekCache = $this->track_login->loginCek($acnt_no);
                if($cekCache == false)
                {
                    return $this->getLogin()
                        ->with('errorMsg', trans('global.notif_cache_login_error')); 
                }
            //

            $acnt_no         = !empty($broker->a31OI01GridOutRecVo[0]->o_acnt_no) ? $broker->a31OI01GridOutRecVo[0]->o_acnt_no : null;
            $customerName    = !empty($broker->a31OI01GridOutRecVo[0]->o_acnt_nm) ? $broker->a31OI01GridOutRecVo[0]->o_acnt_nm : null;
            $firstLoginOrNot = $this->firstLoginOrNot($broker);
            $broker->storeInSession();
            \Mirae::logCustomer((object) [
                'acnt_no'  => $acnt_no,
                'name'     => $customerName,
                'username' => @$broker->membId,
                'status'   => 'success',
            ]);

            if ($firstLoginOrNot == 'not_completed') {
                return redirect(urlLang("user/question"));
            } elseif ($firstLoginOrNot == 'completed' || $firstLoginOrNot == 'pending') {

                return redirect(urlLang("user/pin-login?to=$to"));

                // if ($to == 'checkout') {
                //     return redirect(urlLang('checkout'));
                // } else {
                //     return redirect('/id/user/profile');
                // }
            }
        }
    }

    /**
     * @return mixed
     */
    public function getLogin()
    {
        if (isset($this->broker) && $this->broker->isValid()) {
            return redirect(App::getLocale() . '/user/profile');
        }

        $url = lang() . '/user/login?to=' . request()->get('to');

        return $this->makeView('login', [
            'url' => $url,
        ]);
    }

    public function statusUser()
    {
        $sessionUserStatus = session()->get('user_status');
        $statusUser        = !empty($sessionUserStatus->status) ? $sessionUserStatus->status : 'not_completed';
        if (auth()->guard('broker')->check()) {
            $acntNo        = $this->broker->a31OI01GridOutRecVo[0]->o_acnt_no;
            $modelQuestion = $this->question->where('acnt_no', $acntNo)->where('status', '!=', 'r')->first();
            if (!empty($modelQuestion->id)) {
                if (empty($modelQuestion->data_question)) {
                    $statusUser = 'question';
                } elseif (empty($modelQuestion->data_risk)) {
                    $statusUser = 'risk';
                } else {
                    //$statusUser = $modelQuestion->status == 'n' ? 'pending' : 'completed';
                    if ($modelQuestion->status == 'n') {
                        $statusUser = 'pending';
                    } else {
                        $client = $this->clientInfo->where('acnt_no', $acntNo)->first();
						$model = $this->custInfo->where('acnt_no', $acntNo)->first();
                        if (!empty($client->ifua_cd) && $model->expr_date >= date("Ymd")) {
                            $statusUser = 'completed';
                        } else {
							$statusUser = 'idexpired';
						}
                    }
                }
           }
		
        }

        return $statusUser;
    }

    /**
     * Login using pin
     */

    public function postPinLogin()
    {
        $this->track_login->setTimeExpire();
        \Auth::logout();       
        // Init socket
        $socket = new App\Managers\SocketManager([
            'ano' => (new App\Managers\AesManager($this->broker->a31OI01GridOutRecVo[0]->o_acnt_no))->encrypt(),
            'apw' => (new App\Managers\AesManager(Request::input('pwd')))->encrypt(),
        ], config('mirae.ip_pin'));
        // Connect to the socket
        $socket->pinConnector();

        if ($socket->isError()) {
            return $this->getPinLogin()->with('errorMsg', $socket->getError());
        }

        $this->broker->setData('accountData', $socket->getItem());

        $this->broker->storeInSession();
        //\Cart::destroy();
        $this->getCartFromDb();
        // Redirect
        if (null !== ($redirect = Session::pull('redirect-url'))) {
            return redirect($redirect);
        } else {
            $this->track_login->store_cache();
            // Cache
                $cekCache = $this->track_login->loginCek(acnt_no());
                if($cekCache == false)
                {
                    return $this->getLogin()
                        ->with('errorMsg', trans('global.notif_cache_login_error')); 
                }
            //

            $to = request()->get('to');
            session()->put('pin', Request::input('pwd'));
            if ($to == 'checkout') {
                return redirect(urlLang('checkout'));
            } elseif ($to == 'nilai') {
                return redirect(urlLang('user/question/risk-nilai'));
            } else {
                $statusUser = $this->statusUser();
                if ($statusUser == 'risk') {
                    return redirect(urlLang('user/question/risk'));
                } elseif ($statusUser == 'question' || $statusUser == 'not_completed') {
                    return redirect(urlLang('user/question'));
                } else {
                    // return redirect(App::getLocale() . '/user/portfolio');
                    return redirect('id/user/portfolio');
                }
            }
        }
    }

    /**
     * Login using pin
     *
     * @return mixed
     */
    public function getPinLogin()
    {
        $status = session()->get('user_status');
        if (null !== $this->broker->getData('accountData') && !empty(session()->get('pin'))) {
            return redirect(App::getLocale() . '/user/profile');
        }

        $url = urlLang('user/pin-login?to=' . request()->get('to'));

        return $this->makeView('account-login', [
            'url' => $url,
        ]);
    }

    public function getCekLogin()
    {
        $result = 'false';
        if(!empty(session()->get('pin')))
        {
            $result = 'true';
        }
        return response()->json([
            'result'=>$result,
        ]);
    }
}
