<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use App\Http\Requests;
use App\Models\NewsTranslation;
use App\Models\CompanyTranslations;
use App\Models\BantuanTranslation;

use Illuminate\Pagination\BootstrapThreePresenter;

use DB;
use Table;

class SearchController extends Controller
{
    public function __construct()
    {
        \App::setLocale(lang());

        $this->news = new NewsTranslation();
        $this->company = new CompanyTranslations();
        $this->help = new BantuanTranslation();
    }

    public function getIndex($lang)
    {
        $parameter = $_GET['keyword'];

        $newsTitle = $this->news
                            ->select('title','description')
                            ->where('lang',lang())
                            ->where('title','LIKE','%'.$parameter.'%');
        $newsDescription = $this->news
                            ->select('title','description')
                            ->where('lang',lang())
                            ->where('description','LIKE','%'.$parameter.'%');

        // ABOUT US
        $companyTitle = $this->company
                                ->select('title','description')
                                ->join('company','company_translations.company_id','=' ,'company.id')
                                ->where('company_translations.lang',lang())
                                ->where('company.category','aboutus')
                                ->where('company_translations.title','LIKE','%'.$parameter.'%');

        $companyDescription = $this->company
                                ->select('title','description')
                                ->join('company','company_translations.company_id','=','company.id')
                                ->where('company_translations.lang',lang())
                                ->where('company.category','aboutus')
                                ->where('company_translations.description','LIKE','%'.$parameter.'%');

        // MIRAE ASSET
        $assetTitle = $this->company
                            ->select('title','description')
                            ->join('company','company_translations.company_id','=','company.id')
                            ->where('company_translations.lang',lang())
                            ->where('company.category','miraeasset')
                            ->where('company_translations.title','LIKE','%'.$parameter.'%');
        $assetDescription = $this->company
                            ->select('title','description')
                            ->join('company','company_translations.company_id','=','company.id')
                            ->where('company_translations.lang',lang())
                            ->where('company.category','miraeasset')
                            ->where('company_translations.description','LIKE','%'.$parameter.'%');

        // F.A.Q
        $faqTitle = $this->help
                            ->select('title','description')
                            ->join('bantuans','bantuan_translations.bantuan_id','=','bantuans.id')
                            ->where('bantuan_translations.lang',lang())
                            ->where('bantuans.menu','help')
                            ->where('bantuan_translations.title','LIKE','%'.$parameter.'%');
        $faqDescription = $this->help
                            ->select('title','description')
                            ->join('bantuans','bantuan_translations.bantuan_id','=','bantuans.id')
                            ->where('bantuan_translations.lang',lang())
                            ->where('bantuans.menu','help')
                            ->where('bantuan_translations.description','LIKE','%'.$parameter.'%');

        // DISCLAIMER
        $disclaimerTitle = $this->help
                            ->select('title','description')
                            ->join('bantuans','bantuan_translations.bantuan_id','=','bantuans.id')
                            ->where('bantuan_translations.lang',lang())
                            ->where('bantuans.menu','disclaimer')
                            ->where('bantuan_translations.title','LIKE','%'.$parameter.'%');
        $disclaimerDescription = $this->help
                            ->select('title','description')
                            ->join('bantuans','bantuan_translations.bantuan_id','=','bantuans.id')
                            ->where('bantuan_translations.lang',lang())
                            ->where('bantuans.menu','disclaimer')
                            ->where('bantuan_translations.description','LIKE','%'.$parameter.'%');


        // GLOSSARY
        $glossaryTitle = $this->help
                            ->select('title','description')
                            ->join('bantuans','bantuan_translations.bantuan_id','=','bantuans.id')
                            ->where('bantuan_translations.lang',lang())
                            ->where('bantuans.menu','glossary')
                            ->where('bantuan_translations.title','LIKE','%'.$parameter.'%');
        $glossaryDescription = $this->help
                            ->select('title','description')
                            ->join('bantuans','bantuan_translations.bantuan_id','=','bantuans.id')
                            ->where('bantuan_translations.lang',lang())
                            ->where('bantuans.menu','glossary')
                            ->where('bantuan_translations.description','LIKE','%'.$parameter.'%');

        // MUTUAL FUND
        $mutualfundTitle = $this->help
                            ->select('title','description')
                            ->join('bantuans','bantuan_translations.bantuan_id','=','bantuans.id')
                            ->where('bantuan_translations.lang',lang())
                            ->where('bantuans.menu','mutual_fund')
                            ->where('bantuan_translations.title','LIKE','%'.$parameter.'%');
        $mutualfundDescription = $this->help
                            ->select('title','description')
                            ->join('bantuans','bantuan_translations.bantuan_id','=','bantuans.id')
                            ->where('bantuan_translations.lang',lang())
                            ->where('bantuans.menu','mutual_fund')
                            ->where('bantuan_translations.description','LIKE','%'.$parameter.'%');

        $search = $newsTitle
                    ->union($newsDescription)
                    ->union($companyTitle)
                    ->union($companyDescription)
                    ->union($assetTitle)
                    ->union($assetDescription)
                    ->union($faqTitle)
                    ->union($faqDescription)
                    ->union($disclaimerTitle)
                    ->union($disclaimerDescription)
                    ->union($glossaryTitle)
                    ->union($glossaryDescription)
                    ->union($mutualfundTitle)
                    ->union($mutualfundDescription)
                    ->get();

        $jumlah = count($search);
        
        return view('search',compact('search','jumlah','parameter'));
    }

    public function getSearch($lang)
    {
        $keyword = $_GET['keyword'];

        $url = urlLang('search')."?keyword=".$keyword;

        return response()
            ->json([
                    'url' => $url]
            );
    }
}
