<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Models\CustomerVoice;
use App\Models\MenuSettings;
use Mail;

class ContactusController extends Controller
{
    public function __construct()
    {
        \App::setLocale(lang());

        $this->contact = new CustomerVoice();

        $this->menu_setting = new MenuSettings();
    }

    public function getIndex($lang)
    {
        $setting = menu_setting2('contactus', lang())->first();

        return view('help.contact-us.index', compact('setting'));
    }

    public function postIndex(Requests\ContactusRequest $request, $lang)
    {

        $contact = $this->contact;

        $inputs = $request->all();

        $contact->name = $inputs['name'];

        $contact->email = $inputs['email'];

        $contact->message = $inputs['message'];

        $setting = $this->menu_setting
            ->whereMenu('contactus')
            ->whereKey('send_email')
            ->first();

        if ($setting->value) {

            $email = $this->menu_setting
                ->whereMenu('contactus')
                ->whereKey('email')
                ->first();

            $emailReceiver = "customerservice@miraeasset.co.id";

            if (!empty($email)) {
                $emailReceiver = $email->value;
            }

            Mail::send('help.email',
                array(
                    'name'         => $request->get('name'),
                    'email'        => $request->get('email'),
                    'body_message' => $request->get('message'),
                ), function ($message) use ($emailReceiver) {

                    $message->from('customerservice@miraeasset.co.id');

                    $message->to($emailReceiver);

                    $message->subject("Mirrae Contact Us");

                });

        }

        $contact->save();

        return redirect(urlLang('contact-us'))->with('success', 'Pesan Anda telah Kami terima dan Anda akan dihubungi oleh staff Customer Service kami dalam waktu 2X24 jam pada hari dan jam kerja');
    }
}
