<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class MileageController extends Controller
{
	public function __construct()
	{
		\App::setLocale(lang());
	}

    public function getIndex()
    {
    	return view('mileage.index');
    }
}
