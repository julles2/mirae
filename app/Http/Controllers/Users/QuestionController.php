<?php
//021574
namespace App\Http\Controllers\Users;

use App\Http\Requests;
use App\Models\Question;
use Illuminate\Http\Request;
use App\Repository\Fund;
use App\Repository\Question as QuestionRepo;
use App\Models\Oracle\FundInfo;
use App\Http\Controllers\UserController;
use App\Models\Oracle\ClientInfo;
use App\Models\Oracle\VCashBal;

class QuestionController extends UserController
{
    public $userStatus;

    public function __construct(Question $model,FundInfo $fund)
    {
        $this->middleware('broker:broker,pin', ['except' => ['getIndex','postIndex'], 'guard' => 'broker']);
        $this->userStatus = session()->get('user_status');
        $this->model = $model;
        $this->view = 'frontend.users.first_login.';
        $this->broker = auth()->guard('broker')->user();
        $this->acnt_no = @$this->broker->a31OI01GridOutRecVo[0]->o_acnt_no;
        $this->fund = $fund;
        $this->repoFund = new Fund;
        $this->repoQuestion = new QuestionRepo;
        parent::__construct();

    }

    public function questions()
    {
        return [
            trans('login.religion'),
            trans('login.current_address'),
            trans('login.birth_country'),
            trans('login.nationality'),
        ];
    }

    public function getIndex()
    {
        $model = $this->model->whereAcntNo($this->acnt_no)->first();
        if(!empty($model->id))
        {
            if(empty($model->data_risk))
            {
                return redirect(urlLang("user/question/risk"));
            }
        }
            

        return view($this->view.'question',[
            'questions'=>$this->questions(),
            'religions'=>$this->repoQuestion->religions(),
            'countries'=>$this->repoQuestion->countries(),
        ]);
    }

    public function handlingQuestion($request)
    {
        $inputs = $request->all();
        unset($inputs['_token']);

        $dataQuestion = json_encode($inputs);

        $model = $this->model
            ->where('acnt_no',$this->userStatus->acnt_no)
            ->where('status','!=','r')
            ->first();

        $data = [
            'acnt_no'=>$this->userStatus->acnt_no,
            'name'=>$this->userStatus->name,
            'email'=>$this->userStatus->email,
            'data_question'=> $dataQuestion,
        ];
        
        if(!empty($model->id))
        {
            $model->update($data);
        }else{
            $model = $this->model->create($data);
        }

        return $model;
    }

    public function getNotify()
    {
        return view($this->view.'question_notify');
    }

    public function postIndex(Request $request)
    {
        $validation = [];

        foreach($this->questions() as $q)
        {
            $validation += [
                $q => 'required',
            ];
        }

        $this->validate($request,$validation);

        $model = $this->handlingQuestion($request);
        session()->remove('pin');
        if(!empty($model->id))
        {
            return redirect(lang().'/user/pin-login?to=risk');
        }
    }

    public function getRisk()
    {
        return view($this->view.'risk');
    }

    public function validateRisk($request)
    {
        $key=[];
        for($a=1;$a<=10;$a++)
        {
            $key["r$a"]='required';
        }

        return $key;
    }

    public function messageRisk($request)
    {
        $message=[];
        $a=0;
        foreach($this->validateRisk($request) as $key=>$val)
        {
            $a++;
            $message[$key.'.required']="Pertanyaan $a harus di isi";
        }

        return $message;
    }

    public function postRisk(Request $request)
    {
        $inputs = $request->all();
        $this->validate($request,$this->validateRisk($request),$this->messageRisk($request));
        
        $dataRisk =json_encode($inputs);
        $riskNilai = array_sum(array_flatten($inputs));
        $model = $this->model
            ->where('acnt_no',$this->acnt_no)
            ->where('status','!=','r')
            ->first();

        $model->update([
            'data_risk'=>$dataRisk,
            'risk_total'=>$riskNilai,
        ]);

        // $userOracle = ClientInfo::where('acnt_no',$this->acnt_no)->first();
        // $userOracle->update([
        //     'risk_profile'=> $this->riskValue($riskNilai)
        // ]);

        session()->forget('pin');
        return redirect(lang().'/user/pin-login?to=nilai');
    }

    public function riskParams()
    {
        $list = [
            1 => [0,75],
            3 => [80,129],
            5 => [130,200],
        ];

        return $list;
    }

    public function riskValue($nilai)
    {
        $list = $this->riskParams();

        foreach($list as $key => $val)
        {
            if($nilai >= $val[0]  && $nilai <= $val[1])
            {
                return $key;
            }
        }
    }

    public function riskLabel($number)
    {
        $list = [
            1 => "0 - 75 <span></span>Konservatif",
            3 => "80 - 129 <span></span>Moderat",
            5 => "130 - 200 <span></span>Agresif",
        ];

        if(array_key_exists($number,$list))
        {
            return $list[$number];
        }
    }

    public function riskLabelOnly($number)
    {
        $list = [
            1 => "Konservatif",
            3 => "Moderat",
            5 => "Agresif",
        ];

        if(array_key_exists($number,$list))
        {
            return $list[$number];
        }
    }

    public function products()
    {
        return [
            'pemula'   => [
                'GAMA2MMCMONEYM00',
                'NAR02MMCMONEYM00',
                'GMT01MMCDANPSU00',
            ],
            'ekonomis' => [
                'KK002MMCMONEYM00',
            ],
        ];
    }

    public function cash_bal()
    {
        return VCashBal::whereAcntNo(acnt_no())->first();
    }

    public function getRiskNilai()
    {

        $model = $this->model
            ->where('acnt_no',$this->acnt_no)
            ->first();
        $number = $this->repoQuestion->riskRange($model->risk_total);
        $label = $this->riskLabel($number);
        $labelRiskOnly = $this->riskLabelOnly($number);
        $funds = $this->repoFund->listReksaDanaRisk($number);
        $others = $this->repoFund->listReksaDana(4);
        $userOracle = ClientInfo::where('acnt_no',$this->acnt_no)->first();
        $brokerName = $this->broker->a31OI01GridOutRecVo[0]->o_acnt_nm;
        $brokerId = acnt_no();
        $mileage = VCashBal::whereAcntNo(acnt_no())->first();
        return view($this->view.'risk_nilai',[
            'mileage'=>$mileage,
            'labelRiskOnly'=>$labelRiskOnly,
            'brokerId'=>$brokerId,
            'brokerName'=>$brokerName,
            'label'=>$label,
            'funds'=>$funds,
            'repoFund'=>$this->repoFund,
            'others'=>$others,
            'userOracle'=>$userOracle,
            'products'=>$this->products(),
        ]);
    }

    public function getLastPageQuestion()
    {
        return view($this->view.'question_last_page');
    }
}
