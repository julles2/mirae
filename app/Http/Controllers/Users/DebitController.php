<?php
/**
 * Created by PhpStorm.
 * User: DanielSimangunsong
 * Date: 5/17/2017
 * Time: 3:08 PM
 */

namespace App\Http\Controllers\Users;
use App\Models\Oracle\TransactionAutoDebet;
use App\Models\Oracle\FundInfo;
use App\Models\Oracle\DailyNav;
use App\Models\FundInfo as SqlFund;
use DB;

use App\Http\Controllers\UserController;

class DebitController extends UserController
{
    public function __construct(TransactionAutoDebet $trans)
    {
        $this->middleware('broker:broker,pin', ['except' => 'getLogout', 'guard' => 'broker']);
        $this->menu = 'debit';
        parent::__construct();
        $this->trans = $trans;
        $this->fund = new FundInfo();
        $this->sqlFund = new SqlFund();
    }

    public function funds()
    {
        $item = ['' => 'Pilih Reksa Dana Tujuan'];

        $model = $this->sqlFund
            ->lists('fund_name','id')
            ->toArray();

        return $item + $model;
    }

    public function getAjaxFund()
    {
        $dailyNav = new DailyNav();

        $model = $this->sqlFund->findOrFail(get('fund_id'));

        $data = $dailyNav->lastData($model->fund_cd);

        if($data == null)
        {
            $ajax_unit = '-';
            $ajax_tanggal = '-';
            $ajax_investment = '-';
        }else{
            $ajax_unit = valueDecimal($data->nav_per_unit,4);
            $ajax_tanggal = "Per: ".mirae_date($data->tr_date);
            $ajax_investment = formatUang($data->oneFundInfo->min_investment);
        }


        return response()
            ->json([
                'ajax_unit'=>$ajax_unit,
                'ajax_tanggal'=>$ajax_tanggal,
                'ajax_investment'=>$ajax_investment,
            ]);
    }

    public function getAjaxValidate()
    {
        $fundId = get('fundId');
        $nilai = formatInteger(get('nilai'));
        $model = $this->sqlFund->findorFail($fundId);
        $fee = $this->sqlFund->fee($model,$nilai);
        if(($nilai - $fee < $model->min_investment))
        {
            $result = 'false';
        }else{
            $result = 'true';
        }
        
        return response()->json([
            'result'=>$result,
        ]);
    }

    public function getIndex()
    {
    	$userId = acnt_no();

    	$listing = $this->trans
    		->where('acnt_no',$userId)
    		->orderBy('CAST(TR_DATE as INTEGER)','desc')
    		->get();

		$fund = function($fundCd){
    		$model = $this->fund
    			->select('fund_name')
    			->where('fund_cd',$fundCd)
    			->first();
    		return !empty($model->fund_name) ? $model->fund_name : 'Nama tidak terdafatar di 101M';
    	};

    	$nextPayment = function($debit){
    		$recureDate = $debit->recur_date;
    		$nowDay = date('d');
    		$dateNow = date("Ym").$recureDate;
	    	if($dateNow <= $debit->to_date && $debit->proc_cd == '1')
	    	{
	    		if($nowDay <= $recureDate)
	    		{
	    			$result = $dateNow;
	    		}else{
	    			$result = carbon()->parse($dateNow)->addMonths(1);
	    		}
	    		$result = carbon()->parse($result)->format("d-M-Y");
	    	}else{
	    		$result = "-";
	    	}


    		return $result;
    	};

        $funds = $this->funds();

        return view($this->layout.'.debit',[
    		'vMenu'=>$this->menu,
    		'listing'=>$listing,
    		'fund'=>$fund,
    		'nextPayment'=>$nextPayment,
    		'trans'=> $this->trans,
            'funds'=>$funds,
    	]);
    }
}
