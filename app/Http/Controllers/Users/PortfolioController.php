<?php
namespace App\Http\Controllers\Users;

use App\Http\Controllers\UserController;
use App\Models\FundImage;
use App\Models\FundInfo;
use App\Models\Oracle\DailyNav;
use App\Models\Oracle\FundListSwitching;
use App\Models\Oracle\Portofolio;
use App\Models\Oracle\Transaction;
use App\Models\Oracle\VStockBal;
use App\Models\PopChartDaily;
use App\Models\TransactionTemporary;
use App\Presenters\CartPresenter;
use Cart;
use Illuminate\Http\Request;
use Mirae;

class PortfolioController extends UserController
{
    use CartPresenter;

    public function __construct()
    {
        $this->middleware('broker:broker,pin', ['except' => 'getLogout', 'guard' => 'broker']);
        $this->menu = 'portfolio';
        parent::__construct();
        $this->portofolio = new Portofolio();
        $this->fund       = new FundInfo();
        $this->oraNav     = new DailyNav();
        $this->fundSwitch = new FundListSwitching();
        $this->vStock     = new VStockBal();
        $this->temporary  = new TransactionTemporary();
        $this->funImage   = new FundImage();
        $this->trans      = new Transaction();
        //dd($this->statusUser());
    }

    public function getIndex()
    {
        if (empty($this->broker->accountData->accountNum)) {
            return redirect(urlLang('user/pin-login'));
        }

        $userId = acnt_no();

        $portofolios = $this->portofolio->where('acnt_no', $userId)
            ->where('unit', '>', '0')
            ->get();
        
        $fundMySql = function ($fundCd) {
            return $this->fund->where('fund_cd', $fundCd)->first();
        };

        $colorStyle = function ($int) {
            return $int >= 0 ? 'green' : 'red';
        };

        $filters = $this->broker->a31OI01GridOutRecVo;
        //$arrFilters = [''=>'Select User'];
        $arrFilters = [];
        foreach ($filters as $row) {
            $arrFilters[$row->o_acnt_no] = $row->o_acnt_no;
        }

        $stocks = $this->vStock
            ->where('acnt_no', $userId)
            ->get();
        return view('frontend.users.portfolio', [
            'vMenu'                      => $this->menu,
            'portofolios'                => $portofolios,
            'fundMySql'                  => $fundMySql,
            'userId'                     => $userId,
            'porto'                      => $this->portofolio,
            'stocks'                     => $stocks,
            'colorStyle'                 => $colorStyle,
            'filters'                    => $arrFilters,
            'portofolio'                 => $this->portofolio,
            'temporary'                  => $this->temporary,
            'userId'                     => $userId,
            'usetNilaiInvestasiTerakhir' => $this->portofolio->userNilaiInvestasiTerakhir(),
        ]);
    }

    public function getAjaxFilterStock()
    {
        $userId = request()->get('userId');
        $stocks = $this->vStock
            ->where('acnt_no', $userId)
            ->get();

        $colorStyle = function ($int) {
            return $int >= 0 ? 'green' : 'red';
        };

        $view = view('frontend.users.ajax.table_stock', [
            'stocks'     => $stocks,
            'colorStyle' => $colorStyle,
        ])
            ->render();

        return response()->json([
            'result' => $view,
        ]);
    }

    public function getAjaxPopJual()
    {
        $userId = $this->broker->accountData->accountNum;

        $fundCd = request()->get('fundCd');

        $model = $this->fund
            ->where('fund_cd', $fundCd)
            ->firstOrFail();

        $portofolioInf = $this->portofolio->information($model->fund_cd, $userId);

        $unitWithTemporary = $this->portofolio->unitTemporary($model->fund_cd);

        $unitTemporary = $this->temporary->sumUnit($model->fund_cd);

        $captionUnitTemporary = $unitTemporary > 0 ? valueDecimal($unitTemporary) . ' in process' : '';
        $ajaxImage            = imageWithEmpty(fundImage($model->fund_cd));
        $ajaxBalanceUnit      = $this->portofolio->convertBalanceToUnit($model->fund_cd);
        $data                 = [
            'ajax_fund_cd'                  => $model->fund_cd,
            'ajax_im_name'                  => $model->im->im_nm,
            'ajax_fund_name'                => $model->fund_name,
            'ajax_last_nab'                 => "IDR " . formatUang($portofolioInf->last_nab),
            'ajax_unit'                     => formatUang($unitWithTemporary),
            'ajax_unit_temporary'           => $captionUnitTemporary,
            'ajax_date_last'                => "Per " . $portofolioInf->date_last_nab,
            'ajax_nilai_investasi_semula'   => "IDR " . formatUang($portofolioInf->nilai_investasi_semula),
            'ajax_nilai_investasi_terakhir' => "IDR " . formatUang(round($portofolioInf->nilai_investasi_terakhir)),
            'ajax_gain_los'                 => valueDecimal($portofolioInf->gain_loss),
            'ajax_return'                   => valueDecimal($portofolioInf->return),
            'ajax_minus_plus'               => $portofolioInf->minus_or_plus,
            'ajax_image'                    => $ajaxImage,
            'ajax_biaya'                    => $model->result_fee_redm . ' %',
            'ajax_balance_unit'             => formatUang($ajaxBalanceUnit),
            'ajax_balance'                  => formatUang($model->min_balance),
            'ajax_min_redm'                 => formatUang($model->min_redm_amt),
        ];

        return response()->json([
            'result' => $data,
        ]);
    }

    public function getAjaxValidatePenjualan()
    {
        $view              = "";
        $fundCd            = get('fund_cd');
        $unit              = get('unit');
        $totalUnit         = $this->portofolio->unitTemporary($fundCd);
        $compare           = $totalUnit - $unit;
        $unitHasilKurang   = 0;
        $cekBalance        = 0;
        $minimum_transaksi = 0;
        if ($compare > 0) {
            $result = 'true';

            $cekBalance      = $this->portofolio->convertBalanceToUnit($fundCd); // cek balance
            $unitHasilKurang = $totalUnit - $unit;
            if ($unitHasilKurang <= $cekBalance) {
                $result = "saldo_kurang";
            }

            $cekMinimumPenjualan = $this->fund->where('fund_cd', $fundCd)->firstOrFail();
            if ($cekMinimumPenjualan->unit_min_redeem > get('unit')) {
                $result = 'minimum_kurang';
            }

            $transactionToday = $this->temporary->transactionToday($fundCd, ['r']); // cek beforeTransaction
            if ($transactionToday->exist == 'true') {
                $result = 'double';
                $view   = $transactionToday;
            }

        } else{
            $result = 'false';
            if($totalUnit > 0)
            {
                if($compare == 0)
                {
                    $result = "all";
               }
            }
        }

        $transactionToday = $this->temporary->transactionToday($fundCd, ['r']); // cek beforeTransaction
        if ($transactionToday->exist == 'true') {
            $result = 'double';
            $view   = $transactionToday;
        }

        return response()->json([
            'result'            => $result,
            'sisaSaldo'         => valueDecimal($cekBalance),
            'minimum_transaksi' => $minimum_transaksi,
            'view'              => $view,
        ]);
    }

    public function getAjaxValidatePengalihan()
    {
        $fundCd          = get('fund_cd');
        $toFundCd        = get('to_fund');
        $unit            = floatval(get('unit'));
        $amount          = formatInteger(get('amount'));
        $totalUnit       = $this->portofolio->unitTemporary($fundCd);
        $compare         = $totalUnit - $unit;
        $unitHasilKurang = 0;
        $cekBalance      = 0;
        $view            = "";
        $model           = $this->fund->where('fund_cd', $fundCd)->firstOrFail();
        $toModel         = $this->fund->where('fund_cd', $toFundCd)->firstOrFail();

        if ($compare > 0) {
            $result = 'true';

            $cekBalance = $this->portofolio->convertBalanceToUnit($fundCd);

            $unitHasilKurang = $totalUnit - $unit;

            if ($unitHasilKurang <= $cekBalance) {
                $result = "saldo_kurang";
            }

            $transactionToday = $this->temporary->transactionTodaySwc($fundCd, $toFundCd, ['sw']); // cek beforeTransaction
            if ($transactionToday->exist == 'true') {
                $result = 'double';
                $view   = $transactionToday;
            }

            if ($amount < $model->min_swtc_out_amt) {
                $result = "keluar_kurang";
            }

            if ($amount < $toModel->min_swtc_in_amt) {
                $result = "masuk_kurang";
            }
        } elseif ($compare == 0) {
            $result = "all";
        } else {
            $result = "false";
        }

        return response()->json([
            'result'    => $result,
            'sisaSaldo' => valueDecimal($cekBalance),
            'view'      => $view,
        ]);
    }

    public function postPenjualan(Request $request, $lang)
    {
        $model = $this->fund->where('fund_cd', $request->fund_cd)->firstOrFail();

        $all_unit = isset($request->order_type) ? true : false;

        $data = [
            'id'      => 'p' . $model->id,
            'name'    => $model->fund_name,
            'qty'     => 1,
            'price'   => $request->penjualan_unit,
            'options' => [
                'type'       => 'redeem',
                'amount'     => $request->penjualan_amount,
                'order_type' => 'unit',
                'unit'       => $request->penjualan_unit,
                'all_unit'   => $all_unit,
            ],
        ];

        $search = Mirae::cartFindById('p' . $model->id);

        if (!empty($search)) {
            Cart::update($search->rowId, $data);
        } else {
            // dd(2);
            Cart::add($data);
        }
        $this->cartStoreToDb();
        return redirect(urlLang('user/portfolio'))
            ->with('success', trans('notif.add_to_cart'));
    }

    public function getAjaxPopPengalihan()
    {
        $userId = $this->broker->accountData->accountNum;

        $fundCd = request()->get('fundCd');

        $model = $this->fund
            ->where('fund_cd', $fundCd)
            ->firstOrFail();

        $portofolioInf = $this->portofolio->information($model->fund_cd, $userId);

        $funds = $this->fundSwitch->getLists($fundCd);
        if ($funds == []) {
            $funds = 'false';
        }

        $unitWithTemporary    = $this->portofolio->unitTemporary($model->fund_cd);
        $unitTemporary        = $this->temporary->sumUnit($model->fund_cd);
        $captionUnitTemporary = $unitTemporary > 0 ? valueDecimal($unitTemporary) . ' in process' : '';
        $ajaxImage            = imageWithEmpty(fundImage($model->fund_cd));
        $data                 = [
            'ajax_fund_cd'                  => $model->fund_cd,
            'ajax_im_name'                  => $model->im->im_nm,
            'ajax_fund_name'                => $model->fund_name,
            'ajax_last_nab'                 => "IDR " . formatUang($portofolioInf->last_nab),
            'ajax_unit_temporary'           => $captionUnitTemporary,
            'ajax_unit'                     => $unitWithTemporary,
            'ajax_date_last'                => "Per " . $portofolioInf->date_last_nab,
            'ajax_nilai_investasi_semula'   => "IDR " . formatUang($portofolioInf->nilai_investasi_semula),
            'ajax_nilai_investasi_terakhir' => "IDR " . formatUang(round($portofolioInf->nilai_investasi_terakhir)),
            'ajax_gain_los'                 => valueDecimal($portofolioInf->gain_loss),
            'ajax_return'                   => valueDecimal($portofolioInf->return),
            'ajax_minus_plus'               => $portofolioInf->minus_or_plus,
            'ajax_image'                    => $ajaxImage,
            'ajax_min_unit_out'             => formatUang($model->min_swtc_out_amt),
            'ajax_min_balance'              => formatUang($model->min_balance),
        ];

        return response()->json([
            'result' => $data,
            'funds'  => $funds,
        ]);
    }

    public function postPengalihan(Request $request, $lang)
    {
        $from     = $this->fund->where('fund_cd', $request->from_fund)->firstOrFail();
        $to       = $this->fund->where('fund_cd', $request->to_fund)->firstOrFail();
        $all_unit = isset($request->order_type) ? true : false;
        $data     = [
            'id'      => 'sw' . $from->id,
            'name'    => $from->fund_name,
            'qty'     => 1,
            'price'   => $request->unit,
            'options' => [
                'type'       => 'switch',
                'order_type' => 'unit',
                'to'         => $to->fund_cd,
                'amount'     => $request->amount,
                'unit'       => $request->unit,
                'all_unit'   => $all_unit,
            ],
        ];

        $search = Mirae::cartFindById($from->id);

        if (!empty($search)) {
            Cart::update($search->rowId, $data);
        } else {
            // dd(2);
            Cart::add($data);
        }
        $this->cartStoreToDb();
        return redirect(urlLang('user/portfolio'))
            ->with('success', trans('notif.add_to_cart'));
    }

    public function getAjaxPopTopup()
    {
        $userId = $this->broker->accountData->accountNum;

        $fundCd = request()->get('fundCd');

        $model = $this->fund
            ->where('fund_cd', $fundCd)
            ->firstOrFail();

        $latestNav = $model->daily_navs()
            ->where('fund_id', $model->id)
            ->orderBy(\DB::raw("CAST(TR_DATE AS SIGNED)"), 'desc')
            ->first();
        //dd($latestNav);
        $latestNavDate = notEmpty(@$latestNav->id, carbon()->parse(@$latestNav->tr_date)->format("d/m/Y"));
        $ajaxImage     = imageWithEmpty(fundImage($model->fund_cd));
        $vars          = [
            'im_name'        => $model->im->im_nm,
            'latestNavDate'  => $latestNavDate,
            'latestNavValue' => Mirae::roundSubstr($latestNav->nav_per_unit),
            'ajax_image'     => $ajaxImage,
        ];

        return response()->json([
            'vars' => $vars,
            'fund' => $model,
        ]);
    }

    public function getValidateTopup()
    {
        $this->model   = $this->fund;
        $nilai         = request()->get('nilai');
        $fundId        = request()->get('fund_id');
        $tanggal       = request()->get('tanggal');
        $day           = substr($tanggal, 0, 2);
        $modelFund     = FundInfo::find($fundId);
        $counter_nilai = request()->get('counter_nilai');
        $nilai         = formatInteger($nilai);
        $orderType     = request()->get('orderType');
        $cekMinimum    = $this->model->findOrFail($fundId);
        $view          = "";
        if (($nilai - $this->model->fee($cekMinimum, $nilai)) < $cekMinimum->min_increment) {
            $result = 'false';
        } else {
            //$this->user->accountData->accountNum)
            if ($orderType == 'subscription') {
                $result = "true";
            } else {
                if (!empty($this->user->accountData->accountNum)) {
                    $userId = $this->user->accountData->accountNum;

                    $cek202p = $this->autoDebet->where('tr_date', $this->now)
                        ->where('acnt_no', $userId)
                        ->where('fund_cd', $modelFund->fund_cd)
                        ->where('recur_date', $day)
                        ->first();

                    if (!empty($cek202p->tr_date)) {
                        $result = "false_202";
                    } else {
                        $result = "true";
                    }
                } else {
                    $result = "true";
                }
            }
        }

        $transactionToday = $this->temporary->transactionToday($modelFund->fund_cd, ['s', 'sa']); // cek beforeTransaction
        if ($transactionToday->exist == 'true') {
            $result = 'double';
            $view   = $transactionToday;
        }

        return response()->json([
            'result'        => $result,
            'model'         => $cekMinimum,
            'counter_nilai' => $counter_nilai,
            'view'          => $view,
        ]);
    }

    public function postTopup(Request $request, $lang, $fundId)
    {
        $model = $this->fund->findOrFail($fundId);

        $type = $request->order_type;

        $data = [
            'id'      => 's' . $model->id,
            'name'    => $model->fund_name,
            'qty'     => 1,
            'price'   => formatInteger($request->nilai),
            'options' => [
                'type'    => $type,
                'tanggal' => $request->tanggal,
                'tahun'   => $request->tahun,
            ],
        ];

        $search = Mirae::cartFindById('s' . $model->id);

        if (!empty($search)) {
            // dd(1);
            Cart::update($search->rowId, $data);
        } else {
            // dd(2);
            Cart::add($data);
        }
        $this->cartStoreToDb();
        return redirect(urlLang('user/portfolio'))
            ->with('success', trans('notif.add_to_cart'));
    }

    public function getAjaxNab()
    {
        $fundCd         = request()->get('fund_cd');
        $fund           = $this->fund->where('fund_cd', $fundCd)->firstOrFail();
        $lastData       = $this->oraNav->lastData($fund->fund_cd);
        $lastNab        = formatUang($lastData->nav_per_unit);
        // $dateLastNab    = carbon()->parse($lastData->tr_date)->format("d-M-Y");
        $dateLastNab    = carbon()->parse($lastData->date)->format("d-M-Y");
        $urlProspektuks = urlLang('product/view-pdf/' . $fund->id . '/prospectus');
        return response()->json([
            'ajax_to_date_last'      => !empty($dateLastNab) ? $dateLastNab : "-",
            'ajax_to_nab_last'       => !empty($lastNab) ? $lastNab : "-",
            'ajax_to_min_balance'    => formatUang($fund->min_balance),
            'ajax_to_min_investment' => formatUang($fund->min_investment),
            'ajax_to_min_increment'  => formatUang($fund->min_swtc_in_amt),
            'ajax_url_prospektus'    => $urlProspektuks,
        ]);
    }

    public function getAjaxChangeToAmount()
    {
        $unit    = request()->get('unit');
        $fundCd  = request()->get('fundCd');
        $lastNab = $this->oraNav->nabLast($fundCd);

        $amount = $unit * $lastNab;

        return response()->json([
            'result' => formatUang($amount),
        ]);
    }

    public function getAjaxChangeToUnit()
    {
        $amount  = formatInteger(request()->get('amount'));
        $fundCd  = request()->get('fundCd');
        $lastNab = $this->oraNav->nabLast($fundCd);
        $unit    = $amount / $lastNab;
        return response()->json([
            'result' => valueDecimal($unit),
        ]);
    }

    public function getAjaxChangeToAll()
    {
        $fundCd  = request()->get('fundCd');
        $lastNab = $this->oraNav->nabLast($fundCd);
        $allUnit = $this->portofolio->unitTemporary($fundCd);
        $amount  = $allUnit * $lastNab;

        return response()->json([
            'unit'   => $allUnit,
            'amount' => formatUang($amount),
        ]);
    }

    public function series($range)
    {
        $transactions = [
            'subscription'    => [
                'label'  => 'Subscription',
                'type'   => 'column',
                'method' => 'sumSubscriptionByAcntNo',
            ],
            'hasil_investasi' => [
                'label'  => 'Hasil Investasi',
                'type'   => 'line',
                'method' => 'hasilInvestasiByAcntNo',

            ],
            'dana_investasi'  => [
                'label'  => 'Dana Investasi',
                'type'   => 'line',
                'method' => 'danaInvestasiByAcntNo',
            ],
            'redeem'          => [
                'label'  => 'Redemption',
                'type'   => 'column',
                'method' => 'sumRedeemByAcntNo',
            ],
            'swc_in'          => [
                'label'  => 'Switching In',
                'type'   => 'column',
                'method' => 'sumSwcInByAcntNo',
            ],
            'swc_out'         => [
                'label'  => 'Switching Out',
                'type'   => 'column',
                'method' => 'sumSwcOutByAcntNo',
            ],
            'daily_nav'       => [
                'label'  => 'NAV',
                'type'   => 'line',
                'method' => 'dailyNavByFundCd',
            ],
            'daily_unit'      => [
                'label'  => 'Jumlah Unit',
                'type'   => 'line',
                'method' => 'dailyUnitByFund',
            ],
        ];

        $cacheTitle = 'chart_portfolio_' . get('fund_cd') . '-' . acnt_no();

        $result = \Cache::remember($cacheTitle, 5, function () use ($transactions, $range) {
            $fund   = $this->fund->where('fund_cd', get('fund_cd'))->first();
            $result = [];
            foreach ($transactions as $key => $val) {
                $data   = [];
                $no     = 0;
                $method = $val['method'];

                $counter = 0;

                foreach ($range as $date) {
                    $dailyNav = PopChartDaily::where('fund_id', $fund->id)->where('tr_date', $date)->first();
                    $no++;
                    $nilai = $this->portofolio->$method($date, get('fund_cd'));
                    if ($key == 'dana_investasi') {
                        $counter = $counter + $nilai;
                    } else {
                        $counter = $nilai;
                    }

                    if (!empty($dailyNav->id)) {
                        if ($dailyNav->nav != '0.00000000') {
                            $data[] = $counter;
                        }
                    }
                }

                $result[] = [
                    'name' => $val['label'],
                    'data' => $data,
                    'type' => $val['type'],
                    //'visible'=>false,
                ];
            }

            return $result;
        });

        return $result;
    }

    public function getAjaxChart()
    {
        $fundCd = get('fund_cd');
        $model  = $this->fund
            ->where('fund_cd', $fundCd)
            ->firstOrFail();

        $end = date("Ymd");

        $start = $this->portofolio
            ->firstSubscriptionDate($fundCd);

        //$start=date("Ym01");

        $data = [
            'fund_name' => $model->fund_name,
            'range'     => Mirae::range($start, $end, "F Y"),
            'series'    => $this->series(Mirae::range($start, $end, "Ymd")),
        ];

        return response()->json($data);
    }
}
