<?php
/**
 * Created by PhpStorm.
 * User: DanielSimangunsong
 * Date: 5/17/2017
 * Time: 3:08 PM
 */

namespace App\Http\Controllers\Users;


use App\Http\Controllers\UserController;
use App\Models\Oracle\Transaction;
use App\Models\Oracle\TransactionSwitching;
use App\Models\Oracle\TransactionAutoDebet;
use App\Models\Oracle\FundInfo;
use App\Models\TransactionTemporary;
use DB;
class OrderController extends UserController
{
	public function __construct(Transaction $trans)
    {
        $this->middleware('broker:broker,pin', ['except' => 'getLogout', 'guard' => 'broker']);
        $this->menu = 'order';
        parent::__construct();
        $this->trans = $trans;
        $this->swt = new TransactionSwitching();
        $this->fund = new FundInfo();
		$this->debet = new TransactionAutoDebet();
		$this->timeOracle = substr(date("Hisms"),0,-2);
		$this->temp = new TransactionTemporary();
    }

    public function queryListing($userId)
    {
        return $this->trans->queryListing($userId);
    }

    public function getIndex()
    {
    	$userId = acnt_no();

        $listing  = $this->queryListing($userId)->get();

        $fund = function($fundCd){
    		$model = $this->fund
    			->select('fund_name')
    			->where('fund_cd',$fundCd)
    			->first();
    		return !empty($model->fund_name) ? $model->fund_name : 'Nama tidak terdafatar di 101M';
    	};
        $modelTrans = new Transaction();

    	return view($this->layout.'.order',[
    		'vMenu'=>$this->menu,
    		'listing'=>$listing,
            'fund'=>$fund,
    		'modelTrans'=>$modelTrans,
    	]);
    }

    public function getAjaxRejectMessage()
    {
        $ref_no = request()->get('ref_no');
        $tr_type = request()->get('tr_type');
        if($tr_type == 'swc')
        {
            $model = $this->swt->where('ref_no',$ref_no)->firstOrFail();
        }else{
            $model = $this->trans->where('ref_no',$ref_no)->firstOrFail();
        }

        $html = "<h5>Message</h5><hr><p>".$model->reject_msg."</p>";
        return response()->json([
            'result'=>$html,
        ]);
    }

	public function getCancel()
	{
		$type = request()->get('type');
		$ref_no = request()->get('ref_no');
		$msg = request()->get('msg');

		if($type == 'SA') // autodebet
		{
			$model =$this->debet->where('ref_no',$ref_no)
				->where('acnt_no',acnt_no())
				->firstOrFail();

			if($model->proc_cd != 9)
			{
				$model->update([
					'proc_cd'=>9,
					'cncl_sect'=>1,
					'cncl_date'=>date("Ymd"),
					'cncl_time'=>$this->timeOracle,
                    'rejt_msg'=>$msg,
				]);
			}

		}elseif($type == 'S' || $type == 'R'){
			$model = $this->trans
				->where('acnt_no',acnt_no())
				->where('tr_type',$type)
				->where('ref_no',$ref_no)
				->firstOrFail();

			$model->update([
				'proc_cd'=>19,
			]);
		}elseif($type == 'swc'){
			$model = $this->swt
			->where('acnt_no',acnt_no())
			->where('ref_no',$ref_no)
			->firstOrFail();

			$model->update([
				'proc_cd'=>19,
			]);
		}

        $typeTemp = substr(strtolower($type),0,2);

		$modelTemporary = $this->temp->where('ref_no',$ref_no)
			->where('acnt_no',acnt_no())
			->where('type',$typeTemp)
			->first();

        if(!empty($modelTemporary->id))
        {
            $modelTemporary->update([
    			'status'=>'un_active',
    			'date_cancel'=>date("Y-m-d"),
    			'time_cancel'=>date("His"),
    		]);

    		$user = $modelTemporary->user;

    		$params = [
    			'model'=>$modelTemporary,
    			'user'=>$user,
    		];
    		\Mail::send('frontend.users.emails.cancel',$params,function($m)use($user){
    			$m->from('mirae@mirae.com', 'Redeem');
    			$m->to(trim($user->email))->cc('masagideveloper@gmail.com')->subject('Redeem Mirae Asset');
    		});
        }

        return redirect()
			->back()
			->with('success',trans('notif.transaction_cancel'));
	}

	public function getAjaxDetailPop()
	{
        $type = request()->get('type');
		$ref_no = request()->get('ref_no');
		$unit = "-";
		$view="";
		$pathAjax = 'frontend.users.ajax.order.';
		$fail = "";
		if($type == 'SA') // autodebet
		{
			$model =$this->debet->where('ref_no',$ref_no)
				->where('acnt_no',acnt_no())
				->first();
            //dd($model);
            if(empty($model->tr_date))
			{
				$fail = "fail";
			}

			if($fail != "fail")
			{
				$instruksi = "Subscription Auto Debet";
				$view = view($pathAjax.'subscription_redeem',[
					'model'=>$model,
					'instruksi'=>$instruksi,
                    'type'=>$type,
				])->render();
			}


		}elseif($type == 'S' || $type == 'R'){
			$instruksi = $type == "R" ? "Redeem" : "Subscription";
			$model = $this->trans
				->where('acnt_no',acnt_no())
				->where('tr_type',$type)
				->where('ref_no',$ref_no)
				->first();

			if(empty($model->tr_date))
			{
				$fail = "fail";
			}

			if($fail != "fail")
			{
				$view = view($pathAjax.'subscription_redeem',[
					'model'=>$model,
					'instruksi'=>$instruksi,
				])->render();

			}

		}elseif($type == 'swc'){
			$instruksi = "Switching";

			$model = $this->swt
			->where('acnt_no',acnt_no())
			->where('ref_no',$ref_no)
			->first();

			if(empty($model->tr_date))
			{
				$fail = "fail";
			}

			if($fail != "fail")
			{
				$fund = $model->out_fund->fund_name." <br> to <br>". $model->in_fund->fund_name;
				$allUnit = $model->out_all_unit == 1 ? ' (All Unit)' : '';
				$unit = $model->out_ord_unit.$allUnit;
				$amount = $model->amount;

				$view = view($pathAjax.'swc',[
					'model'=>$model,
					'instruksi'=>$instruksi,
					'swc'=>$this->swt,
					'fail'=>$fail,
				])->render();
			}


		}
		$params = [
			'hidden_ref_no'=>$ref_no,
			'hidden_tr_type'=>$type,
			'fail'=>$fail,
		];

		return response()->json([
			'params'=>$params,
			'view'=>$view,
		]);
	}
}
