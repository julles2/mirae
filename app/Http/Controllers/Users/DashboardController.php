<?php
/**
 * Created by PhpStorm.
 * User: DanielSimangunsong
 * Date: 5/17/2017
 * Time: 1:53 PM
 */

namespace App\Http\Controllers\Users;


use App\Http\Controllers\UserController;
use App\Models\Oracle\Portofolio;
use App\Models\TransactionTemporary;
use App\Models\FundInfo;
use App\Models\Watchlists;
use App\Models\Oracle\VStockBal;
use App\Models\Oracle\Transaction;

class DashboardController extends UserController
{
    public function __construct()
    {
        $this->middleware('broker:broker,pin', ['except' => 'getLogout', 'guard' => 'broker']);
        $this->menu = 'dashboard';
        $this->portofolio = new Portofolio();
        $this->temporary = new TransactionTemporary();
        $this->fund = new FundInfo();
        $this->vStock = new VStockBal();
        $this->trans = new Transaction();
        $this->watch = new Watchlists();
        parent::__construct();
    }

    public function getIndex()
    {
        if(empty($this->broker->accountData->accountNum))
    	{
    		return redirect(urlLang('user/pin-login'));
    	}

    	$userId = acnt_no();

    	$portofolios = $this->portofolio->where('acnt_no',$userId)
            ->where('unit','>','0')
            ->limit(3)
            ->get();

        $fundMySql = function($fundCd){
            return $this->fund->where('fund_cd',$fundCd)->first();
        };

        $colorStyle = function($int){
            return $int >= 0 ? 'green' : 'red';
        };

        $stocks = $this->vStock
            ->where('acnt_no',$userId)
            ->limit(3)
            ->get();

        $trans  = $this->trans;

        $trans->date = 'desc';

        $transactionLists = $trans->queryListing($userId)
    	    ->get()
            ->toArray();

        $transactions = collect($transactionLists)->take(2);

        $latestTransaction  = $this->trans->latestWithSwitch();

        $watchlists = $this->watch
            ->where('acnt_no',acnt_no())
            ->get();

        return view($this->layout.'.dashboard',[
            'vMenu'=>$this->menu,
            'portofolios'=>$portofolios,
            'fundMySql'=>$fundMySql,
            'porto'=>$this->portofolio,
            'temporary'=>$this->temporary,
            'userId'=>$userId,
            'colorStyle'=>$colorStyle,
            'stocks'=>$stocks,
            'transactions'=>$transactions,
            'modelTrans'=>$trans,
            'watchlists'=>$watchlists,
            'latestTransaction'=>$latestTransaction,
        ]);
    }

    public function getDeleteWatch($lang,$id)
    {
        $model = $this->watch->findOrFail($id);
        if(acnt_no() == $model->acnt_no)
        {
            $model->delete();
            return redirect()->back()->with('success',trans('global.data_deleted'));
        }
        abort(404);
    }

    public function postDeleteCheckWatch($lang)
    {
        $request = request();
        $ids = $request->watch_id;
        $model = $this->watch->whereIn('id',$ids)->delete();
        return redirect()->back()->with('success',trans('global.data_deleted'));
    }

}
