<?php
/**
 * Created by PhpStorm.
 * User: DanielSimangunsong
 * Date: 5/17/2017
 * Time: 3:08 PM
 */

namespace App\Http\Controllers\Users;

use App\Http\Controllers\UserController;
use App\Models\Oracle\Transaction;
use App\Models\Oracle\FundInfo;
use DB;

class TransactionController extends UserController
{
    public function __construct(Transaction $trans)
    {
        $this->middleware('broker:broker,pin', ['except' => 'getLogout', 'guard' => 'broker']);
        $this->menu = 'transaction';
        parent::__construct();
        $this->trans = $trans;
        $this->fund = new FundInfo();
    }

    public function getIndex()
    {
        $userId = acnt_no();

        $trans = $this->trans;

        // $trans->date = 'desc';

        $modelListing = $trans->queryListing($userId)
            ->get()
            ->toArray();

        $listing = array_where($modelListing, function ($value, $key) {
            return $key['proc_cd'] == '70' || $key['proc_cd'] == '1';
        });

        //$listing = collect($listing)->reverse();

        $fund = function ($fundCd) {
            $model = $this->fund
                ->select('fund_name')
                ->where('fund_cd', $fundCd)
                ->first();
            return !empty($model->fund_name) ? $model->fund_name : 'Nama tidak terdafatar di 101M';
        };

        return view($this->layout.'.transaction', [
            'vMenu'=>$this->menu,
            'transactions'=>$listing,
            'fund'=>$fund,
        ]);
    }
}
