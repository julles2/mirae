<?php

namespace App\Http\Controllers\Users;

use App\Http\Controllers\UserController;
use App\Models\Oracle\ClientInfo;
use App\Models\Oracle\FundInfo;
use App\Models\Oracle\Transaction;
use App\Models\Oracle\VCashBal;
use Illuminate\Http\Request;

class MileageController extends UserController
{
    public function __construct()
    {
        parent::__construct();
        $this->middleware('broker:broker,pin', ['except' => 'getLogout', 'guard' => 'broker']);
        $this->menu        = 'mileage';
        $this->timeOracle  = substr(date("Hisms"), 0, -2);
        $this->user        = auth()->guard('broker')->user();
        $this->transaction = new Transaction();
        //dd($this->statusUser());
    }

    public function client()
    {
        return ClientInfo::whereAcntNo(acnt_no())->first();
    }

    public function cash_bal()
    {
        return VCashBal::whereAcntNo(acnt_no())->first();
    }

    public function riskLabelOnly($number)
    {
        $list = [
            1 => "Konservatif",
            3 => "Moderat",
            5 => "Agresif",
        ];

        if (array_key_exists($number, $list)) {
            return $list[$number];
        }
    }

    public function products()
    {
        return [
            'pemula'   => [
                'GAMA2MMCMONEYM00',
                'GMT01MMCDANPSU00',
            ],
			'capital'   => [
                'NAR02MMCMONEYM00',
            ],
            'ekonomis' => [
                'KK002MMCMONEYM00',
            ],
        ];
    }

    public function validation_form($fundCode)
    {
        $pemula   = $this->products()['pemula'];
        $capital = $this->products()['capital'];
		$ekonomis = $this->products()['ekonomis'];
        $tipe     = "ekonomis";
        $result   = true;
        if (in_array($fundCode, $pemula)) {
            $tipe = 'pemula';
        }

        if ($tipe == 'pemula') {
            if ($this->cash_bal()->mileage < 100000) {
                $result = false;
            }
        }
		
		if (in_array($fundCode, $capital)) {
            $tipe = 'capital';
        }
		
		if ($tipe == 'capital') {
            if ($this->cash_bal()->mileage < 500000) {
                $result = false;
            }
        }

        return $result;
    }

    public function getAjaxValidation()
    {
        $fundCode = get('code');
        $result   = $this->validation_form($fundCode);
        return response()->json([
            'result' => $result,
        ]);

    }

    public function cek_user()
    {
    	$rejects = $this->transaction->keyStatusReject();
    	$result = 'not_redeem';
    	$model = Transaction::where('acnt_no',acnt_no())	
    		->where('mileage_sect',1)
    		->whereNotIn('proc_cd',$rejects)
    		->count();

    	if($model > 0)
    	{
    		$result = 'redeem';
    	}

    	return $result;
    }

    public function getIndex()
    {
        $mileage       = $this->cash_bal();
        $labelRiskOnly = $this->riskLabelOnly(@$this->client()->risk_profile);
        $brokerName    = $this->broker->a31OI01GridOutRecVo[0]->o_acnt_nm;
        $brokerId      = acnt_no();

        return view('frontend.users.mileage', [
            'vMenu'         => $this->menu,
            'labelRiskOnly' => $labelRiskOnly,
            'brokerId'      => $brokerId,
            'brokerName'    => $brokerName,
            'mileage'       => $mileage,
            'products'      => $this->products(),
            'cek_user'      => $this->cek_user(),
        ]);
    }

    public function postIndex(Request $request)
    {
        $validation = $this->validation_form($request->fund);
        $fund       = FundInfo::whereFundCd($request->fund)->firstOrFail();
        if ($validation == false) {
            abort(404);
        }
        $mileage       = $this->cash_bal();
        $dataOracle = [
            'tr_date'      => date("Ymd"),
            'acnt_no'      => acnt_no(),
            'ifua_cd'      => empty(@$this->client()->ifua_cd) ? " " : @$this->client()->ifua_cd,
            'fund_cd'      => $request->fund,
            'ref_no'       => $this->transaction->makeRefNo(),
            'proc_cd'      => '10',
            'tr_type'      => 'S',
            'im_cd'        => $fund->im->im_cd,
            'ord_type'     => '1',
            'amount'       => $mileage->mileage,
            'fee_type'     => 0,
            'fee'          => 0,
            //'fee'=>!empty($modelFund->fee_subs) ? $modelFund->fee_subs : 0,
            'source'       => '1',
            'input_user'   => $this->user->user_id,
            'proc_time'    => $this->timeOracle,
            'mileage_sect' => 1,
        ];
        
        \DB::connection('oracle')->table('F0T200P')->insert($dataOracle);

        return redirect(urlLang("user/mileage"));
    }
}
