<?php

namespace App\Http\Controllers;

use App\Http\Controllers\BaseController;
use App\Models\DailyNav;
use App\Models\FundImage;
use App\Models\FundInfo;
use App\Models\Oracle\DailyNav as OraDailyNav;
use App\Models\Oracle\TransactionAutoDebet;
use App\Models\TransactionTemporary;
use App\Presenters\CartPresenter;
use App\Traits\ProductTrait;
use Carbon\Carbon;
use Cart;
use Illuminate\Http\Request;
use Mirae;

class ProductController extends BaseController
{
    use CartPresenter, ProductTrait;

    public $view;
    public $model;
    public $dailyNav;
    public $now;
    public $mirae;
    public $allowTypes;

    public function __construct(FundInfo $model, DailyNav $dailyNav, FundImage $imageLogo)
    {
        parent::__construct();
        $this->view           = 'product.';
        $this->model          = $model;
        $this->dailyNav       = $dailyNav;
        $this->now            = date("Ymd");
        $this->allowTypes     = ['EQ', 'MX', 'FI', 'SR', 'MM'];
        $this->oraDailyNav    = new OraDailyNav();
        $this->user           = auth()->guard('broker')->user();
        $this->autoDebet      = new TransactionAutoDebet();
        $this->imageLogo      = $imageLogo;
        $this->transTemporary = new TransactionTemporary();
    }

    public function getAjaxListingReksadana()
    {

        $type     = get('type');
        $limit    = get('limit');
        $fundName = get('fund_name');
        $offset   = get('offset');
        //$queryFunds = $this->model->listingFunds($type,$limit,$fundName);
        $queryFunds = $this->model->queryListingLoadMore($type, $limit, $offset, $fundName)->get();
        $count      = $queryFunds->count();

        $modelDailyNav = function ($row) {
            return $row->daily_navs()->first();
        };

        $listing = view($this->view . 'ajax.listing', [
            'listing'       => $queryFunds,
            'modelDailyNav' => $modelDailyNav,
            'offset'        => $offset,
        ])
            ->render();

        return response()->json([
            'listing' => $listing,
            'count'   => $count,
            //'maxEntries'=>$maxEntries,
            'to'      => $queryFunds->count(),
        ]);
    }

    public function getAjaxPagination()
    {
        $getType = request()->get('type');

        $query   = $this->model->paginationFunds($getType);
        $options = "";
        foreach ($query as $row) {
            $options .= "<option  value = '" . $row . "'>" . $row . "</option>";
        }

        return response()->json([
            'options' => $options,
        ]);
    }

    public function getAjaxPopReksaDana()
    {
        $fund_cd   = request()->get('fund_cd');
        $model     = $this->model->where('fund_cd', $fund_cd)->first();
        $imName    = $model->im->im_nm;
        $latestNav = $model->daily_navs()->where('fund_id', $model->id)->orderBy(\DB::raw("CAST(TR_DATE AS SIGNED)"), 'desc')->first();
        $latestPop = $model->fund_performance_pop()->where('fund_id', $model->id)->orderBy(\DB::raw("CAST(TR_DATE AS SIGNED)"), 'desc')->first();

        // dd($model->fund_orcl);
        $params = [
            ''     => 'NAV',
            'aum_' => 'AUM',
            'ou_'  => 'Unit',
        ];
        $viewPop = view('product.ajax.fund_performance', [
            'row'    => $latestPop,
            'params' => $params,
        ])
            ->render();

        $latestNavDate = notEmpty(@$latestNav->id, Carbon::parse(@$latestNav->tr_date)->format("d/m/Y"));

        $modelInfoProduct = $model->content_details()
            ->where('lang', lang())
            ->get();

        $infoProduk = view('product.ajax.info_produk', [
            'modelInfoProduct' => $modelInfoProduct,
        ])
            ->render();

        $modelInfoCompany = $model->company_details()->where('lang', lang())->first();
        $infoCompany      = view('product.ajax.info_company', [
            'modelInfoCompany' => $modelInfoCompany,
        ])
            ->render();

        $image_logo = $this->imageLogo->where('fund_cd', $model->fund_cd)->first();

        $vars = [
            'imName'               => $imName,
            'latestNavDate'        => $latestNavDate,
            'latestNavValue'       => formatUang($latestNav->nav_per_unit),
            'nav_per_unit'         => $latestNav->nav_per_unit,
            'aum'                  => formatUang($latestNav->nav_per_unit * $latestNav->outstanding_unit),
            'unit'                 => formatUang($latestNav->outstanding_unit),
            'fundPerformanceTable' => $viewPop,
            'ajax_result_image'    => $model->result_image,
            'div_info_produk'      => $infoProduk,
            'div_info_company'     => $infoCompany,
            'image_logo_company'   => @$image_logo->image,
            'ajax_fund_cd'         => $fund_cd,
            'risk_profile'         => $model->fund_orcl->result_fund_risk,
        ];

        return response()->json([
            'model' => $model,
            'vars'  => $vars,
        ]);
    }

    public function getChartAum()
    {
        $fundCd    = get('fund_cd');
        $data      = $this->chart_aum($fundCd);
        $interval  = null;
        $countData = count($data['value']);

        if ($countData > 0) {
            $interval = round($countData / 4);
        }

        return response()->json([
            'date'     => json_encode($data['date']),
            'value'    => json_encode($data['value']),
            'interval' => $interval,
        ]);

    }

    public function getChartNav()
    {
        $fundCd    = get('fund_cd');
        $data      = $this->chart_nav($fundCd);
        $interval  = null;
        $countData = count($data['value']);

        if ($countData > 0) {
            $interval = round($countData / 4);
        }

        return response()->json([
            'date'     => json_encode($data['date']),
            'value'    => json_encode($data['value']),
            'interval' => $interval,
        ]);

    }

    public function getChartUnit()
    {
        $fundCd    = get('fund_cd');
        $data      = $this->chart_unit($fundCd);
        $interval  = null;
        $countData = count($data['value']);

        if ($countData > 0) {
            $interval = round($countData / 4);
        }

        return response()->json([
            'date'     => json_encode($data['date']),
            'value'    => json_encode($data['value']),
            'interval' => $interval,
        ]);

    }

    public function getAjaxPopBeli()
    {
        $fundId = request()->get('fund_id');

        $model = $this->model->findOrFail($fundId);

        return response()->json([
            'model' => $model,
        ]);
    }

    public function getIndex()
    {
        // validation parameter type

        $getType = request()->get('type');

        if (!empty($getType)) {
            if (!in_array($getType, $this->allowTypes)) {
                abort(404);
            }
        }

        //dd($this->oraDailyNav->manipulateNab('GAMA2EQCEQUITY00',Mirae::yesterday('20130115'),'20130115'));
        //
        $active = function ($name) use ($getType) {
            return $name === $getType ? 'active' : null;
        };

        $setting = menu_setting('produk', lang())->first();

        $countButton = $this->model->queryListingLoadMore("", "", $this->model->limitFunds(), "")->get()->count();

        return view($this->view . 'index', [
            'active'      => $active,
            'setting'     => $setting,
            'countButton' => $countButton,
        ]);
    }

    public function getValidatePembelian()
    {
        $nilai         = request()->get('nilai');
        $fundId        = request()->get('fund_id');
        $tanggal       = request()->get('tanggal');
        $day           = substr($tanggal, 0, 2);
        $modelFund     = FundInfo::find($fundId);
        $counter_nilai = request()->get('counter_nilai');
        $nilai         = formatInteger($nilai);
        $orderType     = request()->get('orderType');
        $cekMinimum    = $this->model->findOrFail($fundId);
        $view          = "";

        if (($nilai - $this->model->fee($cekMinimum, $nilai)) < $cekMinimum->min_investment) {
            $result = 'false';
        } else {
            $searchTransactionToday = $this->transTemporary->transactionToday($modelFund->fund_cd, ['s', 'sa']);

            if ($searchTransactionToday->exist == 'true') {
                $view   = $searchTransactionToday;
                $result = 'double';
            } else {
                if ($orderType == 'subscription') {
                    $result = "true";

                } else {
                    if (!empty($this->user->accountData->accountNum)) {
                        $userId = $this->user->accountData->accountNum;

                        $cek202p = $this->autoDebet->where('tr_date', $this->now)
                            ->where('acnt_no', $userId)
                            ->where('fund_cd', $modelFund->fund_cd)
                            ->where('recur_date', $day)
                            ->first();

                        if (!empty($cek202p->tr_date)) {
                            $result = "false_202";
                        } else {
                            $result = "true";
                        }

                    } else {
                        $result = "true";
                    }
                }
            }

        }

        if (intval($tanggal > 30)) {
            $result = 'tanggal_30';
        }

        if ($tanggal == "00") {
            $result = "tanggal_00";
        }

        return response()->json([
            'result'        => $result,
            'model'         => $cekMinimum,
            'counter_nilai' => $counter_nilai,
            'view'          => $view,
        ]);
    }

    public function postPembelian(Request $request, $lang, $fundId, $url = "")
    {
        $model = $this->model->findOrFail($fundId);

        $type = $request->order_type;

        $data = [
            'id'      => 's' . $model->id,
            'name'    => $model->fund_name,
            'qty'     => 1,
            'price'   => formatInteger($request->nilai),
            'options' => [
                'type'    => $type,
                'tanggal' => $request->tanggal,
                'tahun'   => $request->tahun,
            ],
        ];

        $search = Mirae::cartFindById('s' . $model->id);

        if (!empty($search)) {
            // dd(1);
            Cart::update($search->rowId, $data);
        } else {
            // dd(2);
            Cart::add($data);
        }

        $this->cartStoreToDb(); // using trait CartPresenter

        $resultUrl = 'product';
        if (!empty($url)) {
            $resultUrl = str_replace("-", "/", $url);
        }

        return redirect(urlLang($resultUrl))
            ->with('success', trans('alert.buy_product'));
    }

    public function getViewPdf($lang, $id, $field)
    {
        $model = $this->model->findOrFail($id);
        $path  = public_path('contents/pdf/' . $model->{$field});

        try
        {
            return response()->make(file_get_contents($path), 200, [
                'Content-Type'        => 'application/pdf',
                'Content-Disposition' => 'inline; filename="' . $model->{$field} . '"',
            ]);
        } catch (\Exception $e) {
            dd('FILE NOT FOUND!');
        }
    }

    public function getTes()
    {
        $model = $this->model
            ->limit(2)
            ->offset(1)
            ->get();

        dd($model);
    }

    public function getDownload($lang, $pdf)
    {
        return response()->download(public_path('contents/pdf/' . $pdf));
    }
}
