<?php

namespace App\Http\Requests\Backend;

use App\Http\Requests\Request;

class HelpRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $validate['order']   = 'required|integer';
        $validate['status']  = 'required';

        foreach (langs() as $key => $value) {
            $validate['title.'.$key] = 'required';
        }

        return $validate;
    }

    public function messages()
    {
        $validate['order.required']   = 'Order field is required and Cannot be blank';
        $validate['order.integer']   = 'Order field is number';
        $validate['status.required']        = 'Status field is required and Cannot be blank';

        foreach (langs() as $key => $value) {
            $validate['title.'.$key.'.required'] = 'Title '.$value.' field is required';
        }

        return $validate;
    }
}
