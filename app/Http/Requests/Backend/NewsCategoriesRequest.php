<?php

namespace App\Http\Requests\Backend;

use App\Http\Requests\Request;

class NewsCategoriesRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        foreach (langs() as $key => $value) {
            $validate['category.'.$key] = 'required';
        }

        return $validate;
    }

    public function messages()
    {
        foreach (langs() as $key => $value) {
            $validate['category.'.$key.'.required'] = 'Category '.$value.' field is required and Cannot be blank';
        }

        return $validate;
    }
}
