<?php

namespace App\Http\Requests\Backend;

use App\Http\Requests\Request;

class StepRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $validate['status'] = 'required';
        $validate['icon']   = 'image';

        foreach (langs() as $key => $value) {
            $validate['title.'.$key] = 'required';
            $validate['wording.'.$key] = 'max:100';
        }

        return $validate;
    }

    public function messages()
    {
        $validate['status.required']        = 'Status field is required and Cannot be blank';

        foreach (langs() as $key => $value) {
            $validate['title.'.$key.'.required'] = 'Title '.$value.' field is required';
            $validate['wording.'.$key.'.max'] = 'Title '.$value.' field maximal 100 character';
        }

        return $validate;
    }
}
