<?php

namespace App\Http\Requests\Backend;

use App\Http\Requests\Request;

class MenuSettingsRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        foreach (langs() as $key => $value) {
            $validate['title_caption.'.$key]    = 'required';
            $validate['title_main.'.$key]       = 'required';
            //$validate['sub_title.'.$key]        = 'required';
            $validate['banner_image.'.$key]     = 'image';
        }

        return $validate;
    }

    public function messages()
    {
        foreach (langs() as $key => $value) {
            $validate['title_caption.'.$key.'.required'] = 'Title Caption '.$value.' field is required and cannot be blank';
            $validate['title_main.'.$key.'.required'] = 'Title Main '.$value.' field is required and cannot be blank';
            //$validate['sub_title.'.$key.'.required'] = 'Sub Title '.$value.' field is required and cannot be blank';
        }

        return $validate;
    }
}
