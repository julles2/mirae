<?php

namespace App\Http\Requests\Backend;

use App\Http\Requests\Request;

class MIInfoRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        // $validator['description'] = 'required';

        // foreach (langs() as $key => $value) {
        //     $validator['pengelolaan.'.$key] = 'required';
        //     $validator['modal_dasar.'.$key] = 'required';
        //     $validator['modal_disetor.'.$key] = 'required';
        //     $validator['kepemilikan.'.$key] = 'required';
        //     $validator['izin_usaha.'.$key] = 'required';
        //     $validator['status.'.$key] = 'required';
        // }

        // $jumlah = count($this->input('d_nama')) ;

        // for ($i=0; $i < $jumlah; $i++) { 
        //     $validator['d_nama.'.$i] = 'required';
        //     $validator['d_jabatan.'.$i] = 'required';
        // }
        
        // return $validator;

        return [];
    }

    public function messages()
    {
        foreach (langs() as $key => $value) {
            $validate['pengelolaan.'.$key.'.required'] = 'Pengelolaan '.$value.' field is required';
            $validate['modal_dasar.'.$key.'.required'] = 'Modal Dasar '.$value.' field is required';
            $validate['modal_disetor.'.$key.'.required'] = 'Modal Disetor '.$value.' field is required';
            $validate['kepemilikan.'.$key.'.required'] = 'Kepemilikan '.$value.' field is required';
            $validate['izin_usaha.'.$key.'.required'] = 'Izin Usaha '.$value.' field is required';
            $validate['status.'.$key.'.required'] = 'Status '.$value.' field is required';
        }

        $jumlah = count($this->input('d_nama')) ;

        for ($i=0; $i < $jumlah; $i++) { 
            $validate['d_nama.'.$i.'.required'] = 'Nama field is required';
            $validate['d_jabatan.'.$i.'.required'] = 'Jabatan field is required';
        }

        return $validate;
    }
}
