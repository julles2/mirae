<?php

namespace App\Http\Requests\Backend;

use App\Http\Requests\Request;

class CompanyRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $validator['status'] = 'required';

        foreach (langs() as $key => $value) {
            $validator['title.'.$key] = 'required';
        }
        
        return $validator;
    }

    public function messages()
    {
        $validate['status.required']          = 'Status field is required and Cannot be blank';

        foreach (langs() as $key => $value) {
            $validate['title.'.$key.'.required'] = 'Title '.$value.' field is required';
        }

        return $validate;
    }
}
