<?php

namespace App\Http\Requests\Backend;

use App\Http\Requests\Request;

class NewsRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $validate['category_id']   = 'required';
        $validate['status']        = 'required';
        $validate['date']          = 'required';
        $validate['image_intro']   = 'image';
        $validate['image_detail']  = 'image';

        foreach (langs() as $key => $value) {
            $validate['title.'.$key] = 'required';
            $validate['intro.'.$key] = 'required';
        }

        return $validate;
    }

    public function messages()
    {
        $validate['category_id.required']   = 'Category field is required and Cannot be blank';
        $validate['status.required']        = 'Status field is required and Cannot be blank';
        $validate['date.required']          = 'Date field is required and Cannot be blank';

        foreach (langs() as $key => $value) {
            $validate['title.'.$key.'.required'] = 'Title '.$value.' field is required';
            $validate['intro.'.$key.'.required'] = 'Intro '.$value.' field is required';
        }

        return $validate;
    }
}
