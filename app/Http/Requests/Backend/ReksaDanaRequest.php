<?php

namespace App\Http\Requests\Backend;

use App\Http\Requests\Request;

class ReksaDanaRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'image'=>'image',
            'fund_fact_sheet' => 'mimes:pdf',
            'prospectus' => 'mimes:pdf',
        ];

        // $validate['title1.*'] = 'required';
        // $validate['title2.*'] = 'required';
        // $validate['description.*'] = 'required';
        // $validate['image'] = 'image';

        // $jumlah = count($this->input('title1'));

        // if ($jumlah = 0) {
        //     $jumlah = 1;
        // }

        // for ($i=0; $i < $jumlah ; $i++) {
        //     $validate['title1.'.$i] = 'required';
        // }

        // return $validate;
    }
}
