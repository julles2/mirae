<?php

namespace App\Http\Requests\Backend;

use App\Http\Requests\Request;

class SliderRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */

    public function rules()
    {
        $validate['status']         = 'required';
        $validate['image']          = 'image';

        foreach (langs() as $key => $value) {
            $validate['title.'.$key] = 'required';
            $validate['order.'.$key] = 'required|integer';
        }

        return $validate;
    }

    public function messages()
    {
        $validate['status.required']    = 'Status field is required and Cannot be blank';
        $validate['image.required']     = 'Image field is required and Cannot be blank';

        foreach (langs() as $key => $value) {
            $validate['title.'.$key.'.required'] = 'Title '.$value.' field is required and Cannot be blank';
            $validate['order.'.$key.'.required'] = 'Order '.$value.' field is required and Cannot be blank';
            $validate['order.'.$key.'.integer'] = 'Order '.$value.' field must number';
        }

        return $validate;
    }
}
