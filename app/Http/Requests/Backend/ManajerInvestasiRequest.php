<?php

namespace App\Http\Requests\Backend;

use App\Http\Requests\Request;

class ManajerInvestasiRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $validate['bulan']   = 'required';
        $validate['tahun']   = 'required';
        //$validate['excel']     = 'required|mimes:xls,xlsx,csv|max:10240';

        return $validate;
    }

    public function messages()
    {
        $validate['periode.required']   = 'Periode field is required and Cannot be blank';
        $validate['excel.required']   = 'File Excel field is required and Cannot be blank';
        $validate['excel.mimes']   = 'Only File Excel allowed';
        $validate['excel.max']   = 'File can not be greater than 10MB';

        return $validate;
    }
}
