<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class NewsletterRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

         return [
            'email' => 'email|required|unique:newsletters|max:50|',
        ];
    }

    // public function messages()
    // {
    //     return [
    //         'g-recaptcha-response.captcha' => 'CAPTCHA failed, please try again',
    //         'g-recaptcha-response.required' => 'CAPTCHA failed, please try again'
    //     ];
    // }
}
