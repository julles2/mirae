<?php namespace App\Http\Traits;

use App\Models\Feed\Im;
use Cache;

trait ManajerInvestasiTrait
{
	public function model()
	{
		return new Im();
	}

	public function queryTable($like="")
	{
		$model =  $this->model();

		if(!empty($like))
		{
			$model = $model->where('name','like',"%$like%");
		}

		$model = $model->orderBy('name','asc')
			->paginate(10);
	
		return $model;
	}
}