<?php
/**
 * Created by PhpStorm.
 * User: DanielSimangunsong
 * Date: 5/16/2017
 * Time: 2:18 PM
 */

namespace App\Managers;


class SocketManager
{
    /**
     * @var string
     */
    protected $data;

    /**
     * @var
     */
    protected $error;

    /**
     * Server host address
     *
     * @var string
     */
    protected $host;

    /**
     * URL Segment
     *
     * @var string
     */
    protected $path;

    /**
     * @var object
     */
    protected $object;

    public function __construct(array $options, $url = 'http://117.103.33.200/php/loginInfo.do')
    {
        array_walk($options, function (&$val, $key) {
            $val = $key . '=' . urlencode($val);
        });
// Set the data
        $this->data = implode('&', $options);

// Set host and path
        $list = parse_url($url);
        $this->ssl = config('mirae.ssl');
        $this->host = $list['host'];
        $this->path = $list['path'];
    }

    public function uidConnector()
    {
        $result = $this->connect();
        $result = explode("\r\n\r\n", $result, 2);

        if (isset($result[1])) {
            $this->object = json_decode((new AesManager($result[1]))->decrypt());

            if (empty($this->object->loginIdType)) {
                $this->error = 'Check your ID or PWD';
            }
        }
    }

    public function data()
    {
        $aes_i = new AesManager(request()->get('uid'));
        $aes_p = new AesManager(request()->get('pwd'));
        return "uid=".urlencode($aes_i->encrypt())."&pwd=".urlencode($aes_p->encrypt());
    }

    protected function connect()
    {
        $result = '';

        //header & data setup
        $sock = fsockopen($this->ssl.$this->host, config('mirae.port_ssl'), $errNo, $errStr, 1000.0);
        //$this->data = $this->data();

        if ($sock == true) {
            fwrite($sock, "POST " . $this->getPath() . " HTTP/1.1\r\n");
            fwrite($sock, "Host: " . $this->getHost() . "\r\n");
            fwrite($sock, "Content-Type: application/x-www-form-urlencoded\r\n");
            fwrite($sock, 'Content-Length: ' . strlen($this->data) . "\r\n");
            fwrite($sock, "Connection:close\r\n\r\n");
            fwrite($sock, $this->data);

            while (feof($sock) == false) {
                $result .= fread($sock, 256);
            }

            fclose($sock);
            return $result;
        } else {
            $this->error = $errNo . ' ' . $errStr;
        }
    }

    /**
     * @return string
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * @return string
     */
    public function getHost()
    {
        return $this->host;
    }

    public function pinConnector()
    {
        $result = $this->connect();
        $result = explode("\r\n\r\n", $result, 2);

        if (isset($result[1])) {
            $this->object = json_decode((new AesManager($result[1]))->decrypt());

            if (empty($this->object->status)) {
                $this->error = $this->object->msg;
            }
        }
    }

    /**
     * @return bool
     */
    public function isError()
    {
        return !empty($this->error);
    }

    /**
     * @return mixed
     */
    public function getError()
    {
        return $this->error;
    }

    /**
     * @param mixed $key
     * @return mixed
     */
    public function getItem($key = null)
    {
        return is_null($key) || is_null($this->object) ? $this->object : $this->object->{$key};
    }

    /**
     * @param $str
     */
    protected function decodeResult($str, $needLoginType = true)
    {
        if (null !== $str) {
            $this->object = json_decode((new AesManager($str))->decrypt());

            if ($needLoginType && empty($this->object->loginIdType)) {
                $this->error = 'Check your ID or PWD';
            }
        }
    }
}
