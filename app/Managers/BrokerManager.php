<?php
/**
 * Created by PhpStorm.
 * User: DanielSimangunsong
 * Date: 5/16/2017
 * Time: 1:01 PM
 */

namespace App\Managers;


use Illuminate\Contracts\Auth\Authenticatable;
use Session;

/**
 * Class BrokerManager
 * @package App\Managers
 *
 * +"a11oi32": {#254 ?}
 * +"a31OI01GridOutRecVo": array:1 [?]
 * +"addMsg": ""
 * +"addr": ""
 * +"bday": ""
 * +"bsno": ""
 * +"cnct_sect": "2"
 * +"cntcNo": ""
 * +"custNm": "LEE TAE HO"
 * +"dble_cnct_conf_sect": "0"
 * +"dd": ""
 * +"device_token": ""
 * +"email": "#38 Jungang-dong, Gwacheon-si,"
 * +"firstCtnsClasCd": ""
 * +"isAdminYn": "N"
 * +"isExpertYn": "Y"
 * +"isProjectMagBlbdYn": "N"
 * +"logi_time": "175044"
 * +"loginIdType": "01"
 * +"loginType": ""
 * +"membId": "taeho"
 * +"membSectCd": "00301"
 * +"message": "TEST1"
 * +"mm": ""
 * +"msgForMsgNo": "Inquiry completed."
 * +"msgNo": "00002"
 * +"msgType": "N"
 * +"o_acnt_cnt": "01"
 * +"o_at_set_sect": "0"
 * +"o_cid_no": ""
 * +"o_cnct_srvr_nm": "s90i"
 * +"o_cust_nm": ""
 * +"o_cust_use_sect": "2"
 * +"o_dptbrn_no": "***"
 * +"o_f_ver_no": "002.189"
 * +"o_grid_Attr": ""
 * +"o_grid_Cnt": "0001"
 * +"o_grid_Size": "0038"
 * +"o_logi_time1": "175044"
 * +"o_menu_grad": "2A"
 * +"o_ord_mn_sect": "1"
 * +"o_proc_sect": "1"
 * +"o_secu_grad": "001"
 * +"open_key_yn": ""
 * +"otp_yn": ""
 * +"pc_ip": "10.100.100.50"
 * +"pc_ver_no": ""
 * +"rcmdEmail": ""
 * +"roleId": array:1 [?]
 * +"svc_sect": "2"
 * +"user_id": "taeho"
 * +"user_pwd": "JMGTQUGHPUKHSHOLVDOTUDKT"
 * +"ver_proc_sect": ""
 * +"yyyy": ""
 */
class BrokerManager implements Authenticatable
{
    protected $data;

    /**
     * Instantiate BrokerManager class
     *
     * @param null $data
     */
    public function __construct($data = null)
    {
        $this->data = $data;
    }

    /**
     * Get the name of the unique identifier for the user.
     *
     * @return string
     */
    public function getAuthIdentifierName()
    {
        return 'user_id';
    }

    /**
     * Get the unique identifier for the user.
     *
     * @return mixed
     */
    public function getAuthIdentifier()
    {
        return $this->data->{'user_id'};
    }

    /**
     * Get the password for the user.
     *
     * @return string
     */
    public function getAuthPassword()
    {
        return $this->data->{'user_pwd'};
    }

    /**
     * Get the token value for the "remember me" session.
     *
     * @return string
     */
    public function getRememberToken()
    {

    }

    /**
     * Set the token value for the "remember me" session.
     *
     * @param  string $value
     * @return void
     */
    public function setRememberToken($value)
    {

    }

    /**
     * Get the column name for the "remember me" token.
     *
     * @return string
     */
    public function getRememberTokenName()
    {

    }

    /**
     * @return bool
     */
    public function isValid()
    {
        return !is_null($this->data) && !empty($this->data->{'loginIdType'});
    }

    /**
     * @param $key
     * @return mixed
     */
    public function __get($key)
    {
        return $this->getData($key);
    }

    /**
     * @param null $key
     * @return mixed
     */
    public function getData($key = null)
    {
        return !is_null($key) ? (isset($this->data->{$key}) ? $this->data->{$key} : null) : $this->data;
    }

    /**
     * @param $key
     * @param $value
     */
    public function setData($key, $value)
    {
        $this->data->{$key} = $value;
    }

    public function storeInSession()
    {
        if (!is_null($this->data)) {
            Session::put('loginMember', encrypt(json_encode($this->data)));
        }
    }
}
