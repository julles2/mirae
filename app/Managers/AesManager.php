<?php
/**
 * Created by PhpStorm.
 * User: DanielSimangunsong
 * Date: 5/16/2017
 * Time: 12:48 PM
 */

namespace App\Managers;


//??? ??? ??? ?????

class AesManager
{
    protected $key;
    protected $data;
    protected $IV;

    /**
     * @param null $data
     * @param string $key
     */
    function __construct($data = null, $key = 'MiraeAssetSekuritasAESMadeby2fcc')
    {
        $this->setData($data);

        $this->setKey($key);

        $this->IV = substr($this->key, 0, 16);
    }

    public function setData($data)
    {
        $this->data = $data;
    }

    protected function setKey($key)
    {
        $this->key = $key;
    }

    public function encrypt()
    {
        return openssl_encrypt($this->data, "AES-256-CBC", $this->key, 0, $this->getIV());
    }

    public function getIV()
    {
        return $this->IV;
    }

    public function decrypt()
    {
        return openssl_decrypt(base64_decode($this->data), "AES-256-CBC", $this->key, 1, $this->getIV());
    }
}