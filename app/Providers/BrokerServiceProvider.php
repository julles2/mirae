<?php
/**
 * Created by PhpStorm.
 * User: DanielSimangunsong
 * Date: 5/16/2017
 * Time: 5:56 PM
 */

namespace App\Providers;


use App\Managers\AesManager;
use App\Managers\BrokerManager;
use App\Managers\SocketManager;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Contracts\Auth\UserProvider;
use Session;

/**
 * Class BrokerManager
 * @package App\Managers
 *
 * +"a11oi32": {#254 ?}
 * +"a31OI01GridOutRecVo": array:1 [?]
 * +"addMsg": ""
 * +"addr": ""
 * +"bday": ""
 * +"bsno": ""
 * +"cnct_sect": "2"
 * +"cntcNo": ""
 * +"custNm": "LEE TAE HO"
 * +"dble_cnct_conf_sect": "0"
 * +"dd": ""
 * +"device_token": ""
 * +"email": "#38 Jungang-dong, Gwacheon-si,"
 * +"firstCtnsClasCd": ""
 * +"isAdminYn": "N"
 * +"isExpertYn": "Y"
 * +"isProjectMagBlbdYn": "N"
 * +"logi_time": "175044"
 * +"loginIdType": "01"
 * +"loginType": ""
 * +"membId": "taeho"
 * +"membSectCd": "00301"
 * +"message": "TEST1"
 * +"mm": ""
 * +"msgForMsgNo": "Inquiry completed."
 * +"msgNo": "00002"
 * +"msgType": "N"
 * +"o_acnt_cnt": "01"
 * +"o_at_set_sect": "0"
 * +"o_cid_no": ""
 * +"o_cnct_srvr_nm": "s90i"
 * +"o_cust_nm": ""
 * +"o_cust_use_sect": "2"
 * +"o_dptbrn_no": "***"
 * +"o_f_ver_no": "002.189"
 * +"o_grid_Attr": ""
 * +"o_grid_Cnt": "0001"
 * +"o_grid_Size": "0038"
 * +"o_logi_time1": "175044"
 * +"o_menu_grad": "2A"
 * +"o_ord_mn_sect": "1"
 * +"o_proc_sect": "1"
 * +"o_secu_grad": "001"
 * +"open_key_yn": ""
 * +"otp_yn": ""
 * +"pc_ip": "10.100.100.50"
 * +"pc_ver_no": ""
 * +"rcmdEmail": ""
 * +"roleId": array:1 [?]
 * +"svc_sect": "2"
 * +"user_id": "taeho"
 * +"user_pwd": "JMGTQUGHPUKHSHOLVDOTUDKT"
 * +"ver_proc_sect": ""
 * +"yyyy": ""
 */
class BrokerServiceProvider implements UserProvider
{
    /**
     * Retrieve a user by their unique identifier.
     *
     * @param  mixed $identifier
     * @return \Illuminate\Contracts\Auth\Authenticatable|null
     */
    public function retrieveById($identifier)
    {
        $session = Session::get('loginMember');
        if (!is_null($session)) {
            $encode = json_decode(decrypt($session));

            return new BrokerManager($encode);
        }
    }

    /**
     * Retrieve a user by their unique identifier and "remember me" token.
     *
     * @param  mixed $identifier
     * @param  string $token
     * @return \Illuminate\Contracts\Auth\Authenticatable|null
     */
    public function retrieveByToken($identifier, $token)
    {

    }

    /**
     * Update the "remember me" token for the given user in storage.
     *
     * @param  \Illuminate\Contracts\Auth\Authenticatable $user
     * @param  string $token
     * @return void
     */
    public function updateRememberToken(Authenticatable $user, $token)
    {

    }

    /**
     * Retrieve a user by the given credentials.
     *
     * @param  array $credentials
     * @return \Illuminate\Contracts\Auth\Authenticatable|null
     */
    public function retrieveByCredentials(array $credentials)
    {
        try
        {
            $socket = new SocketManager([
                'uid' => (new AesManager(array_get($credentials, 'uid')))->encrypt(),
                'pwd' => (new AesManager(array_get($credentials, 'pwd')))->encrypt()
            ],config('mirae.ip_login'));

            $socket->uidConnector();
            
            return new BrokerManager($socket->getItem());
        }catch(\Exception $e){
            injectModel('LogCustomer')->create(['information'=>\Mirae::stringAgent().' - error message : '.$e->getMessage()]);
            throw new \Exception($e->getMessage(), 1);
        }

    }

    /**
     * Validate a user against the given credentials.
     *
     * @param  \Illuminate\Contracts\Auth\Authenticatable $user
     * @param  array $credentials
     * @return bool
     */
    public function validateCredentials(Authenticatable $user, array $credentials)
    {
        return $user->isValid();
    }

}
