<?php namespace App\Presenters;

use Cache;
use Jenssegers\Agent\Agent;

class TrackLoginPresenter
{
    public $agent;
    public $acnt_no;

    public function __construct()
    {
        $this->agent   = new Agent();
        $this->acnt_no = acnt_no();
    }

    public function data()
    {
        return [
            'acnt_no' => acnt_no(),
            'ip'      => request()->ip(),
            'device'  => $this->agent->device(),
            'browser' => $this->agent->browser(),
        ];
    }

    public function check($acnt_no = "")
    {
        $acnt_no = !empty($acnt_no) ? $acnt_no : acnt_no();
        $result  = false;
        $user    = \Cache::get('online_' . $acnt_no);
        if (!empty($user)) {
            if ($user['ip'] == request()->ip()) {
                $result = true;
            }
        }

        return $result;
    }
    
    public function destroy($acnt_no = "")
    {
    	$acnt_no = !empty($acnt_no) ? $acnt_no : acnt_no();
        \Cache::forget('online_'.$acnt_no);
        if(!empty(\Cache::get($acnt_no)))
        {
            \Cookie::queue(\Cookie::forget($acnt_no));
        }
    }

    public function store_cache()
    {
        \Cache::put('online_' . acnt_no(), $this->data(), 20);
        \Cookie::queue(acnt_no(), acnt_no(), 20);
    }

    public function loginCek($acnt_no = "")
    {
        $acnt_no = !empty($acnt_no) ? $acnt_no : acnt_no();
        $cek_cache = \Cache::get('online_'.$acnt_no);
        $ip = request()->ip();
        $browser = $this->agent->browser();
        $result = false;
        $cookie = \Cookie::get(acnt_no());
        if($cek_cache['acnt_no'] == $acnt_no && $cek_cache['ip'] == $ip && $browser == $cek_cache['browser'] && $cookie == acnt_no())
        {
            $result = true;
        }

        if(empty($cek_cache))
        {
            $result = true;
        }

        return $result;
    }

    public function setTimeExpire()
    {
        \Cookie::queue(\Cookie::forget('login_time'));
        $format = "Y-m-d H:i:s";
        $expire = parse(date($format))->addMinute(20)->format($format);

        $data =  [
            'init'=>date($format),
            'expire'=>$expire,
        ];

        return \Cookie::queue('login_time', $data, 25);
    }

    public function handleExpire()
    {
        $login_time = \Cookie::get('login_time');
        $now        = parse(date("Y-m-d H:i:s"));
        $result     = 'not_expired';
        if (!empty($login_time)) {
            $old    = parse($login_time['init']);
            $expire = parse($login_time['expire']);
            $cek    = $now->diffInMinutes($old);
            // dd($cek);
            if (!empty($cek) && $cek >= 20) {
                $result = 'expired';
            }
        }

        if(empty($login_time))
        {
            $result = 'expired';
        }

        return $result;
    }
}
