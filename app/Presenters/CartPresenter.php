<?php namespace App\Presenters;

use App\Models\SessionCart;
use Cart;

trait CartPresenter
{
    public function cartStoreToDb()
    {
        $cartContent = json_encode(Cart::content());

        if(!empty(@acnt_no()))
        {
            $model = SessionCart::where('acnt_no',@acnt_no())->first();

            if(!empty($model->id))
            {
                $model->update([
                    'data' => $cartContent,
                ]);
            }else{
                SessionCart::create([
                    'acnt_no'=>@acnt_no(),
                    'data'=>$cartContent,
                ]);
            }
        }
    }

    public function rowJsonToArray($row)
    {
        $data = (array)$row;
        unset($data['rowId']);
        unset($data['tax']);
        unset($data['subtotal']);
        $data['options'] = (array)$data['options'];
        return $data;

    }

    public function getCartFromDb()
    {
        $cart = SessionCart::where('acnt_no',@acnt_no())->first();

        $cartBeforeLogin = Cart::content();
        \Cart::destroy();

        $arr = [];
        $ids =[];
        if(count($cartBeforeLogin) > 0)
        {

            foreach($cartBeforeLogin as $row)
            {
                $arr[]=[
                    'id'=>$row->id,
                    'qty'=>$row->qty,
                    'name'=>$row->name,
                    'price'=>$row->price,
					'sale'=>$row->sales,
                    'options'=>(array)json_decode($row->options),
                ];
                $ids[] = $row->id;
            }

            Cart::add($arr);
        }


        if(!empty($cart->id))
        {
            $oldCart = json_decode($cart->data);
            foreach($oldCart as $row)
            {
                $arrRow = $this->rowJsonToArray($row);
                if(!in_array($arrRow['id'],$ids))
                {
                    Cart::add($arrRow);
                }

            }
        }

    }
}
