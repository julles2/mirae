<?php

namespace App;

use Share;

class shareSocialMedia
{
	public function shareSocial($route,$description, $social)
	{
		if ($social == 'twitter') {
	        Share::load($route, $description)->twitter();
	    }elseif($social == 'facebook'){
	        Share::load($route, $description)->facebook();
	    }
	}
}