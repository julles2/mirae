<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        Commands\WebarqConsole::class,
        Commands\Mirae\GrabReksaDana::class,
        Commands\Mirae\GrabDailyNav::class,
        Commands\Mirae\PopChartDaily::class,
        Commands\Mirae\FundPerformancePop::class,
        Commands\Mirae\TransactionTemporary::class,
        Commands\Mirae\All::class,
        Commands\Mirae\BackupDatabase::class,
        Commands\Mirae\DestroyCartDb::class,
        Commands\ClearLog::class,
        Commands\CronSample::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('cron-sample')
        //     ->everyMinute();

        $schedule->command('mirae-trans-temporary')
            ->everyTenMinutes();

        $schedule->command('mirae-grab-daily-nav')
            ->cron('0 */2 * * * *');

        $schedule->command('mirae-grab-reksa-dana')
            ->cron('0 */5 * * * *');

        $schedule->command('mirae-fund-performance-pop')
            ->cron('0 */5 * * * *');

        // $schedule->command('mirae-pop-chart-daily')
        //     ->cron('0 */5 * * * *');

        $schedule->command('mirae-pop-chart-daily')
             ->dailyAt('02:00');

        $schedule->command('mirae-backup-database')
            ->dailyAt('02:00');

        $schedule->command('mirae-clear-log')
         ->monthlyOn(1,'01:00');

         $schedule->command('mirae-destroy-cart')
          ->dailyAt('00:00');
    }
}
