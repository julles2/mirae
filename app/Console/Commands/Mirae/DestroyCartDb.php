<?php

namespace App\Console\Commands\Mirae;

use Illuminate\Console\Command;
use DB;

class DestroyCartDb extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'mirae-destroy-cart';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'To Destroy Cart in session_carts table';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        DB::statement("TRUNCATE TABLE session_carts");
    }
}
