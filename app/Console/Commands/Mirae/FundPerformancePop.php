<?php

namespace App\Console\Commands\Mirae;

use Illuminate\Console\Command;
use Mirae\Console\FundPerformancePop as Query;
use Mirae\MiraeQuery;
use Mirae;
use DB;

class FundPerformancePop extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'mirae-fund-performance-pop';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command Untuk mendapatkan data Daily Nav dari Database Oracle lalu di store ke MySQL (Data di pop up detil product)';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $mirae = new MiraeQuery();
        $query = new Query();
        $this->line('Tunggu');
        DB::beginTransaction();
        try 
        {
            $query->execute();
            DB::commit();
            $mirae->logSuccess('Performa Reksa Dana di Pop Up Produk');
        } catch (\Exception $e) {
           DB::rollback();
           $mirae->logError('Performa Reksa Dana di Pop Up Produk',$e);
        }   
        $this->line('Selesai');
    }
}
