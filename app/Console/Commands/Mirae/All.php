<?php

namespace App\Console\Commands\Mirae;

use Illuminate\Console\Command;

class All extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'mirae-all';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        \Artisan::call('mirae-grab-reksa-dana');
        \Artisan::call('mirae-grab-daily-nav');
        \Artisan::call('mirae-fund-performance-pop');
        \Artisan::call('mirae-pop-chart-daily');
        \Artisan::call('mirae-trans-temporary');
    }
}
