<?php

namespace App\Console\Commands\Mirae;

use Illuminate\Console\Command;
use Mirae\MiraeQuery as Query;
use Mirae;
use App\Models\Oracle\DailyNav;
use DB;

class PopChartDaily extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'mirae-pop-chart-daily';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->query = new Query();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $ora = new DailyNav();
        $last = $ora->dateLast();
        $start = Mirae::yearsAgo($last,3);
        DB::beginTransaction();
        try 
        {
            $this->query->grabDailyNavByRange($start,$last); 
            DB::commit();
            $this->query->logSuccess('Grafik (Chart) Produk');
        } catch (\Exception $e) {
           DB::rollback();
           $this->query->logError('Grafik (Chart) Produk',$e);
        }   
            
    }
}
