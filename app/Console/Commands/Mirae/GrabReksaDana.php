<?php

namespace App\Console\Commands\Mirae;

use Illuminate\Console\Command;
use Mirae\MiraeQuery;

class GrabReksaDana extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'mirae-grab-reksa-dana';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command Untuk mendapatkan data reksa dana (Fund) dari Database Oracle lalu di store ke MySQL';

    /**
     * Create a new command instance.
     *
     * @return void
     */

    public $ora;
    
    public $mySql;

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $query = new MiraeQuery();

        $this->line('Tunggu');
        
        $query->grabReksaDana();

        $this->line('Selesai');
    }
}
