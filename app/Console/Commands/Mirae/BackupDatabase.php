<?php

namespace App\Console\Commands\Mirae;

use Illuminate\Console\Command;
use App\Models\LogCronjob;

class BackupDatabase extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'mirae-backup-database';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try
        {
            $dateTime = date("d-F-Y-H.i.s");
            $path=database_path("backup/".$dateTime.".sql");
            $username = env('DB_USERNAME');
            $password = env('DB_PASSWORD');
            $database = env('DB_DATABASE');
            $command = "mysqldump -u $username -p$password $database > ".$path;
            $returnVar = null;
            $output = null;
            exec($command,$output,$returnVar);
            echo $returnVar;
            LogCronjob::create([
                'message'=>'Database berhasil di backup lokasi file -> '.$path,
                'status'=>'y',
            ]);
        }catch(\Exception $e){
            LogCronjob::create([
                'message'=>'Database gagal di backup error message : '.$e->getException(),
                'status'=>'n',
            ]);
        }
            
    }
}
