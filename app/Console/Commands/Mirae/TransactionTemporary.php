<?php

namespace App\Console\Commands\Mirae;

use Illuminate\Console\Command;
use Mirae;
use Mirae\MiraeQuery as Query;
use DB;

class TransactionTemporary extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'mirae-trans-temporary';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $query = new Query();

        $this->line('Tunggu');
        DB::beginTransaction();
        try 
        {
            $query->balanceTemporary();
            DB::commit();
            $query->logSuccess('Histori Transaksi');
        } catch (\Exception $e) {
           DB::rollback();
           $query->logError('Histori Transaksi',$e);
        }   
       

        $this->line('Selesai');

    }
}
