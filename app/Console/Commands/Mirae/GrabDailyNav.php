<?php

namespace App\Console\Commands\Mirae;

use Illuminate\Console\Command;
use Mirae\Console\GrabDailyNav as DailyNavConsole;

class GrabDailyNav extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'mirae-grab-daily-nav';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command Untuk mendapatkan data Daily Nav dari Database Oracle lalu di store ke MySQL';

    /**
     * Create a new command instance.
     *
     * @return void
     */

    public function __construct()
    {
        parent::__construct();
    }
    
    public function handle()
    {
        $execute = new DailyNavConsole();

        $this->line('Tunggu');
        
        $execute->grabDailyNav();

        $this->line('Selesai');
    }
}
