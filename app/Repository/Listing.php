<?php namespace App\Repository;

use App\Models\Im;
use App\Models\FundInfo;

class Listing
{
    public function __construct()
    {

    }

    public function selectIm()
    {
        $result = [''=> trans('home.selectinvestmentmanager') ];

        $model = Im::orderBy('im_nm','asc');
         $model = $model->lists('im_nm','id')
            ->toArray();

        return $result + $model;
    }

    public function onlyFunds()
    {
        $result = [''=>trans('home.selectmutualfund')];

        $model = FundInfo::orderBy('fund_name','asc');
         $model = $model->lists('fund_name','fund_cd')
            ->toArray();

        return $result + $model;
    }

    public function selectFund($im_id="",$type="")
    {
        $arr = [''=>trans('home.mutualfundname')];
        if(!empty($im_id) || !empty($type))
        {
            $model = new FundInfo();

            if(!empty($im_id))
            {
                $model = $model->where('im_id',$im_id);
            }

            if(!empty($type))
            {
                if($type != 'SR')
                {
                    $model = $model->where('fund_type',$type);
                }else{
                    $model = $model->whereRaw("SUBSTR(fund_cd,8,1)='S'");
                }
            }

            $model= $model->lists('fund_name','fund_cd')
                ->toArray();

            $arr = $arr + $model;
        }

        return $arr;
    }
}
