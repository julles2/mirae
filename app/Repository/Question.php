<?php namespace App\Repository;
use App\Models\Country;

class Question
{

    public function religions()
    {
        $religions = [
            1 => 'Islam',
            2 => 'Protestant',
            3 => 'Catholic',
            4 => 'Hinduism',
            5 => 'Buddhism',
            6 => 'Confucianism',
            7 => 'Others',
        ];

        return $religions;
    }

    public function countries()
    {
        $model = Country::select('code','country')
            ->lists('country','code')
            ->toArray();

        return $model;
    }

    public function riskRange($nilai)
    {
        $ranges = [
            [1 => [0,75]],
            [3 => [80,130]],
            [5 => [131,200]],
        ];

        $result = "";
        $counter = 1;
        foreach($ranges as $index => $range)
        {
            $awal = $range[$counter][0];
            $akhir = $range[$counter][1];
            if($nilai >= $awal && $nilai <= $akhir)
            {
                $result = $counter;
            }

            $counter = $counter + 2;
        }

        return $result;
    }
}
