<?php namespace App\Repository;

use App\Models\FundInfo;
use App\Models\Im;
use App\Models\Oracle\DailyNav as OraDailyNav;
use App\Models\Oracle\Portofolio;
use App\Repository\Listing;

class Home
{
    public $harianOrder;
    public $typeReksadanaHarian;

    public function __construct()
    {
        $this->list                = new Listing();
        $this->modelIm             = new Im();
        $this->modelFund           = new FundInfo();
        $this->harianOrder         = 'desc';
        $this->modelPorto          = new Portofolio();
        $this->oraDailyNav         = new oraDailyNav();
        $this->typeReksadanaHarian = 'EQ';
    }

    public function selectTypeFund()
    {
        $display = ['EQ', 'MX', 'FI', 'SR', 'MM'];
        $types   = $this->modelFund->types();
        $result  = [];
        foreach ($types as $key => $val) {
            if (in_array($key, $display)) {
                $result[$key] = $val;
            }
        }
        return $result;

    }

    public function categories()
    {
        return [
            ''  => trans('global.category'),
            '-' => trans('global.all'),
            's' => trans('global.sharia'),
            'k' => trans('global.conventional'),
        ];
    }

    public function listTime()
    {
        return [
            'one_day'      => '1 ' . trans('global.day'),
            'three_weeks'  => '1 ' . trans('global.week'),
            'one_month'    => '1 ' . trans('global.month'),
            'three_months' => '3 ' . trans('global.month'),
            'six_months'   => '6 ' . trans('global.month'),
            'one_years'    => '1 ' . trans('global.year'),
            'three_years'  => '3 ' . trans('global.year'),
            'five_years'   => '5 ' . trans('global.year'),
        ];
    }

    public function listingReturnHarian()
    {
        $model = $this->modelFund
            ->select('fund_infos.id', 'fund_infos.fund_cd', 'fund_name as reksa_dana', 'one_day as return', 'nav_per_unit as nav')
            ->join('daily_navs', 'daily_navs.fund_id', '=', 'fund_infos.id')
            ->orderBy('one_day', 'desc')
            ->limit(10)
            ->get();

        $data = [];

        foreach ($model as $row) {
            $row    = $row->toArray();
            $icon   = $row['return'] > 0 ? '' : 'down';
            $data[] = ['icon' => $icon] + $row;
        }

        return $data;
    }

    public function selectIm()
    {
        return $this->list->selectIm();
    }

    public function onlyFunds()
    {
        return $this->list->onlyFunds();
    }

    public function selectFund($im_id = "", $type = "")
    {
        return $this->list->selectFund($im_id, $type);
    }

    public function performaReksaDanaHarian()
    {
        $type = $this->typeReksadanaHarian;

        $model = $this->modelFund
            ->select('fund_infos.fund_cd', 'im_nm', 'fund_name', 'nav_per_unit', 'one_day', 'one_month', 'one_years')
            ->join('im', 'im.id', '=', 'fund_infos.im_id')
            ->rightJoin('daily_navs', 'daily_navs.fund_id', '=', 'fund_infos.id');

        if ($type != 'SR') {
            $model = $model->where('fund_type', $type);
        } else {
            $model = $model->whereRaw("SUBSTR(fund_cd,8,1)='S'");
        }

        $model = $model->limit(8)
            ->orderBy('one_day', 'desc')
            ->get();

        return $model;
    }
}
