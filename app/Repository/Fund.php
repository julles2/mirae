<?php namespace App\Repository;

use App\Models\Oracle\FundInfo as OraFund;
use App\Models\FundInfo;
use App\Models\Watchlists;

class Fund
{
    public function __construct()
    {

    }

    public function listReksaDana($limit="")
    {
        $modelOra = OraFund::select('fund_cd')
            ->get()
            ->toArray();

        $fund_cds = array_flatten($modelOra);

        $model = FundInfo::select('fund_infos.fund_cd','im_nm','fund_name','nav_per_unit','one_day','one_month','one_years','fund_images.image as image')
            ->leftJoin('im','im.id','=','fund_infos.im_id')
            ->leftJoin('fund_images','fund_images.fund_cd','=','fund_infos.fund_cd')
            ->leftJoin('daily_navs','daily_navs.fund_id','=','fund_infos.id');

        if(!empty($limit))
        {
            $model = $model->limit($limit);
        }

        $model = $model->get();
        return $model;
    }

    public function listReksaDanaRisk($number)
    {
        $modelOra = OraFund::select('fund_cd')->where('fund_risk',$number)
            ->get()
            ->toArray();

        $fund_cds = array_flatten($modelOra);

        $model = FundInfo::select('fund_infos.fund_cd','im_nm','fund_name','nav_per_unit','one_day','one_month','one_years','fund_images.image as image')
            ->leftJoin('im','im.id','=','fund_infos.im_id')
            ->leftJoin('daily_navs','daily_navs.fund_id','=','fund_infos.id')
            ->leftJoin('fund_images','fund_images.fund_cd','=','fund_infos.fund_cd')
            ->whereIn('fund_infos.fund_cd',$fund_cds)
            ->get();

        

        return $model;

    }

    public function htmlWatchlist($fundCd, $acnt){
        $data = Watchlists::where('fund_cd',$fundCd)
                                ->where('acnt_no', $acnt)
                                ->first();
        if (!empty($data->id)) {
            return 'style="pointer-events: none;cursor: default;"';
        }else{
            return "false";
        }
    }
}
