<?php namespace App\Repository;
use Auth;
use App\Models\Oracle\ClientInfo;

class User
{
    public $user;
    public $client;
    public $statusUser;

    public function __construct()
    {
        $this->user = Auth::guard('broker')->user();
        $this->client = new ClientInfo();
        $this->status_user = $this->statusUser();
    }

    public function profile()
    {
        if(!empty($this->user->a31OI01GridOutRecVo[0]))
        {
            return $this->user->a31OI01GridOutRecVo[0];
        }
    }

    public function statusUser()
    {
        $user = $this->user;
        $profile = $this->profile();
        $status = false;

        $client = $this->client->where('acnt_no',@$profile->o_acnt_no)->first();
        if(!empty($client->acnt_no))
        {
            if(!empty($client->ifua_cd))
            {
                $status = true;
            }
        }

        $result = [
            'status'=>$status,
            'risk_profile'=>$client->risk_profile,
        ];

        return (object)$result;
    }
}
