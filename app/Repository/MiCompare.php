<?php namespace App\Repository;

use App\Repository\Mi;
use Mirae;

class MiCompare extends Mi
{
    public $defaultStartDate;
    public $defaultEndDate;
    public $periode;

    public function __construct()
    {
        $this->periode = $this->list_periode();
        parent::__construct();
        $this->defaultEndDate    = date("Y-m-d");
        $this->defaultStartDate  = $this->defaultStartDate();
        $this->defaultEndMonth   = $this->defaultEndMonth();
        $this->defaultStartMonth = $this->defaultStartMonth();
        $this->lastMonthNow      = parse(date("Y-m"))->endOfMonth()->format("Ymd");
        $this->feed_im_aum       = new \App\Models\Feed\ImAum();
    }

    public function list_periode()
    {
        return [
            // '1'   => language('1 Mth', '1 Bl'),
            '3'   => language('3 Mth', '3 Bl'),
            '6'   => language('6 Mth', '6 Bl'),
            '9'   => language('9 Mth', '9 Bl'),
            '12'  => language('1 Yr', '1 Th'),
            '24'  => language('2 Yr', '2 Th'),
            '36'  => language('3 Yr', '3 Th'),
            'ytd' => 'YTD',
        ];
    }

    public function lastMonthNowQuery($imCode)
    {
        $now = $this->feed_im_aum
            ->where('sinvest_code', $imCode)
            ->whereRaw('CAST(period as SIGNED) <= ' . $this->lastMonthNow)
            ->orderBy(\DB::raw('CAST(period as SIGNED)'), 'desc')
            ->first();

        if (!empty($now->periode)) {
            return $now->periode;
        } else {
            return $this->lastMonthNow;
        }
    }

    public function defaultStartMonth()
    {
        if (!empty(get('start'))) {
            $result = get('start');
        } else {
            if (!empty(get('month'))) {
                $result = get('month') == 'ytd' ? date("Y-01-01") : Mirae::monthsAgo(get('month'));
            } else {
                $result = date("Y-01");
            }
        }

        return $result;
    }

    public function defaultEndMonth()
    {
        return $month = !empty(get('end')) ? get('end') : date("Y-m");
    }

    public function defaultStartDate()
    {
        $periode = array_keys($this->periode);

        $month = !empty(get('month')) ? get('month') : $periode[0];
        $day   = $month * 30;
        return Mirae::daysAgo($day, $this->defaultEndDate);
    }

    public function monthComboStart()
    {
        $startDate = $this->defaultStartDate;

        $month = parse($startDate)->format('m');

        return $month;
    }

    public function yearComboStart()
    {
        $startDate = $this->defaultStartDate;

        $month = parse($startDate)->format('Y');

        return $month;
    }

    public function monthComboEnd()
    {
        $endDate = $this->defaultEndDate;

        $month = parse($endDate)->format('m');

        return $month;
    }

    public function yearComboEnd()
    {
        $endDate = $this->defaultEndDate;

        $month = parse($endDate)->format('Y');

        return $month;
    }

    public function range()
    {
        return $this->dateFromToByDay($this->defaultStartDate, $this->defaultEndDate);
    }

    public function rangeMonth()
    {
        return $this->dateFromTo($this->defaultStartMonth, $this->defaultEndMonth);
    }

    public function handleCode()
    {
        $url       = parse_url(request()->fullUrl());
        $urlResult = "";
        if (!empty($url['query'])) {
            $urlQuery = $url['query'];
            parse_str($urlQuery, $urlResult);
            unset($urlResult['month']);
            unset($urlResult['start_month']);
            unset($urlResult['start_year']);
            unset($urlResult['end_month']);
            unset($urlResult['end_year']);
            unset($urlResult['start']);
            unset($urlResult['end']);
        }

        return $urlResult;
    }

    public function queryStringCode()
    {
        return http_build_query($this->handleCode());
    }

    public function urlNonMonth()
    {
        if (!empty($this->handleCode())) {
            $url = urlLang('manager-investment-comare?') . http_build_query($this->handleCode());
            return $url;
        }
    }

    public function queryData($imCode, $periode)
    {
        $year    = substr($periode, 0, 4);
        $month   = substr($periode, 4, 2);
        $periode = parse($periode)->format("Y-m-d");
        $model   = $this->imAum
            ->select('aum as aum_idr', 'total_unit as unit_idr', 'aum_usd', 'total_unit_usd as unit_usd')
            ->where('sinvest_code', $imCode)
        //->where('periode',$periode)
            ->whereRaw("MONTH(period) = $month AND YEAR(period)=$year")
            ->first();

        return $model;
    }

    public function collectData()
    {
        $resultAum  = [];
        $resultUnit = [];
        if (!empty($this->handleCode())) {
            foreach ($this->handleCode() as $imCode) {
                if (!empty($imCode)) {
                    $im = $this->im->whereImCd($imCode)->firstOrFail();

                    $aum  = [];
                    $unit = [];

                    foreach ($this->range() as $day) {
                        $query  = $this->queryData($imCode, $periode);
                        $aum[]  = !empty($query->aum_idr) ? floatval($query->aum_idr) : null;
                        $unit[] = !empty($query->unit_idr) ? floatval($query->unit_idr) : null;
                    }

                    $resultAum[] = [
                        'name' => $im->im_nm,
                        'data' => $aum,
                    ];

                    $resultUnit[] = [
                        'name' => $im->im_nm,
                        'data' => $unit,
                    ];
                }
            }
        }

        return (object) [
            'unit' => collect($resultUnit),
            'aum'  => collect($resultAum),
        ];
    }

    public function queryDataMonthly($imCode, $periode)
    {
        $model = $this->data
            ->select('aum_idr', 'unit_idr', 'aum_usd', 'unit_usd')
            ->where('im_cd', $imCode)
            ->where('periode', $periode)
            ->first();

        return $model;
    }

    public function collectDataMonthly()
    {
        $resultAum     = [];
        $resultAumUsd  = [];
        $resultUnit    = [];
        $resultUnitUsd = [];
        if (!empty($this->handleCode())) {
            foreach ($this->handleCode() as $imCode) {
                if (!empty($imCode)) {
                    $im      = $this->im->whereImCd($imCode)->firstOrFail();
                    $aum     = [];
                    $aumUsd  = [];
                    $unit    = [];
                    $unitUsd = [];

                    foreach ($this->rangeMonth() as $month) {
                        $periode   = parse($month . '-01')->endOfMonth()->format("Ymd");
                        $query     = $this->queryData($imCode, $periode);
                        $aum[]     = !empty($query->aum_idr) ? floatval($query->aum_idr) : null;
                        $aumUsd[]  = !empty($query->aum_usd) ? floatval($query->aum_usd) : null;
                        $unit[]    = !empty($query->unit_idr) ? floatval($query->unit_idr) : null;
                        $unitUsd[] = !empty($query->unit_usd) ? floatval($query->unit_usd) : null;
                    }

                    $resultAum[] = [
                        'name' => $im->im_nm,
                        'data' => $aum,
                    ];

                    $resultAumUsd[] = [
                        'name' => $im->im_nm,
                        'data' => $aumUsd,
                    ];

                    $resultUnit[] = [
                        'name' => $im->im_nm,
                        'data' => $unit,
                    ];

                    $resultUnitUsd[] = [
                        'name' => $im->im_nm,
                        'data' => $unitUsd,
                    ];
                }
            }
        }

        return (object) [
            'unit'     => collect($resultUnit),
            'aum'      => collect($resultAum),
            'aum_usd'  => collect($resultAumUsd),
            'unit_usd' => collect($resultUnitUsd),
        ];
    }

    public function listRingkasan()
    {
        $imCodes = @array_flatten(@$this->handleCode());
        $model   = $this->im
            ->whereIn('im_cd', $imCodes)
            ->get();

        return $model;
    }

    public function fieldRingkasan()
    {
        return [
            'aum_idr'  => 'AUM (IDR)',
            'aum_usd'  => 'AUM (USD)',
            'unit_idr' => 'UNIT (IDR)',
            'unit_usd' => 'UNIT (USD)',
        ];
    }

    public function labelRingkasan()
    {
        return [

        ];
    }

    public function formula($dataToday, $dataOldDate)
    {
        //dd($dataToday.' - '.$dataOldDate);
        return $result = ($dataToday / $dataOldDate - 1) * 100;
    }

    // public function queryRingkasan($imCode, $date)
    // {
    //     $periode = $this->data
    //         ->select('periode')
    //         ->where('im_cd', $imCode)
    //         ->whereRaw("CAST(periode AS SIGNED) <=$date")
    //     // ->whereRaw("MONTH(periode)=$date->month")
    //     // ->whereRaw("YEAR(periode)=$date->year")
    //         ->orderBy(\DB::raw('CAST(periode as SIGNED)'), 'desc')
    //         ->first();

    //     //dd(parse($periode->periode));
    //     $datePeriode = !empty($periode->periode) ? parse($periode->periode) : parse($date);

    //     $model = $this->data
    //         ->select('id', 'aum_idr', 'aum_usd', 'unit_idr', 'unit_usd', 'periode')
    //         ->where('im_cd', $imCode)
    //         ->whereRaw("MONTH(periode)=$datePeriode->month")
    //         ->whereRaw("YEAR(periode)=$datePeriode->year")
    //         ->orderBy(\DB::raw('CAST(periode as SIGNED)'), 'desc')
    //         ->first();

    //     return $model;
    // }

    public function queryRingkasan($imCode, $date)
    {
        $date  = parse($date)->format("Y-m-d");
        $model = $this->feed_im_aum
            ->select('aum as aum_idr', 'aum_usd', 'total_unit as unit_idr', 'total_unit_usd as unit_usd', 'period as periode')
            ->where('sinvest_code', $imCode)
            ->whereRaw("period='$date'")
            ->first();
        return $model;
    }

    public function ringkasanByDate($imCode, $date)
    {
        return $this->queryRingkasan($imCode, $date);
    }

    public function dataNow($imCode)
    {
        $date  = $this->lastMonthNow;
        $model = $this->ringkasanByDate($imCode, $date);
        return $model;
    }

    public function queryThreeMonths($imCode)
    {
        $date = Mirae::threeMonthsAgo($this->lastMonthNow);
        return $this->ringkasanByDate($imCode, $date);
    }

    public function querySixMonths($imCode)
    {
        $date = Mirae::firstDateOfMonth($this->lastMonthNowQuery($imCode), -6);
        // $date = Mirae::lastDateOfMonth($this->lastMonthNowQuery($imCode), -6);
        $model = $this->ringkasanByDate($imCode, $date);
        return $model;
    }

    public function queryNineMonths($imCode)
    {
        // $date = Mirae::lastDateOfMonth($this->lastMonthNowQuery($imCode), -9);
        $date  = Mirae::firstDateOfMonth($this->lastMonthNowQuery($imCode), -9);
        $model = $this->ringkasanByDate($imCode, $date);
        return $model;
    }

    public function queryOneYear($imCode)
    {
        // $date = Mirae::lastDateOfMonth($this->lastMonthNowQuery($imCode), -12);
        $date  = Mirae::firstDateOfMonth($this->lastMonthNowQuery($imCode), -12);
        $model = $this->ringkasanByDate($imCode, $date);
        return $model;
    }

    public function queryYtd($imCode)
    {
        $date = date("Y-01-01");

        return $this->ringkasanByDate($imCode, $date);
    }

    public function collectRingkasan($now, $old)
    {
        $result = [];

        foreach (array_keys($this->fieldRingkasan()) as $f) {
            if (!empty($old->{$f}) && !empty($now->{$f})) {
                $result[$f] = $this->formula($now->{$f}, $old->{$f});

            } else {
                $result[$f] = 0;
            }
        }
        return (object) $result;
    }

    public function threeMonths($imCode)
    {
        $old    = $this->queryThreeMonths($imCode);
        $now    = $this->dataNow($imCode);
        $result = $this->collectRingkasan($now, $old);
        return (object) $result;
    }

    public function sixMonths($imCode)
    {
        $old    = $this->querySixMonths($imCode);
        $now    = $this->dataNow($imCode);
        $result = $this->collectRingkasan($now, $old);
        return (object) $result;
    }

    public function nineMonths($imCode)
    {
        $old    = $this->queryNineMonths($imCode);
        $now    = $this->dataNow($imCode);
        $result = $this->collectRingkasan($now, $old);
        return (object) $result;
    }

    public function oneYear($imCode)
    {
        $old    = $this->queryOneYear($imCode);
        $now    = $this->dataNow($imCode);
        $result = $this->collectRingkasan($now, $old);
        return (object) $result;
    }

    public function ytd($imCode)
    {
        $old    = $this->queryYtd($imCode);
        $now    = $this->dataNow($imCode);
        $result = $this->collectRingkasan($now, $old);
        return (object) $result;
    }

    public function ringkasan($imCode)
    {
        // $result = [
        //     //'three_months'=>$this->threeMonths($imCode),
        //     'six_months'  => $this->sixMonths($imCode),
        //     'nine_months' => $this->nineMonths($imCode),
        //     'one_year'    => $this->oneYear($imCode),
        //     'YTD'         => $this->ytd($imCode),
        // ];

        // return (object) $result;
    }

    public function to_object_fields($model)
    {
        if(!empty($model))
        {
            $model->aum_idr = bagi_milyar($model->aum_idr);
            $model->aum_usd = bagi_juta($model->aum_usd);
            $model->unit_idr = bagi_juta($model->unit_idr);
            $model->unit_usd = bagi_ribu($model->unit_usd);
            $result = $model->toArray();
        }else{
            $result = [
                'aum_idr'  => '0',
                'aum_usd'  => '0',
                'unit_idr' => '0',
                'unit_usd' => '0',
            ];
        }
        return (object)$result;
    }

    public function ringkasan_feed($imCode)
    {
        $sixMonths = $this->to_object_fields($this->querySixMonths($imCode));
        $nineMonths = $this->to_object_fields($this->queryNineMonths($imCode));
        $oneYear = $this->to_object_fields($this->queryOneYear($imCode));
        $ytd = $this->to_object_fields($this->queryYtd($imCode));
        
        $result = [
            //'three_months'=>$this->threeMonths($imCode),
            'six_months'  => $sixMonths,
            'nine_months' => $nineMonths,
            'one_year'    => $oneYear,
            'YTD'         => $ytd,
        ];

        $result = (object) $result;
        // dd($result->six_months->aum_idr);
        return $result;
    }

    // public function select_mi()
    // {
    //     return 1;
    // }
}
