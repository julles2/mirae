<?php namespace App\Repository;

use App\Models\ImData;
use App\Models\Feed\ImAum;
use App\Models\Im;
use DB;

class Mi
{
    public $data;
    public $im;

    public function __construct()
    {
        $this->data = new ImData();
        $this->im = new Im();
        $this->imAum = new ImAum();
    }

    // public function sum($imCode,$date)
    // {
    //     $month = parse($date)->format('m');
    //     $year = parse($date)->format('Y');

    //     $model = $this->imAum
    //         ->select(DB::raw("SUM(`total_unit`) as unit_idr , SUM(`aum`) as aum_idr,SUM(`aum_usd`) as aum_usd , SUM(`total_unit_usd`) as unit_usd"))
    //         ->whereRaw("MONTH(`period`) = '$month'")
    //         ->whereRaw("YEAR(`period`)='$year'")
    //         ->where('sinvest_code',$imCode)
    //         ->first();
        
    //     return $model;
    // }

    public function sum($imCode,$date)
    {
        $month = parse($date)->format('m');
        $year = parse($date)->format('Y');

        $model = $this->imAum
            ->select(DB::raw("SUM(`total_unit`) as unit_idr , SUM(`aum`) as aum_idr,SUM(`aum_usd`) as aum_usd , SUM(`total_unit_usd`) as unit_usd"))
            ->whereRaw("MONTH(`period`) = '$month'")
            ->whereRaw("YEAR(`period`)='$year'")
            ->where('sinvest_code',$imCode)
            ->first();
        
        return $model;
    }

    public function listing()
    {
        $model = $this->imAum->orderBy('period','desc');

        return $model;
    }

    public function dateFromTo($stringFrom,$stringTo) // (string) date("Y-m")
    {
        $from = parse($stringFrom);
        $to = parse($stringTo);
        $data = [];
        while($from <= $to)
        {
            $data[] = $from->format("M Y");
            $from->addMonth();
        }
        return $data;
    }

    public function dateFromToByDay($stringFrom,$stringTo,$formatDate="") // (string) date("Y-m")
    {
        $from = parse($stringFrom);
        $to = parse($stringTo);
        $data = [];
        $resultFormatDate = !empty($formatDate) ? $formatDate : 'd M Y';
        while($from <= $to)
        {
            $data[] = $from->format($resultFormatDate);
            $from->addDay();
        }
        return $data;
    }

    public function charts($imCode,$from,$to,$currency)
    {
        $dates =  $this->dateFromTo($from,$to);
        $currency = !empty($currency) ? $currency : 'idr';
        $fieldAum = "aum_$currency";
        $fieldUnit = "unit_$currency";
        $aumIdr = [];
        $unitIdr = [];
        foreach($dates as $date)
        {
            $model = $this->sum($imCode,$date);
            $aumIdr[] = floatval($model->{$fieldAum});
            $unitIdr[] = floatval($model->{$fieldUnit});
        }

        $aum = [
            'name'=>'Total AUM',
            'data'=>$aumIdr
        ];
        $unit = [
            'name'=>'Total Unit',
            'data'=>$unitIdr
        ];
        $result = [$aum,$unit];
        return $result;
    }

    public function select_mi()
    {
        $model = $this->im
            ->orderBy('im_nm','asc')
            ->get()
            ->lists('im_nm','im_cd')
            ->toArray();

        return [''=>trans('home.selectinvestmentmanager')] + $model;
    }
}
