<?php namespace App\Repository;

use App\Models\FundInfo;
use App\Models\Oracle\DailyNav as OraNav;
use App\Models\PopChartDaily;
use Cache;

class Compare
{
    public $colors = [
        '#f97105',
        '#307aab',
        '#1b2f48',
    ];

    public $periode;

    public $month;

    public $now;

    public $defaultField;

    public function __construct()
    {
        $this->data         = Cache::get('compare_funds' . request()->ip());
        $this->modelFund    = new FundInfo();
        $this->oraNav       = new OraNav();
        $this->month        = @$this->data['fund_month'];
        $this->now          = date("Ymd");
        $this->fund         = new FundInfo();
        $this->daily        = new PopChartDaily();
        $this->defaultField = @$this->data['fund_field'];
        $this->periode      = $this->list_periode();
    }

    public function list_periode()
    {
        return [
            '1'  => language('1 Mth', '1 Bl'),
            '3'  => language('3 Mth', '3 Bl'),
            '6'  => language('6 Mth', '6 Bl'),
            '9'  => language('9 Mth', '9 Bl'),
            '12' => language('1 Yr', '1 Th'),
            '24' => language('2 Yr', '2 Th'),
            '36' => language('3 Yr', '3 Th'),
        ];
    }

    public function lastDataFromNav($fundCd)
    {
        return $this->oraNav->lastData($fundCd);
    }

    public function fundByCode($code)
    {
        $model = $this->modelFund
            ->whereIn('fund_cd', $code)
            ->get();

        return $model;
    }

    public function dates($varFormat = "")
    {
        if (strlen($this->month) > 2) // jika pilihanya adalah range
        {
            $date      = explode("-", $this->month);
            $old       = carbon()->parse($date[0]);
            $this->now = $date[1];
        } else {
            $old = carbon()->parse()->subMonths($this->month);
        }

        $result = [];

        $format = !empty($varFormat) ? $varFormat : "d-M-Y";

        while ($old->format("Ymd") <= $this->now) {
            $date = $old->format($format);
            if (cekSatOrSun($date) == false) {
                $result[] = $date;
            }
            $old = $old->addDay();
        }
        return $result;

    }

    // public function queryData($fundCd, $field)
    // {
    //     $cacheId = $fundCd . '_' . $field . '_' . $this->month;
    //     $result  = [];
    //     $result  = \Cache::remember($cacheId, 0, function () use ($fundCd, $field, $result) {
    //         $fund_id = $this->fund->where('fund_cd', $fundCd)->first()->id;
    //         $dates   = $this->dates("Ymd");
    //         foreach ($this->dates("Ymd") as $date) {
    //             $model = $this->daily
    //                 ->select($field, 'tr_date')
    //                 ->where('fund_id', $fund_id)
    //                 ->where($field, '!=', '0.00000000');

    //             $cekFirstdate = $model->orderBy('tr_date', 'asc')->first();

    //             if (!empty($cekFirstdate->tr_date)) {
    //                 if ((int) $date >= (int) $cekFirstdate->tr_date) {

    //                     $query = $model->where('tr_date', $date)->first();
    //                     if (!empty($query->{$field})) {
    //                         $data     = $query->{$field};
    //                         $result[] = floatval($data);
    //                     } else {
    //                         continue;
    //                     }
    //                 } else {
    //                     $result[] = null;
    //                 }
    //             } else {
    //                 $result[] = null;
    //             }
    //         }

    //         return $result;
    //     });

    //     return $result;
    // }

    public function selectReturn($fundCd)
    {
        // rumus : one_day_kemarin + bareksa_sekarang;

        $model  = new \App\Models\Feed\FundReturn;
        $dates  = $this->dates("Y-m-d");
        $result = [];
        $one_day_kemarin = 0;
        $debuging = [];
        foreach ($this->dates("Ymd") as $date) {
            $cek = $model->select('id', 'oneday')->where('sinvest_code', $fundCd)->where('date', $date)->first();
            if (!empty($cek->id)) {
                $hasil = $cek->oneday + $one_day_kemarin;
                $result[] = $hasil;
                // $result[] = floatval($cek->oneday * 100);
                // $debuging[] = [
                //     'bareksa'=> $cek->oneday,
                //     'sekarang'=>$hasil,
                //     'one_day_kemarin' => $one_day_kemarin,
                // ];
                $one_day_kemarin = $hasil;
            } else {
                $result[] = null;
            }
        }

        return $result;
    }

    public function selectAumUnit($fundCd, $field)
    {
        $resultField = $field == 'aum' ? 'value' : 'total_unit';
        $model       = new \App\Models\Feed\FundAum;
        $dates       = $this->dates("Y-m-d");
        $result      = [];
        foreach ($dates as $date) {
            $cek = $model->select('id', $resultField)->where('sinvest_code', $fundCd)->where('period', $date)->first();
            if (!empty($cek->id)) {
                $result[] = floatval($cek->{$resultField});
            } else {
                $result[] = null;
            }
        }

        return $result;
    }

    public function queryData($fundCd, $field)
    {
        $cacheId = $fundCd . '_' . $field . '_' . $this->month;
        $result  = [];
        $result  = \Cache::remember($cacheId, 0, function () use ($fundCd, $field, $result) {
            $fund_id = $this->fund->where('fund_cd', $fundCd)->first()->id;
            if ($field == 'nav') {
               return $this->selectReturn($fundCd);
            } else {
                 return $this->selectAumUnit($fundCd, $field);
            }
        });

        return $result;
    }

    public function data($fundCodes)
    {
        $result = [];
        $no     = 0;
        foreach ($fundCodes as $key => $val) {
            if (!empty($val) && $no <= 2) {
                $model     = $this->fund->select('fund_name')->where('fund_cd', $val)->firstOrFail();
                $queryData = $this->queryData($val, $this->defaultField);
                $result[]  = [
                    'name'  => $model->fund_name,
                    'data'  => $queryData,
                    'color' => $this->colors[$no],
                ];
                $no++;
            }
        }

        return $result;
    }

    // public function data($fundCodes)
    // {
    //     $result = [];
    //     $no     = 0;
    //     foreach ($fundCodes as $key => $val) {
    //         if (!empty($val) && $no <= 2) {
    //             $model    = $this->fund->select('fund_name')->where('fund_cd', $val)->firstOrFail();
    //             $result[] = [
    //                 'name'  => $model->fund_name,
    //                 //'data'=>$this->queryData($val,$this->defaultField),
    //                 'data'  => $this->queryData($val, $this->defaultField),
    //                 'color' => $this->colors[$no],
    //             ];
    //             $no++;
    //         }
    //     }

    //     return $result;
    // }

    public function handleFundCodeUrl()
    {
        $url       = parse_url(request()->fullUrl());
        $urlResult = "";
        if (!empty($url['query'])) {
            $urlQuery = $url['query'];
            parse_str($urlQuery, $urlResult);
            unset($urlResult['m']);
            unset($urlResult['f']);
            unset($urlResult['filter']);
        }

        return $urlResult;
    }

    public function handleFundCodeCache()
    {
        $param = 'compare_funds' . request()->ip();
        $cache = Cache::get($param);
        if (!empty($cache)) {
            $fundCodes = $cache['funds'];
        } else {
            $fundCodes = [];
        }

        return $fundCodes;
    }

    public function handleSelectedIm($paramFundCodes = "")
    {
        $fundCodes = !empty($paramFundCodes) ? $paramFundCodes : array_flatten($this->handleFundCodeCache());
        $result    = [];
        foreach ($fundCodes as $code) {
            $model = $this->fund
                ->where('fund_cd', $code)
                ->first();

            if (!empty($model->id)) {
                if (!empty($model->im->id)) {
                    $result[] = $model->im->id;
                }
            }
        }

        return $result;
    }

    public function filterTypes()
    {
        $filter    = get('filter');
        $classType = function ($key) use ($filter) {
            if ($key == $filter) {
                return 'active';
            }
        };

        $allClass = function () use ($filter) {
            $types = array_keys($this->fund->types());
            if (!in_array($filter, $types)) {
                return 'active';
            }
        };

        $result = [
            'ALL' => [
                'image' => 'cat-prod-0.png',
                'trans' => 'type_fund.all',
                'class' => $allClass(),
            ],
            'EQ'  => [
                'image' => 'cat-prod-1.png',
                'trans' => 'type_fund.eq',
                'class' => $classType('EQ'),
            ],
            'MX'  => [
                'image' => 'cat-prod-2.png',
                'trans' => 'type_fund.mx',
                'class' => $classType('MX'),
            ],
            'FI'  => [
                'image' => 'cat-prod-3.png',
                'trans' => 'type_fund.fi',
                'class' => $classType('FI'),
            ],
            'MM'  => [
                'image' => 'cat-prod-5.png',
                'trans' => 'type_fund.mm',
                'class' => $classType('MM'),
            ],
            'SR'  => [
                'image' => 'cat-prod-4.png',
                'trans' => 'type_fund.sr',
                'class' => $classType('SR'),
            ],
        ];

        return (object) $result;
    }

    public function dateTextBox()
    {
        $from = "";
        $to   = "";
        if (strlen($this->month) > 2) // jika pilihanya adalah range
        {
            $date = explode("-", $this->month);
            $from = carbon()->parse($date[0])->format("Y-m-d");
            $to   = carbon()->parse($date[1])->format("Y-m-d");
        }

        return (object) [
            'from' => $from,
            'to'   => $to,
        ];
    }
}
