<?php namespace App\Traits;

use Mirae;
use DB;
use App\Models\DailyNav;

trait ProductTrait
{
    public function model_daily_nav()
    {
        return new DailyNav();
    }

    public function connection()
    {
        return DB::connection('feed');
    }

    public function aum_table()
    {
        return $this->connection()->table('mutualfund_aum');
    }

    public function aum_nav()
    {
        return $this->connection()->table('mutualfund_nav');
    }

    public function three_years_ago()
    {
        return parse(Mirae::yearsAgo(date("Y-m-d"), 3))->format("Y-m-d");
    }

    public function chart_aum($fundCd)
    {
        $model =  $this->aum_table()
            ->select('value', 'period')
            ->where('period', '>=', $this->three_years_ago())
            ->where('sinvest_code', $fundCd)
            ->get();

        $value = [];
        $date = [];
        foreach ($model as $row) {
            $value[] = Mirae::roundSubstr($row->value);
            $date[] = parse($row->period)->format("d-M-Y");
        }

        return ['date' => $date,'value'=>$value];
    }

    public function chart_nav($fundCd)
    {
        $model =  $this->aum_nav()
            ->select('value', 'date')
            ->where('date', '>=', $this->three_years_ago())
            ->where('sinvest_code', $fundCd)
            ->get();

        $value = [];
        $date = [];
        foreach ($model as $row) {
            $value[] = Mirae::roundSubstr($row->value);
            $date[] = parse($row->date)->format("d-M-Y");
        }

        return ['date' => $date,'value'=>$value];
    }

    public function chart_unit($fundCd)
    {
        $model =  $this->aum_table()
            ->select('total_unit', 'period')
            ->where('period', '>=', $this->three_years_ago())
            ->where('sinvest_code', $fundCd)
            ->get();

        $value = [];
        $date = [];
        foreach ($model as $row) {
            $value[] = Mirae::roundSubstr($row->total_unit);
            $date[] = parse($row->period)->format("d-M-Y");
        }

        return ['date' => $date,'value'=>$value];
    }

    public function fund_performance_pop($model)
    {
        return $this->model_daily_nav()
            ->where('fund_id', $model->id)
            ->orderBy(\DB::raw("CAST(TR_DATE AS SIGNED)"), 'desc')
            ->first();
    }
}
