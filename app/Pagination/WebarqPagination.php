<?php
namespace App\Pagination;

use Illuminate\Pagination\BootstrapThreePresenter;


class WebarqPagination extends BootstrapThreePresenter {


    public function render()
    {
        if ($this->hasPages()) {
            // return sprintf(
            //     '<div class="pagi-custom"><div class="pull-left">%s %s</div> <div class="pull-right">%s %s</div></div>',
            //     $this->getFirst(),
            //     $this->getButtonPre(),
            //     $this->getButtonNext(),
            //     $this->getLast()
            // );

            return $this->getPagingAll();
        }
        return "";
    }


    public function getLast()
    {
        $url = $this->paginator->url($this->paginator->lastPage());
        $btnStatus = '';
        if($this->paginator->lastPage() == $this->paginator->currentPage()){
            $btnStatus = 'disabled';
        }
        return $btn = "<a href='".$url."'><button class='btn btn-success ".$btnStatus."'>Last <i class='glyphicon glyphicon-chevron-right'></i></button></a>";
    }


    public function getFirst()

    {
        $url = $this->paginator->url(1);
        $btnStatus = '';
        if(1 == $this->paginator->currentPage()){
            $btnStatus = 'disabled';
        }
        return $btn = "<a href='".$url."'><button class='btn btn-success ".$btnStatus."'><i class='glyphicon glyphicon-chevron-left'></i> First</button></a>";
    }


    public function getButtonPre()
    {
        $url = $this->paginator->previousPageUrl();
        $btnStatus = '';
        if(empty($url)){
            $btnStatus = 'disabled';
        }
        return $btn = '<a href="'.$url.'" class="prev '.$btnStatus.'"><i class="icwp ic_prev_paging"></i></a>';
    }


    public function getButtonNext()
    {
        $url = $this->paginator->nextPageUrl();
        $btnStatus = '';
        if(empty($url)){
            $btnStatus = 'disabled';
        }
        return $btn = '<a href="'.$url.'" class="next '.$btnStatus.'"><i class="icwp ic_next_paging"></i></a>';

    }

    public function getNextPagination()
    {
        if (! $this->paginator->hasMorePages()) {
            return '<li class="pagination-next disabled">Next</li>';
        }

        $url = $this->paginator->url($this->paginator->currentPage() + 1);

        return '<li class="pagination-next"><a href="'.htmlentities($url).'" aria-label="Next page">Next</a></li>';
    }


    public function getPagingAll()
    {
        if ($this->paginator->hasPages())
        {
            $rtn = '<div class="paging a-center">';

                if ($this->paginator->currentPage() == 1) 
                {
                    $rtn .= '<a class="prev disabled"><i class="icwp ic_prev_paging"></i></a>';                   
                }else{
                    $rtn .= '<a href="'.$this->paginator->previousPageUrl().'" class="prev"><i class="icwp ic_prev_paging"></i></a>';
                }


                if($this->paginator->currentPage() > 3)
                {
                    $rtn .= '<a href="'.$this->paginator->url(1).'">1</a></a>';
                }

                if($this->paginator->currentPage() > 4)
                {
                    // $rtn .= '<a href="'.$this->paginator->url($this->paginator->firstItem()).'">....</a>';
                    $rtn .= '<a>....</a>';
                }


                foreach(range(1, $this->paginator->lastPage()) as $i)
                {
                    if($i >= $this->paginator->currentPage() - 2 && $i <= $this->paginator->currentPage() + 2)
                    {
                        if ($i == $this->paginator->currentPage())
                        {
                            $rtn .= '<a class="active">'.$i.'</a>';
                        }else{
                            $rtn .= '<a href="'.$this->paginator->url($i).'">'.$i.'</a>';
                        }
                    }
                }

                if($this->paginator->currentPage() < $this->paginator->lastPage() - 3)
                {
                    // $rtn .= '<a href="'.$this->paginator->url($this->paginator->lastPage()).'">....</a>';
                    $rtn .= '<a>....</a>';
                }

                if($this->paginator->currentPage() < $this->paginator->lastPage() - 2)
                {
                    $rtn .= '<a href="'.$this->paginator->url($this->paginator->lastPage()).'">'.$this->paginator->lastPage().'</a>';
                }


                if ($this->paginator->hasMorePages())
                {
                    $rtn .= '<a href="'.$this->paginator->nextPageUrl().'" class="next"><i class="icwp ic_next_paging"></i></a>';
                }else{
                    $rtn .= '<a class="next disabled"><i class="icwp ic_next_paging"></i></a>';
                }

            $rtn .= '</div>';
            return $rtn;
        }

        return '';
    }


}