<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Bantuan extends Model
{
    public $guarded = [];

    public function translations()
    {
        $this->hasMany(\App\Models\BantuanTranslation::class,'bantuan_id');
    }
}
