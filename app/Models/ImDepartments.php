<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ImDepartments extends Model
{
    protected $guarded = [];

    public function im()
    {
        return $this->belongsTo(\App\Models\Im::class,'im_cd','im_cd');
    }
}
