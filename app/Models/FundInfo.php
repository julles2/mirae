<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Oracle\DailyNav as OraNav;
use App\Models\Oracle\Transaction;
use App\Models\FundInfoDetail;
use App\Models\FundDetailCompany;
use App\Models\FundImage;
use Mirae;
use App\Models\Feed\FundReturn;
use App\Models\Feed\FundAum;

class FundInfo extends Model
{
    public $guarded = [];

    public function feed_returns()
    {
        return $this->hasMany(FundReturn::class,'sinvest_code','fund_cd');
    }

    public function feed_aums()
    {
        return $this->hasMany(FundAum::class,'sinvest_code','fund_cd');
    }    

    public function getFeedLatestReturnAttribute()
    {
        $data = $this->feed_returns()
            ->select('oneday as return')
            ->orderBy('date','desc')
            ->first();
        
        $icon = $data->return > 0 ? '' : 'down';

        return [
            'return'=>$data->return,
            'nav'=> 0,
            'icon'=>$icon,
            'reksa_dana'=>$this->fund_name,
            'fund_cd'=>$this->fund_cd,
        ];
    }

    public function info_details()
    {
        return $this->hasMany(FundInfoDetail::class,'fund_id');
    }

    public function company_details()
    {
        return $this->hasMany(FundDetailCompany::class,'fund_id');
    }

    public function get_image()
    {
        return $this->hasOne(FundImage::class,'fund_cd','fund_cd');
    }

    public function daily_navs()
    {
        return $this->hasMany(\App\Models\DailyNav::class,'fund_id');
    }

    public function daily_nav()
    {
        return $this->hasOne(\App\Models\DailyNav::class,'fund_id');
    }

    public function fund_performance()
    {
        return $this->hasOne(\App\Models\FundPerformancePop::class,'fund_id');
    }

    public function fund_orcl()
    {
        return $this->hasOne(\App\Models\Oracle\FundInfo::class,'fund_cd','fund_cd');
    }

    public function getPortofolioAttribute()
    {
        $model = \App\Models\Oracle\Portofolio::where('fund_cd',$this->fund_cd)
            ->where('acnt_no',acnt_no())
            ->first();

        return $model;
    }

    public function img()
    {
        return $this->hasOne(\App\Models\FundImage::class,'fund_cd','fund_cd');
    }

    public function content_details()
    {
        return $this->hasMany(\App\Models\FundInfoDetail::class,'fund_id');
    }

    public function fund_performance_pop()
    {
        return $this->hasMany(\App\Models\FundPerformancePop::class,'fund_id');
    }

    public function fund_param()
    {
        return $this->hasMany(\App\Models\FundParam::class,'fund_id');
    }


    public function im()
    {
        return $this->belongsTo(\App\Models\Im::class,'im_id');
    }

    // Helper

    public function nabLast($fund_id)
    {
        $latestNav = $this->find($fund_id)
            ->daily_navs()
            ->select('nav_per_unit')
            ->where('fund_id',$fund_id)
            ->orderBy(\DB::raw("CAST(tr_date AS SIGNED)"),'desc')
            ->first();

        return !empty($latestNav->nav_per_unit) ? $latestNav->nav_per_unit : 0;
    }

    public function queryListing($type="" , $limit="",$fundName="")
    {
        $limit = !empty($limit) ? $limit : $this->limitFunds();

        $query = $this->join('daily_navs','daily_navs.fund_id','=','fund_infos.id');

        // if($limit != 'nonlimit')
        // {
        //    $query = $this->limit($limit);
        // }

        if(!empty(get('category')))
        {
            if(get('category')=='s')
            {
                 $query = $query->whereRaw("SUBSTR(fund_cd,8,1)='S'");
               // dd($query->get());
            }
        }

        if(!empty($type))
        {
            if($type != 'SR')
            {
                $query = $query->where('fund_type',$type);
            }else{
                $query = $query->whereRaw("SUBSTR(fund_cd,8,1)='S'");
            }
        }

        if(!empty(get('return')))
        {
            $query = $query->where(get('time'),'like',"%".get('return')."%");
        }
        //dd($query->get());
        if(!empty($fundName))
        {
            $query = $query->whereRaw("LOWER(fund_name) LIKE '%".strtolower($fundName)."%'");
        }

        return $query->orderBy('fund_name','asc');
    }

    public function limitFunds()
    {
        return 15;
    }

    public function queryListingLoadMore($type="",$limit="",$offset="",$fundName="")
    {
        $limit = !empty($limit) ? $limit : $this->limitFunds();
        $offset = !empty($offset) ? $offset : 0;
        $query = $this->join('daily_navs','daily_navs.fund_id','=','fund_infos.id');
        if(!empty(get('category')))
        {
            if(get('category')=='s')
            {
                 $query = $query->whereRaw("SUBSTR(fund_cd,8,1)='S'");
            }
        }

        if(!empty($type))
        {
            if($type != 'SR')
            {
                $query = $query->where('fund_type',$type);
            }else{
                $query = $query->whereRaw("SUBSTR(fund_cd,8,1)='S'");
            }
        }

        // if(!empty(get('return')))
        // {
        //     // $query = $query->whereRaw("ROUND(".get('time').",2)=".get('return'));
        //     $return = floatval(get('return'));
        //     $fromField = floor($return);
        //     $toField = $fromField + 0.99;
        //     $query = $query->where(get('time'),">=",$fromField)->where(get('time'),"<=",$toField);
        // }

        if(!empty(get('return')))
        {
            $time = get("time");
            $return = str_replace(",", ".", get('return'));
            $fromField = $return;
            $toField = ceil($fromField);
            $query = $query->whereRaw("ROUND($time,2) >= $fromField");
            // $query = $query->where("ROUND($time,2)", ">=","$fromField"));

        }

        if(!empty($fundName))
        {
            $query = $query->whereRaw("LOWER(fund_name) LIKE '%".strtolower($fundName)."%'");
        }
        // dd($query->first());
        if(!empty(get('sort_by')))
        {
            $sortBy = get('sort_by');
            $sortByOrder = get('sort_by_order');
            $query->orderBy($sortBy,$sortByOrder);
        }else{
            $query = $query->orderBy('fund_name','asc');
        }

        $query = $query->limit($limit)->offset($offset);
        return $query;
    }

    public function listingFunds($type="",$limit="",$fundName)
    {
        return $this->queryListing($type,$limit,$fundName)
            ->get();
    }

    public function paginationFunds($type="",$limit="")
    {
        $limit = !empty($limit) ? $limit : $this->limitFunds();

        $count =  $this->queryListing($type,$limit)
            ->count();

        if($count > 0)
        {
            $setengah = floor($count / 2);

            return [
                15=>15,
                $setengah=>$setengah,
                $count=>$count,
            ];
        }else{
            return [$limit=>$limit];
        }

    }

    public function types()
    {
        return [
                'MM'=>'Money Market',
                'FI'=>'Fixed Income',
                'EQ'=>'Equity',
                'MX'=>'Mixed Asset',
                'IF'=>'Index',
                'PF'=>'Capital Protected',
                'GR'=>'Guaranteed',
                'PE'=>'Private Equity',
                'ET'=>'Exchange Traded',
                'RE'=>'Real Estate',
                'AR'=>'CIC Asset',
                'SR'=>'Syariah',
            ];
    }

    public function getResultTypeAttribute($val)
    {
        $types = $this->types();

        $cekSyariah = substr(strtolower($this->fund_cd),8,1);
/*        if($cekSyariah == 's')
        {
            $result = "Syariah";
        }else{

            if(array_key_exists($this->fund_type, $types))
            {
                $result = $types[$this->fund_type];
            }
        }
*/		
		if(array_key_exists($this->fund_type, $types))
            {
                $result = $types[$this->fund_type];
            }

        return $result;
    }

    public function labelType($fundCd,$fundType)
    {
        $result = null;

        $types = $this->types();

        $cekSyariah = substr(strtolower($fundCd),8,1);
        if($cekSyariah == 's')
        {
            $result = "Syariah";
        }else{
            if(array_key_exists($fundType, $types))
            {
                $result = $types[$fundType];
            }
        }
		
/*		if(array_key_exists($fundType, $types))
            {
                $result = $types[$fundType];
            }
*/
        return $result;
    }

    public function percentageOrAmount($model,$field)
    {

        $result = 2;
        if($model->{$field} > 0 && $model->{$field} <=10)
        {
            $result = 1;
        }

        return $result;
    }

    public function fee($model,$bill)
    {
        $bill = formatInteger($bill);
        $feeType = $this->percentageOrAmount($model,'fee_subs');
        $feeSubs = !empty($model->fee_subs) ? $model->fee_subs : 0;
        $result = 0;
        if($feeType == 2)
        {
            $result = $feeSubs;
        }else{ // persent
            if($feeSubs !=0)
            {
                $result = $feeSubs / 100 * $bill;
            }
        }

        return $result;
    }

    public function feeRedm($model)
    {
        $feeType = $this->percentageOrAmount($model,'penjualan');

        $feeRedm = !empty($model->fee_redm) ? $model->fee_redm : 0;

        return $feeRedm;
    }

    public function feeSwc($model)
    {
        $feeType = $this->percentageOrAmount($model,'fee_swtc');

        $feeRedm = !empty($model->fee_swtc) ? $model->fee_swtc : 0;

        return $feeRedm;
    }

    public function getResultFeeRedmAttribute()
    {
        return $this->feeRedm($this);
    }

    public function getResultFeeSwcAttribute()
    {
        return $this->feeSwc($this);
    }

    public function getResultImageAttribute($val)
    {
        $img = asset("noimage.png");
        if(!empty($this->img->image))
        {
                $img = asset('contents/'.$this->img->image);
        }

        return $img;
    }

    public function getRoundOneDayAttribute($val)
    {
        // return valueDecimal($this->one_day);
        $result = formatUangKoma2($this->one_day);
        if($this->one_day == 0)
        {
            $result = '0.00';
        }
        return $result;
    }

    public function getRoundThreeWeeksAttribute($val)
    {
        // return valueDecimal($this->three_weeks);
        $result = formatUangKoma2($this->three_weeks);
        if($this->three_weeks == 0)
        {
            $result = '0.00';
        }
        return $result;
    }

    public function getRoundOneMonthAttribute($val)
    {
        // return valueDecimal($this->one_month);
        $result = formatUangKoma2($this->one_month);
        if($this->one_month == 0)
        {
            $result = '0.00';
        }
        return $result;
    }

    public function getRoundThreeMonthsAttribute($val)
    {
        // return valueDecimal($this->three_months);
        $result = formatUangKoma2($this->three_months);
        if($this->three_months == 0)
        {
            $result = '0.00';
        }
        return $result;
    }

    public function getRoundSixMonthsAttribute($val)
    {
        // return valueDecimal($this->six_months);
        $result = formatUangKoma2($this->six_months);
        if($this->six_months == 0)
        {
            $result = '0.00';
        }
        return $result;
    }

    public function getRoundOneYearsAttribute($val)
    {
        // return valueDecimal($this->one_years);
        $result = formatUangKoma2($this->one_years);
        if($this->one_years == 0)
        {
            $result = '0.00';
        }
        return $result;
    }

    public function getRoundYtdAttribute($val)
    {
        // return valueDecimal($this->ytd);
        $result = formatUangKoma2($this->ytd);
        if($this->ytd == 0)
        {
            $result = '0.00';
        }
        return $result;
    }

    public function getRoundThreeYearsAttribute($val)
    {
        // return valueDecimal($this->three_years);
        $result = formatUangKoma2($this->three_years);
        if($this->three_years == 0)
        {
            $result = '0.00';
        }
        return $result;
    }

    public function getRoundFiveYearsAttribute($val)
    {
        // return valueDecimal($this->five_years);
        $result = formatUangKoma2($this->five_years);
        if($this->five_years == 0)
        {
            $result = '0.00';
        }
        return $result;
    }

    public function getAumListingAttribute($val)
    {
        return bagi_milyar($this->aum);
    }

    public function getUnitListingAttribute($val)
    {
        return bagi_juta($this->outstanding_unit);
    }

    public function getLastNabAttribute($val)
    {
        $fundOrcl = new OraNav();
        if(!empty($this->fund_cd))
        {
            return $fundOrcl->nabLast($this->fund_cd);
        }
    }

    public function getLastUnitAttribute($val)
    {
        // $fundOrcl = new OraNav();

        // $lastNav = $fundOrcl->lastData($this->fund_cd);
        // if(!empty($lastNav->tr_date))
        // {
        //         return $lastNav->outstanding_unit;
        // }else{
        //     return 'w';
        // }

        $model = $this->feed_aums()
            ->select('total_unit')
            ->orderBy('period','desc')
            ->first();
        $result = 0;
        if(!empty($model->total_unit))
        {
            $result = $model->total_unit;
        }
        return $result;
    }

    public function getLastAumAttribute($val)
    {
        $model = $this->feed_aums()
            ->select('value')
            ->orderBy('period','desc')
            ->first();
        $result = 0;
        if(!empty($model->value))
        {
            $result = $model->value;
        }
        return $result;
    }

    public function convertAmountToUnit($field)
    {
        $resultField = $this->{$field};
        $unit = '-';
        if($resultField != 0 || $resultField != null)
        {
            $oraNav = new OraNav();
            $lastNab = $oraNav->nabLast($this->fund_cd);
            $unit = $resultField / $lastNab;
        }
        return $unit;
    }

    public function getUnitMinBalanceAttribute()
    {
        return $this->convertAmountToUnit('min_balance');
    }

    public function getUnitMinRedeemAttribute()
    {
        return $this->convertAmountToUnit('min_redm_amt');
    }

    public function getUnitMinSwitchingInAttribute()
    {
        return $this->convertAmountToUnit('min_swtc_in_amt');
    }

    public function getUnitMinSwitchingOutAttribute()
    {
        return $this->convertAmountToUnit('min_swtc_out_amt');
    }




    // public function feeRedm($model,  $bill,$orderType)
    // {
    //     $modelNav = new OraNav();
    //     $date = $modelNav->lastDayByFundCd($model->fund_cd);
    //     $lastNav = $modelNav->manipulateNab($model->fund_cd,\Mirae::yesterday($date),$date);
    //     //$lastNav = $modelNav->nabLast($model->fund_cd);
    //     $bill = formatInteger($bill);

    //     $feeType = $this->percentageOrAmount($model,'penjualan');

    //     $feeRedm = !empty($model->fee_redm) ? $model->fee_redm : 0;

    //     $result = 0;
    //     if($feeType == 2) // nominal
    //     {
    //         $result = $feeRedm;
    //     }else{ // persent
    //         if($feeRedm !=0)
    //         {
    //             $result = $feeRedm / 100 * $bill;
    //         }
    //     }
    //     dd($result);
    //     return $result;
    // }

    //
}

