<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PopChartDaily extends Model
{
	protected $table = 'pop_chart_daily';

    protected $guarded = [];

    public function fund()
    {
    	return $this->belongsTo(\App\Models\FundInfo::class,'fund_id');
    }
}
