<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Oracle\DailyNav as OraDailyNav;

class FundParam extends Model
{
    protected $guarded = [];

    public function fund()
    {
    	return $this->belongsTo(\App\Models\FundInfo::class,'fund_id');
    }

    public function aum($fundId,$date="")
    {
    	$ora = new OraDailyNav();

        $date = !empty($date)?$date : $ora->dateLast();;

        $model = $this->select('aum')
            ->where('tr_date',$date)
            ->where('fund_id',$fundId)
            ->first();
        return notEmpty(@$model->aum);
    }
}
