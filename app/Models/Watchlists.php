<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\FundInfo;
class Watchlists extends Model
{
    public $guarded = [];

    public function fund()
    {
    	return $this->belongsTo(FundInfo::class,'fund_cd','fund_cd');
    }
}
