<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class MilestonesTranslations extends Model
{
    use Sluggable;

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title',
            ]
        ];
    }

    public $guarded = [];

    public function milestones()
    {
        return $this->hasMany(\App\Models\Milestones::class,'milestone_id');
    }
}
