<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;
class NewsTranslation extends Model
{
    use Sluggable;

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title',
            ]
        ];
    }

    public $guarded = [];

    public function news()
    {
        return $this->belongsTo(\App\Models\News::class,'news_id');
    }
}
