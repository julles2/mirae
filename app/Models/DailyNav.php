<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DailyNav extends Model
{
    protected $guarded = [];

    public function fund()
    {
        return $this->belongsTo(\App\Models\FundInfo::class, 'fund_id');
    }

    public function dateLast()
    {
        $model = $this->select('tr_date')
            ->orderBy(\DB::raw("CAST(tr_date AS SIGNED)"), 'desc')
            ->first();

        return @$model->tr_date;
    }
}
