<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\FundInfo;

class TransactionTemporary extends Model
{
    public $fillable = [
    	'tr_date',
    	'tr_time',
    	'acnt_no',
    	'ifua_cd',
    	'fund_cd',
    	'ref_no',
        'bill',
    	'unit',
    	'type',
        'fee',
        'total_fee',
        'source',
        'to_fund',
        'recur_date',
        'last_nav',
        'status',
        'date_cancel',
        'time_cancel',
        'periode_debet',
        'risk',
		'sales',
    ];

    public function oracle_subscription_or_redeem()
    {
        return $this->hasOne(\App\Models\Oracle\Transaction::class,'ref_no','ref_no');
    }

    public function oracle_auto_debet()
    {
        return $this->hasOne(\App\Models\Oracle\TransactionAutoDebet::class,'ref_no','ref_no');
    }

    public function oracle_swc()
    {
        return $this->hasOne(\App\Models\Oracle\TransactionSwitching::class,'ref_no','ref_no');
    }

    public function fund()
    {
        return $this->belongsTo(FundInfo::class,'fund_cd','fund_cd');
    }

    public function user()
    {
        return $this->belongsTo(\App\Models\Oracle\VCustInfo::class,'acnt_no','acnt_no');
    }

    public function toFund()
    {
        return $this->belongsTo(FundInfo::class,'to_fund','fund_cd');
    }

    public function sumUnit($fundCd,$userId="")
    {
        $userId = !empty($userId) ? $userId : acnt_no();
        $unit = $this
            ->select(\DB::raw("SUM(unit) as unit"))
            ->where('fund_cd',$fundCd)
            ->where('acnt_no',$userId)
            ->whereIn('type',['r','sw'])
            ->where('status','active')
            ->first();
        $result = 0;

        if(!empty($unit->unit))
        {
            $result = $unit->unit;
        }

        return $result;
    }

    public function types($val)
    {
        $arr = [
            's'=>'Pembelian',
            'sa'=>'Pembelian',
            'sw'=>'Pengalihan',
            'r'=>'Penjualan'
        ];

        if(array_key_exists($val,$arr))
        {
            return $arr[$val];
        }
    }

    public function getLabelTypeAttribute($val)
    {
        return $this->types($this->type);
    }

    public function getPersentAttribute($val)
    {
        return \Mirae::percentageOrAmount($this->fee) == 1 ? "(".$this->fee."%)" : \Mirae::percentageOrAmount($this->fee);
    }

    public function getStatusOracleAttribute($value)
    {
        $status = "";
        if($this->type == 's' || $this->type == 'r')
        {
            $oracleSubs = $this->oracle_subscription_or_redeem;
            if(!empty($oracleSubs->tr_date))
            {
                $status = $this->oracle_subscription_or_redeem->result_status;
            }
        }elseif($this->type == 'sa'){
            $oracleSubs = $this->oracle_auto_debet;
            if(!empty($oracleSubs->tr_date))
            {
                if(!empty($this->oracle_subscription_or_redeem->result_status))
                {
                    $status = $this->oracle_subscription_or_redeem->result_status;
                }
            }
        }

        return $status;
    }

    public function transactionToday($fundCode,$types) // cek pembelian pada hari yang sama satu fundcode
    {
        $date = date("Ymd");
        $data = ['exist'=>'false'];
        $first = $this->where('fund_cd',$fundCode)
            ->where('tr_date',$date)
            ->where('acnt_no',acnt_no())
            ->whereIn('type',$types)
            ->orderBy('ref_no','desc')
            ->where('status','active')
            ->first();

        if(!empty($first->id))
        {
            $data = [
                'ref_no'=>$first->ref_no,
                'tr_date'=>mirae_date($first->tr_date),
                'tr_time'=>mirae_time($first->tr_time),
                'exist'=>'true',
                'status'=>$first->status_oracle,
            ];
        }

        return (object)$data;
    }

    public function transactionTodaySwc($from,$to,$types) // cek pembelian pada hari yang sama satu fundcode
    {
        $date = date("Ymd");
        $data = ['exist'=>'false'];
        //dd($from.' '.$to);
        $first = $this->where('fund_cd',$from)
            ->where('to_fund',$to)
            ->where('tr_date',$date)
            ->where('acnt_no',acnt_no())
            ->whereIn('type',$types)
            ->orderBy('ref_no','desc')
            ->where('status','active')
            ->first();

        if(!empty($first->id))
        {
            $data = [
                'ref_no'=>$first->ref_no,
                'tr_date'=>mirae_date($first->tr_date),
                'tr_time'=>mirae_time($first->tr_time),
                'exist'=>'true',
                'status'=>$first->status_oracle,
            ];
        }

        return (object)$data;
    }

}
