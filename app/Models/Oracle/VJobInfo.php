<?php

namespace App\Models\Oracle;

use Yajra\Oci8\Eloquent\OracleEloquent;

class VJobInfo extends OracleEloquent
{
    protected $table = 'v_job_info';
    protected $connection = 'oracle';
}
