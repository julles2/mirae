<?php

namespace App\Models\Oracle;

use Yajra\Oci8\Eloquent\OracleEloquent;

class VBankInfo extends OracleEloquent
{
    protected $table = 'v_bank_info';
    protected $connection = 'oracle';
}
