<?php

namespace App\Models\Oracle;

use Yajra\Oci8\Eloquent\OracleEloquent;
use Mirae;
class TransactionAutoDebet extends OracleEloquent
{
    protected $table = 'F0T202P';

    protected $primaryKey = 'ref_no';

    public $incrementing = false;

    protected $connection = 'oracle';

    public $timestamps = false;

    protected $guarded = [];

    public function fund()
    {
        return $this->belongsTo(\App\Models\Oracle\FundInfo::class,'fund_cd','fund_cd');
    }

    public function makeRefNo()
    {
    	$now = date("Ymd");

        $query = $this->where('tr_date',$now)
            ->where('ref_no','like','%_YP%')
            ->orderBy("ref_no",'desc')
            ->first();
        $number=0;

        if(!empty($query->ref_no))
        {
            $expRefNo = explode("_", $query->ref_no);
            if(!empty($expRefNo[1]))
            {
                $number = Mirae::onlyNumber($expRefNo[1]);
            }
        }
        $makeLoop =  Mirae::makeLoopId($number);

        $result = "$now"."_YP"."$makeLoop";

        return $result;
    }

    public function status($status)
    {
        $arr = [
            '0'=>'Request Approval',
            '1'=>'Confirm Approval',
            '9'=>'Reject',
        ];

        if(array_key_exists($status, $arr))
        {
            return $arr[$status];
        }
    }

    public function getResultStatusAttribute($val)
    {
        return $this->status($this->proc_cd);
    }

    public function getFind($trDate,$userId,$fundCd,$recureDate)
    {
        $model =  $this->where('tr_date',$trDate)
                        ->where('acnt_no',$userId)
                        ->where('fund_cd',$fundCd)
                        ->where('recur_date',$recureDate)
                        ->first();
        return $model;
    }

    public function getAmountAttribute($val)
    {
        return $this->ord_amt;
    }

    public function getFeeMoneyAttribute($value)
    {
        return \Mirae::resultPercentageOrAmount($this->fee,$this->amount);
    }

    public function getFeePersentAttribute($value)
    {
        $cek = \Mirae::percentageOrAmount($this->fee);

        return $cek == '1' ? "(".$this->fee." % )" : "";

    }

    public function getTotalOrderAttribute($value)
    {
        $result = $this->ord_amt + $this->fee_money;

        return $result;
    }
}
