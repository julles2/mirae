<?php

namespace App\Models\Oracle;

use Yajra\Oci8\Eloquent\OracleEloquent;

use App\Models\TransactionTemporary;

class VCashBal extends OracleEloquent
{
    protected $table = 'V_CASH_BAL';

    protected $connection = 'oracle';

    protected $primaryKey = 'ACNT_NO';

    public function getBalance($actNo)
    {
    	$model = $this->find($actNo);

        $temporaryBill = TransactionTemporary::select(\DB::raw("SUM(bill) as bill")) // statusnya yg 30 // cron
            ->where('type','s')
            ->where('acnt_no',$actNo)
            ->where('status','active')
            ->first();

    	$result =  $model->cash_bal - $temporaryBill->bill;

        return $result;
    }

    public function balance()
    {
        // $accountNo = auth()->guard('broker')->user()->accountData->accountNum;
    	$accountNo = acnt_no();
    	return $this->getBalance($accountNo);
    }

}
