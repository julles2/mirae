<?php

namespace App\Models\Oracle;

use App\Models\Oracle\DailyNav;
use App\Models\Oracle\FundInfo;
use App\Models\Oracle\Transaction;
use App\Models\Oracle\TransactionHistory;
use App\Models\Oracle\TransactionHistory as History;
use App\Models\Oracle\TransactionSwitching;
use App\Models\TransactionTemporary;
use Yajra\Oci8\Eloquent\OracleEloquent;

class Portofolio extends OracleEloquent
{
    protected $table = 'F0T300P';

    protected $connection = 'oracle';

    public function fund()
    {
        return $this->belongsTo(FundInfo::class, 'fund_cd', 'fund_cd');
    }

    public function minimum_transaksi($fundCd) // dalam unit

    {
        $oraNav = new DailyNav();

        $lastNab = $oraNav->nabLast($fundCd); // rupiah

        return $lastNab;
    }

    public function information($fundCd, $acntNo)
    {
        $modelPortofolio = $this->where('acnt_no', $acntNo)->where('fund_cd', $fundCd)->first();

        $oraNav        = new DailyNav();
        $latestDataNav = $oraNav->lastData($fundCd);

        $lastNab     = $latestDataNav->nav_per_unit;
        $dateLastNab = $latestDataNav->date->format("d-M-Y");

        $unit                   = !empty($modelPortofolio->unit) ? $modelPortofolio->unit : 0;
        $nilaiInvestasiSemula   = $modelPortofolio->avg_price * $unit;
        $nilaiInvestasiTerakhir = $modelPortofolio->unit * $lastNab;
        $gainLos                = $nilaiInvestasiTerakhir - $nilaiInvestasiSemula;

        $nilaiInvestasiSemula   = round($nilaiInvestasiSemula);
        $nilaiInvestasiTerakhir = round($nilaiInvestasiTerakhir);

        @$bagi          = $nilaiInvestasiTerakhir / $nilaiInvestasiSemula;
        $return         = ($bagi - 1) * 100;
        $cekMinusOrPlus = $return > 0 ? 'up' : 'down';

        return (object) [
            'unit'                     => $unit,
            'nilai_investasi_semula'   => $nilaiInvestasiSemula,
            'nilai_investasi_terakhir' => $nilaiInvestasiTerakhir,
            'gain_loss'                => $gainLos,
            'return'                   => $return,
            'minus_or_plus'            => $cekMinusOrPlus,
            'last_nab'                 => $lastNab,
            'date_last_nab'            => $dateLastNab,
            'tes'                      => round($return),
        ];
    }

    public function getMinusOrPlusAttribute($val)
    {
        return $cekMinusOrPlus = $this->return > 0 ? 'up' : 'down';
    }

    public function getLastNavAttribute($val)
    {
        $oraNav = new DailyNav();

        $lastNab = $oraNav->nabLast($this->fund_cd);

        return $lastNab;
    }

    public function getDateLastNavAttribute()
    {
        $oraNav      = new DailyNav();
        $dateLastNab = $oraNav->lastDayByFundCd($fundCd);
        return carbon()->parse($dateLastNab)->format("d-M-Y");
    }

    public function getNilaiInvestasiTerakhirAttribute()
    {
        $nilaiInvestasiTerakhir = $this->unit * $this->last_nav;

        return $nilaiInvestasiTerakhir;
    }

    public function getNilaiInvestasiSemulaAttribute($val)
    {
        $unit = !empty($this->unit) ? $this->unit : 1;

        $nilaiInvestasiSemula = $this->avg_price * $unit;

        return $nilaiInvestasiSemula;
    }

    public function getGainLossAttribute($val)
    {
        return $gainLos = $this->nilaiInvestasiTerakhir - $this->nilaiInvestasiSemula;
    }

    public function getReturnAttribute($val)
    {
        $bagi = $this->nilaiInvestasiSemula > 0 ? $this->nilaiInvestasiTerakhir / $this->nilaiInvestasiSemula : 0;

        $return = ($bagi - 1) * 100;

        return $return;
    }

    public function unitTemporary($fundCd, $userId = "")
    {
        $userId         = !empty($userId) ? $userId : acnt_no();
        $temporary      = new TransactionTemporary();
        $portofolioUnit = $this->select('unit')
            ->where('acnt_no', $userId)
            ->where('fund_cd', $fundCd)
            ->first();

        $temporaryUnit = $temporary->sumUnit($fundCd, $userId);

        return $unitTemporary = $portofolioUnit->unit - $temporaryUnit;
    }

    public function convertBalanceToUnit($fundCd)
    {
        $fund = new FundInfo();

        $oraNav = new DailyNav();

        $model = $fund->select('min_balance')->where('fund_cd', $fundCd)->first();

        $balance = $model->min_balance ? $model->min_balance : 0;

        $lastNab = $oraNav->nabLast($fundCd);

        $unit = $balance / $lastNab;

        return $unit;
    }

    public function userNilaiInvestasiTerakhir($varUser = "")
    {
        $oraNav = new DailyNav();
        $user   = !empty($user) ? $varUser : acnt_no();
        $model  = $this
            ->select('unit', 'fund_cd')
            ->where('acnt_no', $user)
            ->where('unit', '>', '0')
            ->get();
        $result = 0;
        foreach ($model as $row) {
            $nabLast = $oraNav->nabLast($row->fund_cd);
            $kali    = $row->unit * $nabLast;
            $result  = $result + $kali;
        }
        return $result;
    }

    public function haveFund($fundCd, $acnt_no = "")
    {
        $res_acnt_no = !empty($acnt_no) ? $acnt_no : acnt_no();
        $model       = $this->where('fund_cd', $fundCd)
            ->where('acnt_no', $res_acnt_no)
            ->first();
        return $model;
    }

    public function investment_or_increment($fundCd, $acnt_no = "")
    {
        $cek = $this->haveFund($fundCd, $acnt_no = "");
        if (!empty($cek->fund_cd)) {
            return 'min_increment';
        } else {
            return 'min_investment';
        }
    }

    public function cekAcntNo()
    {
        if (empty(acnt_no())) {
            throw new \Exception("acnt_no not found", 1);
        }
    }

    public function firstSubscriptionDate($fundCd)
    {
        $this->cekAcntNo();

        $model = Transaction::select(\DB::raw("cast(tr_date as integer) as tr_date"))
            ->where('acnt_no', acnt_no())
            ->where('fund_cd', $fundCd)
            ->where('proc_cd', 70)
            ->where('tr_type', 'S')
            ->orderBy(\DB::raw("cast(tr_date as integer)"), 'asc')
            ->first();
        return $model->tr_date;
    }

    public function sumSubscriptionByAcntNo($date, $fundCd)
    {
        $this->cekAcntNo();

        $model = Transaction::select("tr_date")
            ->where('tr_date', $date)
            ->where('acnt_no', acnt_no())
            ->where('tr_type', 'S')
            ->where('fund_cd', $fundCd)
            ->where('proc_cd', 70)
            ->sum('amount');

        return floatval($model);
    }

    public function hasilInvestasiByAcntNo($date, $fundCd)
    {

        $nav = DailyNav::select('nav_per_unit as nav')
            ->where('tr_date', $date)
            ->where('fund_cd', $fundCd)
            ->first();

        $nav  = !empty($nav->nav) ? $nav->nav : 0;
        $unit = TransactionHistory::select('unit as unit')
            ->where('tr_date', $date)
            ->where('fund_cd', $fundCd)
            ->where('acnt_no', acnt_no())
            ->first();

        $unit = !empty($unit->unit) ? $unit->unit : 0;

        $result = floatval($nav) * floatval($unit);

        return $result;
    }

    public function danaInvestasiByAcntNo($date, $fundCd)
    {
        $subcscription = Transaction::where('tr_date', $date)
            ->where('fund_cd', $fundCd)
            ->where('acnt_no', acnt_no())
            ->where('proc_cd', 70)
            ->where('tr_type', 'S')
            ->sum('amount');

        $redeem = Transaction::where('tr_date', $date)
            ->where('fund_cd', $fundCd)
            ->where('acnt_no', acnt_no())
            ->where('proc_cd', 70)
            ->where('tr_type', 'R')
            ->sum('amount');

        $swc_out = TransactionSwitching::where('tr_date', $date)
            ->where('out_fund_cd', $fundCd)
            ->where('acnt_no', acnt_no())
            ->where('proc_cd', 70)
            ->sum('out_ord_amt');

        $swc_in = TransactionSwitching::where('tr_date', $date)
            ->where('in_fund_cd', $fundCd)
            ->where('acnt_no', acnt_no())
            ->where('proc_cd', 70)
            ->sum('in_ord_amt');

        return floatval($subcscription - $redeem - $swc_out + $swc_in);
    }

    public function sumRedeemByAcntNo($date, $fundCd)
    {
        $this->cekAcntNo();
        $model = Transaction::select("tr_date")
            ->where('tr_date', $date)
            ->where('acnt_no', acnt_no())
            ->where('tr_type', 'R')
            ->where('fund_cd', $fundCd)
            ->sum('amount');
        return floatval($model);
    }

    public function sumSwcInByAcntNo($date, $fundCd)
    {
        $this->cekAcntNo();
        $model = TransactionSwitching::where('tr_date', $date)
            ->where('in_fund_cd', $fundCd)
            ->where('acnt_no', acnt_no())
            ->where('proc_cd', 70)
            ->sum('in_ord_amt');

        return floatval($model);
    }

    public function sumSwcOutByAcntNo($date, $fundCd)
    {
        $this->cekAcntNo();
        $model = TransactionSwitching::where('tr_date', $date)
            ->where('out_fund_cd', $fundCd)
            ->where('acnt_no', acnt_no())
            ->where('proc_cd', 70)
            ->sum('out_ord_amt');

        return floatval($model);
    }

    public function dailyNavByFundCd($date, $fundCd)
    {
        $this->cekAcntNo();
        $model = DailyNav::where('tr_date', $date)
            ->where('fund_cd', $fundCd)
            ->sum('nav_per_unit');

        return floatval($model);
    }

    public function dailyUnitByFund($date, $fundCd)
    {
        $this->cekAcntNo();
        $model = History::where('tr_date', $date)
            ->where('acnt_no', acnt_no())
            ->where('fund_cd', $fundCd)
            ->sum('unit');

        return floatval($model);
    }
}
