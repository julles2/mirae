<?php

namespace App\Models\Oracle;

use Mirae;
use Yajra\Oci8\Eloquent\OracleEloquent;

class DailyNav extends OracleEloquent
{
    protected $table = 'F0T104H';

    protected $connection = 'oracle';

    protected $guarded = [];

    public function __construct()
    {
        $this->mirae = new Mirae();
    }

    // Relation

    public function fundInfo()
    {
        return $this->hasMany(\App\Models\Oracle\FundInfo::class, 'fund_cd');
    }

    public function oneFundInfo()
    {
        return $this->belongsTo(\App\Models\Oracle\FundInfo::class, 'fund_cd', 'fund_cd');
    }

    //

    // Helpers

    public function dailyNav($fund, $field, $date = "")
    {
        $fixDate = !empty($date) ? $date : date("Ymd");
        $result  = 0;
        if (!empty($fund->fund_cd)) {

            $model = $fund->dailyNav()
                ->select($field)
                ->where('TR_DATE', $fixDate)
                ->first();

            if (!empty($model->{$field})) {
                $result = $model->{$field};
            }
        }

        return $result;
    }

    public function fieldByDate($fundCd, $field, $date)
    {
        $model = $this->select($field)
            ->where('TR_DATE', $date)
            ->where('FUND_CD', $fundCd)
            ->first();

        return !empty($model->{$field}) ? $model->{$field} : 0;
    }

    public function nabByDate($fundCd, $date)
    {
        $model = $this->select('NAV_PER_UNIT')
            ->where('TR_DATE', $date)
            ->where('FUND_CD', $fundCd)
            ->first();

        return !empty($model->nav_per_unit) ? $model->nav_per_unit : 0;
    }

    public function unitByDate($fundCd, $date)
    {
        $model = $this->select('OUTSTANDING_UNIT')
            ->where('TR_DATE', $date)
            ->where('FUND_CD', $fundCd)
            ->first();

        return !empty($model->outstanding_unit) ? $model->outstanding_unit : 0;
    }

    public function nabNow($fundCd)
    {
        $date = date("Ymd");

        return $this->nabByDate($fundCd, $date);
    }

    public function dateLast($fundCd="")
    {
        $model = $this->lastData($fundCd);
        return $model->date;
    }

    public function nabLast($fundCd)
    {
        $model = $this->lastData($fundCd);
        return $model->nav_per_unit;
    }

    public function lastData($fundCd)
    {
        $fund = new \App\Models\FundInfo();

        $model = $fund->where('fund_cd', $fundCd)
            ->select('nav_per_unit', 'daily_navs.created_at')
            ->join('daily_navs', 'daily_navs.fund_id', '=', 'fund_infos.id')
            ->first();

        $nav_per_unit = !empty($model->nav_per_unit) ? $model->nav_per_unit : 0;
        $created_at   = !empty($model->created_at) ? $model->created_at : parse(date("Y-m-d"));
        $data         = [
            'nav_per_unit' => $nav_per_unit,
            'date'         => $created_at,
        ];

        return (object)$data;
    }

    public function nabLastByDate($fundCd, $date)
    {
        $model = $this->select('NAV_PER_UNIT')
            ->where('FUND_CD', $fundCd)
            ->whereRaw("CAST(TR_DATE AS INTEGER) <= " . $date)
            ->orderBy('CAST(TR_DATE AS INTEGER)', 'desc')
            ->first();

        return !empty($model->nav_per_unit) ? $model->nav_per_unit : 0;
    }

    public function formula($nabToday, $nabOldDate)
    {
        return $result = ($nabToday / $nabOldDate - 1) * 100;
    }

    public function queryBefore($fundCd, $today)
    {
        $model = $this->select('TR_DATE', 'NAV_PER_UNIT', 'OUTSTANDING_UNIT')
            ->where('FUND_CD', $fundCd)
            ->whereRaw("CAST(TR_DATE AS INTEGER) <= " . $today)
            ->orderBy('CAST(TR_DATE AS INTEGER)', 'desc')
            ->first();

        return $model;
    }

    public function lastDayByFundCd($fundCd)
    {
        $lastDayNab = $this->select('TR_DATE')
            ->where('fund_cd', $fundCd)
            ->orderBy(\DB::raw("CAST(TR_DATE AS INTEGER)"), "DESC")
            ->first();

        return !empty($lastDayNab->tr_date) ? $lastDayNab->tr_date : null;

    }

    //

    public function nabBeforeDate($fundCd, $date, $before)
    {
        $model = $this->queryBefore($fundCd, $before);

        return !empty($model->nav_per_unit) ? $model->nav_per_unit : 0;
    }

    /**
    example : $this->manipulateNab('GAMA2EQCEQUITY00',Mirae::oneMonthAgo('20121120'),'20121120')
     */

    public function manipulateNab($fundCd, $before = "", $now = "")
    {
        try
        {
            $today = !empty($now) ? $now : Mirae::today();

            $nabToday = $this->nabByDate($fundCd, $today);

            $nabOldDate = $this->nabBeforeDate($fundCd, $today, $before);

            $result = $this->formula($nabToday, $nabOldDate);

            return $result;
            //return $nabOldDate;
        } catch (\Exception $e) {
            return 0;
        }
    }

    //

    // UNIT

    public function unitBeforeDate($fundCd, $date, $before)
    {
        $model = $this->queryBefore($fundCd, $before);

        return !empty($model->outstanding_unit) ? $model->outstanding_unit : 0;
    }

    /**
    example : $this->manipulateUnit('GAMA2EQCEQUITY00',Mirae::oneMonthAgo('20121120'),'20121120')
     */

    public function manipulateUnit($fundCd, $before = "", $now = "")
    {
        try
        {
            $today = !empty($now) ? $now : Mirae::today();

            $unitToday = $this->unitByDate($fundCd, $today);

            $unitOldDate = $this->unitBeforeDate($fundCd, $today, $before);

            $result = $this->formula($unitToday, $unitOldDate);

            return $result;
            //return $nabOldDate;
        } catch (\Exception $e) {
            return 0;
        }
    }

    // AUM

    public function queryBeforeAum($fundCd, $today)
    {
        $model = $this->select(\DB::raw("TR_DATE , NAV_PER_UNIT , OUTSTANDING_UNIT , (OUTSTANDING_UNIT * NAV_PER_UNIT) AS AUM"))
            ->where('FUND_CD', $fundCd)
            ->whereRaw("CAST(TR_DATE AS INTEGER) <= " . $today)
            ->orderBy('CAST(TR_DATE AS INTEGER)', 'desc')
            ->first();

        return $model;
    }

    public function aumBeforeDate($fundCd, $date, $before)
    {
        $model = $this->queryBeforeAum($fundCd, $before);

        return !empty($model->aum) ? $model->aum : 0;
    }

    public function aumByDate($fundCd, $date)
    {
        $model = $this->select(\DB::raw("(OUTSTANDING_UNIT * NAV_PER_UNIT) as aum"))
            ->where('TR_DATE', $date)
            ->where('FUND_CD', $fundCd)
            ->first();

        return !empty($model->aum) ? $model->aum : 0;
    }

    public function manipulateAum($fundCd, $before = "", $now = "")
    {
        try
        {
            $today = !empty($now) ? $now : Mirae::today();

            $aumToday = $this->aumByDate($fundCd, $today);

            $aumOldDate = $this->aumBeforeDate($fundCd, $today, $before);

            $result = $this->formula($aumToday, $aumOldDate);

            return $result;
            //return $nabOldDate;
        } catch (\Exception $e) {
            return 0;
        }
    }

    //

}
