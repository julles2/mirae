<?php

namespace App\Models\Oracle;

use Yajra\Oci8\Eloquent\OracleEloquent;
use App\Models\Oracle\Im;
use App\Models\FundInfo as FundMySql;

class FundInfo extends OracleEloquent
{
    protected $table = 'F0T101M';

    protected $connection = 'oracle';

    protected $guarded = [];

    protected $primaryKey = 'fund_cd';

    public $incrementing = false;

    public function custodian_bank()
    {
        return $this->belongsTo(\App\Models\Oracle\CustodianBank::class, 'cb_cd', 'cb_cd');
    }

    public function dailyNav()
    {
        return $this->hasMany(\App\Models\Oracle\DailyNav::class, 'fund_cd', 'fund_cd');
    }

    public function im()
    {
        return $this->belongsTo(Im::class, 'im_cd', 'im_cd');
    }

    public function getResultFundRiskAttribute()
    {
        $risk =  [
            '1'=>trans('risk.low'),
            '2'=>trans('risk.low_to_moderate'),
            '3'=>trans('risk.moderate'),
            '4'=>trans('risk.moderate_to_high'),
            '5'=>trans('risk.high'),
        ];

        if(array_key_exists($this->fund_risk,$risk))
        {
            return $risk[$this->fund_risk];
        }
    }

    public function mysql_fund()
    {
        return $this->hasOne(FundMySql::class,'fund_cd');
    }
}
