<?php

namespace App\Models\Oracle;

use Yajra\Oci8\Eloquent\OracleEloquent;

class VStockBal extends OracleEloquent
{
    protected $table = 'V_STOCK_BAL';

    protected $connection = 'oracle';
}
