<?php

namespace App\Models\Oracle;

use Yajra\Oci8\Eloquent\OracleEloquent;

class TransactionHistory extends OracleEloquent
{
    protected $table = 'F0T301H';

    public $incrementing = false;

    protected $connection = 'oracle';

    public $timestamps = false;

    protected $guarded = [];
}
