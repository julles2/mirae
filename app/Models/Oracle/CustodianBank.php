<?php

namespace App\Models\Oracle;

use Yajra\Oci8\Eloquent\OracleEloquent;

class CustodianBank extends OracleEloquent
{

    protected $table = 'F0T103M';

    protected $connection = 'oracle';

    protected $guarded = [];
}
