<?php

namespace App\Models\Oracle;

use Yajra\Oci8\Eloquent\OracleEloquent;
use App\Models\FundInfo;
class FundListSwitching extends OracleEloquent
{	
	protected $table = 'F0T105M';

    protected $connection = 'oracle';

    public function getLists($fundCd)
    {
    	$fund = new FundInfo();

    	$tos = $this->select('to_fund_cd')
    		->where('from_fund_cd',$fundCd)
    		->get()
    		->toArray();

    	$tos = array_flatten($tos);

    	$funds = $fund->whereIn('fund_cd',$tos)
	    	->lists('fund_name','fund_cd')
	    	->toArray();

        $funds = [''=>'Pilih Reksa Dana'] + $funds;

	    return $funds;
    }
}
