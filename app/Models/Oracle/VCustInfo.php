<?php

namespace App\Models\Oracle;
use Yajra\Oci8\Eloquent\OracleEloquent;

class VCustInfo extends OracleEloquent
{
    protected $table = 'v_cust_info';
    protected $connection = 'oracle';
    //protected $dates = [];

    public function client_info()
    {
    	return $this->belongsTo(\App\Models\Oracle\ClientInfo::class,'acnt_no','acnt_no');
    }

    public function job()
    {
    	return $this->belongsTo(\App\Models\Oracle\VJobInfo::class,'acnt_no','acnt_no');
    }

    public function bank()
    {
    	return $this->hasOne(\App\Models\Oracle\VBankInfo::class,'acnt_no','acnt_no');
    }

    public function gender($key)
    {
    	$data = [
    		'M'=>'Laki-Laki',
    		'F'=>'Perempuan',
    	];

    	if(array_key_exists($key, $data))
    	{
    		return $data[$key];
    	}
    }

    public function tipe_identitas($key)
    {
    	$data = [
    		'01'=>'KTP',
    		'02'=>'PASSPORT',
    		'04'=>'KITAS',
    		'05'=>'SIUP',
    	];

    	if(array_key_exists($key, $data))
    	{
    		return $data[$key];
    	}
    }


    public function wedding($key)
    {
    	$data = [
    		'1'=>'Belum Menikah',
    		'2'=>'Menikah',
    		'3'=>'Duda',
    		'4'=>'Janda',
    	];

    	if(array_key_exists($key, $data))
    	{
    		return $data[$key];
    	}
    }
}
