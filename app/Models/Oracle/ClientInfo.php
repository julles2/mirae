<?php
namespace App\Models\Oracle;

use Yajra\Oci8\Eloquent\OracleEloquent;

class ClientInfo extends OracleEloquent
{
    protected $table = 'F0T100M';
    protected $connection = 'oracle';
    protected $incermenting = false;
    public $primaryKey = 'acnt_no';
    public $timestamps = false;
    public $guarded = [];

    public function getResultRiskProfileAttribute()
    {
    	$risk =  [
            '1'=>'Low',
            '2'=>'Low to Moderate',
            '3'=>'Moderate',
            '4'=>'Moderate to High',
            '5'=>'High',
        ];

        if(array_key_exists($this->risk_profile,$risk))
        {
            return $risk[$this->risk_profile];
        }
    }

    public function riskLabel($number)
    {
        $list = [
            1 => "Konservatif",
            3 => "Moderat",
            5 => "Agresif",
        ];

        if(array_key_exists($number,$list))
        {
            return $list[$number];
        }
    }
}
