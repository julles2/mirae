<?php

namespace App\Models\Oracle;

use App\Models\Oracle\FundInfo;
use App\Models\FundInfo as SqlFundInfo;
use App\Models\Oracle\TransactionAutoDebet;
use App\Models\Oracle\TransactionSwitching;
use App\Models\TransactionTemporary;
use DB;
use Mirae;
use Yajra\Oci8\Eloquent\OracleEloquent;

class Transaction extends OracleEloquent
{
    protected $table = 'F0T200P';

    public $primaryKey = 'ref_no';


    public $incrementing = false;
    public $timestamps = false;

    protected $connection = 'oracle';

    protected $guarded = [];

    public $date;

    public function __construct()
    {
        $this->date         = request()->get('date');
        $this->instruksi    = request()->get('instruksi');
        $this->reksa_dana   = request()->get('reksa_dana');
        $this->get_status   = request()->get('status');
        $this->start_date   = mirae_date_oracle(get('start_date'));
        $this->end_date     = mirae_date_oracle(get('end_date'));
        $this->tr_date_cast = "CAST(tr_date AS INTEGER)";
    }

    public function fund()
    {
        return $this->belongsTo(\App\Models\Oracle\FundInfo::class, 'fund_cd', 'fund_cd');
    }

    public function sqlFund()
    {
        return $this->belongsTo(SqlFundInfo::class, 'fund_cd', 'fund_cd');
    }

    public function makeRefNo()
    {
        $now = date("Ymd");

        $query = $this->where('tr_date', $now)
            ->where('ref_no', 'like', '%_YP%')
            ->orderBy("ref_no", 'desc')
            ->first();

        $number = 0;

        if (!empty($query->ref_no)) {
            $expRefNo = explode("_", (string) $query->ref_no);

            if (!empty($expRefNo[1])) {
                $number = Mirae::onlyNumber($expRefNo[1]);
            }
        }
        $makeLoop = Mirae::makeLoopId($number);

        $result = "$now" . "_YP" . "$makeLoop";

        return $result;
    }

    public function source($status)
    {
        $arr = [
            '1' => 'Sekali Beli',
            '2' => 'Recurring (Monthly)',
            '3' => 'Manual Input',
        ];

        if (array_key_exists($status, $arr)) {
            return $arr[$status];
        }
    }

    public function status($status)
    {
        $arr = [
            '0'  => 'Request Approval', // auto debet
            '9'  => 'Reject Approval', // auto debet
            '1'  => 'Confirm Approval', // auto debet
            '10' => 'Pending',
            '20' => 'Approval By Manager',
            '30' => 'send to S-INVEST',
            '40' => 'confirm by IM',
            '50' => 'Custodian Bank Retrieve',
            '60' => 'Unit Allocation',
            '70' => 'Complete',
            '19' => 'Reject (Client)',
            '29' => 'Reject (SA)',
            '39' => 'Reject (Send to S-INVEST)',
            '49' => 'Reject (IM)',
            '59' => 'Reject (CB)',
        ];

        if (array_key_exists($status, $arr)) {
            return $arr[$status];
        }
    }

    public function statusColor($status)
    {
        $arr = [
            '9'  => 'red',
            '1'  => 'orange',
            '70' => 'orange',
            '29' => 'red',
            '39' => 'red',
            '49' => 'red',
            '59' => 'red',
            '19' => 'red',
        ];

        if (array_key_exists($status, $arr)) {
            return $arr[$status];
        } else {
            return 'green';
        }
    }

    public function keyStatusPending()
    {
        return [10, 20, 30, 40, 50, 60, 0];
    }

    public function keyStatusReject()
    {
        return [29, 39, 49, 59, 19, 9];
    }

    public function keyStatusConfirm()
    {
        return [70, 1];
    }

    public function statusReject($status)
    {
        $arr = [
            '9'  => 'Reject Approval',
            '29' => 'Reject (SA)',
            '39' => 'Reject (Send to S-INVEST)',
            '49' => 'Reject (IM)',
            '59' => 'Reject (CB)',
            '19' => 'Reject By Client',
        ];

        if (array_key_exists($status, $arr)) {
            return $arr[$status];
        } else {
            return false;
        }
    }

    public function convertOrderType($word)
    {
        $arr = [
            'nominal' => '1',
            'unit'    => '2',
            'all'     => '2',
        ];

        if (array_key_exists($word, $arr)) {
            return $arr[$word];
        }
    }

    public function summerAmountSuccess($userId)
    {
        // $model = $this->where('acnt_no',$userId)
        //     ->where('')
    }

    public function keyFundCdByLike()
    {
        $reksaDana = strtolower($this->reksa_dana);

        $model = FundInfo::select('fund_cd')
            ->whereRaw("LOWER(FUND_NAME) LIKE '%" . $reksaDana . "%'")->get()
            ->toArray();

        return array_flatten($model);
    }

    public function querySwc($userId)
    {
        $fields = "source,tr_date,proc_time,fee,proc_cd,ref_no,cast(proc_cd as integer) as proc_cd_int,out_fund_cd as out";

        $result = TransactionSwitching::select(DB::raw($fields . ",out_ord_unit as unit,in_fund_cd as fund_cd,in_ord_amt as amount,'swc' as tr_type,in_ord_nav as ord_nav"))
            ->where('acnt_no', $userId);

        if (!empty($this->get_status)) {
            $status = ucwords($this->get_status);

            $method = "keyStatus" . $status;

            $result = $result->whereIn('proc_cd', $this->$method());
        }

        if (!empty($this->start_date) && !empty($this->end_date)) {
            $result = $result->where($this->tr_date_cast, '>=', $this->start_date)
                ->where($this->tr_date_cast, '<=', $this->end_date);
        }

        return $result;
    }

    public function querySubsRed($userId)
    {
        $fields = "source,tr_date,proc_time,fee,proc_cd,ref_no,cast(proc_cd as integer) as proc_cd_int,'0' as out";

        $result = $this->select(DB::raw($fields . ",unit as unit,fund_cd,amount,tr_type,ord_nav"))
            ->where('acnt_no', $userId);

        if (!empty($this->instruksi)) {
            $result = $result->where('tr_type', $this->instruksi);
        }

        if (!empty($this->get_status)) {
            $status = ucwords($this->get_status);

            $method = "keyStatus" . $status;

            $result = $result->whereIn('proc_cd', $this->$method());
        }

        if (!empty($this->start_date) && !empty($this->end_date)) {

            $tr_date = "CAST(tr_date AS INTEGER)";
            $result  = $result->where($this->tr_date_cast, '>=', $this->start_date)
                ->where($this->tr_date_cast, '<=', $this->end_date);
        }

        return $result;
    }
    public function queryDebet($userId)
    {
        $fields = "tr_date,proc_time,fee,proc_cd,ref_no,cast(proc_cd as integer) as proc_cd_int,'0' as out";

        $result = TransactionAutoDebet::select(DB::raw($fields . ",0 as unit,fund_cd,ord_amt as amount,'SA' as tr_type,0 as ord_nav"))
            ->where('acnt_no', $userId);
        //->where('ref_no','20170807_YP0000002');

        // if(!empty($this->instruksi))
        // {
        //     $result = $result->where('tr_type',$this->instruksi);
        // }

        if (!empty($this->get_status)) {
            $status = ucwords($this->get_status);

            $method = "keyStatus" . $status;

            $result = $result->whereIn('proc_cd', $this->$method());
        }

        if (!empty($this->start_date) && !empty($end_date)) {
            $tr_date = "CAST(tr_date AS INTEGER)";
            $result  = $result->where($this->tr_date_cast, '>=', $this->start_date)
                ->where($this->tr_date_cast, '<=', $this->end_date);
        }

        return $result;
    }

    public function queryListing($userId)
    {
        $swc = $this->querySwc($userId);

        $subsRed = $this->querySubsRed($userId);

        $subsAuto = $this->queryDebet($userId);

        $subs = $subsRed;

        // $subs = $subsRed->union($subsAuto);

        if (!empty($this->reksa_dana)) {
            $swc = $swc->whereIn('in_fund_cd', $this->keyFundCdByLike());

            $subs = $subs->whereIn('fund_cd', $this->keyFundCdByLike());
        }
        if (!empty($this->date)) {
            $subsRed = $subs->orderBy('tr_date', $this->date)
                ->orderBy('proc_time', $this->date);
        }else{
            $subsRed = $subs->orderBy('tr_date', $this->date)
                ->orderBy('proc_time', 'desc');
        }

        $query = $swc->union($subsRed);

        if (!empty($this->instruksi)) {
            if ($this->instruksi == 'SWC') {
                $query = $swc;
            } else {
                $query = $subs;
            }
        }

        return $query;
    }

    public function latestWithSwitch()
    {
        $latestTransaction = $this->where('acnt_no', acnt_no())
            ->orderBy('tr_date', 'desc')
            ->orderBy('proc_time', 'desc')
            ->first();

        return $latestTransaction;
    }

    public function getFeeMoneyAttribute($value)
    {
        return \Mirae::resultPercentageOrAmount($this->fee, $this->amount);
    }

    public function getFeePersentAttribute($value)
    {
        $cek = \Mirae::percentageOrAmount($this->fee);

        return $cek == '1' ? "(" . $this->fee . " % )" : "";
    }

    public function getTotalOrderAttribute($value)
    {
        $result = $this->amount + $this->fee_money;

        return $result;
    }

    public function getResultStatusAttribute($value)
    {
        return $this->status($this->proc_cd);
    }

    public function typeTemporary($type)
    {
        return $type = substr(strtolower($type), 0, 2);
    }

    public function getTypeTemporayAttribute($val)
    {
        return $type = $this->typeTemporary($this->tr_type);
    }

    public function getTemporaryAttribute($val)
    {
        $model = TransactionTemporary::where('ref_no', $this->ref_no)
            ->where('acnt_no', $this->acnt_no)
            ->where('type', $this->typeTemporary($this->tr_type))
            ->first();

        return $model;
    }
}
