<?php

namespace App\Models\Oracle;

use Yajra\Oci8\Eloquent\OracleEloquent;
use Mirae;

class TransactionSwitching extends OracleEloquent
{
	protected $table = 'F0T201P';

	public $timestamps = false;

	public $primaryKey = "ref_no";

	public $incrementing = false;

    protected $connection = 'oracle';

    public $guarded = [];

	public function out_fund()
    {
        return $this->belongsTo(\App\Models\Oracle\FundInfo::class,'out_fund_cd','fund_cd');
    }

	public function in_fund()
    {
        return $this->belongsTo(\App\Models\Oracle\FundInfo::class,'in_fund_cd','fund_cd');
    }

    public function makeRefNo()
    {
    	$now = date("Ymd");

        $query = $this->where('tr_date',$now)
            ->where('ref_no','like','%_YP%')
            ->orderBy("ref_no",'desc')
            ->first();

        $number=0;

        if(!empty($query->ref_no))
        {
            $expRefNo = explode("_", $query->ref_no);
            if(!empty($expRefNo[1]))
            {
                $number = Mirae::onlyNumber($expRefNo[1]);
            }
        }
        $makeLoop =  Mirae::makeLoopId($number);

        $result = "$now"."_YP"."$makeLoop";

        return $result;
    }

    public function status($status)
    {
        $arr = [
            '10'=>'Pending',
            '20'=>'Approval By Manager',
            '30'=>'send to S-INVEST',
            '40'=>'confirm by IM',
            '50'=>'Custodian Bank Retrieve',
            '60'=>'Unit Allocation',
            '70'=>'Complete',
            '29'=>'Reject (Client)',
            '29'=>'Reject (SA)',
            '39'=>'Reject (Send to S-INVEST)',
            '49'=>'Reject (IM)',
            '59'=>'Reject (CB)',
        ];

        if(array_key_exists($status, $arr))
        {
            return $arr[$status];
        }

    }

	public function getOutFeeMoneyAttribute($value)
    {
        return \Mirae::resultPercentageOrAmount($this->fee,$this->out_ord_amt);
    }

	public function getFeePersentAttribute($value)
    {
        $cek = \Mirae::percentageOrAmount($this->fee);

        return $cek == '1' ? "(".$this->fee." % )" : "";

    }

	public function getResultProcTime($val)
	{
		return mirae_time($this->proc_time);
	}

}
