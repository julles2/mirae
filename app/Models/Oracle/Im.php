<?php

namespace App\Models\Oracle;

use Yajra\Oci8\Eloquent\OracleEloquent;

class Im extends OracleEloquent
{
    protected $table = 'F0T102M';

    protected $connection = 'oracle';

    protected $guarded = [];

    protected $primaryKey = 'IM_CD';
}
