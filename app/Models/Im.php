<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\FundInfo;
use App\Models\Feed\ImAum;

class Im extends Model
{
	protected $table = 'im';

    protected $guarded = [];

    public function funds()
    {
        return $this->hasMany(FundInfo::class,'im_id');
    }

    public function datas()
	{
		return $this->hasMany(\App\Models\ImData::class,'im_cd','im_cd');
	}

	public function details()
	{
		return $this->hasMany(\App\Models\ImInfoDetails::class,'im_cd');
	}

	public function company_details()
	{
		return $this->hasMany(\App\Models\ImDetailCompany::class,'im_cd');
	}

	public function stakeholder()
	{
		return $this->hasMany(\App\Models\ImStakeholders::class,'im_cd');
	}

	public function im_images()
	{
		return $this->hasOne(\App\Models\ImImages::class,'im_cd','im_cd');
	}

    public function getResultImageAttribute($val)
    {
        $image = $this->im_images;

        if(!empty($image->image))
        {
            $result = asset('contents/'.$image->image);
        }else{
            $result = asset('noimage.png');
        }

        return $result;
    }

    public function getLatestUnitAttribute($val)
    {
        $model = $this
            ->datas()
            ->orderBy(\DB::raw('CAST(periode as SIGNED)'),'desc')
            ->first();

        $unitIdr = !empty($model->unit_idr) ? $model->unit_idr : 0;
        $unitUsd = !empty($model->unit_usd) ? $model->unit_usd : 0;

        $result = (object)[
            'unit_idr'=>$unitIdr,
            'unit_usd'=>$unitUsd,
        ];

        return $result;
    }

    public function feed($imCd,$field)
    {
        $model =  ImAum::select('id',$field)->where('sinvest_code',$imCd)->orderBy('period','desc')->first();
        
        return !empty($model->id) ? $model->{$field} : 0;
    }

    public function getLatestUnitFeedAttribute($val)
    {
        $model = ImAum::where('sinvest_code',$this->im_cd)->select('total_unit as unit_idr','total_unit_usd as unit_usd','aum_usd','aum as aum_idr')->orderBy('period','desc')->first();

        $unitIdr = !empty($model->unit_idr) ? $model->unit_idr : 0;
        $unitUsd = !empty($model->unit_usd) ? $model->unit_usd : 0;
        $aumUsd = !empty($model->aum_usd) ? $model->aum_usd : 0;
        $aumIdr = !empty($model->aum_idr) ? $model->aum_idr : 0;

        $result = (object)[
            'unit_idr'=>$unitIdr,
            'unit_usd'=>$unitUsd,
            'aum_usd'=>$aumUsd,
            'aum_idr'=>$aumIdr,
        ];

        return $result;

    }

    public function getAumIdrAttribute()
    {
        return $this->feed($this->im_cd,'aum');
    }

    public function getAumUsdAttribute()
    {
        return $this->feed($this->im_cd,'aum_usd');
    }

    public function getUnitIdrAttribute()
    {
        return $this->feed($this->im_cd,'total_unit');
    }

    public function getUnitUsdAttribute()
    {
        return $this->feed($this->im_cd,'total_unit_usd');
    }

    public function getPeriodeAttribute()
    {
        return $this->feed($this->im_cd,'period');
    }
}
