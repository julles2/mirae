<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;
class NewsCategories extends Model
{   
    public $guarded = [];

    public function categories_details()
    {   
        return $this->hasMany(\App\Models\NewsCategoryTranslations::class,'category_id');
    }
}
