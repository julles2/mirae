<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    public $fillable = ['acnt_no','data_question','data_risk','status','risk_total','name','email','msg_rejected'];

    public function getResultRiskProfileAttribute()
    {
        $var = $this->risk_total;

        $risk = [
            'Konservatif' => [0,79],
            'Moderate' => [80,130],
            'Aggresif' => [81,200],
        ];

        foreach($risk as $label => $range)
        {
            if($var > $range[0] && $var <= $range[1] )
            {
                return $label;
            }
        }

    }
}
