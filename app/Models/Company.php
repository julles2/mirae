<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    public $guarded = [];

    protected $table='company';

    public function trans()
    {
        return $this->hasMany(\App\Models\CompanyTranslations::class,'company_id');
    }
}
