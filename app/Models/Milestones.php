<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Milestones extends Model
{
    public $guarded = [];

    public function trans()
    {
        return $this->hasMany(\App\Models\MilestonesTranslation::class,'milestone_id');
    }
}
