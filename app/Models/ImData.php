<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ImData extends Model
{
    public $guarded = ['bulan','tahun'];

    public function im()
    {
        return $this->belongsTo(\App\Models\Im::class,'im_cd','im_cd');
    }

    public function getTahunAttribute()
    {
        return parse($this->periode)->format("Y");
    }

    public function getBulanAttribute()
    {
    	$m = parse($this->periode)->format("m");
    	$length = strlen($m);
    	return $m < 10 ? substr($m,0,2) : $m;
    }

    public function setPeriodeAttribute()
    {
    	return 1;
    }
}
