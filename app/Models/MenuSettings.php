<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MenuSettings extends Model
{
    public $guarded = [];

    public function display_by_lang($menu)
    {
    	$model = $this->whereMenu($menu)->whereLang(lang())->get();
    	$data = [];
    	foreach($model as $row)
    	{
    		$data[$row->key] = $row->value;
    	}

    	return (object)$data;
    }
}
