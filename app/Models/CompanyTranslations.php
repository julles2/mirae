<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CompanyTranslations extends Model
{

    public $guarded = [];

    public function company()
    {
        return $this->hasMany(\App\Models\Company::class,'company_id');
    }
}
