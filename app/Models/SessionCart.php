<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SessionCart extends Model
{
    public $fillable = ['acnt_no','data'];
}
