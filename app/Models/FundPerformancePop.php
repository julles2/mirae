<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FundPerformancePop extends Model
{
    public $guarded = [];

    public function fund()
    {
    	return $this->belongsTo(\App\Models\FundInfo::class,'fund_id');
    }
}
