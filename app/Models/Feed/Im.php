<?php

namespace App\Models\Feed;

use App\Models\Feed\ImAum;
use App\Models\Feed\Fund;
use Illuminate\Database\Eloquent\Model;

class Im extends Model
{
    protected $connection = 'feed';

    protected $table = 'mutualfund_im';

    public $guarded = [];

    public function aums()
    {
        return $this->hasMany(ImAum::class, 'sinvest_code', 'sinvest_code');
    }

    public function funds()
    {
        return $this->hasMany(Fund::class, 'im_id', 'im_id');
    }

    public function getLatestDataAttribute()
    {
        $query = $this
        	->aums()
            ->select('period', 'aum', 'aum_usd', 'total_unit', 'total_unit_usd')
            ->orderBy('period', 'desc')
            ->first();

        return $query;

    }

    public function scopeQueryReloadTable()
    {

    }
}
