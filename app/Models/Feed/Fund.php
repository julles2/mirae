<?php

namespace App\Models\Feed;

use Illuminate\Database\Eloquent\Model;

use App\Models\Feed\Im;

class Fund extends Model
{
    public $connection = 'feed';

    public $guarded = [];

    public function im()
    {
    	return $this->belongsTo(Im::class,'im_id','im_id');
    }
}
