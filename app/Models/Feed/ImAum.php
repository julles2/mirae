<?php

namespace App\Models\Feed;

use Illuminate\Database\Eloquent\Model;

class ImAum extends Model
{
    protected $connection = 'feed';

    protected $table = 'mutualfund_aum_mi';

    public $guarded = [];
}
