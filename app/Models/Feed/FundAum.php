<?php

namespace App\Models\Feed;

use Illuminate\Database\Eloquent\Model;

class FundAum extends Model
{
	public $connection = 'feed';

    protected $table = 'mutualfund_aum';

    public $guarded = [];

    public function scopeLatestData($query,$sivenstCode)
    {
    	return $query->where('sinvest_code', $sivenstCode)
                ->orderBy('period', 'desc');
	}

	public function scopeLatestDataFirst($query,$sivenstCode)
	{
		return $query->latestData($sivenstCode)->first();
	}


    // GLOBAL HELPER

    public function queryBefore($fundCd, $today)
    {
        $today = parse($today)->format("Y-m-d");
        $model = $this->select('period','value as aum','total_unit as unit')
            ->where('sinvest_code', $fundCd)
            ->where("period",'<=',$today)
            ->orderBy('period', 'desc')
            ->first();
        return $model;
    }

    //


    // AUM HELPER

	public function aumByDate($fundCd, $period)
    {
        $period = parse($period)->format("Y-m-d");
        $model = $this->select(\DB::raw("value as aum"))
            // ->where('period', $period)
            ->where('sinvest_code', $fundCd)
            ->orderBy('period','desc')
            ->first();
        return !empty($model->aum) ? $model->aum : 0;
    }

    public function aumBeforeDate($fundCd, $date, $before)
    {
        $model = $this->queryBefore($fundCd, $before);
        return !empty($model->aum) ? $model->aum : 0;
    }

    public function formula($nabToday, $nabOldDate)
    {
        return $result = ($nabToday / $nabOldDate - 1) * 100;
    }

	public function manipulateAum($fundCd, $before = "", $now = "")
    {
        try
        {
            $today = !empty($now) ? $now : \Mirae::today();
        	$aumToday = $this->aumByDate($fundCd, $today);
            $aumOldDate = $this->aumBeforeDate($fundCd, $today, $before);
            $result = $this->formula($aumToday, $aumOldDate);
            return $result;
            // return $nabOldDate;
        } catch (\Exception $e) {
            return 0;
        }
    }

    // END AUM HELPER

    // UNIT HELPER

    public function unitBeforeDate($fundCd, $date, $before)
    {

        $model = $this->queryBefore($fundCd, $before);

        return !empty($model->unit) ? $model->unit : 0;
    }

    public function unitByDate($fundCd, $date)
    {
        $model = $this->select('total_unit as unit')
            ->where('sinvest_code', $fundCd)
            ->orderBy('period','desc')
            ->first();

        return !empty($model->unit) ? $model->unit : 0;
    }

    /**
    example : $this->manipulateUnit('GAMA2EQCEQUITY00',Mirae::oneMonthAgo('20121120'),'20121120')
     */

    public function manipulateUnit($fundCd, $before = "", $now = "")
    {   
        $today = parse($now)->format("Y-m-d");
        $before = parse($before)->format("Y-m-d");
        try
        {
            $today = !empty($now) ? $now : Mirae::today();
            
            $unitToday = $this->unitByDate($fundCd, $today);
            $unitOldDate = $this->unitBeforeDate($fundCd, $today, $before);
            
            $result = $this->formula($unitToday, $unitOldDate);

            return $result;
            //return $nabOldDate;
        } catch (\Exception $e) {
            return 0;
        }
    }

    // END UNIT HELPER
}
