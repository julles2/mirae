<?php

namespace App\Models\Feed;

use Illuminate\Database\Eloquent\Model;

class FundNav extends Model
{
    public $connection = 'feed';

    public $table = 'mutualfund_nav';

    public $guarded = [];

    public function scopeLatestData($query,$sivenstCode)
    {
    	return $query->where('sinvest_code', $sivenstCode)
                ->orderBy('date', 'desc');
	}

	public function scopeLatestDataFirst($query,$sivenstCode)
	{
		return $query->latestData($sivenstCode)->first();
	}
}
