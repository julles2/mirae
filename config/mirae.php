<?php

return [
	'ip_login'=>env('IP_LOGIN', 'http://117.103.33.200/php/loginInfo.do'),
	'ip_pin'=>env('IP_PIN', 'http://117.103.33.200/php/accountCheck.do'),
	'port_ssl'=>env('PORT_SSL',80),
    'ssl'=>env("SSL",null),
];
