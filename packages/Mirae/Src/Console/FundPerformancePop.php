<?php namespace Mirae\Console;

use App\Models\DailyNav;
use App\Models\Feed\FundAum;
use App\Models\FundInfo as Fund;
use App\Models\FundPerformancePop as PerformancePop;
use Mirae;

class FundPerformancePop
{
    public $table;
    public $model;

    public function __construct()
    {
        $this->table     = 'fund_performance_pops';
        $this->model     = new PerformancePop();
        $this->fund      = new Fund();
        $this->daily_nav = new DailyNav();
        $this->feedAum   = new FundAum();
    }

    public function truncate_table()
    {
        \DB::statement('DELETE FROM ' . $this->table);
        \DB::statement('ALTER TABLE ' . $this->table . ' AUTO_INCREMENT = 1');
    }

    public function execute()
    {

        // $this->truncate_table();

        $result = [];
        $no     = 0;
        foreach ($this->fund->all() as $fund) {
            $no++;
            $dailyNav = $fund->daily_nav;
            if (!empty($dailyNav->tr_date)) {
                $date    = $dailyNav->tr_date;
            	$feedAum = $this->feedAum;
                $result[] = [
                    // 'id'               => $no,
                    'fund_id'          => $fund->id,

                    'one_month'        => $dailyNav->one_month,
                    'three_months'     => $dailyNav->three_months,
                    'six_months'       => $dailyNav->six_months,
                    'one_year'         => $dailyNav->one_year,
                    'three_year'       => $dailyNav->three_year,
                    'ytd'              => $dailyNav->ytd,

                    'aum_one_month'    => $feedAum->manipulateAum($fund->fund_cd, Mirae::oneMonthAgo($date), $date),
                    'aum_three_months' => $feedAum->manipulateAum($fund->fund_cd, Mirae::threeMonthsAgo($date), $date),
                    'aum_six_months'   => $feedAum->manipulateAum($fund->fund_cd, Mirae::sixMonthsAgo($date), $date),
                    'aum_one_year'     => $feedAum->manipulateAum($fund->fund_cd, Mirae::yearsAgo($date, 1), $date),
                    'aum_three_year'   => $feedAum->manipulateAum($fund->fund_cd, Mirae::yearsAgo($date, 3), $date),
                    'aum_ytd'          => $feedAum->manipulateAum($fund->fund_cd, Mirae::desemberYearsAgo($date), $date),

                    'ou_one_month'     => $feedAum->manipulateUnit($fund->fund_cd, Mirae::oneMonthAgo($date), $date),
                    'ou_three_months'  => $feedAum->manipulateUnit($fund->fund_cd, Mirae::threeMonthsAgo($date), $date),
                    'ou_six_months'    => $feedAum->manipulateUnit($fund->fund_cd, Mirae::sixMonthsAgo($date), $date),
                    'ou_one_year'      => $feedAum->manipulateUnit($fund->fund_cd, Mirae::yearsAgo($date, 1), $date),
                    'ou_three_year'    => $feedAum->manipulateUnit($fund->fund_cd, Mirae::yearsAgo($date, 3), $date),
                    'ou_ytd'           => $feedAum->manipulateUnit($fund->fund_cd, Mirae::desemberYearsAgo($date), $date),
                    'tr_date'          => $date,
                ];
                
            }

            //}
        }

        $this->model->insert($result);

        //return 1;
    }
}
