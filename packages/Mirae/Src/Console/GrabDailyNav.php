<?php namespace Mirae\Console;

use App\Models\DailyNav;
use App\Models\Feed\FundAum;
use App\Models\Feed\FundReturn;
use App\Models\Feed\FundNav;
use App\Models\FundInfo;

class GrabDailyNav
{
    public function __construct()
    {
        $this->fundReturn = new FundReturn;
        $this->fund       = new FundInfo;
        $this->dailyNav   = new DailyNav();
        $this->fundAum    = new FundAum();
        $this->fundNav    = new FundNav();
    }

    public function grabDailyNav($paramDate = "")
    {
        makeEmptyTable('daily_navs');

        $funds = $this->fund->select('id', 'fund_cd')->get();

        $result = [];

        foreach ($funds as $fund) {

            $feedReturn = $this->fundReturn->latestDatafirst($fund->fund_cd);
            $feedAum 	= $this->fundAum->select('total_unit','value')->latestDatafirst($fund->fund_cd);
            $feedNav 	= $this->fundNav->select('date','value')->latestDatafirst($fund->fund_cd);

            $result[] = [
                'fund_id'          => $fund->id,
                'nav_per_unit'     => $feedNav->value,
                'one_day'          => x100($feedReturn->oneday),
                'three_weeks'      => x100($feedReturn->oneweek),
                'one_month'        => x100($feedReturn->onemonth),
                'three_months'     => x100($feedReturn->threemonth),
                'six_months'       => x100($feedReturn->sixmonth),
                'one_years'        => x100($feedReturn->oneyear),
                'three_years'      => x100($feedReturn->threeyear),
                'five_years'       => x100($feedReturn->fiveyear),
                'ytd'              => x100($feedReturn->ytd),
                'outstanding_unit' => $feedAum->total_unit,
                'tr_date'          => carbon()->parse($feedReturn->date)->format("Ymd"),
                'aum'              => $feedAum->value,
                'created_at'       => $feedNav->date,
            ];
        }

        $this->dailyNav->insert($result);

    }
}
