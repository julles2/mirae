<?php namespace Mirae;

use Illuminate\Support\ServiceProvider;

use Mirae\Mirae;

class MiraeServiceProvider extends ServiceProvider
{
	public function boot()
	{

	}


	public function register()
	{
		$this->app->bind('register-mirae',function(){
			return new Mirae();
		});
	}
}