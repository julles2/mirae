<?php namespace Mirae;

use App\Models\DailyNav;
use App\Models\Oracle\DailyNav as OracleDailyNav;
use Carbon\Carbon;
use Cart;

class Mirae
{
    public $nav;

    public function __construct()
    {
        $this->nav      = new OracleDailyNav();
        $this->mysqlNav = new DailyNav();
    }

    public function menu()
    {
        return request()->segment(2);
    }

    public function urlAction($newAction)
    {
        return urlLang($this->menu() . '/' . $newAction);
    }

    public function miraeDate($day)
    {
        return $day = notEmpty($day, Carbon::parse($day)->format('Ymd'), "");
    }

    public function today()
    {
        return date("Ymd");
    }

    public function yesterday($today = "")
    {
        $fixToday = empty($today) ? $this->today() : $today;

        return Carbon::parse($fixToday)->subDays(1)->format("Ymd");
    }

    public function daysAgo($days, $today = "")
    {
        $fixToday = empty($today) ? $this->today() : $today;

        return Carbon::parse($fixToday)->subDays($days)->format("Ymd");
    }

    public function threeWeeksAgo($today = "")
    {
        $fixToday = empty($today) ? $this->today() : $today;

        return Carbon::parse($fixToday)->subWeeks(3)->format("Ymd");
    }

    public function monthsAgo($month = "", $date = "")
    {
        $fixDate = empty($date) ? $this->today() : $date;
        $month   = !empty($month) ? $month : 1;
        return Carbon::parse($date)->subMonth($month)->format("Ymd");
    }

    public function lastDateOfMonth($date, $subMonth = "")
    {
        $month = !empty($subMonth) ? $subMonth : 0;
        return parse($date)->addMonthsNoOverflow($subMonth)->endOfMonth()->format("Ymd");
    }

    public function firstDateOfMonth($date, $subMonth = "")
    {
        $month = !empty($subMonth) ? $subMonth : 0;
        // return parse($date)->addMonthsNoOverflow($subMonth)->endOfMonth()->format("Ymd");
        return parse($date)->addMonthsNoOverflow($subMonth)->format("Ym").'01';
       
    }

    public function oneMonthAgo($date = "")
    {
        $fixDate = empty($date) ? $this->today() : $date;

        return Carbon::parse($date)->subMonth(1)->format("Ymd");
    }

    public function threeMonthsAgo($date = "")
    {
        $fixDate = empty($date) ? $this->today() : $date;

        return Carbon::parse($date)->subMonth(3)->format("Ymd");
    }

    public function sixMonthsAgo($date = "")
    {
        $fixDate = empty($date) ? $this->today() : $date;

        return Carbon::parse($date)->subMonth(6)->format("Ymd");
    }

    public function nineMonthsAgo($date = "")
    {
        $fixDate = empty($date) ? $this->today() : $date;

        return Carbon::parse($date)->subMonth(9)->format("Ymd");
    }

    public function oneYearAgo($date = "")
    {
        $fixDate = empty($date) ? $this->today() : $date;

        return Carbon::parse($date)->subYears(1)->format("Ymd");
    }

    public function yearsAgo($date = "", $subYears)
    {
        $fixDate = empty($date) ? $this->today() : $date;

        return Carbon::parse($date)->subYears($subYears)->format("Ymd");
    }

    public function desemberYearsAgo($date = "", $subYears = "")
    {
        $fixDate = empty($date) ? $this->today() : $date;

        $subYears = !empty($subYears) ? $subYears : 1;

        $carbon = $this->yearsAgo($date, $subYears);

        $year = substr($carbon, 0, 4);

        $result = $year . '1231';

        return $result;
    }

    public function toDateString($date)
    {
        $date = Carbon::parse($date)->toDateString();

        return $date;
    }

    public function loopDates($start, $end)
    {
    }

    public function decimalToPercents()
    {
        return $percent = round((float) $str * 100) . '%';
    }

    public function roundSubstr($decimal, $zero = "")
    {
        //return round(substr($decimal,0,4),4);
        //return round(valueDecimal($decimal),2);
        $resultZero = !empty($zero) ? $zero : 2;
        return round($decimal, $resultZero);
    }

    public function cartFindById($id)
    {
        $search = Cart::search(function ($cartItem, $rowId) use ($id) {
            return $cartItem->id === $id;
        });

        $no     = 0;
        $result = null;
        foreach ($search as $s) {
            $no++;
            if ($no == 1) {
                $result = $s;
            }
        }

        return $result;
    }

    public function cartByType($type)
    {
        $search = Cart::search(function ($cartItem, $rowId) use ($type) {
            return $cartItem->options->type === $type;
        });

        return $search;
    }

    public function cartSubtotalByType($type)
    {
        $carts  = $this->cartByType($type);
        $result = 0;
        foreach ($carts as $c) {
            if ($c->options->type == 'subscription' || $c->options->type == 'subscription_auto_debet') {
                $result = $result + intval($c->price);
            } else {
                $result = $result + formatInteger($c->options->amount);
            }
        }

        return $result;
    }

    public function percentageOrAmount($nilai)
    {
        if ($nilai > 0 && $nilai <= 10) {
            $result = 1; // persent
        }else{
            $result = $nilai;
        }
        return $result;
    }

    public function resultPercentageOrAmount($fee, $amount)
    {
        $percentageOrAmount = $this->percentageOrAmount($fee);

        if ($percentageOrAmount == 1) {
            $hasil = $fee / 100 * $amount;

            return $hasil;
        } else {
            return $fee;
        }
    }

    public function makeLoopId($n)
    {
        $result = str_pad($n + 1, 7, 0, STR_PAD_LEFT);
        return $result;
    }

    public function onlyNumber($string)
    {
        return $int = intval(preg_replace('/[^0-9]+/', '', $string), 10);
    }

    public function diffHumanIndo($result)
    {
        $exp = explode(" ", $result);

        return $exp[0] . ' ' . $exp[1];
    }

    public function cartCountByType($type)
    {
        $carts = Cart::content();

        $counter = 0;
        foreach ($carts as $cart) {
            if ($cart->options->type == $type) {
                $counter++;
            }
        }

        return $counter;
    }

    public function cekSatOrSun($date)
    {
        $carbon = carbon()->parse($date)->format('D');
        $carbon = strtolower($carbon);
        if ($carbon == 'sun' || $carbon == 'sat') {
            return true;
        } else {
            return false;
        }
    }

    public function select_months()
    {
        return array(
            1  => 'January',
            2  => 'February',
            3  => 'March',
            4  => 'April',
            5  => 'May',
            6  => 'June',
            7  => 'July ',
            8  => 'August',
            9  => 'September',
            10 => 'October',
            11 => 'November',
            12 => 'December',
        );
    }

    public function select_years()
    {
        $year = date("Y");
        $min  = $year - 5;
        $data = [];
        for ($a = $min; $a <= $year; $a++) {
            $data[$a] = $a;
        }
        return $data;
    }

    public function dateAsOf()
    {
        $date = Carbon::parse($this->mysqlNav->dateLast())->format('d F Y');
        $ex   = explode(" ", $date);
        return str_replace($ex[1], $this->listMonthIndo()[$ex[1]], $date);
    }

    public function listDayIndo()
    {
        return [
            'Monday'    => 'Senin',
            'Tuesday'   => 'Selasa',
            'Wednesday' => 'Rabu',
            'Thursday'  => 'Kamis',
            'Friday'    => 'Jumat',
            'Saturday'  => 'Sabtu',
            'Sunday'    => 'Minggu',
        ];
    }

    public function listMonthIndo()
    {
        return [
            'January'   => 'January',
            'February'  => 'Februari',
            'March'     => 'Maret',
            'April'     => 'April',
            'May'       => 'Mei',
            'June'      => 'Juni',
            'July'      => 'Juli',
            'August'    => 'Agustus',
            'September' => 'September',
            'October'   => 'Oktober',
            'November'  => 'November',
            'December'  => 'Desember',
        ];
    }

    public function device($device)
    {
        if ($device == 'WebKit') {
            $result = 'Dekstop';
        } elseif ($device == false) {
            $result = 'not detected';
        } else {
            return $device;
        }
        return $result;
    }

    public function agent()
    {
        $data = [
            'ip'       => \Request::getClientIp(true),
            'browser'  => \Agent::browser(),
            'device'   => $this->device(\Agent::device()),
            'is_robot' => \Agent::isRobot() == false ? "False" : "True",
            'platform' => \Agent::platform(),
            //'host_name'=>gethostname(),
        ];
        $visitor = visitor(real_ip());
        if ($visitor['status'] != 'fail') {
            $data = array_merge($data, $visitor);
        }

        return $data;
    }

    public function stringAgent()
    {
        $agent = $this->agent();
        $str   = "";
        foreach ($agent as $key => $val) {
            $resultKey = ucwords(str_replace("_", " ", $key));
            $str .= "$resultKey = $val, ";
        }
        return substr($str, 0, -2);
    }

    public function range($start, $end, $format = "")
    {
        $start  = parse($start);
        $end    = parse($end);
        $result = [];
        $format = !empty($format) ? $format : "d M Y";
        for ($date = $start; $date->lte(parse(date("Ymd"))) > 0; $date->addDays(1)) {
            $result[] = $date->format($format);
        }
        return $result;
    }

    /**
     * [logCustomer description]
     * @param  [type] $data [description]
     * @return [type]       [description]
     */
    public function logCustomer($data)
    {
        $data = [
                'information'=>$this->stringAgent(),
                'acnt_no'=>$data->acnt_no,
                'name'=>$data->name,
                'username'=>$data->username,
                'status'=>$data->status
        ];

        injectModel('LogCustomer')->create($data);
    }
}
