<?php namespace Mirae;

use App\Models\Oracle\Im as OraIm;
use App\Models\Oracle\DailyNav as OraNav;
use App\Models\Oracle\FundInfo as OraFund;

use App\Models\Im as SqlIm;
use App\Models\DailyNav as SqlNav;
use App\Models\FundInfo as SqlFund;
use App\Models\FundPerformancePop as SqlPop;
use App\Models\NavDailyPop;
use App\Models\TransactionTemporary;
use App\Models\Oracle\Transaction;
use App\Models\Oracle\TransactionSwitching;
use App\Models\Oracle\TransactionAutoDebet;
use App\Models\LogCronjob;
use Mirae;
use DB;

class MiraeQuery
{
    public $oraIm;

	public $oraNav;

    public $oraFund;

    public $sqlNav;

    public $sqlFund;

    public $sqlIm;

    public $sqlPop;

    public function __construct()
    {
        $this->oraIm = new OraIm();

        $this->oraNav = new OraNav();

        $this->oraFund = new OraFund();

        $this->sqlIm = new SqlIm();

        $this->sqlNav = new SqlNav();

        $this->sqlFund = new SqlFund();

        $this->sqlPop = new SqlPop();

        $this->trans = new Transaction();

        $this->temporary = new TransactionTemporary();

        $this->swc = new TransactionSwitching();

        $this->debet = new TransactionAutoDebet();
    }

    public function grabIm()
    {
        $queryOracleIm = $this->oraIm->all();

        $data = [];

        foreach($queryOracleIm as $oraIm)
        {
            $queryMysqlIm = $this->sqlIm
                ->where('im_cd',$oraIm->im_cd)
                ->first();

            $data = [
                'im_cd'=>$oraIm->im_cd,
                'im_nm'=>$oraIm->im_nm,
            ];

            if(!empty($queryMysqlIm->id))
            {
                $queryMysqlIm->update($data);
            }else{
                $this->sqlIm->create($data);
            }
        }
    }

    public function fundCdExist()
    {
        $model = $this->oraFund->select('fund_cd')->get()->toArray();
        return array_flatten($model);
    }

    public function grabReksaDana()
    {
        DB::beginTransaction();

        try
        {
            $this->grabIm();
                $queryOracleFund = $this->oraFund->all();
                $data = [];
                $cek = $queryOracleFund->toArray();
                $this->sqlFund->whereNotIn('fund_cd',$this->fundCdExist())->delete();
                foreach($queryOracleFund as $oraFund)
                {
                    $queryMysqlFund = $this->sqlFund
                        ->where('fund_cd',$oraFund->fund_cd)
                        ->first();

                    $im = $this->sqlIm->select('id')->where('im_cd',$oraFund->im_cd)->first();
                    if(!empty($im->id))
                    {
                        $data = [
                            'im_id'=> $im->id,
                            'fund_cd'=>$oraFund->fund_cd,
                            'fund_name'=>$oraFund->fund_name,
                            'fund_type'=>$oraFund->fund_type,
                            'fee_subs'=>$oraFund->fee_subs,
                            'fee_redm'=>$oraFund->fee_redm,
                            'fee_swtc'=>$oraFund->fee_swtc,
                            'min_investment'=>$oraFund->min_investment,
                            'min_increment'=>$oraFund->min_increment,
                            'min_balance'=>$oraFund->min_balance,
                            'min_redm_amt'=>$oraFund->min_redm_amt,
                            'min_swtc_in_amt'=>$oraFund->min_swtc_in_amt,
                            'min_swtc_out_amt'=>$oraFund->min_swtc_out_amt,
                        ];

                        if(!empty($queryMysqlFund->id))
                        {
                            $queryMysqlFund->update($data);
                        }else{
                            $this->sqlFund->create($data);
                        }
                    }

                }
                DB::commit();
                $this->logSuccess('Reksa Dana');
        }catch(\Exception $e){
                DB::rollback();
                $this->logError('Reksa Dana',$e);
        }
                
    }

    public function logSuccess($title)
    {
        //$success = "Command php artisan $title Successfully executed ";
        $success = "data $title sukses di sinkronisasi";
        LogCronjob::create([
            'message'=>$success,
            'status'=>'y',
        ]);
    }

    public function logError($title,$e)
    {
        $error = "data $title gagal di sinkronisasi error message : ".$e->getMessage();
        LogCronjob::create([
            'message'=>$error,
            'status'=>'n',
        ]);
    }

    public function grabDailyNav($paramDate="")
    {
        DB::beginTransaction();

        try
        {
                \DB::statement('DELETE FROM daily_navs');

                \DB::statement('ALTER TABLE daily_navs AUTO_INCREMENT = 1');

                //$date = !empty($paramDate) ? $paramDate : $this->oraNav->dateLast();

                $result = [];

                foreach($this->sqlFund->all() as $fund)
                {
                     //if($fund->fund_cd == 'GAMA2EQCEQUITY00')
                     //{
                        $cek = $this->oraNav->where('fund_cd',$fund->fund_cd)->first();
                        if(!empty($cek->tr_date))
                        {
                            $nab = $this->oraNav->nabLast($fund->fund_cd);
                            $date = $this->oraNav->lastDayByFundCd($fund->fund_cd);
                            $unit = $this->oraNav->dailyNav($this->oraFund->find($fund->fund_cd),'outstanding_unit',$date);
                            $aum = $nab * $unit;
                            $result[] = [
                                'fund_id'=>$fund->id,
                                'nav_per_unit' => $nab,
                                'one_day' => $this->oraNav->manipulateNab($fund->fund_cd,Mirae::yesterday($date),$date),
                                'three_weeks' => $this->oraNav->manipulateNab($fund->fund_cd,Mirae::threeWeeksAgo($date),$date),
                                'one_month' => $this->oraNav->manipulateNab($fund->fund_cd,Mirae::oneMonthAgo($date),$date),
                                'three_months' => $this->oraNav->manipulateNab($fund->fund_cd,Mirae::threeMonthsAgo($date),$date),
                                'six_months' => $this->oraNav->manipulateNab($fund->fund_cd,Mirae::sixMonthsAgo($date),$date),
                                'one_years' => $this->oraNav->manipulateNab($fund->fund_cd,Mirae::yearsAgo($date,1),$date),
                                'three_years' => $this->oraNav->manipulateNab($fund->fund_cd,Mirae::yearsAgo($date,3),$date),
                                'five_years' => $this->oraNav->manipulateNab($fund->fund_cd,Mirae::yearsAgo($date,5),$date),
                                'ytd' => $this->oraNav->manipulateNab($fund->fund_cd,Mirae::desemberYearsAgo($date),$date),
                                'outstanding_unit'=>$unit,
                                'tr_date'=>$date,
                                'aum'=>$aum,
                            ];
                        }

                    //}
                }

                $this->sqlNav->insert($result);
                DB::commit();
                $this->logSuccess('Harian NAB (Daily Nav) ');
        }catch(\Exception $e){
                DB::rollback();
                $this->logError('Harian NAB (Daily Nav) ',$e);
        }
    }

    /* WITH FORMULA

    public function grabDailyNavByRange($start,$end,$fundCd="")
    {
        $loopStart = Mirae::toDateString($start);

        $loopEnd = Mirae::toDateString($end);

        $reksaDana = $this->sqlFund->select('id','fund_cd')->get();

        \DB::statement('DELETE FROM pop_chart_daily');

        \DB::statement('ALTER TABLE pop_chart_daily AUTO_INCREMENT = 1');

        $result = [];

        foreach($reksaDana as $rd)
        {
            if($rd->fund_cd == 'GAMA2EQCEQUITY00')
            {
                $loopStart = Mirae::toDateString($start);

                while($loopStart <= $loopEnd)
                {

                    $fixDate = Mirae::miraeDate($loopStart);
                    $result = [
                        'fund_id'=>$rd->id,
                        'tr_date'=>$fixDate,
                        'nav'=> $this->oraNav->manipulateNab($rd->fund_cd,Mirae::yesterday($fixDate),$fixDate),
                        'unit'=> $this->oraNav->manipulateUnit($rd->fund_cd,Mirae::yesterday($fixDate),$fixDate),
                        'aum'=> $this->oraNav->manipulateAum($rd->fund_cd,Mirae::yesterday($fixDate),$fixDate),
                    ];

                    //NavDailyPop::create($result);
                    \DB::table('pop_chart_daily')->insert($result);
                    $loopStart = date('Y-m-d', strtotime($loopStart . ' +1 day'));
                }
            }

        }

        //NavDailyPop::insert($result);
        return $result;


    }


    */

    /*  WITHOUT FORMULA */

    public function grabDailyNavByRange($start,$end,$fundCd="")
    {
        $loopStart = Mirae::toDateString($start);

        $loopEnd = Mirae::toDateString($end);

        $reksaDana = $this->sqlFund->select('id','fund_cd')->get();

        \DB::statement('DELETE FROM pop_chart_daily');

        \DB::statement('ALTER TABLE pop_chart_daily AUTO_INCREMENT = 1');

        $result = [];

        foreach($reksaDana as $rd)
        {
            // if($rd->fund_cd == 'GAMA2EQCEQUITY00')
            // {
                // $cek = $this->oraNav->where('fund_cd',$rd->fund_cd)->first();
                // if(!empty($cek->id))
                // {
                    $loopStart = Mirae::toDateString($start);

                    while($loopStart <= $loopEnd)
                    {

                        $fixDate = Mirae::miraeDate($loopStart);

                        $navValue = $this->oraNav->fieldByDate($rd->fund_cd,'nav_per_unit',$fixDate);
                        $unitValue = $this->oraNav->fieldByDate($rd->fund_cd,'outstanding_unit',$fixDate);
                        $aumValue = $navValue * $unitValue;

                        $result = [
                            'fund_id'=>$rd->id,
                            'tr_date'=>$fixDate,
                            'nav'=> $navValue,
                            'unit'=> $unitValue,
                            'aum'=> $aumValue,
                        ];

                        \DB::table('pop_chart_daily')->insert($result);
                        $loopStart = date('Y-m-d', strtotime($loopStart . ' +1 day'));
                    }
                // }
            // }

        }

        //NavDailyPop::insert($result);
        return $result;


    }

    public function fundPerformancePop($paramDate="")
    {
        //\Artisan::call('mirae-grab-reksa-dana');

        \DB::statement('DELETE FROM fund_performance_pops');

        \DB::statement('ALTER TABLE fund_performance_pops AUTO_INCREMENT = 1');

        $result = [];
        $no=0;
        foreach($this->sqlFund->all() as $fund)
        {
            $no++;

            //if($fund->fund_cd == 'GAMA2EQCEQUITY00')
            //{
               $cek = $this->oraNav->where('fund_cd',$fund->fund_cd)->first();
               if(!empty($cek->tr_date))
               {
                    $date = $this->oraNav->lastDayByFundCd($fund->fund_cd);
                    $result[] = [
                        'id'=>$no,
                        'fund_id'=>$fund->id,

                        'one_month' => $this->oraNav->manipulateNab($fund->fund_cd,Mirae::oneMonthAgo($date),$date),
                        'three_months' => $this->oraNav->manipulateNab($fund->fund_cd,Mirae::threeMonthsAgo($date),$date),
                        'six_months' => $this->oraNav->manipulateNab($fund->fund_cd,Mirae::sixMonthsAgo($date),$date),
                        'one_year' => $this->oraNav->manipulateNab($fund->fund_cd,Mirae::yearsAgo($date,1),$date),
                        'three_year' => $this->oraNav->manipulateNab($fund->fund_cd,Mirae::yearsAgo($date,3),$date),
                        'ytd' => $this->oraNav->manipulateNab($fund->fund_cd,Mirae::desemberYearsAgo($date),$date),

                        'aum_one_month' => $this->oraNav->manipulateAum($fund->fund_cd,Mirae::oneMonthAgo($date),$date),
                        'aum_three_months' => $this->oraNav->manipulateAum($fund->fund_cd,Mirae::threeMonthsAgo($date),$date),
                        'aum_six_months' => $this->oraNav->manipulateAum($fund->fund_cd,Mirae::sixMonthsAgo($date),$date),
                        'aum_one_year' => $this->oraNav->manipulateAum($fund->fund_cd,Mirae::yearsAgo($date,1),$date),
                        'aum_three_year' => $this->oraNav->manipulateAum($fund->fund_cd,Mirae::yearsAgo($date,3),$date),
                        'aum_ytd' => $this->oraNav->manipulateAum($fund->fund_cd,Mirae::desemberYearsAgo($date),$date),

                        'ou_one_month' => $this->oraNav->manipulateUnit($fund->fund_cd,Mirae::oneMonthAgo($date),$date),
                        'ou_three_months' => $this->oraNav->manipulateUnit($fund->fund_cd,Mirae::threeMonthsAgo($date),$date),
                        'ou_six_months' => $this->oraNav->manipulateUnit($fund->fund_cd,Mirae::sixMonthsAgo($date),$date),
                        'ou_one_year' => $this->oraNav->manipulateUnit($fund->fund_cd,Mirae::yearsAgo($date,1),$date),
                        'ou_three_year' => $this->oraNav->manipulateUnit($fund->fund_cd,Mirae::yearsAgo($date,3),$date),
                        'ou_ytd' => $this->oraNav->manipulateUnit($fund->fund_cd,Mirae::desemberYearsAgo($date),$date),


                        'tr_date'=>$date,
                    ];
                }
            //}
        }

        $this->sqlPop->insert($result);

        //return 1;
    }


    public function balanceTemporary()
    {
        $subsRed = [
            'r','s'
        ];

        $temporaries = $this->temporary->where('status','active')->get();

        foreach($temporaries as $temp)
        {
            if(in_array($temp->type,$subsRed))
            {
                $model = $this->trans->where('acnt_no',$temp->acnt_no)
                    ->where('ref_no',$temp->ref_no)
                    ->first();
                if(!empty($model->tr_date))
                {
                    if($model->proc_cd >=29)
                    {
                        $temp->update(['status'=>'un_active']);
                    }
                }
            }elseif($temp->type == 'sw'){
                $model = $this->swc->where('acnt_no',$temp->acnt_no)
                    ->where('ref_no',$temp->ref_no)
                    ->first();
                if(!empty($model->tr_date))
                {
                    if($model->proc_cd >=29)
                    {
                        $temp->update(['status'=>'un_active']);
                    }

                    if($model->proc_cd == 19)
                    {
                        $temp->update(['status'=>'un_active']);
                    }
                }
            }else{
                $model = $this->debet->where('acnt_no',$temp->acnt_no)
                    // ->where('tr_date',$temp->tr_date)
                    // //->where('ifua_cd',$temp->ifua_cd)
                    //  ->where('fund_cd',$temp->fund_cd)
                    //  ->where('recur_date',$temp->recur_date)
                    ->where('ref_no',$temp->ref_no)
                    ->first();
                    if(!empty($model->tr_date))
                    {
                        if($model->proc_cd ==9)
                        {
                            $temp->update(['status'=>'un_active']);
                        }
                    }
            }
        }
    }
}
