<?php namespace Mirae;

use Illuminate\Support\Facades\Facade;

class MiraeFacade extends Facade
{
	public static function getFacadeAccessor()
	{
		return 'register-mirae';
	}
}