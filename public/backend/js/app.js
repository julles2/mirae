function openChild(id)
{
	$("[id^='child']").hide();
	$("#child" + id).show();
}

$(document).ready(function(){
	$("button").click(function() {
		var $btn = $(this);
	    $btn.button('loading');
	    // simulating a timeout
	    setTimeout(function () {
	        $btn.button('reset');
	    }, 1000);
	});
});
