/*=============================================================================================	
 Company    : PT Web Architect Technology - webarq.com
 Document   : Javascript Plugin Exe
 Author     : Rizki Nida Chaerulsyah - akuiki.net
 ==============================================================================================*/
$(document).ready(function () {
    bannerFloat();                                                      //banner home, init slider & adjust image width
    tab();                                                              //function for tabbing
    closeOverlay();                                                     //function for close all popup
    showHide();                                                         //toggle checkbox for show and hide other content
    togglenetInvestment();						//toggle investment for show and hide date invest
    milestone();							//milestone slider & tab
    uploadView();
    dropLanguage();							//dropdown language menu
    dropSearch();							//dropdown search menu
    accordion();							//accordion function
    slideSorting();							//slide sorting
    playVideo();							//for play video iframe
    mscrollbar();							//for custom scrollbar
    mobileMenu();
    dropProdNav();
    responsiveEA();                                                     //responsive element adjustment 
//    $(".opt input[type=checkbox]").optCustom({                          //custom checkbox 
//        className: "check-custom"
//    });
//    $(".opt input[type=radio]").optCustom({                             //custom radio 
//        className: "radio-custom"
//    });

    $(".date input").datepicker();
    $(".box-form .showhide-type2").radioShowHide('.cshowhide-type2');
    $(".list-data.list_2 .showhide-type2").radioShowHide('.expand-priode', '.row_btm');
});
