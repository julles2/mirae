/*=============================================================================================	
 Company    : PT Web Architect Technology - webarq.com
 Document   : Javascript Plugin Lib
 Author     : Rizki Nida Chaerulsyah - akuiki.net
 ==============================================================================================*/

$.fn.isOnScreen = function () {
    if (this.length) {
        var viewport = {};
        viewport.top = $(window).scrollTop();
        viewport.bottom = viewport.top + $(window).height();
        var bounds = {};
        bounds.top = this.offset().top;
        bounds.bottom = bounds.top + this.outerHeight();
        return ((bounds.top <= viewport.bottom) && (bounds.bottom >= viewport.top));
    } else
        return false
};

$.fn.fileInput = function (e) {
    var elem = this;

    elem.wrap('<div class="' + e.class_name + '"></div>');
    elem.css({
        position: 'absolute',
        top: 0,
        left: 0,
        opacity: 0
    });
    elem.parent('.' + e.class_name).css({
        position: 'relative',
        width: elem.outerWidth() - 2,
        height: elem.outerHeight() - 2,
        display: 'inline-block'
    });
    elem.parent('.' + e.class_name).append("<span>" + elem.attr('placeholder-text') + "</span>");
    elem.on('change', function () {
        var value = $(this).val();
        if (value != "") {
            value = value.substring(12, value.length);
            $(this).next("span").html(value);
        } else {
            $(this).next("span").html(elem.attr('placeholder-text'));
        }
    });
};


$.fn.optCustom = function (q) {
    var
            elem = this,
            s = {
                className: 'checkbox_custom',
            };
    s = $.extend(s, q);
    elem.wrap("<div class='" + s.className + "' style='position:relative' ></div>");
    elem.css({
        position: 'absolute',
        top: 0,
        left: 0,
        opacity: 0,
        width: "100%",
        height: "100%"
    });
    elem.each(function () {
        if ($(this).is(":checked")) {
            $(this).parent().addClass('active');
            $(this).parents('.opt').removeClass('active');
        }
    });
    elem.on('change', function () {
        if ($(this).attr('type') === "checkbox")
            if (!$(this).is(":checked")) {
                $(this).parent().removeClass('active').parent();
                $(this).parents('.opt').removeClass('active');
            } else {
                $(this).parent().addClass('active')
                $(this).parents('.opt').addClass('active');
            }
        else
            $("input[type=radio][name=" + $(this).attr('name') + "]").each(function () {
                if ($(this).is(":checked")) {
                    $("input[type=radio][name=" + $(this).attr('name') + "]").parent().removeClass('active');
                    $(this).parent().addClass('active');
                    return false;
                }
            });
    });

    elem.parent().on('click', function () {
        if ($(this).attr('type') === "checkbox")
            if (!$(this).children('input').is(":checked")) {
                $(this).removeClass('active');
                $(this).parents('.opt').removeClass('active');
            } else {
                // console.log('sa')
                $(this).addClass('active');
                $(this).parents('.opt').addClass('active');
            }
        else
            $("input[type=radio][name=" + $(this).attr('name') + "]").each(function () {
                if ($(this).is(":checked")) {
                    $("input[type=radio][name=" + $(this).attr('name') + "]").parent().removeClass('active');
                    $(this).parent().addClass('active');
                    return false;
                }
            });
    });

};



$.fn.placeHolder = function () {
    var
            elem = this,
            data = elem.attr('data-placeholder'),
            _this;

    elem.each(function () {
        _this = $(this);
        data = _this.attr('data-placeholder');

        _this.val(data);

        _this.focusin(function () {
            data = $(this).attr('data-placeholder');
            if ($(this).val() === data) {
                $(this).val("")
            }
        });
        _this.focusout(function () {
            data = $(this).attr('data-placeholder');
            if ($(this).val() === "") {
                $(this).val(data);
            }
        });
    });

};
function bannerFloat() {
    if ($(".banner-float").length) {
        //init slider
        $(".banner-float .slider").slick({dots: true, arrows: false});

        //adjust width imageslider
        var actAdjustment = function () {
            var width = $(window).width() - $(".banner-float .right").outerWidth();
            $(".banner-float .left").css({width: width});
        }
        actAdjustment();
        $(window).resize(function () {
            actAdjustment();
        });
    }
}

function tab() {
    $(".nav-tab a").click(function (e) {
        e.preventDefault();

        if (!$(this).hasClass('active') && !$(this).parents('.wrap-tab').find(".content-tab" + $(this).attr('href')).is(":visible")) {
            var active = $(this).parents('.nav-tab').find('a.active').attr('href');

            $(this).parents('.wrap-tab').find(".content-tab" + $(this).attr('href')).slideDown(300);
            $(this).parents('.wrap-tab').find(".content-tab" + active).slideUp(300);
            $(this).parents('.nav-tab').find('a.active').removeClass('active')
            $(this).addClass('active');
        }
    });
    $(".nav-tab a").each(function () {
        if ($(this).hasClass('active')) {
            $(".content-tab" + $(this).attr('href')).slideDown(300);
        }
    });
}
function closePop(id) {
    var Close = $("#" + id);

    TweenLite.to(Close, 0.5, {
        css: {
            'display': 'none',
            'opacity': '0',
            'scale': '0.9'
        },
        delay: 0,
        ease: Quart.easeOut
    });
    TweenLite.to($('.overlay-popup'), 0.5, {
        css: {
            'display': 'none',
            'opacity': '0'
        },
        delay: 0,
        ease: Quart.easeOut
    });
}
function showPop(id) {
    var Show = $("#" + id);
    TweenLite.set(Show, {opacity: 0, scale: 1.1});
    TweenLite.to(Show, 0.5, {
        css: {
            'display': 'block',
            'opacity': '1',
            'scale': '1'
        },
        delay: 0,
        ease: Quart.easeOut
    });
    TweenLite.to($('.overlay-popup'), 0.5, {
        css: {
            'display': 'block',
            'opacity': '1'
        },
        delay: 0,
        ease: Quart.easeOut
    });
    $("html,body").animate({scrollTop: 0})
}
function closeOverlay() {

    $('.overlay-popup').on('click', function () {
        if (!$(".loaderpopup").is(":visible")) {
            TweenLite.to($(this), 0.5, {
                css: {
                    'display': 'none',
                    'opacity': '0',
                },
                delay: 0,
                ease: Quart.easeOut
            });
            TweenLite.to($('.product-detail'), 0.5, {
                css: {
                    'display': 'none',
                    'opacity': '0',
                    'scale': '0.9'
                },
                delay: 0,
                ease: Quart.easeOut
            });
        }
    });
}

function togglenetInvestment() {
    var box = $(".net-investment"),
            btnperiode = box.find(".btn-periode");
    box.find("input[type=radio]").click(function () {
        var datatoggle = $(this).attr("data-toggle");
        if (datatoggle == "berkala") {
            $(this).parents(".net-investment").find(".box-periode-berkala").slideDown("fast");
        } else {
            $(this).parents(".net-investment").find(".box-periode-berkala").slideUp("fast");
        }
        // console.log($(this).attr("data-toggle"));
    });
}

function showHide() {
    $(".showhide input").on('change', function () {
        var target = $(this).parent().attr('content-target');
        if ($(this).is(':checked')) {
            $(target).slideUp(300);
        } else {
            $(target).slideDown(300);
        }

    });

}
$.fn.radioShowHide = function (target, parents) {
    var elem = this;
    $(target).fadeOut(300);
    parents === undefined ? parents = '.row' : parents = parents;

    elem.each(function () {
        if ($(this).hasClass('trigger-show') && $(this).is(":checked"))
            $(this).parents(parents).find(target).slideDown(200);
        else
            $(this).parents(parents).find(target).slideUp(200);
    });
    elem.on('change', function () {
        if ($(this).hasClass('trigger-show')) {
            $(this).parents(parents).find(target).slideDown(200);
        } else {
            $(this).parents(parents).find(target).slideUp(200);
        }
    });
};

function milestone() {
    if ($(window).width() > 650) {
        $(".btn-milestone").slick({
            dots: false,
            arrows: true,
            infinite: false,
            slidesToShow: 9,
            slidesToScroll: 1,
            focusOnSelect: true,
            vertical: true,
            prevArrow: '<button type="button" class="slick-prev"></button>',
            nextArrow: '<button type="button" class="slick-next"></button>',
        });
    } else {
        $(".btn-milestone").slick({
            dots: false,
                arrows: true,
                infinite: false,
                slidesToShow: 3,
                slidesToScroll: 1,
                focusOnSelect: true,
                prevArrow: '<button type="button" class="slick-prev"></button>',
                nextArrow: '<button type="button" class="slick-next"></button>',
        });
    }
    var validasiSlider = function () {
        if ($(window).width() > 650 && !$(".btn-milestone").hasClass('slick-vertical')) {

            $(".btn-milestone").slick('unslick');
            $(".btn-milestone").slick({
                dots: false,
                arrows: true,
                infinite: false,
                slidesToShow: 9,
                slidesToScroll: 1,
                focusOnSelect: true,
                vertical: true,
                prevArrow: '<button type="button" class="slick-prev"></button>',
                nextArrow: '<button type="button" class="slick-next"></button>',
            });
        }

        if ($(window).width() < 650 && $(".btn-milestone").hasClass('slick-vertical')) {
            $(".btn-milestone").slick('unslick');
            $(".btn-milestone").slick({
                dots: false,
                arrows: true,
                infinite: false,
                slidesToShow: 3,
                slidesToScroll: 1,
                focusOnSelect: true,
                prevArrow: '<button type="button" class="slick-prev"></button>',
                nextArrow: '<button type="button" class="slick-next"></button>',
            });
        }
    };




    var box = $(".box-milestone");
    var dataBox = box.find(".desc-milestone-content");

    dataBox.html(box.find(".btn-milestone .list-milestone.active").find(".box-desc-milestone").html());

    box.find(".btn-milestone .list-milestone a").click(function () {
        if (!$(this).parent().hasClass('active')) {
            box.find(".btn-milestone .list-milestone").removeClass('active');
            $(this).parent().addClass('active');
            var data = $(this).next(".box-desc-milestone").html();
            TweenLite.to(dataBox, 0.3,
                    {
                        bottom: -90,
                        opacity: 0,
                        onComplete: function () {
                            dataBox.html(data);
                        }
                    });

            TweenLite.to(dataBox, 0.3,
                    {
                        delay: 0.4,
                        bottom: 0,
                        opacity: 1,
                    });
        } else {

        }
    });
    $(window).resize(function () {
        validasiSlider();
    });
}

function uploadView() {
    var elem = $(".upload-file"),
            file_list = $(".wrap-files"),
            toggle = elem.find(".toggle"),
            inputname = elem.attr('data-input-name');

    var actappend = function () {
        file_list.find(".input-list").append("<input name='" + inputname + "[]'  type='file' date-index='" + file_list.find('.list-box .box').length + "' />");

    };
    actappend();


    toggle.click(function () {
        file_list.find(".input-list input:last-child").click();
    });

    file_list.find(".input-list").on('change', 'input:last-child', function () {
        var val = $(this).val();
        if (val === '') {
            file_list.append("");
        } else {
            file_list.find('.list-box').append("<div class='box'>" + val + "<a href='' >close</a></div>");
            file_list.hasClass('fill') ? '' : file_list.addClass('fill');
            file_list.find('.list-box').hasClass('fill') ? '' : file_list.find('.list-box').addClass('fill');
            actappend();
        }
    });

    file_list.on('click', '.box a', function (e) {
        e.preventDefault();
        var box = $(this).parent();

        if (file_list.find('.list-box .box').length > 0) {
            file_list.find(".input-list input:nth-child(" + (box.index() + 1) + ")").remove();
            if (file_list.find('.list-box .box').length === 1) {
                file_list.hasClass('fill') ? file_list.removeClass('fill') : '';
                file_list.find('.list-box').hasClass('fill') ? file_list.find('.list-box').removeClass('fill') : '';

            }
        } else {

        }

        box.remove();
    });
}

function dropLanguage() {
    $(".drop-menu > a").click(function () {
        $(".box-drop-menu").slideToggle("fast");
    });

    $(document).bind('click', function (e) {
        var $clicked = $(e.target);
        if (!$clicked.parents().hasClass("drop-menu")) {
            $(".box-drop-menu").slideUp("fast");
        }
    });
}

function dropSearch() {
    $(".search-header > a").click(function () {
        $(".box-search-header").slideToggle("fast");
    });

    $(document).bind('click', function (e) {
        var $clicked = $(e.target);
        if (!$clicked.parents().hasClass("search-header")) {
            $(".box-search-header").slideUp("fast");
        }
    });
}

function accordion() {
    var box = $(".box-accordion");
    box.find(".list-accordion").each(function () {
        if ($(this).hasClass("active")) {
            $(this).find(".box-acc").css('display', 'block');
        }
    });

    box.find(".list-accordion .btn-acc").click(function () {
        if (!$(this).parent(".list-accordion").hasClass("active")) {
            $(this).parents(".box-accordion").find(".list-accordion .box-acc").slideUp(300);
            $(this).parents(".box-accordion").find(".list-accordion").removeClass("active");
            $(this).parent(".list-accordion").find(".box-acc").slideDown(300);
            $(this).parent(".list-accordion").addClass("active");
        } else {
            $(this).parent(".list-accordion").find(".box-acc").slideUp(300);
            $(this).parent(".list-accordion").removeClass("active");
        }
    });
}

function slideSorting() {
    $(".slide-sorting").slick({
        dots: false,
        arrows: true,
        infinite: false,
        slidesToShow: 20,
        slidesToScroll: 1,
        focusOnSelect: true,
        prevArrow: '<button type="button" class="slick-prev"><i class="icwp ic_prev_slide"></i></button>',
        nextArrow: '<button type="button" class="slick-next"><i class="icwp ic_next_slide"></i></button>',

        responsive: [

            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 6,
                    slidesToScroll: 2
                }
            },
                    // You can unslick at a given breakpoint now by adding:
                    // settings: "unslick"
                    // instead of a settings object
        ]
    });

    $(".btnslidd").click(function(){
        var idd = $(this).attr("data-ind");
        $('.slide-sorting').slick('slickGoTo', parseInt(idd));
        console.log("sd "+idd);
    })

}



function playVideo() {
    var boxvids = $(".box-video");
    boxvids.find(".play").click(function (ev) {
        // $(".vids")
        box = $(this).parents(".box-video");
        box.find(".thumb").fadeOut();
        box.find(".caption").fadeOut();
        $(this).parents(".box-video").find(".vids #thisvids")[0].src += "&autoplay=1";
        // $("#video")[0].src += "&autoplay=1";
        ev.preventDefault();
    });
}

function mscrollbar() {
    if ($(window).width() > 1200)
        $(".wrap-scroll").mCustomScrollbar({
            theme: "dark-3"
        })
}
function openPopTab(id, tab) {
    showPop(id);
    if (tab !== undefined) {
        var popup = $("#" + id),
                navtab = popup.find(".nav-tab a:eq(" + tab + ")");

        setTimeout(function () {
            navtab.click();
        }, 100);
        popup.find('img').load(function () {
            navtab.click();
        });
    }
}
function mobileMenu() {
    $("<li class='toggle-menu'><a href='' >MENU</a></li>").insertBefore($("header ul li:nth-child(6)"));
    $("header").append("<div class='mobile-menu'></div>");
    $("header ul").clone().appendTo($("header .mobile-menu"));

    $("header .toggle-menu a").click(function (e) {
        e.preventDefault();
        if ($(".mobile-menu").is(":visible")) {
            $(".mobile-menu").fadeOut(300);
        } else {
            $(".mobile-menu").fadeIn(300);
        }
    });
    $("header li.have-child").children('a').click(function (e) {
        if ($(window).width() < 1180 && $(this).parent().index() !== 7) {
            e.preventDefault();
        }

        $(this).parent().find('.child-menu').slideToggle(200);
    });
    $("header .list-child-menu").children('a').click(function (e) {
        if ($(this).next('.ltl-child-menu').length) {
            e.preventDefault();
        }

        $(this).parent().find('.ltl-child-menu').slideToggle(200);
    });
}
function dropProdNav() {
    if ($(window).width() < 768) {
        $(".std-content .nav-produk .right").click(function () {
            var height = $(this).find('a').length * 26;
            if (!$(this).hasClass('drop')) {
                $(this).addClass('drop');
                $(this).css({'height': height - 16});
            } else {
                $(this).removeClass('drop');
                $(this).css({'height': 0});
            }
        });
        $("body").on('click', '.nav-tab', function () {
            var height = $(this).children('a').length * 36;
            if (!$(this).hasClass('drop')) {
                $(this).addClass('drop');
                $(this).css({'height': height + 2});
            } else {
                $(this).removeClass('drop');
                $(this).css({'height': 0});
            }
        });
        $("body").on('click', '.nav-tab-general', function () {
            var height = $(this).children('.list-tab').length * 36;
            if (!$(this).hasClass('drop')) {
                $(this).addClass('drop');
                $(this).css({'height': height + 2});
            } else {
                $(this).removeClass('drop');
                $(this).css({'height': 0});
            }
        });
    }
}
function responsiveEA() {
    $(".myaccount table.table-radius").each(function () {
        $(this).wrap("<div class='overflow-table'></div>");
    });
    $(".myaccount .title-content").each(function () {
        if ($(this).find('select')) {
            $(this).addClass('type-2');
        }
    });
}