/*=============================================================================================	
 Company    : PT Web Architect Technology - webarq.com
 Document   : Javascript Plugin Exe
 Author     : Rizki Nida Chaerulsyah - akuiki.net
 ==============================================================================================*/
$(document).ready(function () {
    bannerFloat();                                                      //banner home, init slider & adjust image width
    tab();                                                              //function for tabbing
    closeOverlay();                                                     //function for close all popup
    showHide();                                                         //toggle checkbox for show and hide other content
    togglenetInvestment();						//toggle investment for show and hide date invest
    milestone();							//milestone slider & tab
    uploadView();
    dropLanguage();							//dropdown language menu
    dropSearch();							//dropdown search menu
    accordion();							//accordion function
    slideSorting();							//slide sorting
    playVideo();							//for play video iframe
    mscrollbar();							//for custom scrollbar
    mobileMenu();
    dropProdNav();
    responsiveEA();                                                     //responsive element adjustment 
    sliderProduct();
    tabBank();                                                     //tab bank
    tabRadio();                                                    //tab radio
    tabRadioId();                                                    //tab radio
    tabRadioWork();                                                    //tab radio

//    $(".opt input[type=checkbox]").optCustom({                          //custom checkbox 
//        className: "check-custom"
//    });
//    $(".opt input[type=radio]").optCustom({                             //custom radio 
//        className: "radio-custom"
//    });


    $(".h-product .filter .col select").select2({
        width: '100%'
    });
    $(".h-compare select").select2({
        width: '100%'
    });
    $(".lbank .radio-btn input[type=radio]").optCustom(//custom radio bank
            {className: "btn-std sea"});

    $(".date input").datepicker();
    $(".box-form .showhide-type2").radioShowHide('.cshowhide-type2');
    $(".list-data.list_2 .showhide-type2").radioShowHide('.expand-priode', '.row_btm');
    $(".scroll-text").mCustomScrollbar({
        theme: "dark-3"
    });
});

