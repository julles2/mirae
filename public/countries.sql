/*
Navicat MySQL Data Transfer

Source Server         : local
Source Server Version : 50505
Source Host           : localhost:3306
Source Database       : mirae

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2017-08-15 16:31:17
*/

SET FOREIGN_KEY_CHECKS=0;
-- ----------------------------
-- Table structure for `countries`
-- ----------------------------
DROP TABLE IF EXISTS `countries`;
CREATE TABLE `countries` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(2) COLLATE utf8_unicode_ci NOT NULL,
  `country` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `countries_code_index` (`code`)
) ENGINE=InnoDB AUTO_INCREMENT=250 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of countries
-- ----------------------------
INSERT INTO countries VALUES ('1', 'AF', 'Afghanistan', null, null);
INSERT INTO countries VALUES ('2', 'AX', 'Aland Islands', null, null);
INSERT INTO countries VALUES ('3', 'AL', 'Albania', null, null);
INSERT INTO countries VALUES ('4', 'DZ', 'Algeria', null, null);
INSERT INTO countries VALUES ('5', 'AS', 'American Samoa', null, null);
INSERT INTO countries VALUES ('6', 'AD', 'Andorra', null, null);
INSERT INTO countries VALUES ('7', 'AO', 'Angola', null, null);
INSERT INTO countries VALUES ('8', 'AI', 'Anguilla', null, null);
INSERT INTO countries VALUES ('9', 'AQ', 'Antarctica', null, null);
INSERT INTO countries VALUES ('10', 'AG', 'Antigua and Barbuda', null, null);
INSERT INTO countries VALUES ('11', 'AR', 'Argentina', null, null);
INSERT INTO countries VALUES ('12', 'AM', 'Armenia', null, null);
INSERT INTO countries VALUES ('13', 'AW', 'Aruba', null, null);
INSERT INTO countries VALUES ('14', 'AU', 'Australia', null, null);
INSERT INTO countries VALUES ('15', 'AT', 'Austria', null, null);
INSERT INTO countries VALUES ('16', 'AZ', 'Azerbaijan', null, null);
INSERT INTO countries VALUES ('17', 'BS', 'Bahamas (the)', null, null);
INSERT INTO countries VALUES ('18', 'BH', 'Bahrain', null, null);
INSERT INTO countries VALUES ('19', 'BD', 'Bangladesh', null, null);
INSERT INTO countries VALUES ('20', 'BB', 'Barbados', null, null);
INSERT INTO countries VALUES ('21', 'BY', 'Belarus', null, null);
INSERT INTO countries VALUES ('22', 'BE', 'Belgium', null, null);
INSERT INTO countries VALUES ('23', 'BZ', 'Belize', null, null);
INSERT INTO countries VALUES ('24', 'BJ', 'Benin', null, null);
INSERT INTO countries VALUES ('25', 'BM', 'Bermuda', null, null);
INSERT INTO countries VALUES ('26', 'BT', 'Bhutan', null, null);
INSERT INTO countries VALUES ('27', 'BO', 'Bolivia (Plurinational State of)', null, null);
INSERT INTO countries VALUES ('28', 'BQ', 'Bonaire, Sint Eustatius and Saba', null, null);
INSERT INTO countries VALUES ('29', 'BA', 'Bosnia and Herzegovina', null, null);
INSERT INTO countries VALUES ('30', 'BW', 'Botswana', null, null);
INSERT INTO countries VALUES ('31', 'BV', 'Bouvet Island', null, null);
INSERT INTO countries VALUES ('32', 'BR', 'Brazil', null, null);
INSERT INTO countries VALUES ('33', 'IO', 'British Indian Ocean Territory (the)', null, null);
INSERT INTO countries VALUES ('34', 'BN', 'Brunei Darussalam', null, null);
INSERT INTO countries VALUES ('35', 'BG', 'Bulgaria', null, null);
INSERT INTO countries VALUES ('36', 'BF', 'Burkina Faso', null, null);
INSERT INTO countries VALUES ('37', 'BI', 'Burundi', null, null);
INSERT INTO countries VALUES ('38', 'CV', 'Cabo Verde', null, null);
INSERT INTO countries VALUES ('39', 'KH', 'Cambodia', null, null);
INSERT INTO countries VALUES ('40', 'CM', 'Cameroon', null, null);
INSERT INTO countries VALUES ('41', 'CA', 'Canada', null, null);
INSERT INTO countries VALUES ('42', 'KY', 'Cayman Islands (the)', null, null);
INSERT INTO countries VALUES ('43', 'CF', 'Central African Republic (the)', null, null);
INSERT INTO countries VALUES ('44', 'TD', 'Chad', null, null);
INSERT INTO countries VALUES ('45', 'CL', 'Chile', null, null);
INSERT INTO countries VALUES ('46', 'CN', 'China', null, null);
INSERT INTO countries VALUES ('47', 'CX', 'Christmas Island', null, null);
INSERT INTO countries VALUES ('48', 'CC', 'Cocos (Keeling) Islands (the)', null, null);
INSERT INTO countries VALUES ('49', 'CO', 'Colombia', null, null);
INSERT INTO countries VALUES ('50', 'KM', 'Comoros (the)', null, null);
INSERT INTO countries VALUES ('51', 'CD', 'Congo (the Democratic Republic of the)', null, null);
INSERT INTO countries VALUES ('52', 'CG', 'Congo (the)', null, null);
INSERT INTO countries VALUES ('53', 'CK', 'Cook Islands (the)', null, null);
INSERT INTO countries VALUES ('54', 'CR', 'Costa Rica', null, null);
INSERT INTO countries VALUES ('55', 'CI', 'Cote d\'Ivoire', null, null);
INSERT INTO countries VALUES ('56', 'HR', 'Croatia', null, null);
INSERT INTO countries VALUES ('57', 'CU', 'Cuba', null, null);
INSERT INTO countries VALUES ('58', 'CW', 'Curacao', null, null);
INSERT INTO countries VALUES ('59', 'CY', 'Cyprus', null, null);
INSERT INTO countries VALUES ('60', 'CZ', 'Czech Republic (the)', null, null);
INSERT INTO countries VALUES ('61', 'DK', 'Denmark', null, null);
INSERT INTO countries VALUES ('62', 'DJ', 'Djibouti', null, null);
INSERT INTO countries VALUES ('63', 'DM', 'Dominica', null, null);
INSERT INTO countries VALUES ('64', 'DO', 'Dominican Republic (the)', null, null);
INSERT INTO countries VALUES ('65', 'EC', 'Ecuador', null, null);
INSERT INTO countries VALUES ('66', 'EG', 'Egypt', null, null);
INSERT INTO countries VALUES ('67', 'SV', 'El Salvador', null, null);
INSERT INTO countries VALUES ('68', 'GQ', 'Equatorial Guinea', null, null);
INSERT INTO countries VALUES ('69', 'ER', 'Eritrea', null, null);
INSERT INTO countries VALUES ('70', 'EE', 'Estonia', null, null);
INSERT INTO countries VALUES ('71', 'ET', 'Ethiopia', null, null);
INSERT INTO countries VALUES ('72', 'FK', 'Falkland Islands (the) [Malvinas]', null, null);
INSERT INTO countries VALUES ('73', 'FO', 'Faroe Islands (the)', null, null);
INSERT INTO countries VALUES ('74', 'FJ', 'Fiji', null, null);
INSERT INTO countries VALUES ('75', 'FI', 'Finland', null, null);
INSERT INTO countries VALUES ('76', 'FR', 'France', null, null);
INSERT INTO countries VALUES ('77', 'GF', 'French Guiana', null, null);
INSERT INTO countries VALUES ('78', 'PF', 'French Polynesia', null, null);
INSERT INTO countries VALUES ('79', 'TF', 'French Southern Territories (the)', null, null);
INSERT INTO countries VALUES ('80', 'GA', 'Gabon', null, null);
INSERT INTO countries VALUES ('81', 'GM', 'Gambia (the)', null, null);
INSERT INTO countries VALUES ('82', 'GE', 'Georgia', null, null);
INSERT INTO countries VALUES ('83', 'DE', 'Germany', null, null);
INSERT INTO countries VALUES ('84', 'GH', 'Ghana', null, null);
INSERT INTO countries VALUES ('85', 'GI', 'Gibraltar', null, null);
INSERT INTO countries VALUES ('86', 'GR', 'Greece', null, null);
INSERT INTO countries VALUES ('87', 'GL', 'Greenland', null, null);
INSERT INTO countries VALUES ('88', 'GD', 'Grenada', null, null);
INSERT INTO countries VALUES ('89', 'GP', 'Guadeloupe', null, null);
INSERT INTO countries VALUES ('90', 'GU', 'Guam', null, null);
INSERT INTO countries VALUES ('91', 'GT', 'Guatemala', null, null);
INSERT INTO countries VALUES ('92', 'GG', 'Guernsey', null, null);
INSERT INTO countries VALUES ('93', 'GN', 'Guinea', null, null);
INSERT INTO countries VALUES ('94', 'GW', 'Guinea-Bissau', null, null);
INSERT INTO countries VALUES ('95', 'GY', 'Guyana', null, null);
INSERT INTO countries VALUES ('96', 'HT', 'Haiti', null, null);
INSERT INTO countries VALUES ('97', 'HM', 'Heard Island and McDonald Islands', null, null);
INSERT INTO countries VALUES ('98', 'VA', 'Holy See (the)', null, null);
INSERT INTO countries VALUES ('99', 'HN', 'Honduras', null, null);
INSERT INTO countries VALUES ('100', 'HK', 'Hong Kong', null, null);
INSERT INTO countries VALUES ('101', 'HU', 'Hungary', null, null);
INSERT INTO countries VALUES ('102', 'IS', 'Iceland', null, null);
INSERT INTO countries VALUES ('103', 'IN', 'India', null, null);
INSERT INTO countries VALUES ('104', 'ID', 'Indonesia', null, null);
INSERT INTO countries VALUES ('105', 'IR', 'Iran (Islamic Republic of)', null, null);
INSERT INTO countries VALUES ('106', 'IQ', 'Iraq', null, null);
INSERT INTO countries VALUES ('107', 'IE', 'Ireland', null, null);
INSERT INTO countries VALUES ('108', 'IM', 'Isle of Man', null, null);
INSERT INTO countries VALUES ('109', 'IL', 'Israel', null, null);
INSERT INTO countries VALUES ('110', 'IT', 'Italy', null, null);
INSERT INTO countries VALUES ('111', 'JM', 'Jamaica', null, null);
INSERT INTO countries VALUES ('112', 'JP', 'Japan', null, null);
INSERT INTO countries VALUES ('113', 'JE', 'Jersey', null, null);
INSERT INTO countries VALUES ('114', 'JO', 'Jordan', null, null);
INSERT INTO countries VALUES ('115', 'KZ', 'Kazakhstan', null, null);
INSERT INTO countries VALUES ('116', 'KE', 'Kenya', null, null);
INSERT INTO countries VALUES ('117', 'KI', 'Kiribati', null, null);
INSERT INTO countries VALUES ('118', 'KP', 'Korea (the Democratic People\'s Republic of)', null, null);
INSERT INTO countries VALUES ('119', 'KR', 'Korea (the Republic of)', null, null);
INSERT INTO countries VALUES ('120', 'KW', 'Kuwait', null, null);
INSERT INTO countries VALUES ('121', 'KG', 'Kyrgyzstan', null, null);
INSERT INTO countries VALUES ('122', 'LA', 'Lao People\'s Democratic Republic (the)', null, null);
INSERT INTO countries VALUES ('123', 'LV', 'Latvia', null, null);
INSERT INTO countries VALUES ('124', 'LB', 'Lebanon', null, null);
INSERT INTO countries VALUES ('125', 'LS', 'Lesotho', null, null);
INSERT INTO countries VALUES ('126', 'LR', 'Liberia', null, null);
INSERT INTO countries VALUES ('127', 'LY', 'Libya', null, null);
INSERT INTO countries VALUES ('128', 'LI', 'Liechtenstein', null, null);
INSERT INTO countries VALUES ('129', 'LT', 'Lithuania', null, null);
INSERT INTO countries VALUES ('130', 'LU', 'Luxembourg', null, null);
INSERT INTO countries VALUES ('131', 'MO', 'Macao', null, null);
INSERT INTO countries VALUES ('132', 'MK', 'Macedonia (the former Yugoslav Republic of)', null, null);
INSERT INTO countries VALUES ('133', 'MG', 'Madagascar', null, null);
INSERT INTO countries VALUES ('134', 'MW', 'Malawi', null, null);
INSERT INTO countries VALUES ('135', 'MY', 'Malaysia', null, null);
INSERT INTO countries VALUES ('136', 'MV', 'Maldives', null, null);
INSERT INTO countries VALUES ('137', 'ML', 'Mali', null, null);
INSERT INTO countries VALUES ('138', 'MT', 'Malta', null, null);
INSERT INTO countries VALUES ('139', 'MH', 'Marshall Islands (the)', null, null);
INSERT INTO countries VALUES ('140', 'MQ', 'Martinique', null, null);
INSERT INTO countries VALUES ('141', 'MR', 'Mauritania', null, null);
INSERT INTO countries VALUES ('142', 'MU', 'Mauritius', null, null);
INSERT INTO countries VALUES ('143', 'YT', 'Mayotte', null, null);
INSERT INTO countries VALUES ('144', 'MX', 'Mexico', null, null);
INSERT INTO countries VALUES ('145', 'FM', 'Micronesia (Federated States of)', null, null);
INSERT INTO countries VALUES ('146', 'MD', 'Moldova (the Republic of)', null, null);
INSERT INTO countries VALUES ('147', 'MC', 'Monaco', null, null);
INSERT INTO countries VALUES ('148', 'MN', 'Mongolia', null, null);
INSERT INTO countries VALUES ('149', 'ME', 'Montenegro', null, null);
INSERT INTO countries VALUES ('150', 'MS', 'Montserrat', null, null);
INSERT INTO countries VALUES ('151', 'MA', 'Morocco', null, null);
INSERT INTO countries VALUES ('152', 'MZ', 'Mozambique', null, null);
INSERT INTO countries VALUES ('153', 'MM', 'Myanmar', null, null);
INSERT INTO countries VALUES ('154', 'NA', 'Namibia', null, null);
INSERT INTO countries VALUES ('155', 'NR', 'Nauru', null, null);
INSERT INTO countries VALUES ('156', 'NP', 'Nepal', null, null);
INSERT INTO countries VALUES ('157', 'NL', 'Netherlands (the)', null, null);
INSERT INTO countries VALUES ('158', 'NC', 'New Caledonia', null, null);
INSERT INTO countries VALUES ('159', 'NZ', 'New Zealand', null, null);
INSERT INTO countries VALUES ('160', 'NI', 'Nicaragua', null, null);
INSERT INTO countries VALUES ('161', 'NE', 'Niger (the)', null, null);
INSERT INTO countries VALUES ('162', 'NG', 'Nigeria', null, null);
INSERT INTO countries VALUES ('163', 'NU', 'Niue', null, null);
INSERT INTO countries VALUES ('164', 'NF', 'Norfolk Island', null, null);
INSERT INTO countries VALUES ('165', 'MP', 'Northern Mariana Islands (the)', null, null);
INSERT INTO countries VALUES ('166', 'NO', 'Norway', null, null);
INSERT INTO countries VALUES ('167', 'OM', 'Oman', null, null);
INSERT INTO countries VALUES ('168', 'PK', 'Pakistan', null, null);
INSERT INTO countries VALUES ('169', 'PW', 'Palau', null, null);
INSERT INTO countries VALUES ('170', 'PS', 'Palestine, State of', null, null);
INSERT INTO countries VALUES ('171', 'PA', 'Panama', null, null);
INSERT INTO countries VALUES ('172', 'PG', 'Papua New Guinea', null, null);
INSERT INTO countries VALUES ('173', 'PY', 'Paraguay', null, null);
INSERT INTO countries VALUES ('174', 'PE', 'Peru', null, null);
INSERT INTO countries VALUES ('175', 'PH', 'Philippines (the)', null, null);
INSERT INTO countries VALUES ('176', 'PN', 'Pitcairn', null, null);
INSERT INTO countries VALUES ('177', 'PL', 'Poland', null, null);
INSERT INTO countries VALUES ('178', 'PT', 'Portugal', null, null);
INSERT INTO countries VALUES ('179', 'PR', 'Puerto Rico', null, null);
INSERT INTO countries VALUES ('180', 'QA', 'Qatar', null, null);
INSERT INTO countries VALUES ('181', 'RE', 'Reunion', null, null);
INSERT INTO countries VALUES ('182', 'RO', 'Romania', null, null);
INSERT INTO countries VALUES ('183', 'RU', 'Russian Federation (the)', null, null);
INSERT INTO countries VALUES ('184', 'RW', 'Rwanda', null, null);
INSERT INTO countries VALUES ('185', 'BL', 'Saint Barthelemy', null, null);
INSERT INTO countries VALUES ('186', 'SH', 'Saint Helena, Ascension and Tristan da Cunha', null, null);
INSERT INTO countries VALUES ('187', 'KN', 'Saint Kitts and Nevis', null, null);
INSERT INTO countries VALUES ('188', 'LC', 'Saint Lucia', null, null);
INSERT INTO countries VALUES ('189', 'MF', 'Saint Martin (French part)', null, null);
INSERT INTO countries VALUES ('190', 'PM', 'Saint Pierre and Miquelon', null, null);
INSERT INTO countries VALUES ('191', 'VC', 'Saint Vincent and the Grenadines', null, null);
INSERT INTO countries VALUES ('192', 'WS', 'Samoa', null, null);
INSERT INTO countries VALUES ('193', 'SM', 'San Marino', null, null);
INSERT INTO countries VALUES ('194', 'ST', 'Sao Tome and Principe', null, null);
INSERT INTO countries VALUES ('195', 'SA', 'Saudi Arabia', null, null);
INSERT INTO countries VALUES ('196', 'SN', 'Senegal', null, null);
INSERT INTO countries VALUES ('197', 'RS', 'Serbia', null, null);
INSERT INTO countries VALUES ('198', 'SC', 'Seychelles', null, null);
INSERT INTO countries VALUES ('199', 'SL', 'Sierra Leone', null, null);
INSERT INTO countries VALUES ('200', 'SG', 'Singapore', null, null);
INSERT INTO countries VALUES ('201', 'SX', 'Sint Maarten (Dutch part)', null, null);
INSERT INTO countries VALUES ('202', 'SK', 'Slovakia', null, null);
INSERT INTO countries VALUES ('203', 'SI', 'Slovenia', null, null);
INSERT INTO countries VALUES ('204', 'SB', 'Solomon Islands', null, null);
INSERT INTO countries VALUES ('205', 'SO', 'Somalia', null, null);
INSERT INTO countries VALUES ('206', 'ZA', 'South Africa', null, null);
INSERT INTO countries VALUES ('207', 'GS', 'South Georgia and the South Sandwich Islands', null, null);
INSERT INTO countries VALUES ('208', 'SS', 'South Sudan', null, null);
INSERT INTO countries VALUES ('209', 'ES', 'Spain', null, null);
INSERT INTO countries VALUES ('210', 'LK', 'Sri Lanka', null, null);
INSERT INTO countries VALUES ('211', 'SD', 'Sudan (the)', null, null);
INSERT INTO countries VALUES ('212', 'SR', 'Suriname', null, null);
INSERT INTO countries VALUES ('213', 'SJ', 'Svalbard and Jan Mayen', null, null);
INSERT INTO countries VALUES ('214', 'SZ', 'Swaziland', null, null);
INSERT INTO countries VALUES ('215', 'SE', 'Sweden', null, null);
INSERT INTO countries VALUES ('216', 'CH', 'Switzerland', null, null);
INSERT INTO countries VALUES ('217', 'SY', 'Syrian Arab Republic', null, null);
INSERT INTO countries VALUES ('218', 'TW', 'Taiwan (Province of China)', null, null);
INSERT INTO countries VALUES ('219', 'TJ', 'Tajikistan', null, null);
INSERT INTO countries VALUES ('220', 'TZ', 'Tanzania, United Republic of', null, null);
INSERT INTO countries VALUES ('221', 'TH', 'Thailand', null, null);
INSERT INTO countries VALUES ('222', 'TL', 'Timor-Leste', null, null);
INSERT INTO countries VALUES ('223', 'TG', 'Togo', null, null);
INSERT INTO countries VALUES ('224', 'TK', 'Tokelau', null, null);
INSERT INTO countries VALUES ('225', 'TO', 'Tonga', null, null);
INSERT INTO countries VALUES ('226', 'TT', 'Trinidad and Tobago', null, null);
INSERT INTO countries VALUES ('227', 'TN', 'Tunisia', null, null);
INSERT INTO countries VALUES ('228', 'TR', 'Turkey', null, null);
INSERT INTO countries VALUES ('229', 'TM', 'Turkmenistan', null, null);
INSERT INTO countries VALUES ('230', 'TC', 'Turks and Caicos Islands (the)', null, null);
INSERT INTO countries VALUES ('231', 'TV', 'Tuvalu', null, null);
INSERT INTO countries VALUES ('232', 'UG', 'Uganda', null, null);
INSERT INTO countries VALUES ('233', 'UA', 'Ukraine', null, null);
INSERT INTO countries VALUES ('234', 'AE', 'United Arab Emirates (the)', null, null);
INSERT INTO countries VALUES ('235', 'GB', 'United Kingdom of Great Britain and Northern Ireland (the)', null, null);
INSERT INTO countries VALUES ('236', 'UM', 'United States Minor Outlying Islands (the)', null, null);
INSERT INTO countries VALUES ('237', 'US', 'United States of America (the)', null, null);
INSERT INTO countries VALUES ('238', 'UY', 'Uruguay', null, null);
INSERT INTO countries VALUES ('239', 'UZ', 'Uzbekistan', null, null);
INSERT INTO countries VALUES ('240', 'VU', 'Vanuatu', null, null);
INSERT INTO countries VALUES ('241', 'VE', 'Venezuela (Bolivarian Republic of)', null, null);
INSERT INTO countries VALUES ('242', 'VN', 'Viet Nam', null, null);
INSERT INTO countries VALUES ('243', 'VG', 'Virgin Islands (British)', null, null);
INSERT INTO countries VALUES ('244', 'VI', 'Virgin Islands (U.S.)', null, null);
INSERT INTO countries VALUES ('245', 'WF', 'Wallis and Futuna', null, null);
INSERT INTO countries VALUES ('246', 'EH', 'Western Sahara*', null, null);
INSERT INTO countries VALUES ('247', 'YE', 'Yemen', null, null);
INSERT INTO countries VALUES ('248', 'ZM', 'Zambia', null, null);
INSERT INTO countries VALUES ('249', 'ZW', 'Zimbabwe', null, null);
