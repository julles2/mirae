<?php

use Illuminate\Database\Seeder;


class MenuSeed extends Seeder
{
    public function run()
    {
        include(database_path('seeds/menus/transaction.php'));
        include(database_path('seeds/menus/product.php'));
        include(database_path('seeds/menus/news.php'));
        include(database_path('seeds/menus/bantuan.php'));
        include(database_path('seeds/menus/about.php'));
        include(database_path('seeds/menus/home.php'));
        include(database_path('seeds/menus/newsletter.php'));
        include(database_path('seeds/menus/other.php'));
        include(database_path('seeds/menus/disclaimer.php'));
        \Artisan::call('config:cache');
    }
}
