<?php

\webarq::addMenu([
                'parent_id'     => null,
                'title'         => 'Disclaimer',
                'controller'    => '#',
                'slug'          => 'disclaimer-dynamic',
                'order'         => 7,
            ],[]);

            \webarq::addMenu([
                    'parent_id'     => 'disclaimer-dynamic',
                    'title'         => 'Portfolio',
                    'controller'    => 'DisclaimerPortfolioController',
                    'slug'          => 'disclaimer-dynamic-portfolio',
                    'order'         => '1'
                ],['index']
            );
