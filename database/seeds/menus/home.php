<?php

\webarq::addMenu([
                'parent_id'     => null,
                'title'         => 'Home',
                'controller'    => '#',
                'slug'          => 'home',
                'order'         => 1,
            ],[]);

            \webarq::addMenu([
                    'parent_id'     => 'home',
                    'title'         => 'Sliders',
                    'controller'    => 'SliderController',
                    'slug'          => 'slider',
                    'order'         => '1'
                ],['index','update','create','delete']
            );

            \webarq::addMenu([
                    'parent_id'     => 'home',
                    'title'         => 'Step',
                    'controller'    => 'StepController',
                    'slug'          => 'step',
                    'order'         => '3'
                ],['index','update','create','delete']
            );

            \webarq::addMenu([
                    'parent_id'     => 'home',
                    'title'         => 'Customer Voice',
                    'controller'    => 'CustomerVoiceController',
                    'slug'          => 'customer-voice',
                    'order'         => '2'
                ],['index','view','delete']
            );

            \webarq::addMenu([
                    'parent_id'     => 'home',
                    'title'         => 'Video',
                    'controller'    => 'VideoController',
                    'slug'          => 'video',
                    'order'         => '4'
                ],['index','create','delete','update']
            );