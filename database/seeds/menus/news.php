<?php
\webarq::addMenu([
                'parent_id'     => null,
                'title'         => 'News',
                'controller'    => '#',
                'slug'          => 'news',
                'order'         => 1,
            ],[]);

            \webarq::addMenu([
                    'parent_id'     => 'news',
                    'title'         => 'News Listing',
                    'controller'    => 'NewsController',
                    'slug'          => 'news-listing',
                    'order'         => '1'
                ],['index','update','create','delete']
            );

            \webarq::addMenu([
                    'parent_id'     => 'news',
                    'title'         => 'News Categories',
                    'controller'    => 'NewsCategoriesController',
                    'slug'          => 'news-categories',
                    'order'         => '2'
                ],['index','update','create','delete']
            );

            \webarq::updateMenu([
                    'parent_id'     => 'news',
                    'title'         => 'News Categories',
                    'controller'    => 'NewsCategoriesController',
                    'slug'          => 'news-categories',
                    'order'         => '2'
                ],['index','update','create','delete']
            ); 
