<?php

\webarq::addMenu([
        'parent_id'     => 'development',
        'title'         => 'Log Cron Job',
        'controller'    => 'LogCronJobController',
        'slug'          => 'log-cron-job',
        'order'         => '4'
    ],['index']
);

\webarq::addMenu([
        'parent_id'     => 'development',
        'title'         => 'Log Customer',
        'controller'    => 'LogCustomerController',
        'slug'          => 'log-customer',
        'order'         => '5'
    ],['index']
);

\webarq::addMenu([
        'parent_id'     => 'development',
        'title'         => 'Log Email',
        'controller'    => 'LogEmailController',
        'slug'          => 'log-email',
        'order'         => '6'
    ],['index']
);