<?php
\webarq::addMenu([
                'parent_id'     => null,
                'title'         => 'Help',
                'controller'    => '#',
                'slug'          => 'help',
                'order'         => 1,
            ],[]);

            \webarq::addMenu([
                    'parent_id'     => 'help',
                    'title'         => 'F.A.Q',
                    'controller'    => 'HelpController',
                    'slug'          => 'faq',
                    'order'         => '1'
                ],['index','update','create','delete']
            );

            \webarq::addMenu([
                    'parent_id'     => 'help',
                    'title'         => 'Glossary',
                    'controller'    => 'GlossaryController',
                    'slug'          => 'glossary',
                    'order'         => '2'
                ],['index','update','create','delete']
            );

            \webarq::addMenu([
                    'parent_id'     => 'help',
                    'title'         => 'Mutual Fund',
                    'controller'    => 'MutualfundController',
                    'slug'          => 'mutual-fund-admin',
                    'order'         => '3'
                ],['index','update','create','delete']
            );

            \webarq::addMenu([
                    'parent_id'     => 'help',
                    'title'         => 'Disclaimer',
                    'controller'    => 'DisclaimerController',
                    'slug'          => 'disclaimer',
                    'order'         => '4'
                ],['index','update','create','delete']
            );

             \webarq::addMenu([
                    'parent_id'     => 'help',
                    'title'         => 'Contact Us',
                    'controller'    => 'ContactusController',
                    'slug'          => 'contact-us',
                    'order'         => '5'
                ],['index','view','delete']
            );

             \webarq::addMenu([
                    'parent_id'     => 'help',
                    'title'         => 'Terms & Condition',
                    'controller'    => 'TermsConditionController',
                    'slug'          => 'terms-condition',
                    'order'         => '5'
                ],['index','create','delete','update']
            );
