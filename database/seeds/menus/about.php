<?php

\webarq::addMenu([
                'parent_id'     => null,
                'title'         => 'Company',
                'controller'    => '#',
                'slug'          => 'company',
                'order'         => 1,
            ],[]);

            \webarq::addMenu([
                    'parent_id'     => 'company',
                    'title'         => 'About Us',
                    'controller'    => 'AboutUsController',
                    'slug'          => 'about-us',
                    'order'         => '1'
                ],['index','update','create','delete']
            );

            \webarq::addMenu([
                    'parent_id'     => 'company',
                    'title'         => 'Milestone',
                    'controller'    => 'MilestoneController',
                    'slug'          => 'milestone',
                    'order'         => '2'
                ],['index','update','create','delete']
            );

            \webarq::addMenu([
                    'parent_id'     => 'company',
                    'title'         => 'Mirae Asset',
                    'controller'    => 'AssetController',
                    'slug'          => 'mirae-asset',
                    'order'         => '3'
                ],['index','update','create','delete']
            );
