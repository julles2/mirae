<?php

\webarq::addMenu([
                'parent_id'     => null,
                'title'         => 'Product',
                'controller'    => '#',
                'slug'          => 'product',
                'order'         => 1,
            ],[]);
    
            \webarq::addMenu([
                    'parent_id'     => 'product',
                    'title'         => 'Reksa Dana',
                    'controller'    => 'ReksaDanaController',
                    'slug'          => 'product-reksadana',
                    'order'         => '1'
                ],['index','update','create']
            );

            \webarq::updateMenu([
                    'parent_id'     => 'product',
                    'title'         => 'Reksa Dana',
                    'controller'    => 'ReksaDanaController',
                    'slug'          => 'product-reksadana',
                    'order'         => '1'
                ],['index','update','create']
            );

            \webarq::addMenu([
                    'parent_id'     => 'product',
                    'title'         => 'Manajer Investasi',
                    'controller'    => 'ManajerInvestasiController',
                    'slug'          => 'manajer-investasi',
                    'order'         => '1'
                ],['index','update','create']
            );

            \webarq::updateMenu([
                    'parent_id'     => 'product',
                    'title'         => 'Manajer Investasi',
                    'controller'    => 'ManajerInvestasiController',
                    'slug'          => 'manajer-investasi',
                    'order'         => '1'
                ],['index','update','create','delete']
            );

            \webarq::addMenu([
                    'parent_id'     => 'product',
                    'title'         => 'MI Info',
                    'controller'    => 'MiInfoController',
                    'slug'          => 'mi-info',
                    'order'         => '3'
                ],['index','update']
            );
