<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldTotalFeeInTransctionTemporaries extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('transaction_temporaries', function (Blueprint $table) {
            $table->decimal('total_fee',22,8)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('transaction_temporaries', function (Blueprint $table) {
            $table->dropColumn('total_fee');
        });
    }
}
