<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldHeaderIdIntFundInfoDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('fund_info_details', function (Blueprint $table) {
            $table->string('header_id')->index();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('fund_info_details', function (Blueprint $table) {
            $table->droupColumn('header_id');
        });
    }
}
