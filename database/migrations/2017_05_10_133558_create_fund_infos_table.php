<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFundInfosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('im', function (Blueprint $table) {
            $table->increments('id');
            $table->string('im_cd',20)->index();
            $table->string('im_nm',225);
            $table->timestamps();
        });

        Schema::create('fund_infos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('im_id')->unsigned();
            $table->string('fund_cd',20)->index();
            $table->string('fund_name',225);
            $table->string('fund_type',2)->index();
            $table->timestamps();

            $table->foreign('im_id')
                ->references('id')
                ->on('im')
                ->onUpdate('cascade')
                ->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('im');
        Schema::drop('fund_infos');
    }
}
