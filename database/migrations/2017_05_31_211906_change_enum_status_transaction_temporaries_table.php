<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeEnumStatusTransactionTemporariesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('transaction_temporaries', function (Blueprint $table) {
            \DB::statement("ALTER TABLE transaction_temporaries CHANGE type type ENUM('r','s','sa') DEFAULT NULL;");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('transaction_temporaries', function (Blueprint $table) {
            //
        });
    }
}
