<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNavDailyPopsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pop_chart_daily', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('fund_id')->unsigned();
            $table->string('tr_date',10)->index();
            $table->decimal('nav',22,8);
            $table->decimal('aum',22,8);
            $table->decimal('unit',22,8);
            $table->timestamps();

            $table->foreign('fund_id')
                ->references('id')
                ->on('fund_infos')
                ->onUpdate('cascade')
                ->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('pop_chart_daily');
    }
}
