<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransactionTemporariesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transaction_temporaries', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('tr_date',8)->index();
            $table->string('tr_time',8);
            $table->string('acnt_no',6)->index();
            $table->string('ifua_cd',15)->index();
            $table->string('fund_cd',20)->index();
            $table->string('ref_no',20)->index();
            $table->bigInteger('bill');
            $table->bigInteger('fee');
            $table->string('source',2)->index();
            $table->enum('type',['s','r']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('transaction_temporaries');
    }
}
