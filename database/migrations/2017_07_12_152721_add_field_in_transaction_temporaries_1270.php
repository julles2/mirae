<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldInTransactionTemporaries1270 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('transaction_temporaries', function (Blueprint $table) {
            $table->string('date_cancel',12)->nullable();
            $table->string('time_cancel',12)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('transaction_temporaries', function (Blueprint $table) {
            $table->dropColumn('date_cancel','time_cancel');
        });
    }
}
