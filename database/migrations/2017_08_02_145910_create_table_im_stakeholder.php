<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableImStakeholder extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('im_stakeholders', function (Blueprint $table) {
            $table->increments('id');
            $table->string('im_cd',20);
            $table->string('nama');
            $table->decimal('kepemilikan');
            $table->decimal('jumlah', 5, 2);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('im_stakeholders');
    }
}
