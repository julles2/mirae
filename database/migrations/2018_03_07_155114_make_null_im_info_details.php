<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MakeNullImInfoDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \DB::statement('ALTER TABLE im_info_details MODIFY COLUMN description text NULL;');
        \DB::statement('ALTER TABLE im_info_details MODIFY COLUMN pengelolaan text NULL;');
        \DB::statement('ALTER TABLE im_info_details MODIFY COLUMN modal_dasar text NULL;');
        \DB::statement('ALTER TABLE im_info_details MODIFY COLUMN modal_disetor text NULL;');
        \DB::statement('ALTER TABLE im_info_details MODIFY COLUMN kepemilikan text NULL;');
        \DB::statement('ALTER TABLE im_info_details MODIFY COLUMN izin_usaha text NULL;');
        \DB::statement('ALTER TABLE im_info_details MODIFY COLUMN status ENUM("y","n") NULL;');

        \DB::statement('ALTER TABLE im_departments MODIFY COLUMN nama text NULL;');
        \DB::statement('ALTER TABLE im_departments MODIFY COLUMN jabatan text NULL;');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('im_info_details', function (Blueprint $table) {
    
        });
    }
}
