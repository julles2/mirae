<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeFieldStatusOnQuestionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    
    public function up()
    {
        Schema::table('questions', function (Blueprint $table) {
            $table->dropColumn('status');
        });

        Schema::table('questions', function (Blueprint $table) {
            $table->enum('status',['y','n','r'])->default('n');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('questions', function (Blueprint $table) {
            $table->dropColumn('status');
        });

        Schema::table('questions', function (Blueprint $table) {
            $table->enum('status',['y','n'])->default('n');
        });
    }
}
