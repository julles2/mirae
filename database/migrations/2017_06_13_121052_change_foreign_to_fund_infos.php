<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeForeignToFundInfos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('daily_navs', function (Blueprint $table) {
            $table->dropForeign('daily_navs_fund_id_foreign');

            $table->foreign('fund_id')
                ->references('id')
                ->on('fund_infos')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });

        Schema::table('pop_chart_daily', function (Blueprint $table) {
            $table->dropForeign('pop_chart_daily_fund_id_foreign');

            $table->foreign('fund_id')
                ->references('id')
                ->on('fund_infos')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });

        Schema::table('fund_performance_pops', function (Blueprint $table) {
            $table->dropForeign('fund_performance_pops_fund_id_foreign');

            $table->foreign('fund_id')
                ->references('id')
                ->on('fund_infos')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });

        Schema::table('fund_params', function (Blueprint $table) {
            $table->dropForeign('fund_params_fund_id_foreign');

            $table->foreign('fund_id')
                ->references('id')
                ->on('fund_infos')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('fund_infos', function (Blueprint $table) {
            //
        });
    }
}
