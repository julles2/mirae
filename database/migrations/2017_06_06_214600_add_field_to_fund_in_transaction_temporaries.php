<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldToFundInTransactionTemporaries extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('transaction_temporaries', function (Blueprint $table) {
            $table->string('to_fund')->nullable();
            $table->string('recur_date',2)->index()->nullable();
            \DB::statement("ALTER TABLE transaction_temporaries CHANGE type type ENUM('r','s','sa','sw') DEFAULT NULL;");
        
            // $table->foreign('to_fund')
            //     ->references('id')
            //     ->on('fund_infos')
            //     ->onUpdate('cascade')
            //     ->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('transaction_temporaries', function (Blueprint $table) {
            //$table->dropForeign('transaction_temporaries_to_fund_foreign');
            $table->dropColumn('to_fund','recur_date');
        });
    }
}
