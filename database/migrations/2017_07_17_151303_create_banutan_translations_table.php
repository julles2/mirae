<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBanutanTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bantuan_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('bantuan_id')->unsigned();
            $table->string('lang',2);
            $table->string('title');
            $table->text('description');
            $table->timestamps();

            $table->foreign('bantuan_id')
                ->references('id')
                ->on('bantuans')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('bantuan_translations');
    }
}
