<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableImInfoDetails2 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('im_info_details', function (Blueprint $table) {
            $table->increments('id');
            $table->string('im_cd',20);
            $table->string('lang',2)->index();
            $table->string('description');
            $table->string('pengelolaan');
            $table->string('modal_dasar');
            $table->string('modal_disetor');
            $table->string('kepemilikan');
            $table->enum('status',['y','n']);
            $table->string('izin_usaha');
            $table->timestamps();

            $table->foreign('im_cd')
                ->references('im_cd')
                ->on('im')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('im_info_details');
    }
}
