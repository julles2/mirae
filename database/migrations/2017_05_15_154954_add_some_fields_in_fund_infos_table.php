<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSomeFieldsInFundInfosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('fund_infos', function (Blueprint $table) {
            $table->bigInteger('fee_subs')->nullable();
            $table->bigInteger('min_investment')->nullable();
            $table->bigInteger('min_increment')->nullable();
            $table->bigInteger('min_balance')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('fund_infos', function (Blueprint $table) {
            $table->dropColumn([
                'fee_subs',
                'min_investment',
                'min_increment',
                'min_balance',
            ]);
        });
    }
}
