<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIdSeqFieldInFundInfoesDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('fund_info_details', function (Blueprint $table) {
            $table->integer('id_seq');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::create('fund_info_details', function (Blueprint $table) {
            $table->integer('id_seq');
        });
    }
}
