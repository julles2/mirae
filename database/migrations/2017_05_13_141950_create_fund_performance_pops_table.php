<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFundPerformancePopsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fund_performance_pops', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('fund_id')->unsigned();
            $table->string('tr_date',11)->index();
            
            $table->decimal('one_month',22,8);
            $table->decimal('three_months',22,8);
            $table->decimal('six_months',22,8);
            $table->decimal('ytd',22,8);
            $table->decimal('one_year',22,8);
            $table->decimal('three_year',22,8);

            $table->decimal('aum_one_month',22,8);
            $table->decimal('aum_three_months',22,8);
            $table->decimal('aum_six_months',22,8);
            $table->decimal('aum_ytd',22,8);
            $table->decimal('aum_one_year',22,8);
            $table->decimal('aum_three_year',22,8);
            
            $table->decimal('ou_one_month',22,8);
            $table->decimal('ou_three_months',22,8);
            $table->decimal('ou_six_months',22,8);
            $table->decimal('ou_ytd',22,8);
            $table->decimal('ou_one_year',22,8);
            $table->decimal('ou_three_year',22,8);

            $table->timestamps();

            $table->foreign('fund_id')
                ->references('id')
                ->on('fund_infos')
                ->onUpdate('cascade')
                ->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('fund_performance_pops');
    }
}
