<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldFeeRedmInFundInfosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('fund_infos', function (Blueprint $table) {
            $table->bigInteger('fee_redm')->nullable();
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('fund_infos', function (Blueprint $table) {
            $table->dropColumn('fee_redm');
        });
    }
}
