<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldUsernameInLogCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('log_customers', function (Blueprint $table) {
            $table->string('username',100)->nullable();
            $table->enum('status',['success','failed'])->default('failed');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('log_customers', function (Blueprint $table) {
            $table->dropColumn('username','status');
        });
    }
}
