<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableImDepartment extends Migration
{
   /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('im_departments', function (Blueprint $table) {
            $table->increments('id');
            $table->string('im_cd',20);
            $table->string('nama');
            $table->string('jabatan');
            $table->enum('status',['y','n']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('im_departments');
    }
}
