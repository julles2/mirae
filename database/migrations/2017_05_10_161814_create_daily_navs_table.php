<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDailyNavsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('daily_navs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('fund_id')->unsigned();
            $table->decimal('nav_per_unit',22,8);
            $table->decimal('one_day',22,8);
            $table->decimal('three_weeks',22,8);
            $table->decimal('one_month',22,8);
            $table->decimal('three_months',22,8);
            $table->decimal('six_months',22,8);
            $table->decimal('one_years',22,8);
            $table->decimal('three_years',22,8);
            $table->decimal('five_years',22,8);
            $table->decimal('ytd',22,8);
            $table->decimal('outstanding_unit',22,8);
            $table->string('tr_date');
            $table->decimal('aum',22,8);
            $table->timestamps();

            $table->foreign('fund_id')
                ->references('id')
                ->on('fund_infos')
                ->onUpdate('cascade')
                ->onDelete('restrict');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('daily_navs');
    }
}
