<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAboutMilestoneTransations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('milestones_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('milestone_id')->unsigned();
            $table->string('lang',2);
            $table->integer('year');
            $table->string('slug',4);
            $table->string('title');
            $table->string('image');
            $table->text('description');
            $table->timestamps();

            $table->foreign('milestone_id')
                ->references('id')
                ->on('milestones')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('milestones_translations');
    }
}
