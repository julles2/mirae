<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateImDatasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('im_datas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('im_cd',20);
            $table->string('periode',8);
            $table->string('aum_idr',25);
            $table->string('aum_usd',25);
            $table->string('unit_idr',25);
            $table->string('unit_usd',25);
            $table->timestamps();

            $table->foreign('im_cd')
                ->references('im_cd')
                ->on('im')
                ->onUpdate('cascade')
                ->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('im_datas');
    }
}
