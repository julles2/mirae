<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldFilePdf extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('fund_info_details', function (Blueprint $table) {
            $table->string('fund_fact_sheet')->nullable();
            $table->string('prospectus')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::create('fund_info_details', function (Blueprint $table) {
            $table->string('fund_fact_sheet')->nullable();
            $table->string('prospectus')->nullable();
        });
    }
}
