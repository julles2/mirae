<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldInFundInfos3008 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    public function kolom()
    {
        return [
            'min_redm_amt',
            'min_swtc_in_amt',
            'min_swtc_out_amt',
        ];
    }

    public function up()
    {
        Schema::table('fund_infos', function (Blueprint $table) {
            foreach($this->kolom() as $f)
            {
                $table->bigInteger($f)->nullable();
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('fund_infos', function (Blueprint $table) {
            $table->dropColumn($this->kolom());
        });
    }
}
