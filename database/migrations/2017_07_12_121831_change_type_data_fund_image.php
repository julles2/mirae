<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeTypeDataFundImage extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('fund_images', function (Blueprint $table) {
            \DB::statement("ALTER TABLE fund_images CHANGE fund_cd fund_cd varchar(20)");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('fund_images', function (Blueprint $table) {
            //
        });
    }
}
