<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeMenuSizeField extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('menu_settings', function (Blueprint $table) {
            \DB::statement("ALTER TABLE menu_settings CHANGE menu menu VARCHAR(20) DEFAULT NULL;");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('menu_settings', function (Blueprint $table) {
            $table->dropcolumn('menu');
        });
    }
}
