<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropColoumnFilePdfInFuncInfosDetails extends Migration
{
    public function up()
    {
        Schema::table('fund_info_details', function (Blueprint $table)
        {
            if (Schema::hasColumn('fund_info_details', 'prospectus'))
            {
                Schema::table('fund_info_details', function (Blueprint $table)
                {
                    $table->dropColumn('prospectus');
                    $table->dropColumn('fund_fact_sheet');
                });
            }
        });
    }

    public function down()
    {
        Schema::table('fund_info_details', function (Blueprint $table)
        {
            $table->dropColumn('prospectus');
            $table->dropColumn('fund_fact_sheet');
        });
    }

}
