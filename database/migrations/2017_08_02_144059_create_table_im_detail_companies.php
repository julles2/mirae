<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableImDetailCompanies extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('im_detail_companies', function (Blueprint $table) {
            $table->increments('id');
            $table->string('im_cd',20);
            $table->string('lang',2)->index();
            $table->string('image',100)->nullable();
            $table->text('description');
            $table->timestamps();

            $table->foreign('im_cd')
                ->references('im_cd')
                ->on('im')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('im_detail_companies');
    }
}
