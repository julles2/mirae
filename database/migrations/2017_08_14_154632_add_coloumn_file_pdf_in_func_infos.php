<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColoumnFilePdfInFuncInfos extends Migration
{
    public function up()
    {
        Schema::table('fund_infos', function (Blueprint $table) {
            $table->string('prospectus');
            $table->string('fund_fact_sheet');
        });
    }

    public function down()
    {
        Schema::table('fund_infos', function (Blueprint $table) {
            $table->string('prospectus');
            $table->string('fund_fact_sheet');
        });
    }
}
